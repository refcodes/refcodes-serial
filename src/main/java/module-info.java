module org.refcodes.serial {
	requires org.refcodes.factory;
	requires org.refcodes.textual;
	requires transitive org.refcodes.schema;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.numerical;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.struct.ext.factory;

	exports org.refcodes.serial;
}
