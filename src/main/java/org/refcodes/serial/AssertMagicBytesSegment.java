// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.refcodes.textual.CaseStyleBuilder;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link AssertMagicBytesSegment} extends the {@link MagicBytesSegment} and
 * enforces the configured magic bytes to match the received magic bytes (as of
 * {@link #fromTransmission(Sequence)} and
 * {@link #receiveFrom(java.io.InputStream, java.io.OutputStream)} or the like).
 * In case the assertion of the configured magic bytes fails during receiving,
 * then a {@link BadMagicBytesException} or
 * {@link BadMagicBytesSequenceException} is thrown.
 */
public class AssertMagicBytesSegment extends MagicBytesSegment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AssertMagicBytesSegment( String aAlias, String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes. Enforces the configured magic bytes to match the received magic
	 * bytes (as of {@link AssertMagicBytesSegment#fromTransmission(Sequence)}
	 * and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AssertMagicBytesSegment( String aAlias, byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AssertMagicBytesSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), (String) null, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AssertMagicBytesSegment( String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes. Enforces the configured magic bytes to match the received
	 * magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AssertMagicBytesSegment( byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes. Enforces the configured magic bytes to match the received
	 * magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 */
	public AssertMagicBytesSegment( byte... aMagicBytes ) {
		super( aMagicBytes );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes. Enforces the configured magic bytes to match the received magic
	 * bytes (as of {@link AssertMagicBytesSegment#fromTransmission(Sequence)}
	 * and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 */
	public AssertMagicBytesSegment( String aAlias, byte... aMagicBytes ) {
		super( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public AssertMagicBytesSegment( String aMagicBytes, Charset aCharset ) {
		super( aMagicBytes, aCharset );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public AssertMagicBytesSegment( String aAlias, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aMagicBytes, aCharset );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 */
	public AssertMagicBytesSegment( String aAlias, String aMagicBytes ) {
		super( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes (retrieved from the given {@link String}). Enforces the configured
	 * magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 */
	public AssertMagicBytesSegment( String aMagicBytes ) {
		super( aMagicBytes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte[] theMagicBytes = aSequence.toBytes( aOffset, _magicBytesLength );
		if ( !Arrays.equals( _magicBytes, theMagicBytes ) ) {
			throw new BadMagicBytesSequenceException( theMagicBytes, aSequence, "The received magic bytes " + VerboseTextBuilder.asString( theMagicBytes ) + " do not match the expected magic bytes " + VerboseTextBuilder.asString( _magicBytes ) + "!" );
		}
		return aOffset + _magicBytesLength;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException, TransmissionException {
	//		byte[] theMagicBytes = new byte[_magicBytesLength];
	//		aInputStream.read( _magicBytes );
	//		if ( !Arrays.equals( _magicBytes, theMagicBytes ) ) {
	//			throw new BadMagicBytesException( theMagicBytes, "The received magic bytes " + VerboseTextBuilder.asString( theMagicBytes ) + " do not match the expected magic bytes " + VerboseTextBuilder.asString( _magicBytes ) + "!" );
	//		}
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment asserting the configured magic bytes." );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES_TEXT, toMagicBytesString() );
		return theSchema;
	}
}
