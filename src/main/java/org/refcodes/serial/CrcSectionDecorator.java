// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;

/**
 * A {@link CrcSectionDecorator} wraps a {@link Section} instance and enriches
 * the the {@link Section} with a CRC checksum.
 * 
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched with a CRC checksum.
 */
public class CrcSectionDecorator<DECORATEE extends Section> extends AbstractCrcTransmissionDecorator<DECORATEE> implements DecoratorSection<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		super( aDecoratee, aCrcAlgorithm );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		super( aDecoratee, aCrcAlgorithm, aChecksumValidationMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		super( aDecoratee, aCrcAlgorithm );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aDecoratee, aCrcAlgorithm, aChecksumValidationMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		super( aDecoratee, aCrcChecksumConcatenateMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public CrcSectionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @throws BadCrcChecksumSequenceException thrown in case the CRC checksum
	 *         validation failed.
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		switch ( _crcChecksumConcatenateMode ) {
		case PREPEND -> {
			final byte[] theBytes = aSequence.toBytes( aOffset, _crcAlgorithm.getCrcWidth() );
			aLength -= _crcAlgorithm.getCrcWidth();
			aOffset += _crcAlgorithm.getCrcWidth();
			_decoratee.fromTransmission( aSequence, aOffset, aLength );
			_crcChecksum = getEndianess().toUnsignedLong( theBytes );
		}
		case APPEND -> {
			aLength -= _crcAlgorithm.getCrcWidth();
			_decoratee.fromTransmission( aSequence, aOffset, aLength );
			aOffset += aLength;
			final byte[] theBytes = aSequence.toBytes( aOffset, _crcAlgorithm.getCrcWidth() );
			_crcChecksum = getEndianess().toUnsignedLong( theBytes );
		}
		default -> throw new UnhandledEnumBugException( _crcChecksumConcatenateMode );
		}

		if ( _checksumValidationMode == ChecksumValidationMode.ENFORCE_VALID_CHECKSUM ) {
			final long theCrcChecksum = _decoratee.toSequence().toCrcChecksum( _crcAlgorithm );
			if ( theCrcChecksum != _crcChecksum ) {
				throw new BadCrcChecksumSequenceException( _crcChecksum, theCrcChecksum, aSequence, "The CRC checksum <" + _crcChecksum + "> at offset <" + aOffset + "> does not match the calculated CRC checksum <" + theCrcChecksum + ">." );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws BadCrcChecksumException thrown in case the CRC checksum
	 *         validation failed.
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException {
		final byte[] theBytes = new byte[_crcAlgorithm.getCrcWidth()];
		switch ( _crcChecksumConcatenateMode ) {
		case PREPEND -> {
			aInputStream.read( theBytes, 0, _crcAlgorithm.getCrcWidth() );
			aLength -= _crcAlgorithm.getCrcWidth();
			_decoratee.receiveFrom( aInputStream, aLength, aReturnStream );
			_crcChecksum = getEndianess().toUnsignedLong( theBytes );
		}
		case APPEND -> {
			aLength -= _crcAlgorithm.getCrcWidth();
			_decoratee.receiveFrom( aInputStream, aLength, aReturnStream );
			aInputStream.read( theBytes, 0, _crcAlgorithm.getCrcWidth() );
			_crcChecksum = getEndianess().toUnsignedLong( theBytes );
		}
		default -> throw new UnhandledEnumBugException( _crcChecksumConcatenateMode );
		}

		if ( _checksumValidationMode == ChecksumValidationMode.ENFORCE_VALID_CHECKSUM ) {
			final long theCrcChecksum = _decoratee.toSequence().toCrcChecksum( _crcAlgorithm );
			if ( theCrcChecksum != _crcChecksum ) {
				throw new BadCrcChecksumException( _crcChecksum, theCrcChecksum, "The CRC checksum <" + _crcChecksum + "> at offset <" + 0 + "> does not match the calculated CRC checksum <" + theCrcChecksum + ">." );
			}
		}
	}
}
