// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

import org.refcodes.component.Openable;

/**
 * The {@link PortTestBench} provides a test bench merely for diagnostic
 * purposes. Test code using the {@link PortTestBench} can be tested against
 * exchangeable implementation of the {@link PortTestBench}.
 */
public interface PortTestBench extends AutoCloseable, Openable {

	/**
	 * Tests whether a pair of loopback ports can be provided.
	 * 
	 * @return True in case there is a pair of corresponding ports.
	 */
	boolean hasPorts();

	/**
	 * Retrieves the loopback's "receiver" port.
	 * 
	 * @return The according receiver port.
	 * 
	 * @throws IOException thrown in case acquiring the port failed.
	 * 
	 * @throws IllegalStateException thrown case there is no such loopback port
	 *         pair.
	 */
	Port<?> getReceiverPort() throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	void close() throws IOException;

	/**
	 * Retrieves the loopback's "transmitter" port.
	 * 
	 * @return The according transmitter port.
	 * 
	 * @throws IOException thrown in case acquiring the port failed.
	 * 
	 * @throws IllegalStateException thrown case there is no such loopback port
	 *         pair.
	 */
	Port<?> getTransmitterPort() throws IOException;

	/**
	 * Heuristic shortest wait time for the given implementsation's ports to
	 * catch up after issuing a specific port operation (the actual duration
	 * depends on the implementation's requirements).
	 */
	void waitShortestForPortCatchUp();

	/**
	 * Heuristic short wait time for the given implementsation's ports to catch
	 * up after issuing a specific port operation (the actual duration depends
	 * on the implementation's requirements).
	 */
	void waitShortForPortCatchUp();

	/**
	 * Heuristic (normal) wait time for the given implementsation's ports to
	 * catch up after issuing a specific port operation (the actual duration
	 * depends on the implementation's requirements).
	 */
	void waitForPortCatchUp();

	/**
	 * Heuristic long wait time for the given implementsation's ports to catch
	 * up after issuing a specific port operation (the actual duration depends
	 * on the implementation's requirements).
	 */
	void waitLongForPortCatchUp();

	/**
	 * Heuristic longest wait time for the given implementsation's ports to
	 * catch up after issuing a specific port operation (the actual duration
	 * depends on the implementation's requirements).
	 */
	void waitLongestForPortCatchUp();

	/**
	 * Custom wait time for the given implementsation's ports to catch up after
	 * issuing a specific port operation.
	 *
	 * @param aSleepTimeMillis The sleep time in milliseconds.
	 */
	void waitForPortCatchUp( long aSleepTimeMillis );

}
