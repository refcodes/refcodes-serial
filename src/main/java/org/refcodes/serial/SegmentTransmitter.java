// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

import org.refcodes.io.Transmittable;

/**
 * The {@link SegmentTransmitter} is used to transmit {@link Segment} instances
 * in a unified way.
 */
public interface SegmentTransmitter extends SegmentSource, Transmittable {

	/**
	 * Asynchronously sends a {@link Segment}'s {@link Sequence} data (does not
	 * block this thread).
	 * 
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass used.
	 * @param aSegment The {@link Segment} to be sent.
	 * @param aSegmentConsumer The callback asynchronously invoked upon having
	 *        sent the {@link Segment}.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a closed
	 *         connection upon the time of invocation).
	 */
	<SEGMENT extends Segment> void doTransmitSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException;

	/**
	 * Asynchronously sends a {@link Segment}'s {@link Sequence} data (does not
	 * block this thread).
	 * 
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass used.
	 * @param aSegment The {@link Segment} to be sent.
	 * 
	 * @return The {@link SegmentResult} which provides the result of the
	 *         operation (asynchronously), e.g. the {@link Segment} being sent
	 *         or an {@link IOException} having occurred.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a closed
	 *         connection upon the time of invocation).
	 */
	<SEGMENT extends Segment> SegmentResult<SEGMENT> doTransmitSegment( SEGMENT aSegment ) throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void flush() throws IOException {}
}
