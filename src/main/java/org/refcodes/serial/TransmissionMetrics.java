// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.refcodes.data.AckTimeout;
import org.refcodes.data.IoHeurisitcsTimeToLive;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoTimeout;
import org.refcodes.mixin.BlockSizeAccessor;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.mixin.ReadTimeoutMillisAccessor;
import org.refcodes.mixin.WriteTimeoutMillisAccessor;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.ChecksumValidationModeAccessor;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.CrcAlgorithmAccessor;
import org.refcodes.numerical.CrcChecksumConcatenateModeAccessor;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;

/**
 * The {@link TransmissionMetrics} interface represents common parameters
 * required when configuring a transmission. Not all configuration parameters
 * may be required when applying the {@link TransmissionMetrics}. This depends
 * on your setup of {@link Segment} and/or {@link Section} compositions. Usually
 * {@link TransmissionMetrics} are applied to sub-classes of the type
 * {@link Segment} and {@link Section}.
 */
public class TransmissionMetrics implements AcknowledgeMagicBytesAccessor, AcknowledgeTimeoutMillisAccessor, AcknowledgeRetryNumberAccessor, TransmissionTimeoutMillisAccessor, TransmissionRetryNumberAccessor, LengthWidthAccessor, CrcAlgorithmAccessor, ChecksumValidationModeAccessor, EndianessAccessor, BlockSizeAccessor, SequenceNumberWidthAccessor, SequenceNumberInitValueAccessor, ReadTimeoutMillisAccessor, WriteTimeoutMillisAccessor, SequenceNumberConcatenateModeAccessor, CrcChecksumConcatenateModeAccessor, MagicBytesLengthAccessor, EncodingAccessor<Charset>, AcknowledgeSegmentPackagerAccessor, PacketSegmentPackagerAccessor, ReadyToReceiveSegmentPackagerAccessor, ReadyToReceiveTimeoutMillisAccessor, ReadyToReceiveMagicBytesAccessor, ReadyToReceiveRetryNumberAccessor, EnquiryStandbyTimeMillisAccessor, ReadyToSendTimeoutMillisAccessor, ReadyToSendMagicBytesAccessor, ReadyToSendRetryNumberAccessor, ReadyToSendSegmentPackagerAccessor, ClearToSendTimeoutMillisAccessor, ClearToSendMagicBytesAccessor, ClearToSendSegmentPackagerAccessor, EndOfStringByteAccessor, PacketMagicBytesAccessor, PacketLengthWidthAccessor, PingMagicBytesAccessor, PingTimeoutMillisAccessor, PingRetryNumberAccessor, PongMagicBytesAccessor, IoHeuristicsTimeToLiveMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final byte[] DEFAULT_ACKNOWLEDGE_MAGIC_BYTES = MagicBytes.ACKNOWLEDGE.getMagicBytes();
	public static final byte[] DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES = MagicBytes.CLEAR_TO_SEND.getMagicBytes();
	public static final byte[] DEFAULT_PACKET_MAGIC_BYTES = MagicBytes.PACKET.getMagicBytes();
	public static final byte[] DEFAULT_LAST_PACKET_MAGIC_BYTES = MagicBytes.LAST_PACKET.getMagicBytes();
	public static final byte[] DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES = MagicBytes.READY_TO_RECEIVE.getMagicBytes();
	public static final byte[] DEFAULT_READY_TO_SEND_MAGIC_BYTES = MagicBytes.READY_TO_SEND.getMagicBytes();
	private static final byte[] DEFAULT_PONG_MAGIC_BYTES = MagicBytes.PONG.getMagicBytes();
	private static final byte[] DEFAULT_PING_MAGIC_BYTES = MagicBytes.PING.getMagicBytes();

	public static final int DEFAULT_ACKNOWLEDGE_RETRY_NUMBER = IoRetryCount.NORM.getValue();
	public static final long DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();
	public static final int DEFAULT_PING_RETRY_NUMBER = IoRetryCount.MIN.getValue(); // Must be < DEFAULT_ACKNOWLEDGE_RETRY_NUMBER
	public static final long DEFAULT_PING_TIMEOUT_IN_MS = AckTimeout.MIN.getTimeMillis(); // Must < DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS
	public static final int DEFAULT_BLOCK_SIZE = 1024;
	public static final long DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();
	public static final CrcAlgorithm DEFAULT_CRC_ALGORITHM = CrcStandard.CRC_16_X25;
	public static final ConcatenateMode DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE = ConcatenateMode.APPEND;
	public static final ChecksumValidationMode DEFAULT_CHECKSUM_VALIDATION_MODE = ChecksumValidationMode.ENFORCE_VALID_CHECKSUM;
	public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;
	public static final byte DEFAULT_END_OF_STRING_BYTE = 0;
	public static final Endianess DEFAULT_ENDIANESS = Endianess.LITTLE;
	public static final long DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS = -1; // = Wait till shutdown
	public static final int DEFAULT_LENGTH_WIDTH = 4;
	public static final int DEFAULT_TRUNCATE_LENGTH_WIDTH = 4;
	public static final int DEFAULT_MAGIC_BYTES_LENGTH = 4;
	public static final long DEFAULT_READ_TIMEOUT_IN_MS = IoTimeout.NORM.getTimeMillis();
	public static final int DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER = IoRetryCount.MAX.getValue();
	public static final long DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();
	public static final int DEFAULT_READY_TO_SEND_RETRY_NUMBER = IoRetryCount.MAX.getValue();
	public static final long DEFAULT_READY_TO_SEND_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();
	public static final ConcatenateMode DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE = ConcatenateMode.PREPEND;
	public static final int DEFAULT_SEQUENCE_NUMBER_INIT_VALUE = -1;
	public static final int DEFAULT_SEQUENCE_NUMBER_WIDTH = 4;
	public static final int DEFAULT_TRANSMISSION_RETRY_NUMBER = IoRetryCount.NORM.getValue();
	public static final long DEFAULT_TRANSMISSION_TIMEOUT_IN_MS = AckTimeout.NORM.getTimeMillis();
	public static final long DEFAULT_WRITE_TIMEOUT_IN_MS = IoTimeout.NORM.getTimeMillis();
	public static final long DEFAULT_IO_HEURISTICS_TIME_TO_LIVE_IN_MS = IoHeurisitcsTimeToLive.NORM.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected byte[] _acknowledgeMagicBytes = DEFAULT_ACKNOWLEDGE_MAGIC_BYTES;
	protected byte[] _clearToSendMagicBytes = DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES;
	protected byte[] _packetMagicBytes = DEFAULT_PACKET_MAGIC_BYTES;
	protected byte[] _readyToReceiveMagicBytes = DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES;
	protected byte[] _readyToSendMagicBytes = DEFAULT_READY_TO_SEND_MAGIC_BYTES;
	protected byte[] _pingMagicBytes = DEFAULT_PING_MAGIC_BYTES;
	protected byte[] _pongMagicBytes = DEFAULT_PONG_MAGIC_BYTES;

	protected SegmentPackager _acknowledgeSegmentPackager;
	protected int _acknowledgeRetryNumber = DEFAULT_ACKNOWLEDGE_RETRY_NUMBER;
	protected long _acknowledgeTimeoutInMs = DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS;
	protected int _pingRetryNumber = DEFAULT_PING_RETRY_NUMBER;
	protected long _pingTimeoutInMs = DEFAULT_PING_TIMEOUT_IN_MS;
	protected int _blockSize = DEFAULT_BLOCK_SIZE;
	protected ChecksumValidationMode _checksumValidationMode = DEFAULT_CHECKSUM_VALIDATION_MODE;
	protected SegmentPackager _clearToSendSegmentPackager;
	protected long _clearToSendTimeoutInMs = DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS;
	protected CrcAlgorithm _crcAlgorithm = DEFAULT_CRC_ALGORITHM;
	protected ConcatenateMode _crcChecksumConcatenateMode = DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
	protected Charset _encoding = DEFAULT_ENCODING;
	protected Endianess _endianess = DEFAULT_ENDIANESS;
	protected byte _endOfStringByte = DEFAULT_END_OF_STRING_BYTE;
	protected long _enquiryStandbyTimeInMs = DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS;
	protected int _lengthWidth = DEFAULT_LENGTH_WIDTH;
	protected int _packetLengthWidth = DEFAULT_TRUNCATE_LENGTH_WIDTH;
	protected int _magicBytesLength = DEFAULT_MAGIC_BYTES_LENGTH;
	protected SegmentPackager _packetSegmentPackager;
	protected long _readTimeoutInMs = DEFAULT_READ_TIMEOUT_IN_MS;
	protected int _readyToReceiveRetryNumber = DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER;
	protected SegmentPackager _readyToReceiveSegmentPackager;
	protected long _readyToReceiveTimeoutInMs = DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS;
	protected int _readyToSendRetryNumber = DEFAULT_READY_TO_SEND_RETRY_NUMBER;
	protected SegmentPackager _readyToSendSegmentPackager;
	protected long _readyToSendTimeoutInMs = DEFAULT_READY_TO_SEND_TIMEOUT_IN_MS;
	protected ConcatenateMode _sequenceNumberConcatenateMode = DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE;
	protected int _sequenceNumberInitValue = DEFAULT_SEQUENCE_NUMBER_INIT_VALUE;
	protected int _sequenceNumberWidth = DEFAULT_SEQUENCE_NUMBER_WIDTH;
	protected int _transmissionRetryNumber = DEFAULT_TRANSMISSION_RETRY_NUMBER;
	protected long _transmissionTimeoutInMs = DEFAULT_TRANSMISSION_TIMEOUT_IN_MS;
	protected long _writeTimeoutInMs = DEFAULT_WRITE_TIMEOUT_IN_MS;
	protected long _ioHeuristicsTimeToLiveInMs = DEFAULT_IO_HEURISTICS_TIME_TO_LIVE_IN_MS;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new transmission metrics.
	 *
	 * @param aBuilder the builder
	 */
	protected TransmissionMetrics( Builder aBuilder ) {
		_pingMagicBytes = aBuilder.pingMagicBytes;
		_pongMagicBytes = aBuilder.pongMagicBytes;
		_acknowledgeMagicBytes = aBuilder.acknowledgeMagicBytes;
		_clearToSendMagicBytes = aBuilder.clearToSendMagicBytes;
		_packetMagicBytes = aBuilder.packetMagicBytes;
		_readyToReceiveMagicBytes = aBuilder.readyToReceiveMagicBytes;
		_readyToSendMagicBytes = aBuilder.readyToSendMagicBytes;
		_acknowledgeSegmentPackager = aBuilder.acknowledgeSegmentPackager;
		_acknowledgeRetryNumber = aBuilder.acknowledgeRetryNumber;
		_acknowledgeTimeoutInMs = aBuilder.acknowledgeTimeoutInMs;
		_pingRetryNumber = aBuilder.pingRetryNumber;
		_pingTimeoutInMs = aBuilder.pingTimeoutInMs;
		_blockSize = aBuilder.blockSize;
		_clearToSendSegmentPackager = aBuilder.clearToSendSegmentPackager;
		_clearToSendTimeoutInMs = aBuilder.clearToSendTimeoutInMs;
		_crcAlgorithm = aBuilder.crcAlgorithm;
		_crcChecksumConcatenateMode = aBuilder.crcChecksumConcatenateMode;
		_checksumValidationMode = aBuilder.checksumValidationMode;
		_encoding = aBuilder.encoding;
		_endianess = aBuilder.endianess;
		_endOfStringByte = aBuilder.endOfStringByte;
		_enquiryStandbyTimeInMs = aBuilder.enquiryStandbyTimeInMs;
		_lengthWidth = aBuilder.lengthWidth;
		_packetLengthWidth = aBuilder.packetLengthWidth;
		_magicBytesLength = aBuilder.magicBytesLength;
		_packetSegmentPackager = aBuilder.packetSegmentPackager;
		_readTimeoutInMs = aBuilder.readTimeoutInMs;
		_readyToReceiveRetryNumber = aBuilder.readyToReceiveRetryNumber;
		_readyToReceiveSegmentPackager = aBuilder.readyToReceiveSegmentPackager;
		_readyToReceiveTimeoutInMs = aBuilder.readyToReceiveTimeoutInMs;
		_readyToSendRetryNumber = aBuilder.readyToSendRetryNumber;
		_readyToSendSegmentPackager = aBuilder.readyToSendSegmentPackager;
		_readyToSendTimeoutInMs = aBuilder.readyToSendTimeoutInMs;
		_sequenceNumberConcatenateMode = aBuilder.sequenceNumberConcatenateMode;
		_sequenceNumberInitValue = aBuilder.sequenceNumberInitValue;
		_sequenceNumberWidth = aBuilder.sequenceNumberWidth;
		_transmissionRetryNumber = aBuilder.transmissionRetryNumber;
		_transmissionTimeoutInMs = aBuilder.transmissionTimeoutInMs;
		_writeTimeoutInMs = aBuilder.writeTimeoutInMs;
		_ioHeuristicsTimeToLiveInMs = aBuilder.ioHeuristicsTimeToLiveInMs;
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates an instance of the {@link TransmissionMetrics} with default
	 * values being applied as defined in the {@link TransmissionMetrics} type.
	 */
	public TransmissionMetrics() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Inferences the ACK {@link SegmentPackager}. In case one is available as
	 * of {@link #getAcknowledgeSegmentPackager()}, then that is returned. Else
	 * CRC settings are evaluated and if possible sufficient CRC settings are
	 * available, a {@link CrcSegmentPackager} is returned. If there are no
	 * sufficient CRC settings, then a {@link DummySegmentPackager} is returned.
	 * 
	 * @return An interferenced {@link SegmentPackager} as of the instance's
	 *         properties.
	 */
	public SegmentPackager toAckSegmentPackager() {
		if ( getAcknowledgeSegmentPackager() != null ) {
			return getAcknowledgeSegmentPackager();
		}
		if ( getCrcAlgorithm() != null || getChecksumValidationMode() != null ) {
			final CrcAlgorithm theCrcAlgorithm = getCrcAlgorithm() != null ? getCrcAlgorithm() : DEFAULT_CRC_ALGORITHM;
			final ChecksumValidationMode theCrcChecksumValidationMode = getChecksumValidationMode() != null ? getChecksumValidationMode() : DEFAULT_CHECKSUM_VALIDATION_MODE;
			final ConcatenateMode theCrcChecksumConcatenateMode = getCrcChecksumConcatenateMode() != null ? getCrcChecksumConcatenateMode() : DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
			final Endianess theEndianess = getEndianess() != null ? getEndianess() : DEFAULT_ENDIANESS;
			return new CrcSegmentPackager( theCrcAlgorithm, theCrcChecksumConcatenateMode, theCrcChecksumValidationMode, theEndianess );
		}
		return new DummySegmentPackager();
	}

	/**
	 * Inferences the packet {@link SegmentPackager}. In case one is available
	 * as of {@link #getPacketSegmentPackager()}, then that is returned. Else
	 * CRC settings are evaluated and if possible sufficient CRC settings are
	 * available, a {@link CrcSegmentPackager} is returned. If there are no
	 * sufficient CRC settings, then a {@link DummySegmentPackager} is returned.
	 * 
	 * @return An interferenced {@link SegmentPackager} as of the instance's
	 *         properties.
	 */
	public SegmentPackager toPacketSegmentPackager() {
		if ( getPacketSegmentPackager() != null ) {
			return getPacketSegmentPackager();
		}
		if ( getCrcAlgorithm() != null || getChecksumValidationMode() != null ) {
			final CrcAlgorithm theCrcAlgorithm = getCrcAlgorithm() != null ? getCrcAlgorithm() : DEFAULT_CRC_ALGORITHM;
			final ChecksumValidationMode theCrcChecksumValidationMode = getChecksumValidationMode() != null ? getChecksumValidationMode() : DEFAULT_CHECKSUM_VALIDATION_MODE;
			final ConcatenateMode theCrcChecksumConcatenateMode = getCrcChecksumConcatenateMode() != null ? getCrcChecksumConcatenateMode() : DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
			final Endianess theEndianess = getEndianess() != null ? getEndianess() : DEFAULT_ENDIANESS;
			return new CrcSegmentPackager( theCrcAlgorithm, theCrcChecksumConcatenateMode, theCrcChecksumValidationMode, theEndianess );
		}
		return new DummySegmentPackager();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getPongMagicBytes() {
		return _pongMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getPingMagicBytes() {
		return _pingMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPacketLengthWidth() {
		return _packetLengthWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getAcknowledgeMagicBytes() {
		return _acknowledgeMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getAcknowledgeSegmentPackager() {
		return _acknowledgeSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getAcknowledgeRetryNumber() {
		return _acknowledgeRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getAcknowledgeTimeoutMillis() {
		return _acknowledgeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getPingTimeoutMillis() {
		return _pingTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPingRetryNumber() {
		return _pingRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBlockSize() {
		return _blockSize;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getClearToSendMagicBytes() {
		return _clearToSendMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getClearToSendSegmentPackager() {
		return _clearToSendSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getClearToSendTimeoutMillis() {
		return _clearToSendTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CrcAlgorithm getCrcAlgorithm() {
		return _crcAlgorithm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConcatenateMode getCrcChecksumConcatenateMode() {
		return _crcChecksumConcatenateMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChecksumValidationMode getChecksumValidationMode() {
		return _checksumValidationMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _encoding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte getEndOfStringByte() {
		return _endOfStringByte;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getEnquiryStandbyTimeMillis() {
		return _enquiryStandbyTimeInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLengthWidth() {
		return _lengthWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMagicBytesLength() {
		return _magicBytesLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getPacketMagicBytes() {
		return _packetMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getPacketSegmentPackager() {
		return _packetSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadTimeoutMillis() {
		return _readTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getReadyToReceiveMagicBytes() {
		return _readyToReceiveMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getReadyToReceiveRetryNumber() {
		return _readyToReceiveRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getReadyToReceiveSegmentPackager() {
		return _readyToReceiveSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadyToReceiveTimeoutMillis() {
		return _readyToReceiveTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getReadyToSendMagicBytes() {
		return _readyToSendMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getReadyToSendRetryNumber() {
		return _readyToSendRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getReadyToSendSegmentPackager() {
		return _readyToSendSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadyToSendTimeoutMillis() {
		return _readyToSendTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConcatenateMode getSequenceNumberConcatenateMode() {
		return _sequenceNumberConcatenateMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSequenceNumberInitValue() {
		return _sequenceNumberInitValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSequenceNumberWidth() {
		return _sequenceNumberWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTransmissionRetryNumber() {
		return _transmissionRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getTransmissionTimeoutMillis() {
		return _transmissionTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getWriteTimeoutMillis() {
		return _writeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getIoHeuristicsTimeToLiveMillis() {
		return _ioHeuristicsTimeToLiveInMs;
	}

	/**
	 * Creates builder to build {@link TransmissionMetrics}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link TransmissionMetrics}.
	 */
	public static class Builder implements AcknowledgeMagicBytesBuilder<Builder>, AcknowledgeTimeoutMillisBuilder<Builder>, AcknowledgeRetryNumberBuilder<Builder>, TransmissionTimeoutMillisBuilder<Builder>, TransmissionRetryNumberBuilder<Builder>, LengthWidthBuilder<Builder>, CrcAlgorithmBuilder<Builder>, ChecksumValidationModeBuilder<Builder>, EndianessBuilder<Builder>, BlockSizeBuilder<Builder>, SequenceNumberWidthBuilder<Builder>, SequenceNumberInitValueBuilder<Builder>, ReadTimeoutMillisBuilder<Builder>, WriteTimeoutMillisBuilder<Builder>, SequenceNumberConcatenateModeBuilder<Builder>, CrcChecksumConcatenateModeBuilder<Builder>, MagicBytesLengthBuilder<Builder>, EncodingBuilder<Charset, Builder>, AcknowledgeSegmentPackagerBuilder<Builder>, PacketSegmentPackagerBuilder<Builder>, ReadyToReceiveSegmentPackagerBuilder<Builder>, ReadyToReceiveTimeoutMillisBuilder<Builder>, ReadyToReceiveMagicBytesBuilder<Builder>, ReadyToReceiveRetryNumberBuilder<Builder>, EnquiryStandbyTimeMillisBuilder<Builder>, ReadyToSendTimeoutMillisBuilder<Builder>, ReadyToSendMagicBytesBuilder<Builder>, ReadyToSendRetryNumberBuilder<Builder>, ReadyToSendSegmentPackagerBuilder<Builder>, ClearToSendTimeoutMillisBuilder<Builder>, ClearToSendMagicBytesBuilder<Builder>, ClearToSendSegmentPackagerBuilder<Builder>, EndOfStringByteBuilder<Builder>, PacketMagicBytesBuilder<Builder>, PacketLengthWidthBuilder<Builder>, PingMagicBytesBuilder<Builder>, PingRetryNumberBuilder<Builder>, PingTimeoutMillisBuilder<Builder>, PongMagicBytesBuilder<Builder>, IoHeuristicsTimeToLiveMillisBuilder<Builder> {

		protected byte[] acknowledgeMagicBytes = DEFAULT_ACKNOWLEDGE_MAGIC_BYTES;
		protected byte[] clearToSendMagicBytes = DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES;
		protected byte[] packetMagicBytes = DEFAULT_PACKET_MAGIC_BYTES;
		protected byte[] readyToReceiveMagicBytes = DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES;
		protected byte[] readyToSendMagicBytes = DEFAULT_READY_TO_SEND_MAGIC_BYTES;
		protected byte[] pingMagicBytes = DEFAULT_PING_MAGIC_BYTES;
		protected byte[] pongMagicBytes = DEFAULT_PONG_MAGIC_BYTES;

		protected SegmentPackager acknowledgeSegmentPackager;
		protected long acknowledgeTimeoutInMs = DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS;
		protected int acknowledgeRetryNumber = DEFAULT_ACKNOWLEDGE_RETRY_NUMBER;
		protected long pingTimeoutInMs = DEFAULT_PING_TIMEOUT_IN_MS;
		protected int pingRetryNumber = DEFAULT_PING_RETRY_NUMBER;
		protected int blockSize = DEFAULT_BLOCK_SIZE;
		protected SegmentPackager clearToSendSegmentPackager;
		protected long clearToSendTimeoutInMs = DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS;
		protected CrcAlgorithm crcAlgorithm = DEFAULT_CRC_ALGORITHM;
		protected ConcatenateMode crcChecksumConcatenateMode = DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
		protected ChecksumValidationMode checksumValidationMode = DEFAULT_CHECKSUM_VALIDATION_MODE;;
		protected Charset encoding = DEFAULT_ENCODING;
		protected Endianess endianess = DEFAULT_ENDIANESS;
		protected byte endOfStringByte = DEFAULT_END_OF_STRING_BYTE;
		protected long enquiryStandbyTimeInMs = DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS;
		protected int lengthWidth = DEFAULT_LENGTH_WIDTH;
		protected int packetLengthWidth = DEFAULT_TRUNCATE_LENGTH_WIDTH;
		protected int magicBytesLength = DEFAULT_MAGIC_BYTES_LENGTH;
		protected SegmentPackager packetSegmentPackager;
		protected long readTimeoutInMs = DEFAULT_READ_TIMEOUT_IN_MS;
		protected int readyToReceiveRetryNumber = DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER;
		protected SegmentPackager readyToReceiveSegmentPackager;
		protected long readyToReceiveTimeoutInMs = DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS;
		protected int readyToSendRetryNumber = DEFAULT_READY_TO_SEND_RETRY_NUMBER;
		protected SegmentPackager readyToSendSegmentPackager;
		protected long readyToSendTimeoutInMs = DEFAULT_READY_TO_SEND_TIMEOUT_IN_MS;
		protected ConcatenateMode sequenceNumberConcatenateMode = DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE;
		protected int sequenceNumberInitValue = DEFAULT_SEQUENCE_NUMBER_INIT_VALUE;
		protected int sequenceNumberWidth = DEFAULT_SEQUENCE_NUMBER_WIDTH;
		protected int transmissionRetryNumber = DEFAULT_TRANSMISSION_RETRY_NUMBER;
		protected long transmissionTimeoutInMs = DEFAULT_TRANSMISSION_TIMEOUT_IN_MS;
		protected long writeTimeoutInMs = DEFAULT_WRITE_TIMEOUT_IN_MS;
		protected long ioHeuristicsTimeToLiveInMs = DEFAULT_IO_HEURISTICS_TIME_TO_LIVE_IN_MS;

		/**
		 * Instantiates a new builder.
		 */
		protected Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			acknowledgeMagicBytes = aAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendMagicBytes( byte[] aClearToSendMagicBytes ) {
			clearToSendMagicBytes = aClearToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketMagicBytes( byte[] aPacketMagicBytes ) {
			packetMagicBytes = aPacketMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveMagicBytes( byte[] aReadyToReceiveMagicBytes ) {
			readyToReceiveMagicBytes = aReadyToReceiveMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes ) {
			readyToSendMagicBytes = aReadyToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			acknowledgeSegmentPackager = aAcknowledgeSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeRetryNumber( int aAcknowledgeRetryNumber ) {
			acknowledgeRetryNumber = aAcknowledgeRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutInMs ) {
			acknowledgeTimeoutInMs = aAcknowledgeTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingRetryNumber( int aPingRetryNumber ) {
			pingRetryNumber = aPingRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingTimeoutMillis( long aPingTimeoutInMs ) {
			pingTimeoutInMs = aPingTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBlockSize( int aBlockSize ) {
			blockSize = aBlockSize;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode ) {
			checksumValidationMode = aChecksumValidationMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager ) {
			clearToSendSegmentPackager = aClearToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withClearToSendTimeoutMillis( long aClearToSendTimeoutInMs ) {
			clearToSendTimeoutInMs = aClearToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			crcAlgorithm = aCrcAlgorithm;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCrcChecksumConcatenateMode( ConcatenateMode aChecksumConcatenateMode ) {
			crcChecksumConcatenateMode = aChecksumConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEncoding( Charset aEncoding ) {
			encoding = aEncoding;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndOfStringByte( byte aEndOfStringByte ) {
			endOfStringByte = aEndOfStringByte;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEnquiryStandbyTimeMillis( long aEnquiryStandbyTimeInMs ) {
			enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLengthWidth( int aLengthWidth ) {
			lengthWidth = aLengthWidth;
			return this;
		}

		/**
		 * With packet length width.
		 *
		 * @param aPacketLengthWidth the packet length width
		 * 
		 * @return the builder
		 */
		@Override
		public Builder withPacketLengthWidth( int aPacketLengthWidth ) {
			packetLengthWidth = aPacketLengthWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withMagicBytesLength( int aMagicBytesLength ) {
			magicBytesLength = aMagicBytesLength;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPacketSegmentPackager( SegmentPackager aPacketSegmentPackager ) {
			packetSegmentPackager = aPacketSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadTimeoutMillis( long aReadTimeoutInMs ) {
			readTimeoutInMs = aReadTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveRetryNumber( int aReadyToReceiveRetryNumber ) {
			readyToReceiveRetryNumber = aReadyToReceiveRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager ) {
			readyToReceiveSegmentPackager = aReadyToReceiveSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutInMs ) {
			readyToReceiveTimeoutInMs = aReadyToReceiveTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendRetryNumber( int aReadyToSendRetryNumber ) {
			readyToSendRetryNumber = aReadyToSendRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager ) {
			readyToSendSegmentPackager = aReadyToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withReadyToSendTimeoutMillis( long aReadyToSendTimeoutInMs ) {
			readyToSendTimeoutInMs = aReadyToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode ) {
			sequenceNumberConcatenateMode = aSequenceNumberConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			sequenceNumberInitValue = aSequenceNumberInitValue;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSequenceNumberWidth( int aSequenceNumberWidth ) {
			sequenceNumberWidth = aSequenceNumberWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionRetryNumber( int aTransmissionRetryNumber ) {
			transmissionRetryNumber = aTransmissionRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTransmissionTimeoutMillis( long aTransmissionTimeoutInMs ) {
			transmissionTimeoutInMs = aTransmissionTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withWriteTimeoutMillis( long aWriteTimeoutInMs ) {
			writeTimeoutInMs = aWriteTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPongMagicBytes( byte[] aPongMagicBytes ) {
			pongMagicBytes = aPongMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPingMagicBytes( byte[] aPingMagicBytes ) {
			pingMagicBytes = aPingMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveInMs ) {
			ioHeuristicsTimeToLiveInMs = aIoHeuristicsTimeToLiveInMs;
			return this;
		}

		/**
		 * Builds the {@link TransmissionMetrics} instance from this builder's
		 * settings.
		 *
		 * @return The accordingly built {@link TransmissionMetrics} instance.
		 */
		public TransmissionMetrics build() {
			return new TransmissionMetrics( this );
		}
	}
}
