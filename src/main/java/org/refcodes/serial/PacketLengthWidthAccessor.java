// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a packet length width (in bytes) property.
 */
public interface PacketLengthWidthAccessor {

	/**
	 * Retrieves the packet length width (in bytes) from the packet length width
	 * (in bytes) property.
	 * 
	 * @return The packet length width (in bytes) stored by the packet length
	 *         width (in bytes) property.
	 */
	int getPacketLengthWidth();

	/**
	 * Provides a mutator for a packet length width (in bytes) property.
	 */
	public interface PacketLengthWidthMutator {

		/**
		 * Sets the packet length width (in bytes) for the packet length width
		 * (in bytes) property.
		 * 
		 * @param aPacketLengthWidth The packet length width (in bytes) to be
		 *        stored by the packet length width (in bytes) property.
		 */
		void setPacketLengthWidth( int aPacketLengthWidth );
	}

	/**
	 * Provides a builder method for a packet length width (in bytes) property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PacketLengthWidthBuilder<B extends PacketLengthWidthBuilder<B>> {

		/**
		 * Sets the packet length width (in bytes) for the packet length width
		 * (in bytes) property.
		 * 
		 * @param aPacketLengthWidth The packet length width (in bytes) to be
		 *        stored by the packet length width (in bytes) property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPacketLengthWidth( int aPacketLengthWidth );
	}

	/**
	 * Provides a packet length width (in bytes) property.
	 */
	public interface PacketLengthWidthProperty extends PacketLengthWidthAccessor, PacketLengthWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPacketLengthWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPacketLengthWidth The integer to set (via
		 *        {@link #setPacketLengthWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPacketLengthWidth( int aPacketLengthWidth ) {
			setPacketLengthWidth( aPacketLengthWidth );
			return aPacketLengthWidth;
		}
	}
}
