// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link PortHub} implementation is used to retrieve {@link Port} instances
 * of a given type (e.g. TTY-/COM-Ports).
 * 
 * @param <PORT> The actual {@link Port} type to use.
 * @param <PM> The actual {@link PortMetrics} type to use.
 */
public interface PortHub<PORT extends Port<PM>, PM extends PortMetrics> {

	/**
	 * Lists the available ports of the system.
	 * 
	 * @return An array of available {@link Port} instances, if there are no
	 *         ports than an empty array (size 0) is returned.
	 * 
	 * @throws IOException Thrown in case accessing the list of {@link Port}
	 *         instances failed due to I/O problems.
	 */
	PORT[] ports() throws IOException;

	/**
	 * Returns the {@link Port} identified by the given alias.
	 *
	 * @param aAlias The alias identifying the given {@link Port}.
	 * 
	 * @return The given {@link Port}.
	 * 
	 * @throws IOException Thrown in case accessing the list of {@link Port}
	 *         instances failed due to I/O problems.
	 */
	default PORT toPort( String aAlias ) throws IOException {
		for ( PORT ePort : ports() ) {
			if ( ePort.getAlias() != null && ePort.getAlias().equalsIgnoreCase( aAlias ) ) {
				return ePort;
			}
		}
		throw new NoSuchPortExcpetion( aAlias, "No such port with the given alias <" + aAlias + "> found!" );
	}

	/**
	 * Returns the {@link Port} identified by the given alias.
	 *
	 * @param aAlias The alias identifying the given {@link Port}.
	 * @param aPortMetrics The {@link PortMetrics} to use when opening the port
	 *        without providing any specific arguments.
	 * 
	 * @return The given {@link Port}.
	 * 
	 * @throws IOException Thrown in case accessing the list of {@link Port}
	 *         instances failed due to I/O problems.
	 */
	PORT toPort( String aAlias, PM aPortMetrics ) throws IOException;

	/**
	 * Returns the available ports of the system which's {@link Port#getAlias()}
	 * matches the given pattern.
	 * 
	 * @param aPattern The pattern of the {@link Port#getAlias()} to match. A
	 *        "*" stands for any chars, "?" stands for a single name (similar to
	 *        filename wildcards).
	 * 
	 * @return An array of available {@link Port} instances which's
	 *         {@link Port#getAlias()} matches the given pattern or an empty
	 *         array (size = 0) if there are none such ports.
	 * 
	 * @throws IOException Thrown in case accessing the list of {@link Port}
	 *         instances failed due to I/O problems.
	 */
	@SuppressWarnings("unchecked")
	default PORT[] ports( String aPattern ) throws IOException {
		final String thePortsPattern = aPattern.replace( "*", ".*" ).replace( "?", "." );
		String ePortName;
		final List<PORT> thePorts = new ArrayList<>();
		final PORT[] thePortArray = ports();
		for ( PORT ePort : thePortArray ) {
			ePortName = ePort.getAlias();
			if ( thePortsPattern != null && thePortsPattern.length() != 0 ) {
				if ( ePortName.matches( thePortsPattern ) ) {
					thePorts.add( ePort );
				}
			}
			else {
				thePorts.add( ePort );
			}
		}
		return thePorts.toArray( (PORT[]) Array.newInstance( thePortArray.getClass().getComponentType(), thePorts.size() ) );
	}
}
