// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link ByteSegment} is an implementation of a {@link Segment} carrying in
 * byte value as payload.
 */
public class ByteSegment extends AbstractPayloadSegment<Byte> implements Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final int BYTES = Byte.BYTES;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link ByteSegment}.
	 */
	public ByteSegment() {
		this( CaseStyleBuilder.asCamelCase( ByteSegment.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link ByteSegment} with the given byte payload.
	 * 
	 * @param aValue The value (payload) to be contained by the {@link Segment}.
	 */
	public ByteSegment( Byte aValue ) {
		this( CaseStyleBuilder.asCamelCase( ByteSegment.class.getSimpleName() ), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ByteSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public ByteSegment( String aAlias ) {
		this( aAlias, (byte) 0 );
	}

	/**
	 * Constructs a {@link ByteSegment} with the given byte payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the {@link Segment}.
	 */
	public ByteSegment( String aAlias, Byte aValue ) {
		super( aAlias, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( new byte[] { getPayload() } );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte theByte = aSequence.getByteAt( aOffset );
		setPayload( theByte );
		return aOffset + BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "" + getPayload(), "A segment containing an byte payload." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ByteSegment withPayload( Byte aValue ) {
		setPayload( aValue );
		return this;
	}
}
