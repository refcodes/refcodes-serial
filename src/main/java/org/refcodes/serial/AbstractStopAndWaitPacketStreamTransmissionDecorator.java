// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.mixin.PacketSizeAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * A {@link AbstractStopAndWaitPacketStreamTransmissionDecorator} wraps a
 * {@link Transmission} instance and enriches the {@link Transmission} with
 * packet-stream functionality. A packet consists of a sequence number, a block
 * of data and a CRC checksum in the order of the {@link ConcatenateMode} being
 * used.
 * 
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        chunked into blocks and enriched with a CRC checksum and a sequence
 *        number.
 */
public abstract class AbstractStopAndWaitPacketStreamTransmissionDecorator<DECORATEE extends Transmission> implements TransmissionMixin, StopAndWaitPacketStreamTransmission, DecorateeAccessor<DECORATEE>, PacketSizeAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";
	public static final String SEQUENCE_NUMBER_WIDTH = "SEQUENCE_NUMBER_WIDTH";
	public static final String SEQUENCE_NUMBER_INIT_VALUE = "SEQUENCE_NUMBER_INIT_VALUE";
	public static final String BLOCK_SIZE = "BLOCK_SIZE";
	public static final String ACK_MAGIC_BYTES = "ACK_MAGIC_BYTES";
	public static final String ACK_TIMEOUT_IN_MS = "ACK_TIMEOUT_IN_MS";
	public static final String ACK_RETRY_NUMBER = "ACK_RETRY_NUMBER";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected DECORATEE _decoratee;
	protected int _blockSize;
	protected byte[] _acknowledgeMagicBytes;
	protected int _acknowledgeRetryNumber;
	protected long _acknowledgeTimeoutInMs;
	protected long _crcChecksum = -1;
	protected Endianess _endianess;
	protected int _sequenceNumberWidth;
	protected int _sequenceNumberInitValue;
	protected ConcatenateMode _sequenceNumberConcatenateMode;
	protected SegmentPackager _acknowledgeSegmentPackager;
	protected SegmentPackager _packetSegmentPackager;
	protected int _packetSize = -1;
	protected byte[] _lastPacketMagicBytes = TransmissionMetrics.DEFAULT_LAST_PACKET_MAGIC_BYTES;;
	protected byte[] _packetMagicBytes = TransmissionMetrics.DEFAULT_PACKET_MAGIC_BYTES;
	protected int _packetLengthWidth = TransmissionMetrics.DEFAULT_TRUNCATE_LENGTH_WIDTH;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}. The configuration attributes are taken from
	 * the {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractStopAndWaitPacketStreamTransmissionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		this( aDecoratee, aTransmissionMetrics.getBlockSize(), aTransmissionMetrics.getPacketLengthWidth(), aTransmissionMetrics.getPacketMagicBytes(), aTransmissionMetrics.getSequenceNumberInitValue(), aTransmissionMetrics.getSequenceNumberWidth(), aTransmissionMetrics.getSequenceNumberConcatenateMode(), aTransmissionMetrics.toPacketSegmentPackager(), aTransmissionMetrics.getAcknowledgeMagicBytes(), aTransmissionMetrics.getAcknowledgeRetryNumber(), aTransmissionMetrics.getAcknowledgeTimeoutMillis(), aTransmissionMetrics.toAckSegmentPackager(), aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aBlockSize The block size of a data block for each packet.
	 * @param aPacketLengthWidth The width (bytes) for declaring the (max)
	 *        length of a package.
	 * @param aPacketMagicBytes The magic bytes identifying a packet and
	 *        distinguishing a packet from a last package.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aPacketSegmentPackager An (optional) {@link SegmentPackager} used
	 *        to modify a packet's data e.g. with a CRC checksum.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAcknowledgeRetryNumber The number of retries waiting for an ACK
	 *        from the return channel.
	 * @param aAcknowledgeTimeoutInMs The timeout in milliseconds to pend till
	 *        the next retry.
	 * @param aAckSegmentPackager An (optional) {@link SegmentPackager} used to
	 *        modify a ACK response data e.g. with a CRC checksum.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitPacketStreamTransmissionDecorator( DECORATEE aDecoratee, int aBlockSize, int aPacketLengthWidth, byte[] aPacketMagicBytes, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, SegmentPackager aPacketSegmentPackager, byte[] aAcknowledgeMagicBytes, int aAcknowledgeRetryNumber, long aAcknowledgeTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		_decoratee = aDecoratee;
		_acknowledgeMagicBytes = aAcknowledgeMagicBytes;
		_acknowledgeRetryNumber = aAcknowledgeRetryNumber;
		_acknowledgeTimeoutInMs = aAcknowledgeTimeoutInMs;
		_sequenceNumberConcatenateMode = aSequenceNumberConcatenateMode;
		_sequenceNumberInitValue = aSequenceNumberInitValue;
		_sequenceNumberWidth = aSequenceNumberWidth;
		_blockSize = aBlockSize;
		_endianess = aEndianess;
		_packetMagicBytes = aPacketMagicBytes;
		_packetLengthWidth = aPacketLengthWidth;
		_acknowledgeSegmentPackager = aAckSegmentPackager != null ? aAckSegmentPackager : new DummySegmentPackager();
		_packetSegmentPackager = aPacketSegmentPackager != null ? aPacketSegmentPackager : new DummySegmentPackager();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the {@link ConcatenateMode} describing the positions of the
	 * sequence number, the block data and the CRC checksum within the packet.
	 * 
	 * @return The according {@link ConcatenateMode}.
	 */
	public ConcatenateMode getCrcChecksumConcatenateMode() {
		return _sequenceNumberConcatenateMode;
	}

	/**
	 * Retrieves the width (in bytes) dedicated for the sequence number.
	 * 
	 * @return The according length of the sequence number.
	 */
	public int getSequenceNumberWidth() {
		return _sequenceNumberWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBlockSize() {
		return _blockSize;
	}

	/**
	 * {@inheritDoc} ATTENTION: The packet size is not available until
	 * transmission has been put into effect!
	 */
	@Override
	public int getPacketSize() {
		return _packetSize - 1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSequenceNumberInitValue() {
		return _sequenceNumberInitValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getAcknowledgeRetryNumber() {
		return _acknowledgeRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getAcknowledgeMagicBytes() {
		return _acknowledgeMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getAcknowledgeSegmentPackager() {
		return _acknowledgeSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getAcknowledgeTimeoutMillis() {
		return _acknowledgeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		final StopAndWaitPacketOutputStream theBlockOutputStream = new StopAndWaitPacketOutputStream( aOutputStream, _blockSize, _packetLengthWidth, _packetMagicBytes, _sequenceNumberInitValue, _sequenceNumberWidth, _sequenceNumberConcatenateMode, _packetSegmentPackager, aReturnStream, _acknowledgeMagicBytes, _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, _acknowledgeSegmentPackager, _endianess );
		_packetSize = theBlockOutputStream.getPacketSize();
		_decoratee.transmitTo( theBlockOutputStream ); // No feedback stream to succeeding carriers
		theBlockOutputStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A segment decorator enriching the encapsulated segment's streams with packet functionality.", getDecoratee().toSchema() );
		theSchema.put( ENDIANESS, getEndianess() );
		theSchema.put( BLOCK_SIZE, getBlockSize() );
		theSchema.put( SEQUENCE_NUMBER_INIT_VALUE, getSequenceNumberInitValue() );
		theSchema.put( SEQUENCE_NUMBER_WIDTH, getSequenceNumberWidth() );
		theSchema.put( ACK_MAGIC_BYTES, getAcknowledgeMagicBytes() );
		theSchema.put( ACK_TIMEOUT_IN_MS, getAcknowledgeTimeoutMillis() );
		theSchema.put( ACK_RETRY_NUMBER, getAcknowledgeRetryNumber() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}
}
