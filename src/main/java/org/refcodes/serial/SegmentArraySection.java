// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.refcodes.factory.TypeFactory;

/**
 * A {@link SegmentArraySection} is a {@link Segment} consisting of
 * {@link Segment} elements with each element of presumably different lengths (
 * in contrast to the {@link FixedSegmentArraySection}).
 * 
 * @param <ARRAY> The type of the array elements to be contained in this
 *        instance.
 */
public class SegmentArraySection<ARRAY extends Segment> extends AbstractArrayTransmission<ARRAY> implements Section, ArraySection<ARRAY> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( Class<ARRAY> aSegmentClass ) {
		super( aSegmentClass );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( Collection<ARRAY> aSegments ) {
		super( aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public SegmentArraySection( ARRAY... aSegments ) {
		super( aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( String aAlias, Class<ARRAY> aSegmentClass ) {
		super( aAlias, aSegmentClass );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( String aAlias, Collection<ARRAY> aSegments ) {
		super( aAlias, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public SegmentArraySection( String aAlias, ARRAY... aSegments ) {
		super( aAlias, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		super( aAlias, aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public SegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		super( aAlias, aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory ) {
		super( aAlias, aSegmentFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		super( aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public SegmentArraySection( TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		super( aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public SegmentArraySection( TypeFactory<ARRAY> aSegmentFactory ) {
		super( aSegmentFactory );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentArraySection<ARRAY> withArray( ARRAY[] aValue ) {
		setArray( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		if ( aLength != 0 ) {
			int theOffset = aOffset;
			ARRAY eSegment;
			final List<ARRAY> theSegments = new ArrayList<>();
			while ( theOffset < aOffset + aLength ) {
				eSegment = _sequenceableFactory.create();
				eSegment.fromTransmission( aSequence, theOffset );
				theSegments.add( eSegment );
				theOffset += eSegment.getLength();
			}
			if ( theOffset - aOffset != aLength ) {
				throw new TransmissionSequenceException( aSequence, "The sequence with length <" + aLength + "> at the offset <" + aOffset + "> results in <" + ( theOffset - aOffset ) + "> bytes consumed, which is not the expected length <" + aLength + ">!" );
			}
			if ( theSegments.size() != 0 ) {
				_array = theSegments.toArray( (ARRAY[]) Array.newInstance( theSegments.get( 0 ).getClass(), theSegments.size() ) );
			}
			else {
				_array = theSegments.toArray( (ARRAY[]) Array.newInstance( _sequenceableFactory.getType(), 0 ) );
			}
		}
	}
}
