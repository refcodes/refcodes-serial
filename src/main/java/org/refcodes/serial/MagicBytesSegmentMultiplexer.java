// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

import org.refcodes.io.ReplayInputStream;

/**
 * THe {@link MagicBytesSegmentMultiplexer} dispatches a transmission to one of
 * the aggregated {@link Segment} instances depending on the magic number
 * provided by the transmission. A transmission is passed to each of the
 * aggregated {@link Segment} instances till one {@link Segment} accepts the
 * transmission, e.g. until a {@link Segment} does not throw a
 * {@link BadMagicBytesException}. To enforce throwing a
 * {@link BadMagicBytesException} use a magic bytes {@link Segment} or
 * {@link Section} which enforces the magic bytes to of a transmission to match
 * its own magic bytes, e.e. use one of the following:
 * {@link AssertMagicBytesSectionDecorator}, {@link AssertMagicBytesSegment},
 * {@link AssertMagicBytesSegmentDecorator} Attention: A {@link Segment}
 * throwing a {@link TransmissionException} other than a
 * {@link BadMagicBytesException} is considered to be responsible for the
 * transmission so that dispatching is *not* continued with the succeeding
 * {@link Segment}! The last {@link Segment} which was responsible for a
 * transmission's magic bytes will be the responsible {@link Segment} till
 * another {@link Segment} claims responsibility for a transmsision's magic
 * bytes. Initially the first {@link Segment} passed to this instance is the
 * responsible {@link Segment}.
 */
public class MagicBytesSegmentMultiplexer extends AbstractMagicBytesTransmissionMultiplexer<Segment> implements Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public MagicBytesSegmentMultiplexer() {}

	/**
	 * {@inheritDoc}
	 */
	public MagicBytesSegmentMultiplexer( Collection<Segment> aSegments, int aReadLimit ) {
		super( aSegments, aReadLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	public MagicBytesSegmentMultiplexer( Collection<Segment> aSegments ) {
		super( aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public MagicBytesSegmentMultiplexer( int aReadLimit, Segment... aSegments ) {
		super( aReadLimit, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public MagicBytesSegmentMultiplexer( Segment... aSegments ) {
		super( aSegments );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( byte[] aChunk ) throws TransmissionException {
		return fromTransmission( new ByteArraySequence( aChunk ), 0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence ) throws TransmissionException {
		return fromTransmission( aSequence, 0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( byte[] aChunk, int aOffset ) throws TransmissionException {
		return fromTransmission( new ByteArraySequence( aChunk ), aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream ) throws IOException {
		receiveFrom( aInputStream, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( SerialTransceiver aSerialTransceiver ) throws IOException {
		receiveFrom( aSerialTransceiver.getInputStream(), aSerialTransceiver.getOutputStream() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		if ( _children != null ) {
			int eReturn;
			for ( Segment a_children : _children ) {
				try {
					eReturn = a_children.fromTransmission( aSequence, aOffset );
					_responsibility = a_children;
					return eReturn;
				}
				catch ( BadCrcChecksumException | BadMagicBytesException ignore ) {}
			}
		}
		throw new TransmissionSequenceException( aSequence, aOffset, "There are no segments to which to transmit the given sequence!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( _children != null ) {
			final InputStream theInputStream = aInputStream.markSupported() ? aInputStream : new ReplayInputStream( aInputStream );
			theInputStream.mark( _readLimit );
			for ( Segment a_children : _children ) {
				try {
					a_children.receiveFrom( theInputStream, aReturnStream );
					_responsibility = a_children;
					return;
				}
				catch ( BadMagicBytesException ignore ) {
					theInputStream.reset();
				}
				catch ( BadCrcChecksumException ignore ) {
					theInputStream.reset();
					ignore.printStackTrace();
				}
			}
		}
		throw new TransmissionException( "There are no segments to which to transmit the given transmission to!" );
	}
}
