// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a CTS ("clear-to-send") timeout in milliseconds
 * property.
 */
public interface ClearToSendTimeoutMillisAccessor {

	/**
	 * The CTS ("clear-to-send") timeout attribute in milliseconds.
	 * 
	 * @return An integer with the CTS ("clear-to-send") timeout in
	 *         milliseconds.
	 */
	long getClearToSendTimeoutMillis();

	/**
	 * Provides a mutator for a CTS ("clear-to-send") timeout in milliseconds
	 * property.
	 */
	public interface ClearToSendTimeoutMillisMutator {

		/**
		 * The CTS ("clear-to-send") timeout attribute in milliseconds.
		 * 
		 * @param aClearToSendTimeoutMillis An integer with the CTS
		 *        ("clear-to-send") timeout in milliseconds.
		 */
		void setClearToSendTimeoutMillis( long aClearToSendTimeoutMillis );
	}

	/**
	 * Provides a builder method for a the CTS ("clear-to-send") timeout
	 * property returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ClearToSendTimeoutMillisBuilder<B extends ClearToSendTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the CTS ("clear-to-send") timeout property.
		 * 
		 * @param aClearToSendTimeoutMillis The CTS ("clear-to-send") timeout in
		 *        milliseconds to be stored by the CTS timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withClearToSendTimeoutMillis( long aClearToSendTimeoutMillis );
	}

	/**
	 * Provides a CTS ("clear-to-send") timeout in milliseconds property.
	 */
	public interface ClearToSendTimeoutMillisProperty extends ClearToSendTimeoutMillisAccessor, ClearToSendTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setClearToSendTimeoutMillis(long)} and returns the very same
		 * value (getter).
		 * 
		 * @param aClearToSendTimeoutMillis The long to set (via
		 *        {@link #setClearToSendTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letClearToSendTimeoutMillis( long aClearToSendTimeoutMillis ) {
			setClearToSendTimeoutMillis( aClearToSendTimeoutMillis );
			return aClearToSendTimeoutMillis;
		}
	}
}
