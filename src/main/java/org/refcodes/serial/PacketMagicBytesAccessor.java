// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a packet magic bytes property.
 */
public interface PacketMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the packet magic bytes property.
	 * 
	 * @return The magic bytes stored by the packet magic bytes property.
	 */
	byte[] getPacketMagicBytes();

	/**
	 * Provides a mutator for a packet magic bytes property.
	 */
	public interface PacketMagicBytesMutator {

		/**
		 * Sets the magic bytes for the packet magic bytes property.
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the packet
		 *        magic bytes property.
		 */
		void setPacketMagicBytes( byte[] aPacketMagicBytes );

		/**
		 * Sets the magic bytes for the packet magic bytes property. Uses
		 * {@link TransmissionMetrics#DEFAULT_ENCODING} for converting the
		 * {@link String} into a byte array.
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 */
		default void setPacketMagicBytes( String aPacketMagicBytes ) {
			setPacketMagicBytes( aPacketMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the packet magic bytes property.
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setPacketMagicBytes( String aPacketMagicBytes, Charset aEncoding ) {
			setPacketMagicBytes( aPacketMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a packet magic bytes property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PacketMagicBytesBuilder<B extends PacketMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the packet magic bytes property.
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the packet
		 *        magic bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPacketMagicBytes( byte[] aPacketMagicBytes );

		/**
		 * Sets the magic bytes for the packet magic bytes property.
		 *
		 * @param aPacketMagicBytes The magic bytes to be stored by the magic
		 *        bytes property. Uses
		 *        {@link TransmissionMetrics#DEFAULT_ENCODING} for converting
		 *        the {@link String} into a byte array.
		 * 
		 * @return the b
		 */
		default B withPacketMagicBytes( String aPacketMagicBytes ) {
			return withPacketMagicBytes( aPacketMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the packet magic bytes property.
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withPacketMagicBytes( String aPacketMagicBytes, Charset aEncoding ) {
			return withPacketMagicBytes( aPacketMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a packet magic bytes property.
	 */
	public interface PacketMagicBytesProperty extends PacketMagicBytesAccessor, PacketMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPacketMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPacketMagicBytes The character to set (via
		 *        {@link #setPacketMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letPacketMagicBytes( byte[] aPacketMagicBytes ) {
			setPacketMagicBytes( aPacketMagicBytes );
			return aPacketMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPacketMagicBytes(byte[])} and returns the very same value
		 * (getter). Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 *
		 * @param aPacketMagicBytes The character to set (via
		 *        {@link #setPacketMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPacketMagicBytes( String aPacketMagicBytes ) {
			return letPacketMagicBytes( aPacketMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #letPacketMagicBytes(byte[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aPacketMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPacketMagicBytes( String aPacketMagicBytes, Charset aEncoding ) {
			setPacketMagicBytes( aPacketMagicBytes, aEncoding );
			return aPacketMagicBytes;
		}
	}
}
