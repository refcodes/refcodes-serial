// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;
import java.util.Iterator;

/**
 * A {@link AbstractTransmissionComposite} is a {@link Transmission} consisting
 * of {@link Transmission} elements with each element.
 * 
 * @param <CHILD> The type of the child elements to be contained in this
 *        instance.
 */
public abstract class AbstractTransmissionComposite<CHILD extends Transmission> implements Transmission, TransmissionComposite<CHILD>, Iterable<CHILD> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CHILD[] _children; // Children as of "https://en.wikipedia.org/wiki/Composite_pattern"

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link AbstractTransmissionComposite} for sub-classes
	 * to use having the responsibility to set the children by themselves.
	 */
	protected AbstractTransmissionComposite() {}

	/**
	 * Constructs a {@link AbstractTransmissionComposite} containing the
	 * provided {@link Segment} elements.
	 * 
	 * @param aSegments The {@link Segment} elements being contained in this
	 *        instance.
	 */
	@SafeVarargs
	public AbstractTransmissionComposite( CHILD... aSegments ) {
		_children = aSegments;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<CHILD> iterator() {
		return Arrays.asList( _children ).iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CHILD[] getChildren() {
		return _children;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		if ( _children != null && _children.length != 0 ) {
			for ( var eTransmission : _children ) {
				eTransmission.reset();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [children=" + Arrays.toString( _children ) + "]";
	}
}
