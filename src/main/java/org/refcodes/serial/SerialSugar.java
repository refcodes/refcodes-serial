// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.function.Supplier;

import javax.crypto.Cipher;

import org.refcodes.factory.TypeFactory;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.BijectiveFunction;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.InverseFunction;
import org.refcodes.numerical.Invertible;
import org.refcodes.properties.Properties;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the construction of various serial types
 * such as {@link Segment} or {@link Section} type instances (and the like).
 */
public class SerialSugar {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new serial sugar.
	 */
	protected SerialSugar() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link NullSegment} represents a no-operation {@link Segment}, useful
	 * as placeholder in a {@link SegmentComposite}.
	 *
	 * @return The accordingly created {@link NullSegment}.
	 */
	public static NullSegment nullSegment() {
		return new NullSegment();
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param <E> The type of the enumeration.
	 * @param aType the enumeratrion's type
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( Class<E> aType, TransmissionMetrics aTransmissionMetrics ) {
		return new EnumSegment<>( aType, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param <E> The type of the enumeration.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( Enum<E> aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new EnumSegment<>( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aType the enumeratrion's type
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Class<E> aType, TransmissionMetrics aTransmissionMetrics ) {
		return new EnumSegment<>( aAlias, aType, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Enum<E> aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new EnumSegment<>( aAlias, aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link EnumSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link EnumSegment}'s value.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aType the enumeratrion's type
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( Class<E> aType ) {
		return new EnumSegment<>( aType );
	}

	/**
	 * Constructs an empty {@link EnumSegment} with the given {@link Endianess}.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aType the enumeratrion's type
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( Class<E> aType, Endianess aEndianess ) {
		return new EnumSegment<>( aType, aEndianess );
	}

	/**
	 * Constructs a {@link EnumSegment} with the given enumeration value
	 * (payload) and the given {@link Endianess} for the representation of the
	 * {@link EnumSegment}'s value.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link EnumSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( Enum<E> aValue, Endianess aEndianess ) {
		return new EnumSegment<>( aValue, aEndianess );
	}

	/**
	 * Constructs an empty {@link EnumSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link EnumSegment}'s value.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType the enumeratrion's type
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Class<E> aType ) {
		return new EnumSegment<>( aAlias, aType );
	}

	/**
	 * Constructs an empty {@link EnumSegment} with the given {@link Endianess}.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType the enumeratrion's type
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Class<E> aType, Endianess aEndianess ) {
		return new EnumSegment<>( aAlias, aType, aEndianess );
	}

	/**
	 * Constructs a {@link EnumSegment} with the given enumeration value
	 * (payload) and the given {@link Endianess} for the representation of the
	 * {@link EnumSegment}'s value.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link EnumSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Enum<E> aValue, Endianess aEndianess ) {
		return new EnumSegment<>( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs a {@link EnumSegment} with the given enumeration value
	 * (payload) and the given {@link Endianess} for the representation of the
	 * {@link EnumSegment}'s value.
	 *
	 * @param <E> The type of the enumeration.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType the enumeration's type
	 * @param aValue The value (payload) to be contained by the
	 *        {@link EnumSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link EnumSegment}.
	 */
	public static <E extends Enum<E>> EnumSegment<E> enumSegment( String aAlias, Class<E> aType, Enum<E> aValue, Endianess aEndianess ) {
		return new EnumSegment<>( aAlias, aType, aValue, aEndianess );
	}

	/**
	 * Constructs a {@link AllocSectionDecoratorSegment} with the given
	 * decoratee and a width of {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}
	 * bytes used to specify the decoratee's length a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the decoratee's length.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * 
	 * @return The accordingly created {@link AllocSectionDecoratorSegment}.
	 */
	public static <DECORATEE extends Section> AllocSectionDecoratorSegment<DECORATEE> allocSegment( DECORATEE aDecoratee ) {
		return new AllocSectionDecoratorSegment<>( aDecoratee );
	}

	/**
	 * Constructs a {@link AllocSectionDecoratorSegment} with the given
	 * decoratee and a width of {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}
	 * bytes used to specify the decoratee's length and the provided
	 * {@link Endianess} representation of the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSectionDecoratorSegment}.
	 */
	public static <DECORATEE extends Section> AllocSectionDecoratorSegment<DECORATEE> allocSegment( DECORATEE aDecoratee, Endianess aEndianess ) {
		return new AllocSectionDecoratorSegment<>( aDecoratee, aEndianess );
	}

	/**
	 * Constructs a {@link AllocSectionDecoratorSegment} with the given
	 * decoratee and with the given number of bytes used to specify the
	 * decoratee's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSectionDecoratorSegment}.
	 */
	public static <DECORATEE extends Section> AllocSectionDecoratorSegment<DECORATEE> allocSegment( DECORATEE aDecoratee, int aLengthWidth ) {
		return new AllocSectionDecoratorSegment<>( aDecoratee, aLengthWidth );
	}

	/**
	 * Constructs a {@link AllocSectionDecoratorSegment} with the given
	 * decoratee and with the given number of bytes used to specify the
	 * decoratee's length and the provided {@link Endianess} representation of
	 * the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSectionDecoratorSegment}.
	 */
	public static <DECORATEE extends Section> AllocSectionDecoratorSegment<DECORATEE> allocSegment( DECORATEE aDecoratee, int aLengthWidth, Endianess aEndianess ) {
		return new AllocSectionDecoratorSegment<>( aDecoratee, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs an according {@link AllocSectionDecoratorSegment} instance.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AllocSectionDecoratorSegment}.
	 */
	public static <DECORATEE extends Section> AllocSectionDecoratorSegment<DECORATEE> allocSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new AllocSectionDecoratorSegment<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs the decorator with the given decoratee and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the decoratee's length a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the decoratee's length.
	 * 
	 * @param <DECORATEE> The type of the {@link Section} decoratee.
	 * @param aDecoratee The decoratee used for this decorator.
	 * 
	 * @return The accordingly created {@link AllocSegmentBody}.
	 */
	public static <DECORATEE extends Section> AllocSegmentBody<DECORATEE> allocSegmentBody( DECORATEE aDecoratee ) {
		return new AllocSegmentBody<>( aDecoratee );
	}

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the body's length.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead() {
		return new AllocSegmentHead();
	}

	/**
	 * Constructs the head with the given body and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the body's length.
	 * 
	 * @param aBody The body referenced by this head.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( AllocSegmentBody<?> aBody ) {
		return new AllocSegmentHead( aBody );
	}

	/**
	 * Constructs the head with the given body and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( AllocSegmentBody<?> aBody, Endianess aEndianess ) {
		return new AllocSegmentHead( aBody, aEndianess );
	}

	/**
	 * Constructs the head with the given body and with the given number of
	 * bytes used to specify the body's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( AllocSegmentBody<?> aBody, int aLengthWidth ) {
		return new AllocSegmentHead( aBody, aLengthWidth );
	}

	/**
	 * Constructs the head with the given body and with the given number of
	 * bytes used to specify the body's length and the provided
	 * {@link Endianess} representation of the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( AllocSegmentBody<?> aBody, int aLengthWidth, Endianess aEndianess ) {
		return new AllocSegmentHead( aBody, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the head with the given body. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aBody The body referenced by this head.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( AllocSegmentBody<?> aBody, TransmissionMetrics aTransmissionMetrics ) {
		return new AllocSegmentHead( aBody, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( Endianess aEndianess ) {
		return new AllocSegmentHead( aEndianess );
	}

	/**
	 * Constructs an empty head with the given number of bytes used to specify
	 * the body's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the body's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( int aLengthWidth ) {
		return new AllocSegmentHead( aLengthWidth );
	}

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link AllocSegmentHead}.
	 */
	public static AllocSegmentHead allocSegmentHead( int aLengthWidth, Endianess aEndianess ) {
		return new AllocSegmentHead( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link SegmentArraySection} with the given {@link Segment}
	 * elements. {@link Segment} instances for the array are created using the
	 * provided array's component type.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link SegmentArraySection}.
	 * @param aSegmentArray The array containing the according {@link Segment}
	 *        elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	@SuppressWarnings("unchecked")
	public static <ARRAY extends Segment> SegmentArraySection<ARRAY> arraySection( ARRAY... aSegmentArray ) {
		return new SegmentArraySection<>( aSegmentArray );
	}

	/**
	 * Constructs a {@link SegmentArraySection} with instances of the array
	 * being created using the provided {@link Class} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link SegmentArraySection}.
	 * @param aSegmentClass The class from which to produce the the fixed length
	 *        {@link Segment} elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	public static <ARRAY extends Segment> SegmentArraySection<ARRAY> arraySection( Class<ARRAY> aSegmentClass ) {
		return new SegmentArraySection<>( aSegmentClass );
	}

	/**
	 * Constructs a {@link SegmentArraySection} with instances of the array
	 * being created using the provided {@link TypeFactory} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link SegmentArraySection}.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	public static <ARRAY extends Segment> SegmentArraySection<ARRAY> arraySection( TypeFactory<ARRAY> aSegmentFactory ) {
		return new SegmentArraySection<>( aSegmentFactory );
	}

	/**
	 * Constructs a {@link SegmentArraySection} with the given elements.
	 * {@link Segment} instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link SegmentArraySection}.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegmentArray The array containing the according {@link Segment}
	 *        elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	@SuppressWarnings("unchecked")
	public static <ARRAY extends Segment> SegmentArraySection<ARRAY> arraySection( TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegmentArray ) {
		return new SegmentArraySection<>( aSegmentFactory, aSegmentArray );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} using the provided
	 * {@link String} elements and using the provided {@link Segment} class for
	 * creating {@link Segment} instances.
	 *
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * @param aLengthWidth The width (in bytes) to be used for size values
	 *        (number of elements in the payload array). * @param aEndianess The
	 *        {@link Endianess} to be used for size values.
	 * @param aEndianess the endianess
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( byte aEndOfStringByte, int aLengthWidth, Endianess aEndianess, String... aPayload ) {
		return new AsciizArraySegment( aEndOfStringByte, aLengthWidth, aEndianess, aPayload );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} as well as
	 * the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( byte aEndOfStringByte, String... aPayload ) {
		return new AsciizArraySegment( aEndOfStringByte, aPayload );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} using the provided
	 * {@link String} elements and using the provided {@link Segment} class for
	 * creating {@link Segment} instances.
	 *
	 * @param aLengthWidth The width (in bytes) to be used for size values
	 *        (number of elements in the payload array). * @param aEndianess The
	 *        {@link Endianess} to be used for size values.
	 * @param aEndianess the endianess
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( int aLengthWidth, Endianess aEndianess, String... aPayload ) {
		return new AsciizArraySegment( aLengthWidth, aEndianess, aPayload );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} as well as
	 * the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 * 
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String... aPayload ) {
		return new AsciizArraySegment( aPayload );
	}

	// ------------------------------------------------------------------------

	/**
	 * Constructs an {@link AsciizArraySegment} using the provided arguments.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * @param aLengthWidth The width (in bytes) to be used for size values
	 *        (number of elements in the payload array).
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String aAlias, byte aEndOfStringByte, int aLengthWidth, Endianess aEndianess, String... aPayload ) {
		return new AsciizArraySegment( aAlias, aEndOfStringByte, aLengthWidth, aEndianess, aPayload );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} using the provided arguments.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for size values
	 *        (number of elements in the payload array).
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String aAlias, int aLengthWidth, Endianess aEndianess, String... aPayload ) {
		return new AsciizArraySegment( aAlias, aLengthWidth, aEndianess, aPayload );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} from the given configuration.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new AsciizArraySegment( aAlias, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} from the given configuration.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String aAlias, TransmissionMetrics aTransmissionMetrics, byte aEndOfStringByte ) {
		return new AsciizArraySegment( aAlias, aTransmissionMetrics.getEndOfStringByte(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} from the given configuration.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link AsciizArraySegment}.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( String aAlias, TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		return new AsciizArraySegment( aAlias, aTransmissionMetrics, aValue );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} from the given configuration.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( TransmissionMetrics aTransmissionMetrics ) {
		return new AsciizArraySegment( aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AsciizArraySegment} from the given configuration.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link AsciizArraySegment}.
	 *
	 * @return The accordingly created {@link AsciizArraySegment}.
	 */
	public static AsciizArraySegment asciizArraySegment( TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		return new AsciizArraySegment( aTransmissionMetrics, aValue );
	}

	/**
	 * Constructs an empty {@link AsciizSegment}. The serialized content (as of
	 * {@link AsciizSegment#toSequence()} or
	 * {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString).
	 *
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment() {
		return new AsciizSegment();
	}

	/**
	 * Constructs an empty {@link AsciizSegment}. The serialized content (as of
	 * {@link AsciizSegment#toSequence()} or
	 * {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString).
	 * 
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( byte aEndOfStringByte ) {
		return new AsciizSegment( aEndOfStringByte );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 *
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( byte[] aValue ) {
		return new AsciizSegment( aValue );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( byte[] aValue, byte aEndOfStringByte ) {
		return new AsciizSegment( aValue, aEndOfStringByte );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 *
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aValue ) {
		return new AsciizSegment( aValue );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aValue, byte aEndOfStringByte ) {
		return new AsciizSegment( aValue, aEndOfStringByte );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 *
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, byte[] aValue ) {
		return new AsciizSegment( aAlias, aValue );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, byte[] aValue, byte aEndOfStringByte ) {
		return new AsciizSegment( aAlias, aValue, aEndOfStringByte );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 *
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, String aValue ) {
		return new AsciizSegment( aAlias, aValue );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * @param aEndOfStringByte The alternate value instead of 0 "zero"
	 *        identifying the end of the string.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, String aValue, byte aEndOfStringByte ) {
		return new AsciizSegment( aAlias, aValue, aEndOfStringByte );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, TransmissionMetrics aTransmissionMetrics, byte[] aValue ) {
		return new AsciizSegment( aAlias, aValue, aTransmissionMetrics.getEndOfStringByte() );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( String aAlias, TransmissionMetrics aTransmissionMetrics, String aValue ) {
		return new AsciizSegment( aAlias, aValue, aTransmissionMetrics.getEndOfStringByte() );
	}

	/**
	 * Constructs an empty {@link AsciizSegment}. The serialized content (as of
	 * {@link AsciizSegment#toSequence()} or
	 * {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString).
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new AsciizSegment( aTransmissionMetrics.getEndOfStringByte() );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given (ASCII encoded) bytes
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the bytes do not contain any 0 ("zero") values as a value of 0
	 * unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( TransmissionMetrics aTransmissionMetrics, byte[] aValue ) {
		return new AsciizSegment( aValue, aTransmissionMetrics.getEndOfStringByte() );
	}

	/**
	 * Constructs an {@link AsciizSegment} with the given {@link String}
	 * payload. The serialized content (as of {@link AsciizSegment#toSequence()}
	 * or {@link AsciizSegment#transmitTo(java.io.OutputStream)}) of an
	 * {@link AsciizSegment} represents an an ASCIIZ {@link String} (also known
	 * as CString). It is in the responsibility of the programmer to make sure
	 * that the {@link String} does not contain any 0 ("zero") values as a value
	 * of 0 unintendedly terminates the resulting {@link String}.
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by this {@link AsciizSegment}.
	 * 
	 * @return The accordingly created {@link AsciizSegment}.
	 */
	public static AsciizSegment asciizSegment( TransmissionMetrics aTransmissionMetrics, String aValue ) {
		return new AsciizSegment( aValue, aTransmissionMetrics.getEndOfStringByte() );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed. Enforces the configured magic bytes to match the received magic
	 * bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new AssertMagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( DECORATEE aDecoratee, String aMagicBytes ) {
		return new AssertMagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed. Enforces the configured magic bytes to match the received magic
	 * bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new AssertMagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		return new AssertMagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSectionDecorator#fromTransmission(Sequence, int, int)}
	 * and
	 * {@link AssertMagicBytesSectionDecorator#receiveFrom(java.io.InputStream, int, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> AssertMagicBytesSectionDecorator<DECORATEE> assertMagicBytesSection( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes. Enforces the configured magic bytes to match the received
	 * magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( byte... aMagicBytes ) {
		return new AssertMagicBytesSegment( aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes. Enforces the configured magic bytes to match the received
	 * magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new AssertMagicBytesSegment( aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed. Enforces the configured magic bytes to match the received magic
	 * bytes (as of {@link AssertMagicBytesSegment#fromTransmission(Sequence)}
	 * and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new AssertMagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegmentDecorator#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegmentDecorator#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( DECORATEE aDecoratee, String aMagicBytes ) {
		return new AssertMagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegmentDecorator#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegmentDecorator#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes (retrieved from the given {@link String}). Enforces the configured
	 * magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aMagicBytes ) {
		return new AssertMagicBytesSegment( aMagicBytes );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes. Enforces the configured magic bytes to match the received magic
	 * bytes (as of {@link AssertMagicBytesSegment#fromTransmission(Sequence)}
	 * and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aAlias, byte... aMagicBytes ) {
		return new AssertMagicBytesSegment( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an {@link AssertMagicBytesSegment} with the according magic
	 * bytes. Enforces the configured magic bytes to match the received magic
	 * bytes (as of {@link AssertMagicBytesSegment#fromTransmission(Sequence)}
	 * and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aAlias, byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new AssertMagicBytesSegment( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSegment( aMagicBytes, aCharset );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee. Enforces the configured magic
	 *        bytes to match the received magic bytes (as of
	 *        {@link AssertMagicBytesSegmentDecorator#fromTransmission(Sequence)}
	 *        and
	 *        {@link AssertMagicBytesSegmentDecorator#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 *        or the like). In case the assertion of the configured magic bytes
	 *        fails during receiving, then a {@link BadMagicBytesException} or
	 *        {@link BadMagicBytesSequenceException} is thrown.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new AssertMagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegmentDecorator#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegmentDecorator#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		return new AssertMagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}). Enforces the configured magic
	 * bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegmentDecorator#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegmentDecorator#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> AssertMagicBytesSegmentDecorator<DECORATEE> assertMagicBytesSegment( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aAlias, String aMagicBytes ) {
		return new AssertMagicBytesSegment( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aAlias, String aMagicBytes, Charset aCharset ) {
		return new AssertMagicBytesSegment( aAlias, aMagicBytes, aCharset );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aAlias, String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new AssertMagicBytesSegment( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new AssertMagicBytesSegment( aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}). Enforces the
	 * configured magic bytes to match the received magic bytes (as of
	 * {@link AssertMagicBytesSegment#fromTransmission(Sequence)} and
	 * {@link AssertMagicBytesSegment#receiveFrom(java.io.InputStream, java.io.OutputStream)}
	 * or the like). In case the assertion of the configured magic bytes fails
	 * during receiving, then a {@link BadMagicBytesException} or
	 * {@link BadMagicBytesSequenceException} is thrown.
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created {@link AssertMagicBytesSegment}.
	 */
	public static AssertMagicBytesSegment assertMagicBytesSegment( String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new AssertMagicBytesSegment( aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link BooleanArraySection}.
	 * 
	 * @return The empty {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection() {
		return new BooleanArraySection();
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 * 
	 * @return The accordingly created {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection( boolean... aValue ) {
		return new BooleanArraySection( aValue );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 * 
	 * @return The accordingly created {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection( Boolean... aValue ) {
		return new BooleanArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link BooleanArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The empty {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection( String aAlias ) {
		return new BooleanArraySection( aAlias );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 * 
	 * @return The accordingly created {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection( String aAlias, boolean... aValue ) {
		return new BooleanArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 * 
	 * @return The accordingly created {@link BooleanArraySection}.
	 */
	public static BooleanArraySection booleanArraySection( String aAlias, Boolean... aValue ) {
		return new BooleanArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an empty {@link BooleanSegment}.
	 * 
	 * @return The accordingly created {@link BooleanSegment}.
	 */
	public static BooleanSegment booleanSegment() {
		return new BooleanSegment();
	}

	/**
	 * Constructs a {@link BooleanSegment} with the given boolean payload.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link BooleanSegment}.
	 * 
	 * @return The accordingly created {@link BooleanSegment}.
	 */
	public static BooleanSegment booleanSegment( boolean aValue ) {
		return new BooleanSegment( aValue );
	}

	/**
	 * Constructs an empty {@link BooleanSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link BooleanSegment}.
	 */
	public static BooleanSegment booleanSegment( String aAlias ) {
		return new BooleanSegment( aAlias );
	}

	/**
	 * Constructs a {@link BooleanSegment} with the given boolean payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link BooleanSegment}.
	 * 
	 * @return The accordingly created {@link BooleanSegment}.
	 */
	public static BooleanSegment booleanSegment( String aAlias, boolean aValue ) {
		return new BooleanSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link BreakerSectionDecorator} instance with the given
	 * decoratee breaking deserialization of the decorated segments by the given
	 * number of times. After the total number of breaking the decoratee has
	 * been reached, the decorator behaves transparent (it just delegates
	 * without breaking the decoratee any more). This is good to see if a retry
	 * mechanism works when using some kind of error correction segment.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be contained by this facade.
	 * @param aBreakNumber The number of times to break deserialization.
	 * 
	 * @return The accordingly created {@link BreakerSectionDecorator}.
	 */
	public static <DECORATEE extends Section> BreakerSectionDecorator<DECORATEE> breakerSection( DECORATEE aDecoratee, int aBreakNumber ) {
		return new BreakerSectionDecorator<>( aDecoratee, aBreakNumber );
	}

	/**
	 * Constructs a {@link BreakerSegmentDecorator} instance with the given
	 * decoratee breaking deserialization of the decorated segments by the given
	 * number of times. After the total number of breaking the decoratee has
	 * been reached, the decorator behaves transparent (it just delegates
	 * without breaking the decoratee any more). This is good to see if a retry
	 * mechanism works when using some kind of error correction segment.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be contained by this facade.
	 * @param aBreakNumber The number of times to break deserialization.
	 * 
	 * @return The accordingly created {@link BreakerSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> BreakerSegmentDecorator<DECORATEE> breakerSegment( DECORATEE aDecoratee, int aBreakNumber ) {
		return new BreakerSegmentDecorator<>( aDecoratee, aBreakNumber );
	}

	/**
	 * Constructs an empty {@link ByteArraySection}.
	 * 
	 * @return The empty {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection() {
		return new ByteArraySection();
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aArray The array (payload) to be contained by the
	 *        {@link ByteArraySection}.
	 * 
	 * @return The accordingly created {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection( byte... aArray ) {
		return new ByteArraySection( aArray );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aArray The array (payload) to be contained by the
	 *        {@link ByteArraySection}.
	 * 
	 * @return The accordingly created {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection( Byte... aArray ) {
		return new ByteArraySection( aArray );
	}

	/**
	 * Constructs an empty {@link ByteArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The empty {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection( String aAlias ) {
		return new ByteArraySection( aAlias );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aArray The array (payload) to be contained by the
	 *        {@link ByteArraySection}.
	 * 
	 * @return The accordingly created {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection( String aAlias, byte... aArray ) {
		return new ByteArraySection( aAlias, aArray );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aArray The array (payload) to be contained by the
	 *        {@link ByteArraySection}.
	 * 
	 * @return The accordingly created {@link ByteArraySection}.
	 */
	public static ByteArraySection byteArraySection( String aAlias, Byte... aArray ) {
		return new ByteArraySection( aAlias, aArray );
	}

	/**
	 * Constructs an empty {@link ByteSegment}.
	 * 
	 * @return The accordingly created {@link ByteSegment}.
	 */
	public static ByteSegment byteSegment() {
		return new ByteSegment();
	}

	/**
	 * Constructs a {@link ByteSegment} with the given byte payload.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ByteSegment}.
	 * 
	 * @return The accordingly created {@link ByteSegment}.
	 */
	public static ByteSegment byteSegment( Byte aValue ) {
		return new ByteSegment( aValue );
	}

	/**
	 * Constructs an empty {@link ByteSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link ByteSegment}.
	 */
	public static ByteSegment byteSegment( String aAlias ) {
		return new ByteSegment( aAlias );
	}

	/**
	 * Constructs a {@link ByteSegment} with the given byte payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ByteSegment}.
	 * 
	 * @return The accordingly created {@link ByteSegment}.
	 */
	public static ByteSegment byteSegment( String aAlias, Byte aValue ) {
		return new ByteSegment( aAlias, aValue );
	}

	/**
	 * Constructs an empty {@link CharArraySection}. Uses UTF-16 by default for
	 * encoding.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection() {
		return new CharArraySection();
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( char[] aPayload ) {
		return new CharArraySection( aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( Character[] aPayload ) {
		return new CharArraySection( aPayload );
	}

	/**
	 * Constructs an empty {@link CharArraySection}.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( Charset aCharset ) {
		return new CharArraySection( aCharset );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( Charset aCharset, char... aPayload ) {
		return new CharArraySection( aCharset, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( Charset aCharset, Character... aPayload ) {
		return new CharArraySection( aCharset, aPayload );
	}

	/**
	 * Constructs an empty {@link CharArraySection}. Uses UTF-16 by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias ) {
		return new CharArraySection( aAlias );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, char[] aPayload ) {
		return new CharArraySection( aAlias, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, Character[] aPayload ) {
		return new CharArraySection( aAlias, aPayload );
	}

	/**
	 * Constructs an empty {@link CharArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, Charset aCharset ) {
		return new CharArraySection( aAlias, aCharset );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, Charset aCharset, char... aPayload ) {
		return new CharArraySection( aAlias, aCharset, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, Charset aCharset, Character... aPayload ) {
		return new CharArraySection( aAlias, aCharset, aPayload );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new CharArraySection( aAlias, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, char... aValue ) {
		return new CharArraySection( aAlias, aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Character... aValue ) {
		return new CharArraySection( aAlias, aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new CharArraySection( aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( TransmissionMetrics aTransmissionMetrics, char... aValue ) {
		return new CharArraySection( aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 * 
	 * @return The accordingly created {@link CharArraySection}.
	 */
	public static CharArraySection charArraySection( TransmissionMetrics aTransmissionMetrics, Character... aValue ) {
		return new CharArraySection( aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an empty {@link CharSection}.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection() {
		return new CharSection( (char) 0 );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link CharSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( char aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs a {@link CharSection} with the given char payload.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( Character aPayload ) {
		return new CharSection( aPayload );
	}

	/**
	 * Constructs a {@link CharSection} with the given char payload and the
	 * given {@link Charset}.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( Character aPayload, Charset aCharset ) {
		return new CharSection( aPayload, aCharset );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link CharSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( Character aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an empty {@link CharSection} with the given {@link Charset}.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( Charset aCharset ) {
		return new CharSection( (char) 0, aCharset );
	}

	/**
	 * Constructs an empty {@link CharSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias ) {
		return new CharSection( aAlias, (char) 0 );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link CharSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, char aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aAlias, aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs a {@link CharSection} with the given char payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, Character aPayload ) {
		return new CharSection( aAlias, aPayload );
	}

	/**
	 * Constructs a {@link CharSection} with the given char payload and the
	 * given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, Character aPayload, Charset aCharset ) {
		return new CharSection( aAlias, aPayload, aCharset );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link CharSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, Character aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aAlias, aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an empty {@link CharSection} with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, Charset aCharset ) {
		return new CharSection( aAlias, (char) 0, aCharset );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aAlias, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CharSection}.
	 */
	public static CharSection charSection( TransmissionMetrics aTransmissionMetrics ) {
		return new CharSection( aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs a {@link Cipher} transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decorator having applied the {@link Cipher}'s
	 *        {@link BijectiveFunction} to encode and the {@link Cipher}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aCipher The {@link Cipher} providing the {@link BijectiveFunction}
	 *        to encode and the {@link Cipher}'s {@link InverseFunction} to
	 *        decode any data being delegated.
	 *
	 * @return The accordingly created {@link CipherSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CipherSectionDecorator<DECORATEE> cipherSection( DECORATEE aDecoratee, Cipher aCipher ) {
		return new CipherSectionDecorator<>( aDecoratee, aCipher );
	}

	/**
	 * Constructs a {@link Cipher} transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decorator having applied the {@link Cipher}'s
	 *        {@link BijectiveFunction} to encode and the {@link Cipher}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aCipher The {@link Cipher} providing the {@link BijectiveFunction}
	 *        to encode and the {@link Cipher}'s {@link InverseFunction} to
	 *        decode any data being delegated.
	 *
	 * @return The accordingly created {@link CipherSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CipherSegmentDecorator<DECORATEE> cipherSegment( DECORATEE aDecoratee, Cipher aCipher ) {
		return new CipherSegmentDecorator<>( aDecoratee, aCipher );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType ) {
		return new ComplexTypeSegment<>( aType );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new ComplexTypeSegment<>( aType, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new ComplexTypeSegment<>( aType, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType, String... aAttributes ) {
		return new ComplexTypeSegment<>( aType, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #complexTypeSegment(Class, TransmissionMetrics, String[])}
	 * instead! The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType, TransmissionMetrics aTransmissionMetrics ) {
		return new ComplexTypeSegment<>( aType, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( Class<T> aType, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new ComplexTypeSegment<>( aType, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, Class<T> aType ) {
		return new ComplexTypeSegment<>( aAlias, aType );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new ComplexTypeSegment<>( aAlias, aType, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new ComplexTypeSegment<>( aAlias, aType, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given {@link Class} for
	 * the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, Class<T> aType, String... aAttributes ) {
		return new ComplexTypeSegment<>( aAlias, aType, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure instance.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, T aValue ) {
		return new ComplexTypeSegment<>( aAlias, aValue );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure instance.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new ComplexTypeSegment<>( aAlias, aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value from which to construct the complex type..
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new ComplexTypeSegment<>( aAlias, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value from which to construct the complex type..
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( String aAlias, T aValue, String... aAttributes ) {
		return new ComplexTypeSegment<>( aAlias, aValue, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure instance.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue ) {
		return new ComplexTypeSegment<>( aValue );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #complexTypeSegment(Class, String[])} instead!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure instance.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new ComplexTypeSegment<>( aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The value from which to construct the complex type..
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new ComplexTypeSegment<>( aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The value from which to construct the complex type..
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue, String... aAttributes ) {
		return new ComplexTypeSegment<>( aValue, aAttributes );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #complexTypeSegment(Class, TransmissionMetrics, String[])}
	 * instead! The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new ComplexTypeSegment<>( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The value from which to construct the complex type..
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link ComplexTypeSegment}.
	 */
	public static <T> ComplexTypeSegment<T> complexTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new ComplexTypeSegment<>( aValue, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcPrefixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcPrefixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcPrefixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcPrefixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcPrefixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcPrefixSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new CrcSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcPrefixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcPrefixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcPrefixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.PREPEND, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSection( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new CrcSectionDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new CrcSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}..
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new CrcSegmentPackager( aCrcChecksumConcatenateMode );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 *
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm ) {
		return new CrcSegmentPackager( aCrcAlgorithm );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aChecksumValidationMode );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}..
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 *
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}..
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}..
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new CrcSegmentPackager( aCrcAlgorithm, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum. The configuration attributes
	 * are taken from the {@link TransmissionMetrics} configuration object,
	 * though only those attributes are supported which are also supported by
	 * the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CrcSegmentPackager}.
	 */
	public static CrcSegmentPackager crcSegmentPackager( TransmissionMetrics aTransmissionMetrics ) {
		return new CrcSegmentPackager( aTransmissionMetrics.getCrcAlgorithm(), aTransmissionMetrics.getCrcChecksumConcatenateMode(), aTransmissionMetrics.getChecksumValidationMode(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSuffixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.APPEND );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSuffixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSuffixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSectionDecorator} wrapping the given
	 * {@link Section}.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The {@link Section} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSectionDecorator}.
	 */
	public static <DECORATEE extends Section> CrcSectionDecorator<DECORATEE> crcSuffixSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new CrcSectionDecorator<>( aDecoratee, aCrcAlgorithm, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSuffixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSuffixSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new CrcSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment} (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by
	 * default).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSuffixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.APPEND, aChecksumValidationMode );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSuffixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.APPEND, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs a {@link CrcSegmentDecorator} wrapping the given
	 * {@link Segment}.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 * 
	 * @return The accordingly created {@link CrcSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> CrcSegmentDecorator<DECORATEE> crcSuffixSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new CrcSegmentDecorator<>( aDecoratee, aCrcAlgorithm, ConcatenateMode.APPEND, aEndianess );
	}

	/**
	 * Constructs an empty {@link DoubleArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection() {
		return new DoubleArraySection();
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link DoubleArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( double... aValue ) {
		return new DoubleArraySection( aValue );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link DoubleArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( Double... aValue ) {
		return new DoubleArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link DoubleArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( Endianess aEndianess ) {
		return new DoubleArraySection( aEndianess );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and the given {@link Endianess} for the representation of the
	 * {@link DoubleArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( Endianess aEndianess, double... aValue ) {
		return new DoubleArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and the given {@link Endianess} for the representation of the
	 * {@link DoubleArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( Endianess aEndianess, Double... aValue ) {
		return new DoubleArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs an empty {@link DoubleArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias ) {
		return new DoubleArraySection( aAlias );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link DoubleArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, double... aValue ) {
		return new DoubleArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link DoubleArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, Double... aValue ) {
		return new DoubleArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an empty {@link DoubleArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, Endianess aEndianess ) {
		return new DoubleArraySection( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and the given {@link Endianess} for the representation of the
	 * {@link DoubleArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, Endianess aEndianess, double... aValue ) {
		return new DoubleArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link DoubleArraySection} with the given double array
	 * payload and the given {@link Endianess} for the representation of the
	 * {@link DoubleArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, Endianess aEndianess, Double... aValue ) {
		return new DoubleArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleArraySection( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, double... aValue ) {
		return new DoubleArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Double... aValue ) {
		return new DoubleArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleArraySection( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( TransmissionMetrics aTransmissionMetrics, double... aValue ) {
		return new DoubleArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * 
	 * @return The accordingly created {@link DoubleArraySection}.
	 */
	public static DoubleArraySection doubleArraySection( TransmissionMetrics aTransmissionMetrics, Double... aValue ) {
		return new DoubleArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an empty {@link DoubleSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link DoubleSegment}'s value.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment() {
		return new DoubleSegment();
	}

	/**
	 * Constructs a {@link DoubleSegment} with the given double payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link DoubleSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link DoubleSegment}.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( Double aValue ) {
		return new DoubleSegment( aValue );
	}

	/**
	 * Constructs a {@link DoubleSegment} with the given double payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link DoubleSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( Double aValue, Endianess aEndianess ) {
		return new DoubleSegment( aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( Double aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleSegment( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link DoubleSegment} with the given
	 * {@link Endianess} endian representation of the {@link DoubleSegment}'s
	 * value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( Endianess aEndianess ) {
		return new DoubleSegment( aEndianess );
	}

	/**
	 * Constructs an empty {@link DoubleSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link DoubleSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias ) {
		return new DoubleSegment( aAlias );
	}

	/**
	 * Constructs a {@link DoubleSegment} with the given double payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link DoubleSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link DoubleSegment}.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias, Double aValue ) {
		return new DoubleSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link DoubleSegment} with the given double payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link DoubleSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias, Double aValue, Endianess aEndianess ) {
		return new DoubleSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias, Double aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleSegment( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link DoubleSegment} with the given
	 * {@link Endianess} endian representation of the {@link DoubleSegment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias, Endianess aEndianess ) {
		return new DoubleSegment( aAlias, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleSegment( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DoubleSegment}.
	 */
	public static DoubleSegment doubleSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new DoubleSegment( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according {@link DummySegmentPackager}.
	 * 
	 * @return The accordingly created {@link DummySegmentPackager}.
	 */
	public static DummySegmentPackager dummySegmentPackager() {
		return new DummySegmentPackager();
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection() {
		return new DynamicTypeSection();
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( Charset aCharset ) {
		return new DynamicTypeSection( aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSection( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSection( aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( String aAlias ) {
		return new DynamicTypeSection( aAlias );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( String aAlias, Charset aCharset ) {
		return new DynamicTypeSection( aAlias, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( String aAlias, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSection( aAlias, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSection( aAlias, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue ) {
		return new DynamicTypeSection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, Charset aCharset ) {
		return new DynamicTypeSection( aAlias, aValue, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSection( aAlias, aValue, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		return new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, String... aAttributes ) {
		return new DynamicTypeSection( aAlias, aValue, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSection( aAlias, aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new DynamicTypeSection( aAlias, aValue, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSection( aAlias, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue ) {
		return new DynamicTypeSection( aValue );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, Charset aCharset ) {
		return new DynamicTypeSection( aValue, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSection( aValue, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSection( aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		return new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSection#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, String... aAttributes ) {
		return new DynamicTypeSection( aValue, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSection( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static <T> DynamicTypeSection dynamicTypeSection( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new DynamicTypeSection( aValue, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSection}.
	 */
	public static DynamicTypeSection dynamicTypeSection( TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSection( aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment() {
		return new DynamicTypeSegment();
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( Charset aCharset ) {
		return new DynamicTypeSegment( aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSegment( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSegment( aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( String aAlias ) {
		return new DynamicTypeSegment( aAlias );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( String aAlias, Charset aCharset ) {
		return new DynamicTypeSegment( aAlias, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( String aAlias, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSegment( aAlias, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSegment( aAlias, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue ) {
		return new DynamicTypeSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, Charset aCharset ) {
		return new DynamicTypeSegment( aAlias, aValue, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 *
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSegment( aAlias, aValue, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSegment( aAlias, aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSegment( aAlias, aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSegment( aAlias, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		return new DynamicTypeSegment( aAlias, aValue, aLengthWidth, aEndianess, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, String... aAttributes ) {
		return new DynamicTypeSegment( aAlias, aValue, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSegment( aAlias, aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new DynamicTypeSegment( aAlias, aValue, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSegment( aAlias, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue ) {
		return new DynamicTypeSegment( aValue );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, Charset aCharset ) {
		return new DynamicTypeSegment( aValue, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSegment( aValue, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess ) {
		return new DynamicTypeSegment( aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		return new DynamicTypeSegment( aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		return new DynamicTypeSegment( aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties as well
	 * as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		return new DynamicTypeSegment( aValue, aLengthWidth, aEndianess, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of
	 * {@link DynamicTypeSegment#fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, String... aAttributes ) {
		return new DynamicTypeSegment( aValue, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSegment( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static <T> DynamicTypeSegment dynamicTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		return new DynamicTypeSegment( aValue, aTransmissionMetrics, aAttributes );
	}

	/**
	 * Constructs a {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link DynamicTypeSegment}.
	 */
	public static DynamicTypeSegment dynamicTypeSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new DynamicTypeSegment( aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link FileSection}.
	 * 
	 * @param aFile The {@link File} representing the {@link FileSection}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( File aFile ) {
		return new FileSection( aFile );
	}

	/**
	 * Constructs a {@link FileSection}, the underlying {@link File} is created
	 * on demand dynamically using the provided {@link Supplier}.
	 * 
	 * @param aBasePath The {@link File} representing the base path for the path
	 *        provided by the {@link Supplier}.
	 * @param aFilePathSupplier The {@link Supplier} of the file path of
	 *        {@link File} representing the {@link FileSection}. Whenever the
	 *        underlying file is to be accessed, then the {@link Supplier} is
	 *        called to construct the according {@link File}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( File aBasePath, Supplier<String> aFilePathSupplier ) {
		return new FileSection( aBasePath, aFilePathSupplier );
	}

	/**
	 * Constructs a {@link FileSection}.
	 * 
	 * @param aFilePath The file path of {@link File} representing the
	 *        {@link FileSection}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aFilePath ) {
		return new FileSection( aFilePath );
	}

	/**
	 * Constructs a {@link FileSection}.
	 * 
	 * @param aAlias The {@link FileSection}'S alias.
	 * @param aFile The {@link File} representing the {@link FileSection}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aAlias, File aFile ) {
		return new FileSection( aAlias, aFile );
	}

	/**
	 * Constructs a {@link FileSection}, the underlying {@link File} is created
	 * on demand dynamically using the provided {@link Supplier}.
	 * 
	 * @param aAlias The {@link FileSection}'S alias.
	 * @param aBasePath The {@link File} representing the base path for the path
	 *        provided by the {@link Supplier}.
	 * @param aFilePathSupplier The {@link Supplier} of the file path of
	 *        {@link File} representing the {@link FileSection}. Whenever the
	 *        underlying file is to be accessed, then the {@link Supplier} is
	 *        called to construct the according {@link File}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aAlias, File aBasePath, Supplier<String> aFilePathSupplier ) {
		return new FileSection( aAlias, aBasePath, aFilePathSupplier );
	}

	/**
	 * Constructs a {@link FileSection}.
	 * 
	 * @param aAlias The {@link FileSection}'S alias.
	 * @param aFilePath The file path of {@link File} representing the
	 *        {@link FileSection}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aAlias, String aFilePath ) {
		return new FileSection( aAlias, new File( aFilePath ) );
	}

	/**
	 * Constructs a {@link FileSection}, the underlying {@link File} is created
	 * on demand dynamically using the provided {@link Supplier}.
	 * 
	 * @param aAlias The {@link FileSection}'S alias.
	 * @param aBasePath The base path for the path provided by the
	 *        {@link Supplier}.
	 * @param aFilePathSupplier The {@link Supplier} of the file path of
	 *        {@link File} representing the {@link FileSection}. Whenever the
	 *        underlying file is to be accessed, then the {@link Supplier} is
	 *        called to construct the according {@link File}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aAlias, String aBasePath, Supplier<String> aFilePathSupplier ) {
		return new FileSection( aAlias, aBasePath, aFilePathSupplier );
	}

	/**
	 * Constructs a {@link FileSection}, the underlying {@link File} is created
	 * on demand dynamically using the provided {@link Supplier}.
	 * 
	 * @param aBasePath The base path for the path provided by the
	 *        {@link Supplier}.
	 * @param aFilePathSupplier The {@link Supplier} of the file path of
	 *        {@link File} representing the {@link FileSection}. Whenever the
	 *        underlying file is to be accessed, then the {@link Supplier} is
	 *        called to construct the according {@link File}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( String aBasePath, Supplier<String> aFilePathSupplier ) {
		return new FileSection( aBasePath, aFilePathSupplier );
	}

	/**
	 * Constructs a {@link FileSection}, the underlying {@link File} is created
	 * on demand dynamically using the provided {@link Supplier}.
	 * 
	 * @param aFilePathSupplier The {@link Supplier} of the file path of
	 *        {@link File} representing the {@link FileSection}. Whenever the
	 *        underlying file is to be accessed, then the {@link Supplier} is
	 *        called to construct the according {@link File}.
	 *
	 * @return The accordingly created {@link FileSection}.
	 */
	public static FileSection fileSection( Supplier<String> aFilePathSupplier ) {
		return new FileSection( aFilePathSupplier );
	}

	/**
	 * Constructs a {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aSequenceSize The size of the {@link Sequence}.
	 *
	 * @return The accordingly created {@link FixedLengthSequenceSegment}.
	 */
	public static FixedLengthSequenceSegment fixedLengthSequenceSegment( int aSequenceSize ) {
		return new FixedLengthSequenceSegment( aSequenceSize );
	}

	/**
	 * Constructs a {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link FixedLengthSequenceSegment}.
	 *
	 * @return The accordingly created {@link FixedLengthSequenceSegment}.
	 */
	public static FixedLengthSequenceSegment fixedLengthSequenceSegment( Sequence aSequence ) {
		return new FixedLengthSequenceSegment( aSequence );
	}

	/**
	 * Constructs a {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequenceSize The size of the {@link Sequence}.
	 *
	 * @return The accordingly created {@link FixedLengthSequenceSegment}.
	 */
	public static FixedLengthSequenceSegment fixedLengthSequenceSegment( String aAlias, int aSequenceSize ) {
		return new FixedLengthSequenceSegment( aAlias, aSequenceSize );
	}

	/**
	 * Constructs a {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link FixedLengthSequenceSegment}.
	 *
	 * @return The accordingly created {@link FixedLengthSequenceSegment}.
	 */
	public static FixedLengthSequenceSegment fixedLengthSequenceSegment( String aAlias, Sequence aSequence ) {
		return new FixedLengthSequenceSegment( aAlias, aSequence );
	}

	/**
	 * Constructs a {@link FixedSegmentArraySection} with the given
	 * {@link Segment} elements. {@link Segment} instances for the array are
	 * created using the provided array's component type.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link FixedSegmentArraySection}.
	 * @param aSegmentArray The array containing the according {@link Segment}
	 *        elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	@SuppressWarnings("unchecked")
	public static <ARRAY extends Segment> FixedSegmentArraySection<ARRAY> fixedSegmentArraySection( ARRAY... aSegmentArray ) {
		return new FixedSegmentArraySection<>( aSegmentArray );
	}

	/**
	 * Constructs a {@link FixedSegmentArraySection} with instances of the array
	 * being created using the provided {@link Class} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link FixedSegmentArraySection}.
	 * @param aSegmentClass The class from which to produce the the fixed length
	 *        {@link Segment} elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	public static <ARRAY extends Segment> FixedSegmentArraySection<ARRAY> fixedSegmentArraySection( Class<ARRAY> aSegmentClass ) {
		return new FixedSegmentArraySection<>( aSegmentClass );
	}

	/**
	 * Constructs a {@link FixedSegmentArraySection} with instances of the array
	 * being created using the provided {@link TypeFactory} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link FixedSegmentArraySection}.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	public static <ARRAY extends Segment> FixedSegmentArraySection<ARRAY> fixedSegmentArraySection( TypeFactory<ARRAY> aSegmentFactory ) {
		return new FixedSegmentArraySection<>( aSegmentFactory );
	}

	/**
	 * Constructs a {@link FixedSegmentArraySection} with the given elements.
	 * {@link Segment} instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param <ARRAY> The type of the {@link Segment} elements to be contained
	 *        in this {@link FixedSegmentArraySection}.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegmentArray The array containing the according {@link Segment}
	 *        elements.
	 * 
	 * @return The accordingly created {@link Segment}.
	 */
	@SuppressWarnings("unchecked")
	public static <ARRAY extends Segment> FixedSegmentArraySection<ARRAY> fixedSegmentArraySection( TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegmentArray ) {
		return new FixedSegmentArraySection<>( aSegmentFactory, aSegmentArray );
	}

	/**
	 * Constructs an empty {@link FloatArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection() {
		return new FloatArraySection();
	}

	/**
	 * Constructs an empty {@link FloatArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( Endianess aEndianess ) {
		return new FloatArraySection( aEndianess );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link FloatArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( Endianess aEndianess, float... aValue ) {
		return new FloatArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link FloatArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( Endianess aEndianess, Float... aValue ) {
		return new FloatArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link FloatArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( float... aValue ) {
		return new FloatArraySection( aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link FloatArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( Float... aValue ) {
		return new FloatArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link FloatArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias ) {
		return new FloatArraySection( aAlias );
	}

	/**
	 * Constructs an empty {@link FloatArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, Endianess aEndianess ) {
		return new FloatArraySection( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link FloatArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, Endianess aEndianess, float... aValue ) {
		return new FloatArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link FloatArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, Endianess aEndianess, Float... aValue ) {
		return new FloatArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link FloatArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, float... aValue ) {
		return new FloatArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link FloatArraySection} with the given float array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link FloatArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, Float... aValue ) {
		return new FloatArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new FloatArraySection( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, float... aValue ) {
		return new FloatArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Float... aValue ) {
		return new FloatArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new FloatArraySection( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( TransmissionMetrics aTransmissionMetrics, float... aValue ) {
		return new FloatArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link FloatArraySection}.
	 * 
	 * @return The accordingly created {@link FloatArraySection}.
	 */
	public static FloatArraySection floatArraySection( TransmissionMetrics aTransmissionMetrics, Float... aValue ) {
		return new FloatArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an empty {@link FloatSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link FloatSegment}'s value.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment() {
		return new FloatSegment();
	}

	/**
	 * Constructs an empty {@link FloatSegment} with the given {@link Endianess}
	 * endian representation of the {@link FloatSegment}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( Endianess aEndianess ) {
		return new FloatSegment( aEndianess );
	}

	/**
	 * Constructs a {@link FloatSegment} with the given float payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link FloatSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link FloatSegment}.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( Float aValue ) {
		return new FloatSegment( aValue );
	}

	/**
	 * Constructs a {@link FloatSegment} with the given float payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link FloatSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( Float aValue, Endianess aEndianess ) {
		return new FloatSegment( aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( Float aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new FloatSegment( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link FloatSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link FloatSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias ) {
		return new FloatSegment( aAlias );
	}

	/**
	 * Constructs an empty {@link FloatSegment} with the given {@link Endianess}
	 * endian representation of the {@link FloatSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias, Endianess aEndianess ) {
		return new FloatSegment( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link FloatSegment} with the given float payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link FloatSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link FloatSegment}.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias, Float aValue ) {
		return new FloatSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link FloatSegment} with the given float payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link FloatSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias, Float aValue, Endianess aEndianess ) {
		return new FloatSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias, Float aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new FloatSegment( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new FloatSegment( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link FloatSegment}.
	 */
	public static FloatSegment floatSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new FloatSegment( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link IntArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection() {
		return new IntArraySection();
	}

	/**
	 * Constructs an empty {@link IntArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( Endianess aEndianess ) {
		return new IntArraySection( aEndianess );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * the given {@link Endianess} for the representation of the
	 * {@link IntArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( Endianess aEndianess, int... aValue ) {
		return new IntArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * the given {@link Endianess} for the representation of the
	 * {@link IntArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( Endianess aEndianess, Integer... aValue ) {
		return new IntArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( int... aValue ) {
		return new IntArraySection( aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( Integer... aValue ) {
		return new IntArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link IntArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias ) {
		return new IntArraySection( aAlias );
	}

	/**
	 * Constructs an empty {@link IntArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, Endianess aEndianess ) {
		return new IntArraySection( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * the given {@link Endianess} for the representation of the
	 * {@link IntArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, Endianess aEndianess, int... aValue ) {
		return new IntArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * the given {@link Endianess} for the representation of the
	 * {@link IntArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, Endianess aEndianess, Integer... aValue ) {
		return new IntArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, int... aValue ) {
		return new IntArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link IntArraySection} with the given int array payload and
	 * a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, Integer... aValue ) {
		return new IntArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new IntArraySection( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, int... aValue ) {
		return new IntArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Integer... aValue ) {
		return new IntArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new IntArraySection( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( TransmissionMetrics aTransmissionMetrics, int... aValue ) {
		return new IntArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the {@link IntArraySection}.
	 * 
	 * @return The accordingly created {@link IntArraySection}.
	 */
	public static IntArraySection intArraySection( TransmissionMetrics aTransmissionMetrics, Integer... aValue ) {
		return new IntArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an empty {@link IntSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntSegment}'s value.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment() {
		return new IntSegment();
	}

	/**
	 * Constructs an empty {@link IntSegment} with the given {@link Endianess}
	 * endian representation of the {@link IntSegment}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( Endianess aEndianess ) {
		return new IntSegment( aEndianess );
	}

	/**
	 * Constructs a {@link IntSegment} with the given int payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link IntSegment}.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( Integer aValue ) {
		return new IntSegment( aValue );
	}

	/**
	 * Constructs a {@link IntSegment} with the given int payload and the given
	 * {@link Endianess} for the representation of the {@link Segment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link IntSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( Integer aValue, Endianess aEndianess ) {
		return new IntSegment( aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( Integer aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new IntSegment( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link IntSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias ) {
		return new IntSegment( aAlias );
	}

	/**
	 * Constructs an empty {@link IntSegment} with the given {@link Endianess}
	 * endian representation of the {@link IntSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias, Endianess aEndianess ) {
		return new IntSegment( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link IntSegment} with the given int payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link IntSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link IntSegment}.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias, Integer aValue ) {
		return new IntSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link IntSegment} with the given int payload and the given
	 * {@link Endianess} for the representation of the {@link Segment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link IntSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias, Integer aValue, Endianess aEndianess ) {
		return new IntSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias, Integer aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new IntSegment( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new IntSegment( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link IntSegment}.
	 */
	public static IntSegment intSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new IntSegment( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decorator having applied the
	 *        {@link BijectiveFunction} to encode and the
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aBijectiveFunction The {@link BijectiveFunction} to encode any
	 *        data being delegated.
	 * @param aInverseFunction The {@link InverseFunction} to decode any data
	 *        being delegated.
	 *
	 * @return The accordingly created {@link InvertibleSectionDecorator}.
	 */
	public static <DECORATEE extends Section> InvertibleSectionDecorator<DECORATEE> invertibleSection( DECORATEE aDecoratee, BijectiveFunction<Byte, Byte> aBijectiveFunction, InverseFunction<Byte, Byte> aInverseFunction ) {
		return new InvertibleSectionDecorator<>( aDecoratee, aBijectiveFunction, aInverseFunction );
	}

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decorator having applied the {@link Invertible}'s
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aInvertible The {@link Invertible} providing the
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 *
	 * @return The accordingly created {@link InvertibleSectionDecorator}.
	 */
	public static <DECORATEE extends Section> InvertibleSectionDecorator<DECORATEE> invertibleSection( DECORATEE aDecoratee, Invertible<Byte, Byte> aInvertible ) {
		return new InvertibleSectionDecorator<>( aDecoratee, aInvertible );
	}

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decorator having applied the
	 *        {@link BijectiveFunction} to encode and the
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aBijectiveFunction The {@link BijectiveFunction} to encode any
	 *        data being delegated.
	 * @param aInverseFunction The {@link InverseFunction} to decode any data
	 *        being delegated.
	 *
	 * @return The accordingly created {@link InvertibleSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> InvertibleSegmentDecorator<DECORATEE> invertibleSegment( DECORATEE aDecoratee, BijectiveFunction<Byte, Byte> aBijectiveFunction, InverseFunction<Byte, Byte> aInverseFunction ) {
		return new InvertibleSegmentDecorator<>( aDecoratee, aBijectiveFunction, aInverseFunction );
	}

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decorator having applied the {@link Invertible}'s
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aInvertible The {@link Invertible} providing the
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 *
	 * @return The accordingly created {@link InvertibleSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> InvertibleSegmentDecorator<DECORATEE> invertibleSegment( DECORATEE aDecoratee, Invertible<Byte, Byte> aInvertible ) {
		return new InvertibleSegmentDecorator<>( aDecoratee, aInvertible );
	}

	/**
	 * Constructs a {@link LengthSegmentDecoratorSegment} with the given
	 * decoratee and a width of {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}
	 * bytes used to specify the decoratee's length a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the decoratee's length.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aSegment The decoratee used for this decorator.
	 * 
	 * @return The accordingly created {@link LengthSegmentDecoratorSegment}.
	 */
	public static <DECORATEE extends Segment> LengthSegmentDecoratorSegment<DECORATEE> lengthSegment( DECORATEE aSegment ) {
		return new LengthSegmentDecoratorSegment<>( aSegment );
	}

	/**
	 * Constructs a {@link LengthSegmentDecoratorSegment} with the given
	 * decoratee and a width of {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}
	 * bytes used to specify the decoratee's length and the provided
	 * {@link Endianess} representation of the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aSegment The decoratee used for this decorator.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegmentDecoratorSegment}.
	 */
	public static <DECORATEE extends Segment> LengthSegmentDecoratorSegment<DECORATEE> lengthSegment( DECORATEE aSegment, Endianess aEndianess ) {
		return new LengthSegmentDecoratorSegment<>( aSegment, aEndianess );
	}

	/**
	 * Constructs a {@link LengthSegmentDecoratorSegment} with the given
	 * decoratee and with the given number of bytes used to specify the
	 * decoratee's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aSegment The decoratee used for this decorator.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegmentDecoratorSegment}.
	 */
	public static <DECORATEE extends Segment> LengthSegmentDecoratorSegment<DECORATEE> lengthSegment( DECORATEE aSegment, int aLengthWidth ) {
		return new LengthSegmentDecoratorSegment<>( aSegment, aLengthWidth );
	}

	/**
	 * Constructs a {@link LengthSegmentDecoratorSegment} with the given
	 * decoratee and with the given number of bytes used to specify the
	 * decoratee's length and the provided {@link Endianess} representation of
	 * the decoratee's length.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aSegment The decoratee used for this decorator.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegmentDecoratorSegment}.
	 */
	public static <DECORATEE extends Segment> LengthSegmentDecoratorSegment<DECORATEE> lengthSegment( DECORATEE aSegment, int aLengthWidth, Endianess aEndianess ) {
		return new LengthSegmentDecoratorSegment<>( aSegment, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link LengthSegmentDecoratorSegment} with the given
	 * decoratee and a width of {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}
	 * bytes used to specify the decoratee's length a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the decoratee's length.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aSegment The decoratee used for this decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LengthSegmentDecoratorSegment}.
	 */
	public static <DECORATEE extends Segment> LengthSegmentDecoratorSegment<DECORATEE> lengthSegment( DECORATEE aSegment, TransmissionMetrics aTransmissionMetrics ) {
		return new LengthSegmentDecoratorSegment<>( aSegment, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link LongArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection() {
		return new LongArraySection();
	}

	/**
	 * Constructs an empty {@link LongArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( Endianess aEndianess ) {
		return new LongArraySection( aEndianess );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( Endianess aEndianess, long... aValue ) {
		return new LongArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( Endianess aEndianess, Long... aValue ) {
		return new LongArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( long... aValue ) {
		return new LongArraySection( aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( Long... aValue ) {
		return new LongArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link LongArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias ) {
		return new LongArraySection( aAlias );
	}

	/**
	 * Constructs an empty {@link LongArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, Endianess aEndianess ) {
		return new LongArraySection( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, Endianess aEndianess, long... aValue ) {
		return new LongArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, Endianess aEndianess, Long... aValue ) {
		return new LongArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, long... aValue ) {
		return new LongArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, Long... aValue ) {
		return new LongArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new LongArraySection( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, long... aValue ) {
		return new LongArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Long... aValue ) {
		return new LongArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new LongArraySection( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( TransmissionMetrics aTransmissionMetrics, long... aValue ) {
		return new LongArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 * 
	 * @return The accordingly created {@link LongArraySection}.
	 */
	public static LongArraySection longArraySection( TransmissionMetrics aTransmissionMetrics, Long... aValue ) {
		return new LongArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an empty {@link LongSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment() {
		return new LongSegment();
	}

	/**
	 * Constructs an empty {@link LongSegment} with the given {@link Endianess}
	 * endian representation of the {@link LongSegment}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( Endianess aEndianess ) {
		return new LongSegment( aEndianess );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( Long aValue ) {
		return new LongSegment( aValue );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( Long aValue, Endianess aEndianess ) {
		return new LongSegment( aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new LongSegment( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link LongSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias ) {
		return new LongSegment( aAlias );
	}

	/**
	 * Constructs an empty {@link LongSegment} with the given {@link Endianess}
	 * endian representation of the {@link LongSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias, Endianess aEndianess ) {
		return new LongSegment( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias, Long aValue ) {
		return new LongSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias, Long aValue, Endianess aEndianess ) {
		return new LongSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias, Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new LongSegment( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new LongSegment( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LongSegment}.
	 */
	public static LongSegment longSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new LongSegment( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new MagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( DECORATEE aDecoratee, int aMagicBytesLength ) {
		return new MagicBytesSectionDecorator<>( aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( DECORATEE aDecoratee, String aMagicBytes ) {
		return new MagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSectionDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new MagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( String aAlias, DECORATEE aDecoratee, int aMagicBytesLength ) {
		return new MagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		return new MagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link MagicBytesSectionDecorator}.
	 */
	public static <DECORATEE extends Section> MagicBytesSectionDecorator<DECORATEE> magicBytesSection( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSectionDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Constructs a {@link MagicBytesSectionMultiplexer} containing the provided
	 * {@link Section} elements.
	 * 
	 * @param aSections The {@link Section} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSectionMultiplexer}.
	 */
	public static MagicBytesSectionMultiplexer magicBytesSectionMultiplexer( Collection<Section> aSections ) {
		return new MagicBytesSectionMultiplexer( aSections );
	}

	/**
	 * Constructs a {@link MagicBytesSectionMultiplexer} containing the provided
	 * {@link Section} elements.
	 * 
	 * @param aSections The {@link Section} elements being contained in this
	 *        instance.
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 * 
	 * @return The accordingly created {@link MagicBytesSectionMultiplexer}.
	 */
	public static MagicBytesSectionMultiplexer magicBytesSectionMultiplexer( Collection<Section> aSections, int aReadLimit ) {
		return new MagicBytesSectionMultiplexer( aSections, aReadLimit );
	}

	/**
	 * Constructs a {@link MagicBytesSectionMultiplexer} containing the provided
	 * {@link Section} elements.
	 * 
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 * @param aSections The {@link Section} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSectionMultiplexer}.
	 */
	public static MagicBytesSectionMultiplexer magicBytesSectionMultiplexer( int aReadLimit, Section... aSections ) {
		return new MagicBytesSectionMultiplexer( aReadLimit, aSections );
	}

	/**
	 * Constructs a {@link MagicBytesSectionMultiplexer} containing the provided
	 * {@link Section} elements.
	 * 
	 * @param aSections The {@link Section} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSectionMultiplexer}.
	 */
	public static MagicBytesSectionMultiplexer magicBytesSectionMultiplexer( Section... aSections ) {
		return new MagicBytesSectionMultiplexer( aSections );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes.
	 * 
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( byte... aMagicBytes ) {
		return new MagicBytesSegment( aMagicBytes );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegment( aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new MagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( DECORATEE aDecoratee, int aMagicBytesLength ) {
		return new MagicBytesSegmentDecorator<>( aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( DECORATEE aDecoratee, String aMagicBytes ) {
		return new MagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSegmentDecorator<>( aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with magic bytes of
	 * the given length.
	 *
	 * @param aMagicBytesLength The length of the magic bytes sequence..
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( int aMagicBytesLength ) {
		return new MagicBytesSegment( aMagicBytesLength );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aMagicBytes ) {
		return new MagicBytesSegment( aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, byte... aMagicBytes ) {
		return new MagicBytesSegment( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegment( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSegment( aMagicBytes, aCharset );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		return new MagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( String aAlias, DECORATEE aDecoratee, int aMagicBytesLength ) {
		return new MagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		return new MagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 *
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSegmentDecorator<>( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> MagicBytesSegmentDecorator<DECORATEE> magicBytesSegment( String aAlias, DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegmentDecorator<>( aAlias, aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with magic bytes of
	 * the given length.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytesLength The length of the magic bytes sequence..
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, int aMagicBytesLength ) {
		return new MagicBytesSegment( aAlias, aMagicBytesLength );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, String aMagicBytes ) {
		return new MagicBytesSegment( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, String aMagicBytes, Charset aCharset ) {
		return new MagicBytesSegment( aAlias, aMagicBytes, aCharset );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( String aAlias, String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegment( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new MagicBytesSegment( aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * 
	 * @return The accordingly created {@link MagicBytesSegment}.
	 */
	public static MagicBytesSegment magicBytesSegment( TransmissionMetrics aTransmissionMetrics, String aMagicBytes ) {
		return new MagicBytesSegment( aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link MagicBytesSegmentMultiplexer} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param aSegments The {@link Segment} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentMultiplexer}.
	 */
	public static MagicBytesSegmentMultiplexer magicBytesSegmentMultiplexer( Collection<Segment> aSegments ) {
		return new MagicBytesSegmentMultiplexer( aSegments );
	}

	/**
	 * Constructs a {@link MagicBytesSegmentMultiplexer} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param aSegments The {@link Segment} elements being contained in this
	 *        instance.
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentMultiplexer}.
	 */
	public static MagicBytesSegmentMultiplexer magicBytesSegmentMultiplexer( Collection<Segment> aSegments, int aReadLimit ) {
		return new MagicBytesSegmentMultiplexer( aSegments, aReadLimit );
	}

	/**
	 * Constructs a {@link MagicBytesSegmentMultiplexer} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 * @param aSegments The {@link Segment} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentMultiplexer}.
	 */
	public static MagicBytesSegmentMultiplexer magicBytesSegmentMultiplexer( int aReadLimit, Segment... aSegments ) {
		return new MagicBytesSegmentMultiplexer( aReadLimit, aSegments );
	}

	/**
	 * Constructs a {@link MagicBytesSegmentMultiplexer} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param aSegments The {@link Segment} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link MagicBytesSegmentMultiplexer}.
	 */
	public static MagicBytesSegmentMultiplexer magicBytesSegmentMultiplexer( Segment... aSegments ) {
		return new MagicBytesSegmentMultiplexer( aSegments );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 *
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth ) {
		return new NumberSegment( aNumberWidth );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with the given
	 * {@link Endianess}.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, Endianess aEndianess ) {
		return new NumberSegment( aNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The payload to be contained by the {@link NumberSegment}.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, Long aValue ) {
		return new NumberSegment( aNumberWidth, aValue );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link NumberSegment}'s value (payload).
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, Long aValue, Endianess aEndianess ) {
		return new NumberSegment( aNumberWidth, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new NumberSegment( aNumberWidth, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias ) {
		return new NumberSegment( aAlias, aNumberWidth, 0L );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with the given
	 * {@link Endianess}.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias, Endianess aEndianess ) {
		return new NumberSegment( aAlias, aNumberWidth, 0L, aEndianess );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the {@link NumberSegment}.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias, Long aValue ) {
		return new NumberSegment( aAlias, aNumberWidth, aValue );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link NumberSegment}'s value (payload).
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias, Long aValue, Endianess aEndianess ) {
		return new NumberSegment( aAlias, aNumberWidth, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias, Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new NumberSegment( aAlias, aNumberWidth, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new NumberSegment( aAlias, aNumberWidth, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link NumberSegment}.
	 */
	public static NumberSegment numberSegment( int aNumberWidth, TransmissionMetrics aTransmissionMetrics ) {
		return new NumberSegment( aNumberWidth, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link PropertiesSection} using UTF-8 encoding for
	 * the payload.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection() {
		return new PropertiesSection();
	}

	/**
	 * Constructs a {@link PropertiesSection} with a payload expected to be
	 * encoded with the given {@link Charset}.
	 * 
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( Charset aCharset ) {
		return new PropertiesSection( aCharset );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being UTF-8 encoded. The properties from the payload are copied
	 * into the {@link PropertiesSection}.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( Properties aPayload ) {
		return new PropertiesSection( aPayload );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being encoded with the given {@link Charset}. The properties from
	 * the payload are copied into the {@link PropertiesSection}.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( Properties aPayload, Charset aCharset ) {
		return new PropertiesSection( aPayload, aCharset );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being encoded with the given {@link Charset}. The properties from
	 * the payload are copied into the {@link PropertiesSection}. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( Properties aPayload, TransmissionMetrics aTransmissionMetrics ) {
		return new PropertiesSection( aPayload, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link PropertiesSection} using UTF-8 encoding for
	 * the payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( String aAlias ) {
		return new PropertiesSection( aAlias );
	}

	/**
	 * Constructs a {@link PropertiesSection} with a payload expected to be
	 * encoded with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( String aAlias, Charset aCharset ) {
		return new PropertiesSection( aAlias, aCharset );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being UTF-8 encoded. The properties from the payload are copied
	 * into the {@link PropertiesSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( String aAlias, Properties aPayload ) {
		return new PropertiesSection( aAlias, aPayload );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being encoded with the given {@link Charset}. The properties from
	 * the payload are copied into the {@link PropertiesSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( String aAlias, Properties aPayload, Charset aCharset ) {
		return new PropertiesSection( aAlias, aPayload, aCharset );
	}

	/**
	 * Constructs a {@link PropertiesSection} with the given {@link Properties}
	 * payload being encoded with the given {@link Charset}. The properties from
	 * the payload are copied into the {@link PropertiesSection}. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link PropertiesSection}.
	 */
	public static PropertiesSection propertiesSection( String aAlias, Properties aPayload, TransmissionMetrics aTransmissionMetrics ) {
		return new PropertiesSection( aAlias, aPayload, aTransmissionMetrics );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        an RTR request (transmitter) from the receiver.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aCtsTimeoutInMs, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToReceiveSectionDecorator<DECORATEE> readyToReceiveSection( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new ReadyToReceiveSectionDecorator<>( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToReceiveMagicBytes(), aTransmissionMetrics.getReadyToReceiveRetryNumber(), aTransmissionMetrics.getReadyToReceiveTimeoutMillis(), aTransmissionMetrics.getReadyToReceiveSegmentPackager() );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR magic bytes to be used to signal
	 *        a RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, long aReadyToReceiveTimeoutInMs ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aCtsTimeoutInMs The timeout in milliseconds to wait for an RTR
	 *        request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aCtsTimeoutInMs, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aCtsTimeoutInMs, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-send") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ReadyToReceiveSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToReceiveSegmentDecorator<DECORATEE> readyToReceiveSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new ReadyToReceiveSegmentDecorator<>( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToReceiveMagicBytes(), aTransmissionMetrics.getReadyToReceiveRetryNumber(), aTransmissionMetrics.getReadyToReceiveTimeoutMillis(), aTransmissionMetrics.getReadyToReceiveSegmentPackager() );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSectionPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        no timeout (blocking mode).
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        CTS response (transmitter) from the receiver after signaling a RTS
	 *        ("ready-to-send") to the receiver.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSectionPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSectionPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendSectionPackager, aClearToSendMagicBytes, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSectionPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSectionPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSectionPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSectionPackager ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aReadyToSendTimeoutInMs, aReadyToSendSectionPackager, aClearToSendTimeoutInMs, aClearToSendSectionPackager );
	}

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ReadyToSendSectionDecorator}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator<DECORATEE> readyToSendSection( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new ReadyToSendSectionDecorator<>( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToSendMagicBytes(), aTransmissionMetrics.getReadyToSendRetryNumber(), aTransmissionMetrics.getReadyToSendTimeoutMillis(), aTransmissionMetrics.getReadyToSendSegmentPackager(), aTransmissionMetrics.getClearToSendMagicBytes(), aTransmissionMetrics.getClearToSendTimeoutMillis(), aTransmissionMetrics.getClearToSendSegmentPackager() );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link ReadyToSendSectionDecorator} instance.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 *
	 * @return The accordingly created
	 *         {@link ReadyToSendSectionDecorator.Builder}.
	 */
	public static <DECORATEE extends Section> ReadyToSendSectionDecorator.Builder<DECORATEE> readyToSendSectionBuilder() {
		return ReadyToSendSectionDecorator.builder();
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendMagicBytes, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        no timeout (blocking mode).
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        CTS response (transmitter) from the receiver after signaling a RTS
	 *        ("ready-to-send") to the receiver.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );

	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ReadyToSendSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator<DECORATEE> readyToSendSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new ReadyToSendSegmentDecorator<>( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToSendMagicBytes(), aTransmissionMetrics.getReadyToSendRetryNumber(), aTransmissionMetrics.getReadyToSendTimeoutMillis(), aTransmissionMetrics.getReadyToSendSegmentPackager(), aTransmissionMetrics.getClearToSendMagicBytes(), aTransmissionMetrics.getClearToSendTimeoutMillis(), aTransmissionMetrics.getClearToSendSegmentPackager() );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link ReadyToSendSegmentDecorator} instance.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be wrapped by the according decorator.
	 *
	 * @return The accordingly created
	 *         {@link ReadyToSendSegmentDecorator.Builder}.
	 */
	public static <DECORATEE extends Segment> ReadyToSendSegmentDecorator.Builder<DECORATEE> readyToSendSegmentBuilder() {
		return ReadyToSendSegmentDecorator.builder();
	}

	/**
	 * Constructs a {@link SegmentComposite} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param <CHILD> The {@link Segment} type describing the composite's
	 *        {@link Segment} children.
	 * @param aChildren The {@link Segment} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link SegmentComposite}.
	 */
	@SafeVarargs
	public static <CHILD extends Segment> SegmentComposite<CHILD> segmentComposite( CHILD... aChildren ) {
		return new SegmentComposite<>( aChildren );
	}

	/**
	 * Constructs a {@link SegmentComposite} containing the provided
	 * {@link Segment} elements.
	 * 
	 * @param <CHILD> The {@link Segment} type describing the composite's
	 *        {@link Segment} children.
	 * @param aChildren The {@link Segment} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link SegmentComposite}.
	 */
	public static <CHILD extends Segment> SegmentComposite<CHILD> segmentComposite( Collection<CHILD> aChildren ) {
		return new SegmentComposite<>( aChildren );
	}

	/**
	 * Constructs an empty allocation value with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the referencee's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the referencee's length.
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength() {
		return new LengthSegment<>();
	}

	/**
	 * Constructs an empty allocation value with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the referencee's length and the provided {@link Endianess} representation
	 * of the referencee's length.
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( Endianess aEndianess ) {
		return new LengthSegment<>( aEndianess );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the referencee's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the referencee's length.
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( int aLengthWidth ) {
		return new LengthSegment<>( aLengthWidth );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the referencee's length and the provided {@link Endianess}
	 * representation of the referencee's length.
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( int aLengthWidth, Endianess aEndianess ) {
		return new LengthSegment<>( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the allocation value with the given referencee and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the referencee's length a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the referencee's length.
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aReferencee The referencee used for this allocation value.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( REFERENCEE aReferencee ) {
		return new LengthSegment<>( aReferencee );
	}

	/**
	 * Constructs the allocation value with the given referencee and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the referencee's length and the provided {@link Endianess} representation
	 * of the referencee's length.
	 *
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aReferencee The referencee used for this allocation value.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( REFERENCEE aReferencee, Endianess aEndianess ) {
		return new LengthSegment<>( aReferencee, aEndianess );
	}

	/**
	 * Constructs the allocation value with the given referencee and with the
	 * given number of bytes used to specify the referencee's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the referencee's length.
	 *
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aReferencee The referencee used for this allocation value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( REFERENCEE aReferencee, int aLengthWidth ) {
		return new LengthSegment<>( aReferencee, aLengthWidth );
	}

	/**
	 * Constructs the allocation value with the given referencee and with the
	 * given number of bytes used to specify the referencee's length and the
	 * provided {@link Endianess} representation of the referencee's length.
	 *
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aReferencee The referencee used for this allocation value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( REFERENCEE aReferencee, int aLengthWidth, Endianess aEndianess ) {
		return new LengthSegment<>( aReferencee, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs an according instance. The configuration attributes are taken
	 * from the {@link TransmissionMetrics} configuration object, though only
	 * those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aReferencee The referencee used for this allocation value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( REFERENCEE aReferencee, TransmissionMetrics aTransmissionMetrics ) {
		return new LengthSegment<>( aReferencee, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance. The configuration attributes are taken
	 * from the {@link TransmissionMetrics} configuration object, though only
	 * those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <REFERENCEE> The type of the {@link Segment} referencee.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link LengthSegment}.
	 */
	public static <REFERENCEE extends Transmission> LengthSegment<REFERENCEE> segmentLength( TransmissionMetrics aTransmissionMetrics ) {
		return new LengthSegment<>( aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( int aSequenceNumberWidth, Endianess aEndianess ) {
		return new SequenceNumberSegment( aSequenceNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( int aSequenceNumberWidth, Long aSequenceNumberInitValue, Endianess aEndianess ) {
		return new SequenceNumberSegment( aSequenceNumberWidth, aSequenceNumberInitValue, aEndianess );
	}

	/**
	 * Constructs an empty {@link SequenceNumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link SequenceNumberSegment}'s value.
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link SequenceNumberSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 *
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new SequenceNumberSegment( aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( String aAlias, int aSequenceNumberWidth, Endianess aEndianess ) {
		return new SequenceNumberSegment( aAlias, aSequenceNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( String aAlias, int aSequenceNumberWidth, Long aSequenceNumberInitValue, Endianess aEndianess ) {
		return new SequenceNumberSegment( aAlias, aSequenceNumberWidth, aSequenceNumberInitValue, aEndianess );
	}

	/**
	 * Constructs an empty {@link SequenceNumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link SequenceNumberSegment}'s value.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link SequenceNumberSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 *
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( String aAlias, long aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new SequenceNumberSegment( aAlias, aValue, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link SequenceNumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link SequenceNumberSegment}'s value.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 *
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new SequenceNumberSegment( aAlias, aTransmissionMetrics );
	}

	/**
	 * Constructs an empty {@link SequenceNumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link SequenceNumberSegment}'s value.
	 *
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 *
	 * @return The accordingly created {@link SequenceNumberSegment}.
	 */
	public static SequenceNumberSegment sequenceNumberSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new SequenceNumberSegment( aTransmissionMetrics );
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection() {
		return new SequenceSection();
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 * 
	 * @param aSequenceSize The size of the {@link Sequence}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection( int aSequenceSize ) {
		return new SequenceSection( aSequenceSize );
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 * 
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link SequenceSection}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection( Sequence aSequence ) {
		return new SequenceSection( aSequence );
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Section}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection( String aAlias ) {
		return new SequenceSection( aAlias );
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequenceSize The size of the {@link Sequence}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection( String aAlias, int aSequenceSize ) {
		return new SequenceSection( aAlias, aSequenceSize );
	}

	/**
	 * Constructs a {@link SequenceSection}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link SequenceSection}.
	 *
	 * @return The accordingly created {@link SequenceSection}.
	 */
	public static SequenceSection sequenceSection( String aAlias, Sequence aSequence ) {
		return new SequenceSection( aAlias, aSequence );
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection() {
		return new ShortArraySection();
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( Endianess aEndianess ) {
		return new ShortArraySection( aEndianess );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( Endianess aEndianess, short... aValue ) {
		return new ShortArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( Endianess aEndianess, Short... aValue ) {
		return new ShortArraySection( aEndianess, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( short... aValue ) {
		return new ShortArraySection( aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( Short... aValue ) {
		return new ShortArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias ) {
		return new ShortArraySection( aAlias );
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, Endianess aEndianess ) {
		return new ShortArraySection( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, Endianess aEndianess, short... aValue ) {
		return new ShortArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, Endianess aEndianess, Short... aValue ) {
		return new ShortArraySection( aAlias, aEndianess, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, short... aValue ) {
		return new ShortArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, Short... aValue ) {
		return new ShortArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new ShortArraySection( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, short... aValue ) {
		return new ShortArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Short... aValue ) {
		return new ShortArraySection( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new ShortArraySection( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( TransmissionMetrics aTransmissionMetrics, short... aValue ) {
		return new ShortArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 * 
	 * @return The accordingly created {@link ShortArraySection}.
	 */
	public static ShortArraySection shortArraySection( TransmissionMetrics aTransmissionMetrics, Short... aValue ) {
		return new ShortArraySection( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an empty {@link ShortSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment() {
		return new ShortSegment();
	}

	/**
	 * Constructs an empty {@link ShortSegment} with the given {@link Endianess}
	 * endian representation of the {@link ShortSegment}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( Endianess aEndianess ) {
		return new ShortSegment( aEndianess );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( Short aValue ) {
		return new ShortSegment( aValue );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( Short aValue, Endianess aEndianess ) {
		return new ShortSegment( aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( Short aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new ShortSegment( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an empty {@link ShortSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias ) {
		return new ShortSegment( aAlias );
	}

	/**
	 * Constructs an empty {@link ShortSegment} with the given {@link Endianess}
	 * endian representation of the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias, Endianess aEndianess ) {
		return new ShortSegment( aAlias, aEndianess );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias, Short aValue ) {
		return new ShortSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias, Short aValue, Endianess aEndianess ) {
		return new ShortSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias, Short aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new ShortSegment( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new ShortSegment( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link ShortSegment}.
	 */
	public static ShortSegment shortSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new ShortSegment( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be chunked into blocks and enriched with a CRC checksum and a
	 *        sequence number.
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aBlockSize The block size of a data block for each packet.
	 * @param aPacketLengthWidth The width (bytes) for declaring the (max)
	 *        length of a package.
	 * @param aPacketMagicBytes The magic bytes identifying a packet and
	 *        distinguishing a packet from a last package.
	 * @param aLastPacketMagicBytes the a last packet magic bytes
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aPacketSegmentPackager An (optional) {@link SegmentPackager} used
	 *        to modify a packet's data e.g. with a CRC checksum.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager An (optional) {@link SegmentPackager} used to
	 *        modify a ACK response data e.g. with a CRC checksum.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created
	 *         {@link StopAndWaitPacketStreamSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitPacketStreamSectionDecorator<DECORATEE> stopAndWaitPacketStreamSection( DECORATEE aDecoratee, int aBlockSize, int aPacketLengthWidth, byte[] aPacketMagicBytes, byte[] aLastPacketMagicBytes, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, SegmentPackager aPacketSegmentPackager, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitPacketStreamSectionDecorator<>( aDecoratee, aBlockSize, aPacketLengthWidth, aPacketMagicBytes, aLastPacketMagicBytes, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aPacketSegmentPackager, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}. The configuration attributes are taken from
	 * the {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be chunked into blocks and enriched with a CRC checksum and a
	 *        sequence number.
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created
	 *         {@link StopAndWaitPacketStreamSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitPacketStreamSectionDecorator<DECORATEE> stopAndWaitPacketStreamSection( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new StopAndWaitPacketStreamSectionDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link StopAndWaitPacketStreamSectionDecorator} instance.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The {@link StopAndWaitPacketStreamSectionDecorator.Builder} to
	 *         build a {@link StopAndWaitPacketStreamSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitPacketStreamSectionDecorator.Builder<DECORATEE> stopAndWaitPacketStreamSectionBuilder() {
		return StopAndWaitPacketStreamSectionDecorator.builder();
	}

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be chunked into blocks and enriched with a CRC checksum and a
	 *        sequence number.
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aBlockSize The block size of a data block for each packet.
	 * @param aPacketLengthWidth The width (bytes) for declaring the (max)
	 *        length of a package.
	 * @param aPacketMagicBytes The magic bytes identifying a packet and
	 *        distinguishing a packet from a last package.
	 * @param aLastPacketMagicBytes the a last packet magic bytes
	 * @param aSequenceNumberInitValue the a sequence number init value
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aPacketSegmentPackager An (optional) {@link SegmentPackager} used
	 *        to modify a packet's data e.g. with a CRC checksum.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager An (optional) {@link SegmentPackager} used to
	 *        modify a ACK response data e.g. with a CRC checksum.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created
	 *         {@link StopAndWaitPacketStreamSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitPacketStreamSegmentDecorator<DECORATEE> stopAndWaitPacketStreamSegment( DECORATEE aDecoratee, int aBlockSize, int aPacketLengthWidth, byte[] aPacketMagicBytes, byte[] aLastPacketMagicBytes, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, SegmentPackager aPacketSegmentPackager, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitPacketStreamSegmentDecorator<>( aDecoratee, aBlockSize, aPacketLengthWidth, aPacketMagicBytes, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aPacketSegmentPackager, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according packet-stream decorator instance wrapping the
	 * given {@link OutputStream}. The configuration attributes are taken from
	 * the {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be chunked into blocks and enriched with a CRC checksum and a
	 *        sequence number.
	 * @param aDecoratee The decoratee to be wrapped by the packet-stream
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created
	 *         {@link StopAndWaitPacketStreamSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitPacketStreamSegmentDecorator<DECORATEE> stopAndWaitPacketStreamSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new StopAndWaitPacketStreamSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link StopAndWaitPacketStreamSegmentDecorator} instance.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The {@link StopAndWaitPacketStreamSegmentDecorator.Builder} to
	 *         build a {@link StopAndWaitPacketStreamSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitPacketStreamSegmentDecorator.Builder<DECORATEE> stopAndWaitPacketStreamSegmentBuilder() {
		return StopAndWaitPacketStreamSegmentDecorator.builder();
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, (SegmentPackager) null );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcChecksumConcatenateMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAckSegmentPackager the ack segment packager
	 * 
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, SegmentPackager aAckSegmentPackager ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aAckSegmentPackager );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The {@link Section} type describing the
	 *        {@link Section} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator<DECORATEE> stopAndWaitSection( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new StopAndWaitSectionDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link StopAndWaitSectionDecorator} instance.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The {@link StopAndWaitSectionDecorator.Builder} to build a
	 *         {@link StopAndWaitSectionDecorator}. The accordingly created
	 *         {@link StopAndWaitSectionDecorator}.
	 */
	public static <DECORATEE extends Section> StopAndWaitSectionDecorator.Builder<DECORATEE> stopAndWaitSectionBuilder() {
		return StopAndWaitSectionDecorator.builder();
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, (SegmentPackager) null );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcChecksumConcatenateMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode ) );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager the ack segment packager
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAckSegmentPackager the ack segment packager
	 * 
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, SegmentPackager aAckSegmentPackager ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aAckSegmentPackager );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param <DECORATEE> The {@link Segment} type describing the
	 *        {@link Segment} subclass decoratee.
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 *
	 * @return The accordingly created {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator<DECORATEE> stopAndWaitSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		return new StopAndWaitSegmentDecorator<>( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Constructs a builder to build an according
	 * {@link StopAndWaitSegmentDecorator} instance.
	 *
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The {@link StopAndWaitSegmentDecorator.Builder} to build a
	 *         {@link StopAndWaitSegmentDecorator}. The accordingly created
	 *         {@link StopAndWaitSegmentDecorator}.
	 */
	public static <DECORATEE extends Segment> StopAndWaitSegmentDecorator.Builder<DECORATEE> stopAndWaitSegmentBuilder() {
		return StopAndWaitSegmentDecorator.builder();
	}

	/**
	 * Constructs an empty {@link StringArraySection} using the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection() {
		return new StringArraySection();
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, as well
	 * as the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 * 
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( Charset aCharset, String... aValue ) {
		return new StringArraySection( aValue );
	}

	/**
	 * Constructs a {@link StringArraySection} using containing the provided
	 * {@link StringSection} elements and using the provided {@link Segment}
	 * class for creating {@link Segment} instances.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for size values.
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aValue ) {
		return new StringArraySection( aLengthWidth, aEndianess, aCharset, aValue );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String... aValue ) {
		return new StringArraySection( aValue );
	}

	/**
	 * Constructs an empty {@link StringArraySection} using the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 *
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias ) {
		return new StringArraySection( aAlias );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, as well
	 * as the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias, Charset aCharset, String... aValue ) {
		return new StringArraySection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link StringArraySection} using containing the provided
	 * {@link StringSection} elements and using the provided {@link Segment}
	 * class for creating {@link Segment} instances.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for size values.
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aValue ) {
		return new StringArraySection( aAlias, aLengthWidth, aEndianess, aCharset, aValue );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The {@link String} elements being contained in this
	 *        instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias, String[] aValue ) {
		return new StringArraySection( aAlias, aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		return new StringArraySection( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link StringArraySection}.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		return new StringArraySection( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( TransmissionMetrics aTransmissionMetrics ) {
		return new StringArraySection( aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link StringArraySection}.
	 * 
	 * @return The accordingly created {@link StringArraySection}.
	 */
	public static StringArraySection stringArraySection( TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		return new StringArraySection( aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an empty {@link StringSection} using the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING} encoding for the payload.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection() {
		return new StringSection();
	}

	/**
	 * Constructs a {@link StringSection} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 * 
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( Charset aCharset ) {
		return new StringSection( aCharset );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aValue ) {
		return new StringSection( aValue );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aValue, Charset aCharset ) {
		return new StringSection( aValue, aCharset );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aAlias, String aValue ) {
		return new StringSection( aAlias, aValue );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aAlias, String aValue, Charset aCharset ) {
		return new StringSection( aAlias, aValue, aCharset );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link StringSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aAlias, String aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new StringSection( aAlias, aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link StringSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( String aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new StringSection( aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSection}.
	 */
	public static StringSection stringSection( TransmissionMetrics aTransmissionMetrics ) {
		return new StringSection( aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs a {@link StringSegment}.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment() {
		return new StringSegment();
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 * 
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( Charset aEncoding ) {
		return new StringSegment( aEncoding );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( Charset aEncoding, Endianess aEndianess ) {
		return new StringSegment( aEncoding, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( Charset aEncoding, int aLengthWidth ) {
		return new StringSegment( aEncoding, aLengthWidth );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aEncoding, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs an empty allocation value with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the decoratee's length and the provided {@link Endianess} representation
	 * of the decoratee's length.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( Endianess aEndianess ) {
		return new StringSegment( aEndianess );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the decoratee's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the decoratee's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( int aLengthWidth ) {
		return new StringSegment( aLengthWidth );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the decoratee's length and the provided {@link Endianess}
	 * representation of the decoratee's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue ) {
		return new StringSegment( aValue );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, Charset aEncoding ) {
		return new StringSegment( aValue, aEncoding );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, Charset aEncoding, Endianess aEndianess ) {
		return new StringSegment( aValue, aEncoding, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, Charset aEncoding, int aLengthWidth ) {
		return new StringSegment( aValue, aEncoding, aLengthWidth );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aValue, aEncoding, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, Endianess aEndianess ) {
		return new StringSegment( aValue, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String}payload*
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.**
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */

	public static StringSegment stringSegment( String aValue, int aLengthWidth ) {
		return new StringSegment( aValue, aLengthWidth );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue ) {
		return new StringSegment( aAlias, aValue );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, Charset aEncoding ) {
		return new StringSegment( aAlias, aValue, aEncoding );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, Charset aEncoding, Endianess aEndianess ) {
		return new StringSegment( aAlias, aValue, aEncoding, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, Charset aEncoding, int aLengthWidth ) {
		return new StringSegment( aAlias, aValue, aEncoding, aLengthWidth );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aAlias, aValue, aEncoding, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, Endianess aEndianess ) {
		return new StringSegment( aAlias, aValue, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, int aLengthWidth ) {
		return new StringSegment( aAlias, aValue, aLengthWidth );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, int aLengthWidth, Endianess aEndianess ) {
		return new StringSegment( aAlias, aValue, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link StringSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aAlias, String aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new StringSegment( aAlias, aValue, aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link StringSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( String aValue, TransmissionMetrics aTransmissionMetrics ) {
		return new StringSegment( aValue, aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * 
	 * @return The accordingly created {@link StringSegment}.
	 */
	public static StringSegment stringSegment( TransmissionMetrics aTransmissionMetrics ) {
		return new StringSegment( aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a builder to build an according {@link StringSegment}
	 * instance.
	 * 
	 * @return The accordingly created {@link StringSegment.Builder}.
	 */
	public static StringSegment.Builder stringSegmentBuilder() {
		return StringSegment.builder();
	}
}
