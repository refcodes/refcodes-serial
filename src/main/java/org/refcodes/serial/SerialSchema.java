// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.CommentAccessor;
import org.refcodes.mixin.DescriptionAccessor;
import org.refcodes.mixin.LengthAccessor;
import org.refcodes.mixin.ValueAccessor;
import org.refcodes.schema.Schema;

/**
 * The purpose of a {@link SerialSchema} is to automatically outline and
 * generate documentation of serial data (structures).
 */
public class SerialSchema extends Schema implements LengthAccessor, DescriptionAccessor, ValueAccessor<Sequence>, CommentAccessor {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String LENGTH = "LENGTH";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public SerialSchema( Class<?> aType, String aDescription, Schema... aChildren ) {
		super( aType, aDescription, aChildren );
		put( LENGTH, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	public SerialSchema( String aAlias, Class<?> aType, String aDescription, int aLength ) {
		super( aAlias, aType, aDescription );
		put( LENGTH, aLength );
	}

	/**
	 * Constructs a {@link SerialSchema} with the provided arguments.
	 * 
	 * @param aType The type providing the this {@link SerialSchema} instance.
	 * @param aValue The content of the {@link SerialSchema} providing type.
	 * @param aVerbose The verbose representation of the value.
	 * @param aDescription The description of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aLength The length of the {@link SerialSchema} providing type.
	 */
	public SerialSchema( Class<?> aType, Sequence aValue, String aVerbose, String aDescription, int aLength ) {
		super( aType, aDescription );
		put( VALUE, aValue );
		put( VERBOSE, aVerbose );
		put( LENGTH, aLength );
	}

	/**
	 * Constructs a {@link SerialSchema} with the provided arguments.
	 * 
	 * @param aType The type providing the this {@link SerialSchema} instance.
	 * @param aValue The content of the {@link SerialSchema} providing type.
	 * @param aLength The length of the {@link SerialSchema} providing type.
	 * @param aDescription The description of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aChildren The children's {@link SerialSchema} descriptions
	 *        representing part of the {@link SerialSchema} providing type.
	 */
	public SerialSchema( Class<?> aType, Sequence aValue, int aLength, String aDescription, Schema... aChildren ) {
		super( aType, aDescription, aChildren );
		put( VALUE, aValue );
		put( LENGTH, aLength );
	}

	/**
	 * Constructs a {@link SerialSchema} with the provided arguments.
	 * 
	 * @param aAlias The alias (name) of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aType The type providing the this {@link SerialSchema} instance.
	 * @param aValue The content of the {@link SerialSchema} providing type.
	 * @param aLength The length of the {@link SerialSchema} providing type.
	 * @param aDescription The description of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aChildren The children's {@link SerialSchema} descriptions
	 *        representing part of the {@link SerialSchema} providing type.
	 */
	public SerialSchema( String aAlias, Class<?> aType, Sequence aValue, int aLength, String aDescription, Schema... aChildren ) {
		super( aAlias, aType, aDescription, aChildren );
		put( VALUE, aValue );
		put( LENGTH, aLength );
	}

	/**
	 * Constructs a {@link SerialSchema} with the provided arguments.
	 * 
	 * @param aAlias The alias (name) of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aType The type providing the this {@link SerialSchema} instance.
	 * @param aValue The content of the {@link SerialSchema} providing type.
	 * @param aLength The length of the {@link SerialSchema} providing type.
	 * @param aVerbose The verbose representation of the value.
	 * @param aDescription The description of the schema described by the
	 *        {@link SerialSchema} providing type.
	 * @param aChildren The children's {@link SerialSchema} descriptions
	 *        representing part of the {@link SerialSchema} providing type.
	 */
	public SerialSchema( String aAlias, Class<?> aType, Sequence aValue, int aLength, String aVerbose, String aDescription, Schema... aChildren ) {
		super( aAlias, aType, aDescription, aChildren );
		put( VALUE, aValue );
		put( VERBOSE, aVerbose );
		put( LENGTH, aLength );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		final Object theLength = get( LENGTH );
		return theLength != null ? (Integer) theLength : -1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence getValue() {
		return (Sequence) get( VALUE );
	}

	/**
	 * Retrieves the verbose representation of the value.
	 * 
	 * @return The human readable value representation.
	 */
	@Override
	public String getComment() {
		return (String) get( VERBOSE );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}