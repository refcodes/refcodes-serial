// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.ValueAccessor.ValueBuilder;
import org.refcodes.mixin.ValueAccessor.ValueProperty;
import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link NumberSegment} is an implementation of a {@link Segment} carrying
 * an integer value with a configurable width (in bytes) as payload.
 */
public class NumberSegment extends AbstractPayloadSegment<Long> implements Segment, ValueProperty<Long>, ValueBuilder<Long, NumberSegment> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Endianess _endianess;
	private int _numberWidth;;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public NumberSegment( int aNumberWidth, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public NumberSegment( int aNumberWidth, Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth, aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public NumberSegment( String aAlias, int aNumberWidth, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aNumberWidth, aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link NumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 */
	public NumberSegment( int aNumberWidth ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with the given
	 * {@link Endianess}.
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public NumberSegment( int aNumberWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 * 
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The payload to be contained by the {@link NumberSegment}.
	 */
	public NumberSegment( int aNumberWidth, Long aValue ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth, aValue );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link NumberSegment}'s value (payload).
	 *
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public NumberSegment( int aNumberWidth, Long aValue, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( NumberSegment.class.getSimpleName() ), aNumberWidth, aValue, aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link NumberSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 */
	public NumberSegment( String aAlias, int aNumberWidth ) {
		this( aAlias, aNumberWidth, 0L, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an empty {@link NumberSegment} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public NumberSegment( String aAlias, int aNumberWidth, Endianess aEndianess ) {
		this( aAlias, aNumberWidth, 0L, aEndianess );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link NumberSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The payload to be contained by the {@link NumberSegment}.
	 */
	public NumberSegment( String aAlias, int aNumberWidth, Long aValue ) {
		this( aAlias, aNumberWidth, aValue, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link NumberSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link NumberSegment}'s value (payload).
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aNumberWidth The number of bytes to be occupied by the number.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public NumberSegment( String aAlias, int aNumberWidth, Long aValue, Endianess aEndianess ) {
		super( aAlias, aValue );
		_endianess = aEndianess;
		_numberWidth = aNumberWidth;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _endianess.toBytes( getPayload(), _numberWidth ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, _numberWidth );
		setPayload( _endianess.toLong( theRecord ) );
		return aOffset + _numberWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _numberWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = 0L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), Long.toString( getPayload() ), "A body containing an long payload." );
		theSchema.put( ENDIANESS, _endianess );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NumberSegment withPayload( Long aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getValue() {
		return _payload;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue( Long aValue ) {
		_payload = aValue;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NumberSegment withValue( Long aValue ) {
		setValue( aValue );
		return this;
	}
}
