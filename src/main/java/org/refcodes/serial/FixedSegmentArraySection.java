// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.lang.reflect.Array;
import java.util.Collection;

import org.refcodes.factory.TypeFactory;

/**
 * A {@link FixedSegmentArraySection} is a {@link Section} consisting of
 * {@link Segment} elements with each element of the same (uniform) length (in
 * contrast to the {@link SegmentArraySection}). When using the
 * {@link FixedSegmentArraySection}, it is to be taken care of the uniform
 * {@link Segment} elements' lengths.
 * 
 * @param <ARRAY> The type of the array elements to be contained in this
 *        instance.
 */
public class FixedSegmentArraySection<ARRAY extends Segment> extends AbstractArrayTransmission<ARRAY> implements ArraySection<ARRAY> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( Class<ARRAY> aSegmentClass ) {
		super( aSegmentClass );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( Collection<ARRAY> aSegments ) {
		super( aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public FixedSegmentArraySection( ARRAY... aSegments ) {
		super( aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( String aAlias, Class<ARRAY> aSegmentClass ) {
		super( aAlias, aSegmentClass );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( String aAlias, Collection<ARRAY> aSegments ) {
		super( aAlias, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public FixedSegmentArraySection( String aAlias, ARRAY... aSegments ) {
		super( aAlias, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		super( aAlias, aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public FixedSegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		super( aAlias, aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( String aAlias, TypeFactory<ARRAY> aSegmentFactory ) {
		super( aAlias, aSegmentFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		super( aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public FixedSegmentArraySection( TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		super( aSegmentFactory, aSegments );
	}

	/**
	 * {@inheritDoc}
	 */
	public FixedSegmentArraySection( TypeFactory<ARRAY> aSegmentFactory ) {
		super( aSegmentFactory );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FixedSegmentArraySection<ARRAY> withArray( ARRAY[] aValue ) {
		setArray( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		if ( aLength != 0 ) {
			final ARRAY theFirst = _sequenceableFactory.create();
			final int theFixedLength = theFirst.getLength();
			if ( aLength % theFixedLength != 0 ) {
				throw new TransmissionSequenceException( aSequence, "The length dedicated <" + aLength + "> does not match a multiple element's length <" + theFixedLength + "> for retrieving an array from the given sequence at the offset <" + aOffset + ">!" );
			}
			final int theLength = aLength / theFixedLength;
			if ( theLength != 0 ) {
				_array = (ARRAY[]) Array.newInstance( theFirst.getClass(), theLength );
				_array[0] = theFirst;
				_array[0].fromTransmission( aSequence, aOffset + 0 * theFixedLength );
				for ( int i = 1; i < _array.length; i++ ) {
					_array[i] = _sequenceableFactory.create();
					_array[i].fromTransmission( aSequence, aOffset + i * theFixedLength );
				}
			}
		}
	}
}
