// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;

import org.refcodes.numerical.NumericalUtility;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link BooleanArraySection} is an implementation of a {@link Section}
 * carrying a boolean array as payload. As the boolean array is mapped as a bit
 * field (and the boolean array's length is not necessarily a multiple of 8)
 * into a byte array, we prefix the modulo 8 value of the length of the boolean
 * array when working with binary representations as of the {@link Section}
 * interface.
 */
public class BooleanArraySection extends AbstractPayloadSection<boolean[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link BooleanArraySection}.
	 */
	public BooleanArraySection() {
		this( CaseStyleBuilder.asCamelCase( BooleanArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 */
	public BooleanArraySection( boolean... aValue ) {
		this( CaseStyleBuilder.asCamelCase( BooleanArraySection.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 */
	public BooleanArraySection( Boolean... aValue ) {
		this( CaseStyleBuilder.asCamelCase( BooleanArraySection.class.getSimpleName() ), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link BooleanArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public BooleanArraySection( String aAlias ) {
		this( aAlias, new boolean[0] );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 */
	public BooleanArraySection( String aAlias, boolean... aValue ) {
		super( aAlias, aValue );
	}

	/**
	 * Constructs a {@link BooleanArraySection} with the given boolean array
	 * payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The array (payload) to be contained by the
	 *        {@link BooleanArraySection}.
	 */
	public BooleanArraySection( String aAlias, Boolean... aValue ) {
		this( aAlias, toPrimitiveArray( aValue ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final boolean[] theBooleans = getPayload();
		final Sequence theHeader = toHeader( theBooleans );
		final Sequence theBody = toBody( theBooleans );
		final Sequence theTransmission = theHeader.toAppend( theBody );
		return theTransmission;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, aLength );
		final byte theModulo = theRecord[0];
		if ( theModulo >= 8 ) {
			throw new TransmissionSequenceException( aSequence, "The number of bits <" + theModulo + "> specified by the header still belonging to the payload of the body's last byte must not be greater than or equal to <8>!" );
		}
		if ( theRecord.length > 1 ) {
			final int theOpenByte = theModulo == 0 ? 0 : 1;
			final int theArrayLength = ( theRecord.length - theOpenByte - 1 ) * 8 + theModulo;
			final boolean[] theBooleans = NumericalUtility.toBooleans( theRecord, 1, theArrayLength );
			setPayload( theBooleans );
		}
		else if ( theRecord.length == 1 && theModulo != 0 ) {
			throw new TransmissionSequenceException( aSequence, "The sequence just contains one (header) value <" + theModulo + "> which should be <0> as the there cannot be any succeeding bits for an empty array!" );
		}
		else if ( theRecord.length == 0 ) {
			throw new TransmissionSequenceException( aSequence, "The sequence is empty, cannot determine the header!" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return getPayload() != null ? getPayload().length / 8 + ( getPayload().length % 8 == 0 ? 0 : 1 ) + 1 : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final Sequence theHeader = toHeader( getPayload() );
		final Sequence theBody = toBody( getPayload() );
		final SerialSchema theHeaderSchema = new SerialSchema( getAlias() + ".Header", getClass(), theHeader, theHeader.getLength(), "A header containing the number of booleans in the last byte belonging to the payload represented by the succeeding byte array." );

		String theBitField = "";
		for ( int i = 0; i < getPayload().length; i++ ) {
			if ( i != 0 && i % 8 == 0 ) {
				theBitField += " ";
			}
			theBitField += getPayload()[i] == true ? "1" : "0";
		}

		final SerialSchema theBodySchema = new SerialSchema( getAlias() + ".Body", getClass(), theBody, theBody.getLength(), theBitField, "A body containing a boolean array payload represented by a byte array (as of the header's value not all bits of the last byte necessarily belong to the payload!)." );
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A body containing a byte array representing a boolean array payload prefixed with the number of booleans.", theHeaderSchema, theBodySchema );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + Arrays.toString( getPayload() ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BooleanArraySection withPayload( boolean[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #setPayload(Object)}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 */
	public void setPayload( Boolean[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #withPayload(boolean[])}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public BooleanArraySection withPayload( Boolean[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean[] toPrimitiveArray( Boolean[] aPayload ) {
		final boolean[] theResult = new boolean[aPayload.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aPayload[i];
		}
		return theResult;
	}

	private Sequence toHeader( boolean[] aBooleans ) {
		return new ByteArraySequence( (byte) ( aBooleans.length % 8 ) );

	}

	private Sequence toBody( boolean[] aBooleans ) {
		final byte[] theRecord = NumericalUtility.toBytes( aBooleans );
		final Sequence theBody = new ByteArraySequence( theRecord );
		return theBody;
	}
}
