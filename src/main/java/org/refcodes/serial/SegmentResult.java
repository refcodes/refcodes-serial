// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

import org.refcodes.io.IOResultAccessor;

/**
 * A {@link SegmentResult} instance is issued by the {@link SerialReceiver} to
 * receive a {@link Transmission}'s data asynchronously.
 * 
 * @param <SEGMENT> The {@link Transmission} instance to be (re-)initialized
 *        asynchronously.
 */
public class SegmentResult<SEGMENT extends Segment> implements IOResultAccessor<SEGMENT, IOException> { // , DecorateeAccessor<SEGMENT> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final SEGMENT _segment;
	private volatile boolean _hasResult = false;
	private IOException _exception = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link SegmentResult} with the according arguments.
	 * 
	 * @param aSegment The {@link Transmission} which to manage,
	 */
	public SegmentResult( SEGMENT aSegment ) {
		_segment = aSegment;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SEGMENT getResult() throws IOException {
		if ( !_hasResult ) {
			synchronized ( this ) {
				if ( !_hasResult ) {
					try {
						wait();
					}
					catch ( InterruptedException ignore ) {}
				}
			}
		}
		if ( _exception != null ) {
			throw _exception;
		}
		return _segment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasResult() {
		return _hasResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "SegmentResult [segment=" + _segment + ", hasResult=" + _hasResult + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the segment.
	 *
	 * @return the segment
	 */
	protected SEGMENT getSegment() {
		return _segment;
	}

	/**
	 * Notify result.
	 */
	protected void notifyResult() {
		// boolean isNotifyAll = !_hasResult;
		_hasResult = true;
		// if ( isNotifyAll ) {
		synchronized ( this ) {
			this.notifyAll();
		}
		// }
	}

	/**
	 * Sets the exception.
	 *
	 * @param aException the new exception
	 */
	protected void setException( IOException aException ) {
		_exception = aException;
	}
}
