// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import org.refcodes.exception.TimeoutIOException;
import org.refcodes.io.TimeoutInputStream;

/**
 * The {@link ReadyToSendSegmentDecorator} class implements a decorator
 * providing {@link ReadyToSendTransmission} functionality for a
 * {@link Segment}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public class ReadyToSendSegmentDecorator<DECORATEE extends Segment> extends AbstractReadyToSendTransmissionDecorator<DECORATEE> implements Segment, DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ReadyToSendSegmentDecorator( Builder<DECORATEE> aBuilder ) {
		this( aBuilder.decoratee, aBuilder.enquiryStandbyTimeInMs, aBuilder.readyToSendMagicBytes, aBuilder.readyToSendRetryNumber, aBuilder.readyToSendTimeoutInMs, aBuilder.readyToSendSegmentPackager, aBuilder.clearToSendMagicBytes, aBuilder.clearToSendTimeoutInMs, aBuilder.clearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator() {}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		super( aDecoratee, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendMagicBytes, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		super( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aClearToSendMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aClearToSendMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aClearToSendMagicBytes, aClearToSendTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aClearToSendTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		super( aDecoratee, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToSendSegmentDecorator( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return _decoratee.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.receiveFrom( aInputStream, aReturnStream );
			return;
		}
		else {
			long theEnquiryStandbyTimeInMs = _enquiryStandbyTimeInMs;
			long theBeginTimeInMs = System.currentTimeMillis();
			long ePassedTimeInMs;
			long eCurrentTimeInMs;
			// @SuppressWarnings("resource") // Do not close me after done!
			final TimeoutInputStream theEnquiryStandbyInputStream = SerialUtility.createTimeoutInputStream( aInputStream, theEnquiryStandbyTimeInMs );
			final TimeoutInputStream theTimeoutCtsInputStream = SerialUtility.createTimeoutInputStream( aInputStream, _clearToSendTimeoutInMs );
			//	@SuppressWarnings("resource") // Do not close me after done!
			//	SkipAvailableInputStream theSkipInputStream = new SkipAvailableInputStream( aInputStream, _readyToSendTimeoutInMs );
			while ( theEnquiryStandbyTimeInMs == -1 || theEnquiryStandbyTimeInMs > 0 ) {
				// theSkipInputStream.skipAvailableWithin( IoTimeout.toTimeoutSleepLoopTimeInMs( _readyToSendTimeoutInMs ) ); // Clear anything before a CTS signal was sent
				try {
					_readyToSendSegment.receiveFrom( theEnquiryStandbyInputStream );
				}
				catch ( TimeoutIOException e ) { // This is a serial exception now:
					throw new FlowControlTimeoutException( e.getTimeoutInMs(), "Encountered a timeout of <" + e.getTimeoutInMs() + "> ms while receiving from input stream <" + aInputStream + ">!", e );
				}
				if ( Arrays.equals( _readyToSendMagicBytesSegment.getPayload(), _readyToSendMagicBytes ) ) {
					_clearToSendMagicBytesSegment.transmitTo( aReturnStream );
					_decoratee.receiveFrom( theTimeoutCtsInputStream, aReturnStream );
					return;
				}
				else {
					if ( theEnquiryStandbyTimeInMs != -1 ) {
						eCurrentTimeInMs = System.currentTimeMillis();
						ePassedTimeInMs = eCurrentTimeInMs - theBeginTimeInMs;
						theBeginTimeInMs = eCurrentTimeInMs;
						if ( ePassedTimeInMs < theEnquiryStandbyTimeInMs ) {
							theEnquiryStandbyTimeInMs -= ePassedTimeInMs;
						}
						else {
							throw new FlowControlTimeoutException( _readyToSendTimeoutInMs, "Failed after a timeout of <" + _readyToSendTimeoutInMs + "> as the current (received) RTS byte <" + _readyToSendMagicBytesSegment.getPayload() + "> does not match the expected RTS byte <" + _readyToSendMagicBytes + ">." );
						}
					}
				}
			}
			throw new FlowControlTimeoutException( _readyToSendTimeoutInMs, "Failed after a timeout of <" + _readyToSendTimeoutInMs + "> as the current (received) RTS byte <" + _readyToSendMagicBytesSegment.getPayload() + "> does not match the expected RTS byte <" + _readyToSendMagicBytes + ">." );
		}
	}

	/**
	 * Creates builder to build {@link ReadyToSendSegmentDecorator}.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return created builder
	 */
	public static <DECORATEE extends Segment> Builder<DECORATEE> builder() {
		return new Builder<>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link ReadyToSendSegmentDecorator} instances.
	 *
	 * @param <DECORATEE> the generic type
	 */
	public static final class Builder<DECORATEE extends Segment> implements DecorateeBuilder<DECORATEE, Builder<DECORATEE>>, EnquiryStandbyTimeMillisBuilder<Builder<DECORATEE>>, ReadyToSendTimeoutMillisBuilder<Builder<DECORATEE>>, ReadyToSendMagicBytesBuilder<Builder<DECORATEE>>, ReadyToSendSegmentPackagerBuilder<Builder<DECORATEE>>, ReadyToSendRetryNumberBuilder<Builder<DECORATEE>>, ClearToSendTimeoutMillisBuilder<Builder<DECORATEE>>, ClearToSendMagicBytesBuilder<Builder<DECORATEE>>, ClearToSendSegmentPackagerBuilder<Builder<DECORATEE>> {

		private DECORATEE decoratee;
		private long enquiryStandbyTimeInMs = TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS;
		private long clearToSendTimeoutInMs = TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS;
		private byte[] clearToSendMagicBytes = TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES;
		private SegmentPackager clearToSendSegmentPackager;
		private long readyToSendTimeoutInMs = TransmissionMetrics.DEFAULT_READ_TIMEOUT_IN_MS;
		private byte[] readyToSendMagicBytes = TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES;
		private SegmentPackager readyToSendSegmentPackager;
		private int readyToSendRetryNumber = TransmissionMetrics.DEFAULT_READY_TO_SEND_RETRY_NUMBER;

		private Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withDecoratee( DECORATEE aDecoratee ) {
			decoratee = aDecoratee;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withEnquiryStandbyTimeMillis( long aEnquiryStandbyTimeInMs ) {
			enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToSendTimeoutMillis( long aReadyToSendTimeoutInMs ) {
			readyToSendTimeoutInMs = aReadyToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes ) {
			readyToSendMagicBytes = aReadyToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager ) {
			readyToSendSegmentPackager = aReadyToSendSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToSendRetryNumber( int aReadyToSendRetryNumber ) {
			readyToSendRetryNumber = aReadyToSendRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withClearToSendTimeoutMillis( long aClearToSendTimeoutInMs ) {
			clearToSendTimeoutInMs = aClearToSendTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withClearToSendMagicBytes( byte[] aClearToSendMagicBytes ) {
			clearToSendMagicBytes = aClearToSendMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager ) {
			clearToSendSegmentPackager = aClearToSendSegmentPackager;
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return built class
		 */
		public ReadyToSendSegmentDecorator<DECORATEE> build() {
			return new ReadyToSendSegmentDecorator<>( this );
		}
	}
}
