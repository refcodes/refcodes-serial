// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import org.refcodes.exception.TimeoutIOException;
import org.refcodes.io.SkipAvailableInputStream;
import org.refcodes.io.TimeoutInputStream;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * The {@link AbstractReadyToReceiveTransmissionDecorator} class implements a
 * decorator providing {@link ReadyToReceiveTransmission} functionality for a
 * {@link Transmission}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public abstract class AbstractReadyToReceiveTransmissionDecorator<DECORATEE extends Transmission> implements ReadyToReceiveTransmission, TransmissionMixin, DecorateeAccessor<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String RTR_MAGIC_BYTES = "RTR_MAGIC_BYTES";
	public static final String RTR_TIMEOUT_IN_MS = "RTR_TIMEOUT_IN_MS";
	public static final String RTR_RETRY_NUMBER = "RTR_RETRY_NUMBER";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected DECORATEE _decoratee = null;
	protected long _enquiryStandbyTimeInMs;
	protected long _readyToReceiveTimeoutInMs;
	protected byte[] _readyToReceiveMagicBytes;
	protected SegmentPackager _readyToReceiveSegmentPackager;
	protected int _readyToReceiveRetryNumber;
	protected MagicBytesSegment _readyToReceiveMagicByteSegment;
	protected Segment _readyToReceiveSegment;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract ready to receive transmission decorator.
	 */
	protected AbstractReadyToReceiveTransmissionDecorator() {}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		this( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToReceiveMagicBytes(), aTransmissionMetrics.getReadyToReceiveRetryNumber(), aTransmissionMetrics.getReadyToReceiveTimeoutMillis(), aTransmissionMetrics.getReadyToReceiveSegmentPackager() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );

	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToReceiveTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToReceiveSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );

	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTR request (transmitter) from the receiver.
	 * @param aReadyToReceiveMagicBytes The RTR character to be used to signal a
	 *        RTR.
	 * @param aReadyToReceiveRetryNumber The number of retries sending an RTR
	 *        over the return channel.
	 * @param aReadyToReceiveTimeoutInMs The timeout in milliseconds to wait for
	 *        a payload response (receiver) from the transmitter after signaling
	 *        a RTR ("ready-to-receive") to the transmitter.
	 * @param aReadyToReceiveSegmentPackager The RTR {@link SegmentPackager} for
	 *        packaging RTR responses.
	 */
	public AbstractReadyToReceiveTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		_decoratee = aDecoratee;
		_enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
		_readyToReceiveMagicBytes = aReadyToReceiveMagicBytes;
		_readyToReceiveRetryNumber = aReadyToReceiveRetryNumber;
		_readyToReceiveTimeoutInMs = aReadyToReceiveTimeoutInMs;
		_readyToReceiveSegmentPackager = aReadyToReceiveSegmentPackager != null ? aReadyToReceiveSegmentPackager : new DummySegmentPackager();
		_readyToReceiveSegment = _readyToReceiveSegmentPackager.toPackaged( _readyToReceiveMagicByteSegment = new MagicBytesSegment( _readyToReceiveMagicBytes ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.transmitTo( aOutputStream, aReturnStream );
		}
		else {
			long theEnquiryStandbyTimeInMs = _enquiryStandbyTimeInMs;
			long theBeginTimeInMs = System.currentTimeMillis();
			long ePassedTimeInMs;
			long eCurrentTimeInMs;
			final TimeoutInputStream theTimeoutReturnStream = SerialUtility.createTimeoutInputStream( aReturnStream, theEnquiryStandbyTimeInMs );
			@SuppressWarnings("resource")
			final // Do not close me after done!
			SkipAvailableInputStream theSkipReturnStream = new SkipAvailableInputStream( aReturnStream, _readyToReceiveTimeoutInMs );
			while ( theEnquiryStandbyTimeInMs == -1 || theEnquiryStandbyTimeInMs > 0 ) {
				theSkipReturnStream.skipAvailableExcept( _readyToReceiveSegment.getLength() );
				try {
					_readyToReceiveSegment.receiveFrom( theTimeoutReturnStream );
				}
				catch ( TimeoutIOException e ) { // This is a serial exception now:
					throw new FlowControlTimeoutException( e.getTimeoutInMs(), "Encountered a timeout of <" + e.getTimeoutInMs() + "> ms while receiving from return stream <" + aReturnStream + ">!", e );
				}
				if ( Arrays.equals( _readyToReceiveMagicByteSegment.getPayload(), _readyToReceiveMagicBytes ) ) {
					_decoratee.transmitTo( aOutputStream, aReturnStream );
					return;
				}
				else {
					if ( theEnquiryStandbyTimeInMs != -1 ) {
						eCurrentTimeInMs = System.currentTimeMillis();
						ePassedTimeInMs = eCurrentTimeInMs - theBeginTimeInMs;
						theBeginTimeInMs = eCurrentTimeInMs;
						if ( ePassedTimeInMs < theEnquiryStandbyTimeInMs ) {
							theEnquiryStandbyTimeInMs -= ePassedTimeInMs;
						}
						else {
							throw new FlowControlTimeoutException( _readyToReceiveTimeoutInMs, "Failed after a timeout of <" + _readyToReceiveTimeoutInMs + "> as the current (received) RTR byte <" + _readyToReceiveMagicByteSegment.getPayload() + "> does not match the expected RTR byte <" + _readyToReceiveMagicBytes + ">." );
						}
					}
				}
			}
			throw new FlowControlTimeoutException( _readyToReceiveTimeoutInMs, "Failed after a timeout of <" + _readyToReceiveTimeoutInMs + "> as the current (received) RTR byte <" + _readyToReceiveMagicByteSegment.getPayload() + "> does not match the expected RTR byte <" + _readyToReceiveMagicBytes + ">." );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A transmission decorator enriching the encapsulated transmission with \"Ready-to-receive RTR\".", getDecoratee().toSchema() );
		theSchema.put( RTR_MAGIC_BYTES, getReadyToReceiveMagicBytes() );
		theSchema.put( RTR_TIMEOUT_IN_MS, getReadyToReceiveTimeoutMillis() );
		theSchema.put( RTR_RETRY_NUMBER, getReadyToReceiveRetryNumber() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getEnquiryStandbyTimeMillis() {
		return _enquiryStandbyTimeInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadyToReceiveTimeoutMillis() {
		return _readyToReceiveTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getReadyToReceiveMagicBytes() {
		return _readyToReceiveMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getReadyToReceiveRetryNumber() {
		return _readyToReceiveRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getReadyToReceiveSegmentPackager() {
		return _readyToReceiveSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [segment=" + _decoratee + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractReadyToReceiveTransmissionDecorator<?> other = (AbstractReadyToReceiveTransmissionDecorator<?>) obj;
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}
}
