// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.ConcatenateModeAccessor.ConcatenateModeBuilder;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.ChecksumValidationModeAccessor.ChecksumValidationModeBuilder;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.CrcAlgorithmAccessor.CrcAlgorithmBuilder;
import org.refcodes.numerical.Endianess;
import org.refcodes.serial.PacketLengthWidthAccessor.PacketLengthWidthBuilder;
import org.refcodes.serial.PacketMagicBytesAccessor.PacketMagicBytesBuilder;
import org.refcodes.serial.PacketSegmentPackagerAccessor.PacketSegmentPackagerBuilder;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.serial.SequenceNumberWidthAccessor.SequenceNumberWidthBuilder;

/**
 * A {@link StopAndWaitPacketStreamSectionDecorator} chunks any outgoing stream
 * ({@link OutputStream}) into blocks of definite length and reassembles any
 * incoming streams ({@link InputStream}) from blocks of definite size into a
 * contiguous stream. Each block is enriched by a sequence number as well as a
 * CRC checksum for error detection and error correction means. To do error
 * correction, upon a bad CRC checksum, a feedback stream is used to request the
 * same block (as of the sequence number) again. A "window" of blocks not been
 * acknowledged may be buffered for out of order recovery as well as other
 * recovery strategies.
 * 
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        chunked into blocks and enriched with a CRC checksum and a sequence
 *        number.
 */
public class StopAndWaitPacketStreamSectionDecorator<DECORATEE extends Section> extends AbstractStopAndWaitPacketStreamTransmissionDecorator<DECORATEE> implements Section, DecoratorSection<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private StopAndWaitPacketStreamSectionDecorator( Builder<DECORATEE> aBuilder ) {
		this( aBuilder.decoratee, aBuilder.blockSize, aBuilder.truncateLengthWidth, aBuilder.packetMagicBytes, aBuilder.lastPacketMagicBytes, aBuilder.sequenceNumberInitValue, aBuilder.sequenceNumberWidth, aBuilder.sequenceNumberConcatenateMode, aBuilder.toPacketSegmentPackager(), aBuilder.acknowledgeMagicBytes, aBuilder.ackRetryNumber, aBuilder.ackTimeoutInMs, aBuilder.toAckSegmentPackager(), aBuilder.endianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitPacketStreamSectionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitPacketStreamSectionDecorator( DECORATEE aDecoratee, int aBlockSize, int aPacketLengthWidth, byte[] aPacketMagicBytes, byte[] aLastPacketMagicBytes, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, SegmentPackager aPacketSegmentPackager, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		super( aDecoratee, aBlockSize, aPacketLengthWidth, aPacketMagicBytes, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aPacketSegmentPackager, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc} This method merely delegates to the decoratee and does not
	 * do any packaging as we do not have a stream which we can packetize, we
	 * have a {@link Sequence} provided.
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		_decoratee.fromTransmission( aSequence, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException {
		final StopAndWaitPacketInputStream thePacketInputStream = new StopAndWaitPacketInputStream( aInputStream, _blockSize, _packetLengthWidth, _packetMagicBytes, _sequenceNumberInitValue, _sequenceNumberWidth, _sequenceNumberConcatenateMode, _packetSegmentPackager, aReturnStream, _acknowledgeMagicBytes, _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, _acknowledgeSegmentPackager, _endianess );
		_packetSize = thePacketInputStream.getPacketSize();
		_decoratee.receiveFrom( thePacketInputStream, aLength, aReturnStream );
	}

	/**
	 * Creates builder to build {@link StopAndWaitPacketStreamSectionDecorator}.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The created builder.
	 */
	public static <DECORATEE extends Section> Builder<DECORATEE> builder() {
		return new Builder<>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link StopAndWaitSegmentDecorator} instances.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 */
	public static final class Builder<DECORATEE extends Section> implements AcknowledgeRetryNumberBuilder<Builder<DECORATEE>>, AcknowledgeTimeoutMillisBuilder<Builder<DECORATEE>>, DecorateeBuilder<DECORATEE, Builder<DECORATEE>>, AcknowledgeMagicBytesBuilder<Builder<DECORATEE>>, SequenceNumberWidthBuilder<Builder<DECORATEE>>, SequenceNumberInitValueBuilder<Builder<DECORATEE>>, ConcatenateModeBuilder<Builder<DECORATEE>>, ChecksumValidationModeBuilder<Builder<DECORATEE>>, CrcAlgorithmBuilder<Builder<DECORATEE>>, BlockSizeBuilder<Builder<DECORATEE>>, EndianessBuilder<Builder<DECORATEE>>, AcknowledgeSegmentPackagerBuilder<Builder<DECORATEE>>, PacketSegmentPackagerBuilder<Builder<DECORATEE>>, PacketLengthWidthBuilder<Builder<DECORATEE>>, PacketMagicBytesBuilder<Builder<DECORATEE>> {

		private DECORATEE decoratee = null;
		private int ackRetryNumber = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER;
		private long ackTimeoutInMs = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS;
		private byte[] acknowledgeMagicBytes = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES;
		private int sequenceNumberWidth = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH;
		private int sequenceNumberInitValue = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE;
		private ConcatenateMode sequenceNumberConcatenateMode = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE;
		private Endianess endianess = TransmissionMetrics.DEFAULT_ENDIANESS;
		private int blockSize = TransmissionMetrics.DEFAULT_BLOCK_SIZE;
		private CrcAlgorithm crcAlgorithm = null;
		private final ConcatenateMode crcChecksumConcatenateMode = null;
		private ChecksumValidationMode crcChecksumValidationMode = null;
		private SegmentPackager packetSegmentPackager = null;
		private SegmentPackager ackSegmentPackager = null;
		byte[] lastPacketMagicBytes = TransmissionMetrics.DEFAULT_LAST_PACKET_MAGIC_BYTES;;
		byte[] packetMagicBytes = TransmissionMetrics.DEFAULT_PACKET_MAGIC_BYTES;
		int truncateLengthWidth = TransmissionMetrics.DEFAULT_LENGTH_WIDTH;

		private Builder() {}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeRetryNumber( int aAcknowledgeRetryNumber ) {
			ackRetryNumber = aAcknowledgeRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeTimeoutMillis( long aAckTimeoutInMs ) {
			ackTimeoutInMs = aAckTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withDecoratee( DECORATEE aDecoratee ) {
			decoratee = aDecoratee;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			acknowledgeMagicBytes = aAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withSequenceNumberWidth( int aSequenceNumberWidth ) {
			sequenceNumberWidth = aSequenceNumberWidth;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			sequenceNumberInitValue = aSequenceNumberInitValue;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withConcatenateMode( ConcatenateMode aConcatenateMode ) {
			sequenceNumberConcatenateMode = aConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			crcAlgorithm = aCrcAlgorithm;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode ) {
			crcChecksumValidationMode = aChecksumValidationMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withBlockSize( int aBlockSize ) {
			blockSize = aBlockSize;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			ackSegmentPackager = aAcknowledgeSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withPacketMagicBytes( byte[] aPacketMagicBytes ) {
			packetMagicBytes = aPacketMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withPacketLengthWidth( int aPacketLengthWidth ) {
			truncateLengthWidth = aPacketLengthWidth;
			return this;
		}

		/**
		 * Inferences the ACK {@link SegmentPackager}. In case one is available
		 * as of {@link #getAcknowledgeSegmentPackager()}, then that is
		 * returned. Else CRC settings are evaluated and if possible sufficient
		 * CRC settings are available, a {@link CrcSegmentPackager} is returned.
		 * If there are no sufficient CRC settings, then a
		 * {@link DummySegmentPackager} is returned.
		 * 
		 * @return An interferenced {@link SegmentPackager} as of the instance's
		 *         properties.
		 */
		SegmentPackager toAckSegmentPackager() {
			if ( ackSegmentPackager != null ) {
				return ackSegmentPackager;
			}
			if ( crcAlgorithm != null || crcChecksumValidationMode != null ) {
				final CrcAlgorithm theCrcAlgorithm = crcAlgorithm != null ? crcAlgorithm : TransmissionMetrics.DEFAULT_CRC_ALGORITHM;
				final ChecksumValidationMode theCrcChecksumValidationMode = crcChecksumValidationMode != null ? crcChecksumValidationMode : TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE;
				final ConcatenateMode theCrcChecksumConcatenateMode = crcChecksumConcatenateMode != null ? crcChecksumConcatenateMode : TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
				final Endianess theEndianess = endianess != null ? endianess : TransmissionMetrics.DEFAULT_ENDIANESS;
				return new CrcSegmentPackager( theCrcAlgorithm, theCrcChecksumConcatenateMode, theCrcChecksumValidationMode, theEndianess );
			}
			return new DummySegmentPackager();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withPacketSegmentPackager( SegmentPackager aPacketSegmentPackager ) {
			packetSegmentPackager = aPacketSegmentPackager;
			return this;
		}

		/**
		 * Inferences the packet {@link SegmentPackager}. In case one is
		 * available as of {@link #getPacketSegmentPackager()}, then that is
		 * returned. Else CRC settings are evaluated and if possible sufficient
		 * CRC settings are available, a {@link CrcSegmentPackager} is returned.
		 * If there are no sufficient CRC settings, then a
		 * {@link DummySegmentPackager} is returned.
		 * 
		 * @return An interferenced {@link SegmentPackager} as of the instance's
		 *         properties.
		 */
		SegmentPackager toPacketSegmentPackager() {
			if ( packetSegmentPackager != null ) {
				return packetSegmentPackager;
			}
			if ( crcAlgorithm != null || crcChecksumValidationMode != null ) {
				final CrcAlgorithm theCrcAlgorithm = crcAlgorithm != null ? crcAlgorithm : TransmissionMetrics.DEFAULT_CRC_ALGORITHM;
				final ChecksumValidationMode theCrcChecksumValidationMode = crcChecksumValidationMode != null ? crcChecksumValidationMode : TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE;
				final ConcatenateMode theCrcChecksumConcatenateMode = crcChecksumConcatenateMode != null ? crcChecksumConcatenateMode : TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
				final Endianess theEndianess = endianess != null ? endianess : TransmissionMetrics.DEFAULT_ENDIANESS;
				return new CrcSegmentPackager( theCrcAlgorithm, theCrcChecksumConcatenateMode, theCrcChecksumValidationMode, theEndianess );
			}
			return new DummySegmentPackager();
		}

		/**
		 * Returns the {@link StopAndWaitSegmentDecorator} instance build
		 * according to the {@link Builder} configuration.
		 * 
		 * @return The accordingly configured
		 *         {@link StopAndWaitSegmentDecorator}.
		 */
		public StopAndWaitPacketStreamSectionDecorator<DECORATEE> build() {
			return new StopAndWaitPacketStreamSectionDecorator<>( this );
		}
	}
}
