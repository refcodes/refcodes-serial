// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a pong magic bytes property.
 */
public interface PongMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the pong magic bytes property.
	 * 
	 * @return The magic bytes stored by the pong magic bytes property.
	 */
	byte[] getPongMagicBytes();

	/**
	 * Provides a mutator for a pong magic bytes property.
	 */
	public interface PongMagicBytesMutator {

		/**
		 * Sets the magic bytes for the pong magic bytes property.
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the pong magic
		 *        bytes property.
		 */
		void setPongMagicBytes( byte[] aPongMagicBytes );

		/**
		 * Sets the magic bytes for the pong magic bytes property. Uses
		 * {@link TransmissionMetrics#DEFAULT_ENCODING} for converting the
		 * {@link String} into a byte array.
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 */
		default void setPongMagicBytes( String aPongMagicBytes ) {
			setPongMagicBytes( aPongMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the pong magic bytes property.
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setPongMagicBytes( String aPongMagicBytes, Charset aEncoding ) {
			setPongMagicBytes( aPongMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a pong magic bytes property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PongMagicBytesBuilder<B extends PongMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the pong magic bytes property.
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the pong magic
		 *        bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPongMagicBytes( byte[] aPongMagicBytes );

		/**
		 * Sets the magic bytes for the pong magic bytes property.
		 *
		 * @param aPongMagicBytes The magic bytes to be stored by the magic
		 *        bytes property. Uses
		 *        {@link TransmissionMetrics#DEFAULT_ENCODING} for converting
		 *        the {@link String} into a byte array.
		 * 
		 * @return the b
		 */
		default B withPongMagicBytes( String aPongMagicBytes ) {
			return withPongMagicBytes( aPongMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the pong magic bytes property.
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withPongMagicBytes( String aPongMagicBytes, Charset aEncoding ) {
			return withPongMagicBytes( aPongMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a pong magic bytes property.
	 */
	public interface PongMagicBytesProperty extends PongMagicBytesAccessor, PongMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPongMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPongMagicBytes The character to set (via
		 *        {@link #setPongMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letPongMagicBytes( byte[] aPongMagicBytes ) {
			setPongMagicBytes( aPongMagicBytes );
			return aPongMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPongMagicBytes(byte[])} and returns the very same value
		 * (getter). Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 *
		 * @param aPongMagicBytes The character to set (via
		 *        {@link #setPongMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPongMagicBytes( String aPongMagicBytes ) {
			return letPongMagicBytes( aPongMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #letPongMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPongMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPongMagicBytes( String aPongMagicBytes, Charset aEncoding ) {
			setPongMagicBytes( aPongMagicBytes, aEncoding );
			return aPongMagicBytes;
		}
	}
}
