// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

import org.refcodes.io.Receivable;

/**
 * The {@link SegmentReceiver} is used to receive {@link Segment} instances in a
 * unified way.
 */
public interface SegmentReceiver extends SegmentDestination, Receivable {

	/**
	 * Receives a {@link Segment} (and blocks this thread) till all it's
	 * {@link Sequence} data has been received and updates the {@link Segment}
	 * by invoking {@link Segment#fromTransmission(Sequence)}. This method
	 * blocks till all bytes are read or the timeout has been reached.
	 *
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass of implementing {@link Segment} types.
	 * @param aTimeoutMillis The default timeout for read operations not
	 *        explicitly called with a timeout argument. With a value of -1
	 *        timeout handling is disabled (blocking mode) or a technical
	 *        timeout occurs (implementation depended).
	 * @param aSegment The {@link Segment} to be (re-)initialized with the
	 *        received data.
	 * 
	 * @throws TransmissionException Thrown in case the received data cannot be
	 *         used to (re-)initialize the given {@link Segment}.
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         receiving.
	 */
	<SEGMENT extends Segment> void receiveSegmentWithin( long aTimeoutMillis, SEGMENT aSegment ) throws IOException;

	/**
	 * Asynchronously receives a {@link Segment}'s {@link Sequence} data (does
	 * not block this thread) and updates the {@link Segment} by invoking
	 * {@link Segment#fromTransmission(Sequence)}, passing the result to the
	 * callback {@link SegmentConsumer#onSegment(Segment)}.
	 * 
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass of implementing {@link Segment} types.
	 * @param aSegment The {@link Segment} to be (re-)initialized with the
	 *        received data.
	 * @param aSegmentConsumer The callback asynchronously invoked upon having
	 *        received the {@link Segment}.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a closed
	 *         connection) while receiving.
	 */
	<SEGMENT extends Segment> void onReceiveSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException;

	/**
	 * Asynchronously receives a {@link Segment}'s {@link Sequence} data (does
	 * not block this thread) and updates the {@link Segment} by invoking
	 * {@link Segment#fromTransmission(Sequence)}. The result is provided by the
	 * returned {@link SegmentResult} instance providing the (asynchronously)
	 * (re-)initialized provided {@link Segment} instance.
	 * 
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass used.
	 * @param aSegment The {@link Segment} to be (re-)initialized with the
	 *        received data.
	 * 
	 * @return The {@link SegmentResult} which provides the result
	 *         (asynchronously).
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a closed
	 *         connection) while receiving.
	 */
	<SEGMENT extends Segment> SegmentResult<SEGMENT> onReceiveSegment( SEGMENT aSegment ) throws IOException;
}
