// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a ping retry number. A ping retry number is the
 * overall number of retries to use when counting retries.
 */
public interface PingRetryNumberAccessor {

	/**
	 * Retrieves the number of retries from the ping retry number. A ping retry
	 * number is the overall number of retries to use when counting retries.
	 * 
	 * @return The number of retries stored by the ping retry number.
	 */
	int getPingRetryNumber();

	/**
	 * Provides a mutator for a ping retry number. A ping retry number is the
	 * overall number of retries to use when counting retries.
	 */
	public interface PingRetryNumberMutator {

		/**
		 * Sets the number of retries for the ping retry number. A ping retry
		 * number is the overall number of retries to use when counting retries.
		 * 
		 * @param aPingRetryNumber The number of retries to be stored by the
		 *        number of ping retry number.
		 */
		void setPingRetryNumber( int aPingRetryNumber );
	}

	/**
	 * Provides a builder method for a ping retry number returning the builder
	 * for applying multiple build operations. A ping retry number is the
	 * overall number of retries to use when counting retries.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PingRetryNumberBuilder<B extends PingRetryNumberBuilder<B>> {

		/**
		 * Sets the number of retries for the ping retry number. A ping retry
		 * number is the overall number of retries to use when counting retries.
		 * 
		 * @param aPingRetryNumber The number of retries to be stored by the
		 *        number of ping retry number.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPingRetryNumber( int aPingRetryNumber );
	}

	/**
	 * Provides a ping retry number.A ping retry number is the overall number of
	 * retries to use when counting retries.
	 */
	public interface PingRetryNumberProperty extends PingRetryNumberAccessor, PingRetryNumberMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPingRetryNumber(int)} and returns the very same value
		 * (getter). A ping retry number is the overall number of retries to use
		 * when counting retries.
		 * 
		 * @param aPingRetryNumber The integer to set (via
		 *        {@link #setPingRetryNumber(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPingRetryNumber( int aPingRetryNumber ) {
			setPingRetryNumber( aPingRetryNumber );
			return aPingRetryNumber;
		}
	}
}
