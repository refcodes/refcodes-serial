// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.RetryNumberAccessor;

/**
 * Thrown in case a flow control failed due t a timeout.
 */
public class FlowControlRetryException extends FlowControlTimeoutException implements RetryNumberAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _retryNumber;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, String aMessage, String aErrorCode ) {
		super( aTimeoutMillis, aMessage, aErrorCode );
		_retryNumber = aRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aTimeoutMillis, aMessage, aCause, aErrorCode );
		_retryNumber = aRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, String aMessage, Throwable aCause ) {
		super( aTimeoutMillis, aMessage, aCause );
		_retryNumber = aRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, String aMessage ) {
		super( aTimeoutMillis, aMessage );
		_retryNumber = aRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, Throwable aCause, String aErrorCode ) {
		super( aTimeoutMillis, aCause, aErrorCode );
		_retryNumber = aRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aRetryNumber The number of retries after which the exception
	 *        occurred.
	 */
	public FlowControlRetryException( int aRetryNumber, long aTimeoutMillis, Throwable aCause ) {
		super( aTimeoutMillis, aCause );
		_retryNumber = aRetryNumber;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	@Override
	public int getRetryNumber() {
		return _retryNumber;
	}
}
