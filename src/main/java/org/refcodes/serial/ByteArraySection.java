// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;

import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link ByteArraySection} is an implementation of a {@link Section}
 * carrying a byte array as payload.
 */
public class ByteArraySection extends AbstractPayloadSection<byte[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link ByteArraySection}.
	 */
	public ByteArraySection() {
		this( CaseStyleBuilder.asCamelCase( ByteArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 */
	public ByteArraySection( byte[] aPayload ) {
		this( CaseStyleBuilder.asCamelCase( ByteArraySection.class.getSimpleName() ), aPayload );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aPayload The payload to be contained by the {@link Section}.
	 */
	public ByteArraySection( Byte[] aPayload ) {
		this( CaseStyleBuilder.asCamelCase( ByteArraySection.class.getSimpleName() ), aPayload );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ByteArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public ByteArraySection( String aAlias ) {
		super( aAlias, new byte[0] );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 */
	public ByteArraySection( String aAlias, byte[] aPayload ) {
		super( aAlias, aPayload );
	}

	/**
	 * Constructs a {@link ByteArraySection} with the given byte array payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the {@link Section}.
	 */
	public ByteArraySection( String aAlias, Byte[] aPayload ) {
		super( aAlias, toPrimitiveArray( aPayload ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( getPayload() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, aLength );
		setPayload( theRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return getPayload() != null ? getPayload().length * Byte.BYTES : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A body containing a byte array payload." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + Arrays.toString( getPayload() ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ByteArraySection withPayload( byte[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #setPayload(Object)}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 */
	public void setPayload( Byte[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #withPayload(byte[])}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public ByteArraySection withPayload( Byte[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] toPrimitiveArray( Byte[] aPayload ) {
		final byte[] theResult = new byte[aPayload.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aPayload[i];
		}
		return theResult;
	}
}
