// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * The {@link LoopbackPortHub} is a plain implementation of a {@link PortHub}
 * serving an infinite number of {@link LoopbackPort} instances: When calling
 * {@link #toPort(String)} and the such port is unknown, then it is created (and
 * from then on known).
 */
public class LoopbackPortHub implements PortHub<LoopbackPort, PortMetrics> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Set<LoopbackPort> _ports = new HashSet<>();;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link LoopbackPortHub}. Add ports by calling
	 * {@link #toPort(String)}.
	 */
	public LoopbackPortHub() {}

	/**
	 * Constructs the {@link LoopbackPortHub} with the given ports (e.g. ports
	 * with the given port aliases).
	 * 
	 * @param aPorts The aliases for the ports to be contained within this
	 *        {@link PortHub}.
	 */
	public LoopbackPortHub( String... aPorts ) {
		if ( aPorts != null && aPorts.length != 0 ) {
			for ( String aPort : aPorts ) {
				_ports.add( new LoopbackPort( aPort ) );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackPort[] ports() throws IOException {
		return _ports.toArray( new LoopbackPort[_ports.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackPort toPort( String aAlias ) throws IOException {
		LoopbackPort thePort;
		try {
			thePort = PortHub.super.toPort( aAlias );
		}
		catch ( NoSuchPortExcpetion e ) {
			thePort = new LoopbackPort( aAlias );
			_ports.add( thePort );
		}
		return thePort;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackPort toPort( String aAlias, PortMetrics aPortMetrics ) throws IOException {
		return toPort( aAlias );
	}
}
