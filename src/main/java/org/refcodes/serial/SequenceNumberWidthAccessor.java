// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a sequence number width (in bytes) property.
 */
public interface SequenceNumberWidthAccessor {

	/**
	 * Retrieves the sequence number width (in bytes) from the sequence number
	 * width (in bytes) property.
	 * 
	 * @return The sequence number width (in bytes) stored by the sequence
	 *         number width (in bytes) property.
	 */
	int getSequenceNumberWidth();

	/**
	 * Provides a mutator for a sequence number width (in bytes) property.
	 */
	public interface SequenceNumberWidthMutator {

		/**
		 * Sets the sequence number width (in bytes) for the sequence number
		 * width (in bytes) property.
		 * 
		 * @param aSequenceNumberWidth The sequence number width (in bytes) to
		 *        be stored by the sequence number width (in bytes) property.
		 */
		void setSequenceNumberWidth( int aSequenceNumberWidth );
	}

	/**
	 * Provides a builder method for a sequence number width (in bytes) property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SequenceNumberWidthBuilder<B extends SequenceNumberWidthBuilder<B>> {

		/**
		 * Sets the sequence number width (in bytes) for the sequence number
		 * width (in bytes) property.
		 * 
		 * @param aSequenceNumberWidth The sequence number width (in bytes) to
		 *        be stored by the sequence number width (in bytes) property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSequenceNumberWidth( int aSequenceNumberWidth );
	}

	/**
	 * Provides a sequence number width (in bytes) property.
	 */
	public interface SequenceNumberWidthProperty extends SequenceNumberWidthAccessor, SequenceNumberWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setSequenceNumberWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aSequenceNumberWidth The integer to set (via
		 *        {@link #setSequenceNumberWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letSequenceNumberWidth( int aSequenceNumberWidth ) {
			setSequenceNumberWidth( aSequenceNumberWidth );
			return aSequenceNumberWidth;
		}
	}
}
