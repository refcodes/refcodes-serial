// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import org.refcodes.component.ConnectionAutomatonImpl;
import org.refcodes.controlflow.ControlFlowUtility;

/**
 * A {@link AbstractPort} is a base {@link Port} implementation.
 *
 * @param <PM> The actual {@link PortMetrics} type to use.
 */
public abstract class AbstractPort<PM extends PortMetrics> extends ConnectionAutomatonImpl<PM> implements Port<PM> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _alias;
	protected PM _portMetrics;
	protected ExecutorService _executorService;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractPort} with the given properties. The alias
	 * corresponds to the underlying TTY-/COM-Port's name.
	 * 
	 * @param aAlias The unambiguous technical name for this
	 *        {@link AbstractPort}.
	 * @param aPortMetrics The metrics of the {@link AbstractPort}.
	 */
	protected AbstractPort( String aAlias, PM aPortMetrics ) {
		this( aAlias, aPortMetrics, ControlFlowUtility.createCachedExecutorService( true ) );
	}

	/**
	 * Constructs a {@link AbstractPort} with the given properties and the given
	 * {@link ExecutorService} to be used by asynchronous functionality. The
	 * alias corresponds to the underlying TTY-/COM-Port's name.
	 * 
	 * @param aAlias The unambiguous technical name for this
	 *        {@link AbstractPort}.
	 * @param aPortMetrics The metrics of the {@link AbstractPort}.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        invoking asynchronously working methods.
	 */
	protected AbstractPort( String aAlias, PM aPortMetrics, ExecutorService aExecutorService ) {
		_alias = aAlias;
		_portMetrics = aPortMetrics;
		_executorService = aExecutorService != null ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the unambiguous technical name of the TTY-/COM-Port.
	 * 
	 * @return The port's name.
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable() {
		return !isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		open( (PM) null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( PM aConnection ) throws IOException {
		if ( aConnection != null ) {
			_portMetrics = aConnection;
		}
		super.open( _portMetrics );
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <HEADER extends Segment> void receiveSegment( HEADER aSegment ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		Port.super.receiveSegment( aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void onReceiveSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		_executorService.execute( new ReceiveSegmentConsumerDaemon<SEGMENT>( aSegmentConsumer, aSegment, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> SegmentResult<SEGMENT> onReceiveSegment( SEGMENT aSegment ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		final ReceiveSegmentResultDaemon<SEGMENT> theDaemon = new ReceiveSegmentResultDaemon<>( aSegment, this );
		_executorService.execute( theDaemon );
		return theDaemon.getSegmentResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> SegmentResult<SEGMENT> doTransmitSegment( SEGMENT aSegment ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot receive a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		final TransmitSegmentResultDaemon<SEGMENT> theDaemon = new TransmitSegmentResultDaemon<>( aSegment, this );
		_executorService.execute( theDaemon );
		return theDaemon.getSegmentResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void doTransmitSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException {
		if ( !isOpened() ) {
			throw new IOException( "Cannot transmit a segment as the connection is in status <" + getConnectionStatus() + ">!" );
		}
		_executorService.execute( new TransmitSegmentConsumerDaemon<SEGMENT>( aSegmentConsumer, aSegment, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		super.close();
		_executorService.shutdownNow();
	}
}
