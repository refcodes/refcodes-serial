// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link SegmentPackager} RTR ("ready-to-receive")
 * property.
 */
public interface ReadyToReceiveSegmentPackagerAccessor {

	/**
	 * Retrieves the {@link SegmentPackager} RTR ("ready-to-receive") property.
	 * 
	 * @return The {@link SegmentPackager} stored by the RTR
	 *         ("ready-to-receive") {@link SegmentPackager} property.
	 */
	SegmentPackager getReadyToReceiveSegmentPackager();

	/**
	 * Provides a mutator for a {@link SegmentPackager} RTR ("ready-to-receive")
	 * property.
	 */
	public interface ReadyToReceiveSegmentPackagerMutator {

		/**
		 * Sets the {@link SegmentPackager} RTR ("ready-to-receive") property.
		 * 
		 * @param aReadyToReceiveSegmentPackager The {@link SegmentPackager} to
		 *        be stored by the {@link SegmentPackager} RTR
		 *        ("ready-to-receive") property.
		 */
		void setReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager );
	}

	/**
	 * Provides a builder method for a {@link SegmentPackager} RTR
	 * ("ready-to-receive") property returning the builder for applying multiple
	 * build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToReceiveSegmentPackagerBuilder<B extends ReadyToReceiveSegmentPackagerBuilder<B>> {

		/**
		 * Sets the {@link SegmentPackager} RTR ("ready-to-receive") property.
		 * 
		 * @param aReadyToReceiveSegmentPackager The {@link SegmentPackager} to
		 *        be stored by the {@link SegmentPackager} RTR
		 *        ("ready-to-receive") property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager );
	}

	/**
	 * Provides a {@link SegmentPackager} RTR ("ready-to-receive") property.
	 */
	public interface ReadyToReceiveSegmentPackagerProperty extends ReadyToReceiveSegmentPackagerAccessor, ReadyToReceiveSegmentPackagerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SegmentPackager}
		 * (setter) as of
		 * {@link #setReadyToReceiveSegmentPackager(SegmentPackager)} and
		 * returns the very same value (getter).
		 * 
		 * @param aReadyToReceiveSegmentPackager The {@link SegmentPackager} to
		 *        set (via
		 *        {@link #setReadyToReceiveSegmentPackager(SegmentPackager)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SegmentPackager letReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager ) {
			setReadyToReceiveSegmentPackager( aReadyToReceiveSegmentPackager );
			return aReadyToReceiveSegmentPackager;
		}
	}
}
