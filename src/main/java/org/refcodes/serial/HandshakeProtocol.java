// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Enumeration with the various protocol settings when receiving a transmission.
 * A transmission may require an acknowledge handshake for a transmission.
 */
public enum HandshakeProtocol {

	/**
	 * No CRC error detection and no acknowledge handshake.
	 */
	NO_ACKNOWLEDGE(false),

	/**
	 * No CRC error detection but acknowledge handshake.
	 */
	REQUEST_FOR_ACKNOWLEDGE(true);

	private boolean _isAck;

	private HandshakeProtocol( boolean isAck ) {
		_isAck = isAck;
	}

	/**
	 * Determines whether an acknowledge handshake is to be established upon
	 * receival of a transmission.
	 * 
	 * @return True in case of an acknowledge handshake.
	 */
	public boolean isAcknowledge() {
		return _isAck;
	}
}
