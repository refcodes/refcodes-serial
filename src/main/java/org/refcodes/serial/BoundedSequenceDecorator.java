// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

import org.refcodes.io.BoundedInputStream;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.LengthAccessor.LengthProperty;
import org.refcodes.mixin.TruncateMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;
import org.refcodes.struct.BoundedIterator;

/**
 * The {@link BoundedSequenceDecorator} decorates a {@link Sequence} whilst
 * bounding its size to a given maximum size (to be set via
 * {@link #setLength(int)}) . In case the underlying {@link Sequence} is smaller
 * than the bounded size, then the {@link BoundedSequenceDecorator} will "bound"
 * to the smaller decorated {@link Sequence}'s size. If the underlying Sequence
 * is greater than the bounded size, then the size if the underlying
 * {@link Sequence} appears through this decorator smaller, although it by
 * itself remains at its larger size. This functionality is useful when
 * allocating a buffer which by usages of the decorator appears smaller without
 * the need to copy the original sequence (thereby wasting bytes to be taken
 * care of by the garbage collector).
 * 
 * ATTENTION: Currently just the basic methods as well as the one required by
 * the {@link SequenceSection} are implemented, unimplemented methods throw an
 * {@link UnsupportedOperationException} exception. For manipulating a
 * {@link Sequence} with more complex operations, use the underlying (decorated)
 * {@link Sequence}.
 */
public class BoundedSequenceDecorator implements Sequence, LengthProperty {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Sequence _sequence;
	private int _lengthBounds = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link Sequence} with a bounded maximum size.
	 * 
	 * @param aSequence The {@link Sequence} to be decorated.
	 * 
	 * @param aLengthBounds The maximum size to bound this {@link Sequence} to.
	 *        If the length if the provided {@link Sequence} is smaller, than
	 *        the smaller size represents the bounds. A value of -1 disables any
	 *        length bounds.
	 */
	public BoundedSequenceDecorator( Sequence aSequence, int aLengthBounds ) {
		_sequence = aSequence;
		_lengthBounds = aLengthBounds;
	}

	/**
	 * Decorates the given {@link Sequence} with a bounded maximum size, as soon
	 * as a bounded size greater than -1 is provided via
	 * {@link #setLength(int)}.
	 * 
	 * @param aSequence The {@link Sequence} to be decorated.
	 */
	public BoundedSequenceDecorator( Sequence aSequence ) {
		_sequence = aSequence;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * A value of -1 indicates that the length bounds are disabled.
	 */
	@Override
	public int getLength() {
		return _lengthBounds == -1 ? _sequence.getLength() : ( _lengthBounds < _sequence.getLength() ? _lengthBounds : _sequence.getLength() );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * A value of -1 disables any length bounds.
	 */
	@Override
	public void setLength( int aLength ) {
		_lengthBounds = aLength;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Byte> iterator() {
		return new BoundedIterator<>( _sequence.iterator(), _lengthBounds );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void forEach( Consumer<? super Byte> aAction ) {
		final Iterator<Byte> e = iterator();
		while ( e.hasNext() ) {
			aAction.accept( e.next() );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void append( byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void append( byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Spliterator<Byte> spliterator() {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void append( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear( byte aValue ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void concatenate( ConcatenateMode aConcatenateMode, byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void concatenate( ConcatenateMode aConcatenateMode, byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void concatenate( Sequence aSequence, ConcatenateMode aConcatenateMode ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void empty() {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte getByteAt( int aIndex ) {
		if ( aIndex >= _lengthBounds ) {
			throw new IndexOutOfBoundsException( aIndex );
		}
		return _sequence.getByteAt( aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getInputStream() {
		return new BoundedInputStream( _sequence.getInputStream(), _lengthBounds );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputStream getOutputStream() {
		return _sequence.getOutputStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( byte[] aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( byte[] aBytes, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( int aOffset, byte[] aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( int aOffset, byte[] aBytes, int aBytesOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( int aOffset, Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( int aOffset, Sequence aSequence, int aSequenceOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void overwrite( Sequence aSequence, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void prepend( byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void prepend( byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void prepend( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( byte[] aBytes ) {
		if ( aBytes.length > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( byte[] aBytes, int aLength ) {
		if ( aLength > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aBytes, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( byte[] aBytes, int aOffset, int aLength ) {
		if ( aOffset + aLength > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aBytes, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( Sequence aSequence ) {
		if ( aSequence.getLength() > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aSequence );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( Sequence aSequence, int aLength ) {
		if ( aLength > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aSequence, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replace( Sequence aSequence, int aOffset, int aLength ) {
		if ( aOffset + aLength > _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.replace( aSequence, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setByteAt( int aIndex, byte aByte ) {
		if ( aIndex >= _lengthBounds ) {
			throw new IndexOutOfBoundsException( aIndex );
		}
		_sequence.setByteAt( aIndex, aByte );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toAppend( byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toAppend( byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toAppend( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toBytes() {
		return _sequence.toBytes( 0, _sequence.getLength() < _lengthBounds ? _sequence.getLength() : _lengthBounds );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toBytes( int aOffset, int aLength ) {
		if ( aOffset + aLength >= _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		return _sequence.toBytes( aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toBytes( int aOffset, int aLength, byte[] aBytes, int aBytesOffset ) {
		if ( aOffset + aLength >= _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		_sequence.toBytes( aOffset, aLength, aBytes, aBytesOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toClone() {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toConcatenate( ConcatenateMode aConcatenateMode, byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toConcatenate( ConcatenateMode aConcatenateMode, byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toConcatenate( Sequence aSequence, ConcatenateMode aConcatenateMode ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		long theChecksum = 0;
		for ( int i = 0; i < getLength(); i++ ) {
			theChecksum = aCrcAlgorithm.toCrcChecksum( theChecksum, getByteAt( i ) );
		}
		return aEndianess.toBytes( theChecksum, aCrcAlgorithm.getCrcWidth() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrcChecksum, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		for ( int i = 0; i < getLength(); i++ ) {
			aCrcChecksum = aCrcAlgorithm.toCrcChecksum( aCrcChecksum, getByteAt( i ) );
		}
		return aEndianess.toBytes( aCrcChecksum, aCrcAlgorithm.getCrcWidth() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( CrcAlgorithm aCrcAlgorithm ) {
		long theChecksum = 0;
		for ( int i = 0; i < getLength(); i++ ) {
			theChecksum = aCrcAlgorithm.toCrcChecksum( theChecksum, getByteAt( i ) );
		}
		return theChecksum;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrcChecksum, CrcAlgorithm aCrcAlgorithm ) {
		for ( int i = 0; i < getLength(); i++ ) {
			aCrcChecksum = aCrcAlgorithm.toCrcChecksum( aCrcChecksum, getByteAt( i ) );
		}
		return aCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream toInputStream() {
		return new BoundedInputStream( _sequence.toInputStream(), _lengthBounds );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( byte[] aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( byte[] aBytes, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( int aOffset, byte[] aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( int aOffset, byte[] aBytes, int aBytesOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( int aOffset, Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( int aOffset, Sequence aSequence, int aBytesOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toOverwrite( Sequence aSequence, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toPrepend( byte... aBytes ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toPrepend( byte[] aBytes, int aOffset, int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toPrepend( Sequence aSequence ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence( int aOffset, int aLength ) {
		if ( aOffset + aLength >= _lengthBounds ) {
			throw new IndexOutOfBoundsException( _lengthBounds );
		}
		return _sequence.toSequence( aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toTruncate( int aLength, TruncateMode aTruncateMode ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toTruncateHead( int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toTruncateTail( int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void truncate( int aLength, TruncateMode aTruncateMode ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void truncateHead( int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void truncateTail( int aLength ) {
		throw new UnsupportedOperationException( "Bounding this operation is not supported by this implementation!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withAppend( byte... aBytes ) {
		append( aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withAppend( byte[] aBytes, int aOffset, int aLength ) {
		_sequence.append( aBytes, aOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withAppend( Sequence aSequence ) {
		append( aSequence );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withConcatenate( ConcatenateMode aConcatenateMode, byte... aBytes ) {
		concatenate( aConcatenateMode, aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withConcatenate( Sequence aSequence, ConcatenateMode aConcatenateMode ) {
		concatenate( aSequence, aConcatenateMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( byte[] aBytes ) {
		overwrite( aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( byte[] aBytes, int aLength ) {
		overwrite( aBytes, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( int aOffset, byte[] aBytes ) {
		overwrite( aOffset, aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( int aOffset, byte[] aBytes, int aBytesOffset, int aLength ) {
		overwrite( aOffset, aBytes, aBytesOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( int aOffset, Sequence aSequence ) {
		overwrite( aOffset, aSequence );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( int aOffset, Sequence aSequence, int aSequenceOffset, int aLength ) {
		overwrite( aOffset, aSequence, aSequenceOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( Sequence aSequence ) {
		overwrite( aSequence );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withOverwrite( Sequence aSequence, int aLength ) {
		overwrite( aSequence, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withPrepend( byte... aBytes ) {
		prepend( aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withPrepend( byte[] aBytes, int aOffset, int aLength ) {
		prepend( aBytes, aOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withPrepend( Sequence aSequence ) {
		prepend( aSequence );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( byte[] aBytes ) {
		replace( aBytes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( byte[] aBytes, int aLength ) {
		replace( aBytes, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( Sequence aSequence, int aLength ) {
		replace( aSequence, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( byte[] aBytes, int aOffset, int aLength ) {
		replace( aBytes, aOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( Sequence aSequence ) {
		replace( aSequence );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withReplace( Sequence aSequence, int aOffset, int aLength ) {
		replace( aSequence, aOffset, aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withTruncate( int aLength, TruncateMode aTruncateMode ) {
		truncate( aLength, aTruncateMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withTruncateHead( int aLength ) {
		truncateHead( aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence withTruncateTail( int aLength ) {
		truncateTail( aLength );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeTo( OutputStream aOutputStream ) throws IOException {
		final int theLength = getLength();
		for ( int i = 0; i < theLength; i++ ) {
			aOutputStream.write( (int) getByteAt( i ) );
		}
	}
}
