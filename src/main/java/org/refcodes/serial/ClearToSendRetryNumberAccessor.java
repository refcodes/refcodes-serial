// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a CTS ("clear-to-send") retry number. A CTS retry
 * number is the overall number of retries to use when counting retries.
 */
public interface ClearToSendRetryNumberAccessor {

	/**
	 * Retrieves the number of retries from the CTS ("clear-to-send") retry
	 * number. A CTS retry number is the overall number of retries to use when
	 * counting retries.
	 * 
	 * @return The number of retries stored by the CTS ("clear-to-send") retry
	 *         number.
	 */
	int getClearToSendRetryNumber();

	/**
	 * Provides a mutator for a CTS ("clear-to-send") retry number. A CTS retry
	 * number is the overall number of retries to use when counting retries.
	 */
	public interface ClearToSendRetryNumberMutator {

		/**
		 * Sets the number of retries for the CTS ("clear-to-send") retry
		 * number. A CTS retry number is the overall number of retries to use
		 * when counting retries.
		 * 
		 * @param aClearToSendRetryNumber The number of retries to be stored by
		 *        the number of CTS ("clear-to-send") retry number.
		 */
		void setClearToSendRetryNumber( int aClearToSendRetryNumber );
	}

	/**
	 * Provides a builder method for a CTS ("clear-to-send") retry number
	 * returning the builder for applying multiple build operations. A CTS retry
	 * number is the overall number of retries to use when counting retries.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ClearToSendRetryNumberBuilder<B extends ClearToSendRetryNumberBuilder<B>> {

		/**
		 * Sets the number of retries for the CTS ("clear-to-send") retry
		 * number. A CTS retry number is the overall number of retries to use
		 * when counting retries.
		 * 
		 * @param aClearToSendRetryNumber The number of retries to be stored by
		 *        the number of CTS ("clear-to-send") retry number.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withClearToSendRetryNumber( int aClearToSendRetryNumber );
	}

	/**
	 * Provides a CTS ("clear-to-send") retry number.A CTS ( "clear-to-send")
	 * retry number is the overall number of retries to use when counting
	 * retries.
	 */
	public interface ClearToSendRetryNumberProperty extends ClearToSendRetryNumberAccessor, ClearToSendRetryNumberMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setClearToSendRetryNumber(int)} and returns the very same
		 * value (getter). A CTS ("clear-to-send") retry number is the overall
		 * number of retries to use when counting retries.
		 * 
		 * @param aClearToSendRetryNumber The integer to set (via
		 *        {@link #setClearToSendRetryNumber(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letClearToSendRetryNumber( int aClearToSendRetryNumber ) {
			setClearToSendRetryNumber( aClearToSendRetryNumber );
			return aClearToSendRetryNumber;
		}
	}
}
