// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

import org.refcodes.factory.ClassTypeFactory;
import org.refcodes.factory.TypeFactory;

/**
 * An {@link AbstractArrayTransmission} is a {@link Transmission} consisting of
 * {@link Transmission} elements (an array of elements).
 * 
 * @param <ARRAY> The type of the array elements to be contained in this
 *        instance.
 */
public abstract class AbstractArrayTransmission<ARRAY extends Transmission> implements Transmission, ArrayTransmission<ARRAY> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ARRAY[] _array;
	protected TypeFactory<ARRAY> _sequenceableFactory;
	protected String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance with the given {@link Segment} elements.
	 * {@link Segment} instances for the array are created using the provided
	 * array's component type.
	 * 
	 * @param aSegments The array containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( ARRAY... aSegments ) {
		this( (String) null, aSegments );
	}

	/**
	 * Constructs an according instance with the given {@link Segment} elements.
	 * {@link Segment} instances for the array are created using the provided
	 * array's component type.
	 * 
	 * @param aSegments The collection containing the according {@link Segment}
	 *        elements.
	 */
	public AbstractArrayTransmission( Collection<ARRAY> aSegments ) {
		this( (String) null, aSegments );
	}

	/**
	 * Constructs an according instance with the given elements. {@link Segment}
	 * instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegments The array containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		this( (String) null, aSegmentFactory, aSegments );
	}

	/**
	 * Constructs an according instance with the given elements. {@link Segment}
	 * instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegments The collection containing the according {@link Segment}
	 *        elements.
	 */
	public AbstractArrayTransmission( TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		this( (String) null, aSegmentFactory, aSegments );
	}

	/**
	 * Constructs an according instance with instances of the array being
	 * created using the provided {@link TypeFactory} instance.
	 * 
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 */
	public AbstractArrayTransmission( TypeFactory<ARRAY> aSegmentFactory ) {
		this( (String) null, aSegmentFactory );
	}

	/**
	 * Constructs an according instance with instances of the array being
	 * created using the provided {@link Class} instance.
	 * 
	 * @param aSegmentClass The class from which to produce the the fixed length
	 *        {@link Segment} elements.
	 */
	public AbstractArrayTransmission( Class<ARRAY> aSegmentClass ) {
		this( (String) null, aSegmentClass );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according instance with the given {@link Segment} elements.
	 * {@link Segment} instances for the array are created using the provided
	 * array's component type.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegments The array containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, ARRAY... aSegments ) {
		_sequenceableFactory = new ClassTypeFactory<>( (Class<ARRAY>) aSegments.getClass().getComponentType() );
		_array = aSegments;
		_alias = aAlias;
	}

	/**
	 * Constructs an according instance with the given {@link Segment} elements.
	 * {@link Segment} instances for the array are created using the provided
	 * array's component type.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegments The collection containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, Collection<ARRAY> aSegments ) {
		this( aAlias, (ARRAY[]) aSegments.toArray( new Segment[aSegments.size()] ) );
	}

	/**
	 * Constructs an according instance with the given elements. {@link Segment}
	 * instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegments The array containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, TypeFactory<ARRAY> aSegmentFactory, ARRAY... aSegments ) {
		_sequenceableFactory = aSegmentFactory;
		_array = aSegments;
		_alias = aAlias;
	}

	/**
	 * Constructs an according instance with the given elements. {@link Segment}
	 * instances for the array are created using the provided
	 * {@link TypeFactory} instance.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 * @param aSegments The collection containing the according {@link Segment}
	 *        elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, TypeFactory<ARRAY> aSegmentFactory, Collection<ARRAY> aSegments ) {
		this( aAlias, aSegmentFactory, (ARRAY[]) aSegments.toArray( new Segment[aSegments.size()] ) );
	}

	/**
	 * Constructs an according instance with instances of the array being
	 * created using the provided {@link TypeFactory} instance.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegmentFactory The factory producing the the fixed length
	 *        {@link Segment} elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, TypeFactory<ARRAY> aSegmentFactory ) {
		_sequenceableFactory = aSegmentFactory;
		_array = (ARRAY[]) Array.newInstance( aSegmentFactory.getType(), 0 );
		_alias = aAlias;
	}

	/**
	 * Constructs an according instance with instances of the array being
	 * created using the provided {@link Class} instance.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSegmentClass The class from which to produce the the fixed length
	 *        {@link Segment} elements.
	 */
	@SuppressWarnings("unchecked")
	public AbstractArrayTransmission( String aAlias, Class<ARRAY> aSegmentClass ) {
		_sequenceableFactory = new ClassTypeFactory<>( aSegmentClass );
		_array = (ARRAY[]) Array.newInstance( aSegmentClass, 0 );
		_alias = aAlias;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		if ( _array != null && _array.length != 0 ) {
			for ( var eTransmission : _array ) {
				eTransmission.reset();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		SerialSchema[] theSchemas = null;
		if ( _array != null && _array.length != 0 ) {
			theSchemas = new SerialSchema[_array.length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = _array[i].toSchema();
			}
		}
		return new SerialSchema( getClass(), toSequence(), getLength(), "An array segment containing a fixed length elements array as payload.", theSchemas );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		int theLength = 0;
		if ( _array != null ) {
			for ( ARRAY a_array : _array ) {
				theLength += a_array.getLength();
			}
		}
		return theLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ARRAY[] getArray() {
		return _array;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setArray( ARRAY[] aValue ) {
		_array = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractArrayTransmission<ARRAY> withArray( ARRAY[] aValue ) {
		setArray( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [array=" + Arrays.toString( _array ) + ", segmentFactory=" + _sequenceableFactory + "]";
	}
}
