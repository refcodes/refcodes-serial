// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * Magic bytes are usually found (somewhere) at the beginning of a file or a
 * stream. A {@link MagicBytesSegmentDecorator} decorates a {@link Segment}
 * decoratee and prefixes this {@link Segment} instance with given magic bytes.
 * 
 * @param <DECORATEE> The {@link Segment} type describing the {@link Segment}
 *        subclass decoratee.
 */
public class MagicBytesSegmentDecorator<DECORATEE extends Segment> extends AbstractMagicBytesTransmissionDecorator<DECORATEE> implements SegmentMixin, DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegmentDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegmentDecorator.class.getSimpleName() ), aDecoratee, aTransmissionMetrics );
	}

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * Enriches the provided {@link Segment} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public MagicBytesSegmentDecorator( DECORATEE aDecoratee, int aMagicBytesLength ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegmentDecorator.class.getSimpleName() ), aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSegmentDecorator( DECORATEE aDecoratee, String aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegmentDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSegmentDecorator( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegmentDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 */
	public MagicBytesSegmentDecorator( DECORATEE aDecoratee, byte... aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegmentDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	// -------------------------------------------------------------------------

	/**
	 * Enriches the provided {@link Segment} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public MagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, int aMagicBytesLength ) {
		super( aAlias, aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Segment} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Segment} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 */
	public MagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		_magicBytes = aSequence.toBytes( aOffset, _magicBytesLength );
		aOffset += _magicBytesLength;
		aOffset = getDecoratee().fromTransmission( aSequence, aOffset );
		return aOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		_magicBytes = ( _magicBytes != null && _magicBytes.length != _magicBytesLength ) ? new byte[_magicBytesLength] : _magicBytes;
		aInputStream.read( _magicBytes );
		getDecoratee().receiveFrom( aInputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MagicBytesSegmentDecorator<DECORATEE> withPayload( byte[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment decorator prefixing the encapsulated segment with magic bytes.", _decoratee.toSchema() );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES_TEXT, toMagicBytesString() );
		return theSchema;
	}
}
