// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a acknowledge magic bytes property.
 */
public interface AcknowledgeMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the acknowledge magic bytes property.
	 * 
	 * @return The magic bytes stored by the acknowledge magic bytes property.
	 */
	byte[] getAcknowledgeMagicBytes();

	/**
	 * Provides a mutator for a acknowledge magic bytes property.
	 */
	public interface AcknowledgeMagicBytesMutator {

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property.
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        acknowledge magic bytes property.
		 */
		void setAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes );

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property. Uses
		 * {@link TransmissionMetrics#DEFAULT_ENCODING} for converting the
		 * {@link String} into a byte array.
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        magic bytes property.
		 */
		default void setAcknowledgeMagicBytes( String aAcknowledgeMagicBytes ) {
			setAcknowledgeMagicBytes( aAcknowledgeMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property.
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        magic bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setAcknowledgeMagicBytes( String aAcknowledgeMagicBytes, Charset aEncoding ) {
			setAcknowledgeMagicBytes( aAcknowledgeMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a acknowledge magic bytes property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AcknowledgeMagicBytesBuilder<B extends AcknowledgeMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property.
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        acknowledge magic bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes );

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property.
		 *
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        magic bytes property. Uses
		 *        {@link TransmissionMetrics#DEFAULT_ENCODING} for converting
		 *        the {@link String} into a byte array.
		 * 
		 * @return the b
		 */
		default B withAcknowledgeMagicBytes( String aAcknowledgeMagicBytes ) {
			return withAcknowledgeMagicBytes( aAcknowledgeMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the acknowledge magic bytes property.
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        magic bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withAcknowledgeMagicBytes( String aAcknowledgeMagicBytes, Charset aEncoding ) {
			return withAcknowledgeMagicBytes( aAcknowledgeMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a acknowledge magic bytes property.
	 */
	public interface AcknowledgeMagicBytesProperty extends AcknowledgeMagicBytesAccessor, AcknowledgeMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setAcknowledgeMagicBytes(byte[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aAcknowledgeMagicBytes The character to set (via
		 *        {@link #setAcknowledgeMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			setAcknowledgeMagicBytes( aAcknowledgeMagicBytes );
			return aAcknowledgeMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setAcknowledgeMagicBytes(byte[])} and returns the very same
		 * value (getter). Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 *
		 * @param aAcknowledgeMagicBytes The character to set (via
		 *        {@link #setAcknowledgeMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letAcknowledgeMagicBytes( String aAcknowledgeMagicBytes ) {
			return letAcknowledgeMagicBytes( aAcknowledgeMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #letAcknowledgeMagicBytes(byte[])} and returns the very
		 * same value (getter).
		 * 
		 * @param aAcknowledgeMagicBytes The magic bytes to be stored by the
		 *        magic bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letAcknowledgeMagicBytes( String aAcknowledgeMagicBytes, Charset aEncoding ) {
			setAcknowledgeMagicBytes( aAcknowledgeMagicBytes, aEncoding );
			return aAcknowledgeMagicBytes;
		}
	}
}
