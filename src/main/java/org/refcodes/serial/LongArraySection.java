// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;

import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link LongArraySection} is an implementation of a {@link Section}
 * carrying a long array as payload.
 */
public class LongArraySection extends AbstractPayloadSection<long[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Endianess _endianess;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongArraySection( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( TransmissionMetrics aTransmissionMetrics, long... aValue ) {
		this( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( TransmissionMetrics aTransmissionMetrics, Long... aValue ) {
		this( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, long... aValue ) {
		this( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Long... aValue ) {
		this( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link LongArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongArraySection}'s value.
	 */
	public LongArraySection() {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs an empty {@link LongArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongArraySection( Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link LongArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( long... aValue ) {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( Long... aValue ) {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( Endianess aEndianess, long... aValue ) {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ), aEndianess, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( Endianess aEndianess, Long... aValue ) {
		this( CaseStyleBuilder.asCamelCase( LongArraySection.class.getSimpleName() ), aEndianess, toPrimitiveArray( aValue ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link LongArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public LongArraySection( String aAlias ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, new long[0] );
	}

	/**
	 * Constructs an empty {@link LongArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongArraySection( String aAlias, Endianess aEndianess ) {
		this( aAlias, aEndianess, new long[0] );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, long... aValue ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, Long... aValue ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, Endianess aEndianess, long... aValue ) {
		super( aAlias, aValue );
		_endianess = aEndianess;
	}

	/**
	 * Constructs a {@link LongArraySection} with the given long array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link LongArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link LongArraySection}.
	 */
	public LongArraySection( String aAlias, Endianess aEndianess, Long... aValue ) {
		super( aAlias, toPrimitiveArray( aValue ) );
		_endianess = aEndianess;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final byte[] theBytes = new byte[getPayload().length * Long.BYTES];
		byte[] eRecord;
		for ( int i = 0; i < getPayload().length; i++ ) {
			eRecord = _endianess.toBytes( getPayload()[i], Long.BYTES );
			for ( int j = 0; j < eRecord.length; j++ ) {
				theBytes[i * Long.BYTES + j] = eRecord[j];
			}
		}
		return new ByteArraySequence( theBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		if ( aLength % Long.BYTES != 0 ) {
			throw new TransmissionSequenceException( aSequence, "The length dedicated <" + aLength + "> does not match a multiple long length <" + Long.BYTES + "> for retrieving an long array from the given sequence at the offset <" + aOffset + ">!" );
		}
		final long[] theValues = new long[aLength / Long.BYTES];
		byte[] eRecord;
		for ( int i = 0; i < theValues.length; i++ ) {
			eRecord = aSequence.toBytes( aOffset + i * Long.BYTES, Long.BYTES );
			theValues[i] = _endianess.toLong( eRecord );
		}
		setPayload( theValues );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return getPayload() != null ? getPayload().length * Long.BYTES : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A body containing an long payload." );
		theSchema.put( ENDIANESS, _endianess );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + Arrays.toString( getPayload() ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LongArraySection withPayload( long[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #setPayload(Object)}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 */
	public void setPayload( Long[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #withPayload(long[])}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public LongArraySection withPayload( Long[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static long[] toPrimitiveArray( Long[] aValue ) {
		final long[] theResult = new long[aValue.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aValue[i];
		}
		return theResult;
	}
}
