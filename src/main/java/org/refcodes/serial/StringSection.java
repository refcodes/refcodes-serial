// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link StringSection} is an implementation of a {@link Section} carrying
 * a {@link String} as payload. By default, if not otherwise specified, the
 * {@link TransmissionMetrics#DEFAULT_ENCODING} encoding is used for encoding
 * and decoding {@link String} instances.
 */
public class StringSection extends AbstractPayloadSection<String> implements EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _charset; // <--| "String" instead of "Charset" in order to be serializable

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSection( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link StringSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSection( String aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aValue, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link StringSection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSection( String aAlias, String aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aValue, aTransmissionMetrics.getEncoding() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link StringSection} using the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING} encoding for the payload.
	 */
	public StringSection() {
		this( CaseStyleBuilder.asCamelCase( StringSection.class.getSimpleName() ), (String) null );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 */
	public StringSection( String aValue ) {
		this( CaseStyleBuilder.asCamelCase( StringSection.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link StringSection} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 * 
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public StringSection( Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( StringSection.class.getSimpleName() ), null, aCharset );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 */
	public StringSection( String aValue, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( StringSection.class.getSimpleName() ), aValue, aCharset );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 */
	public StringSection( String aAlias, String aValue ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs a {@link StringSection} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSection}.
	 * @param aCharset The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 */
	public StringSection( String aAlias, String aValue, Charset aCharset ) {
		super( aAlias, aValue );
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		if ( getPayload() != null ) {
			return new ByteArraySequence( getPayload().getBytes( getEncoding() ) );
		}
		return new ByteArraySequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, aLength );
		super.setPayload( new String( theRecord, getEncoding() ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return getPayload() != null ? toSequence().getLength() : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), getPayload(), "A section containing a string payload." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringSection withPayload( String aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset != null ? Charset.forName( _charset ) : TransmissionMetrics.DEFAULT_ENCODING;
	}
}
