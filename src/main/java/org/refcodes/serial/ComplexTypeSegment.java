// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.factory.PrototypeFactory;
import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.struct.ClassStructMap;
import org.refcodes.struct.ClassStructMap.ClassStructMapBuilder;
import org.refcodes.struct.ClassStructMapBuilderImpl;
import org.refcodes.struct.ClassStructMapImpl;
import org.refcodes.struct.SimpleType;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMap.SimpleTypeMapBuilder;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;
import org.refcodes.struct.TypeUtility;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * A {@link ComplexTypeSegment} takes a POJO (a data structure) and creates from
 * its content a {@link Segment} representing the POJO's structure. Inspect it's
 * structure with {@link #toSchema()} and {@link SerialSchema#toString()}!
 * 
 * @param <T> The type of the data structure representing the payload.
 */
public class ComplexTypeSegment<T> extends SegmentComposite<Segment> implements PayloadSegment<T>, TypeAccessor<T>, LengthWidthAccessor, EndianessAccessor, EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _alias;
	private Class<T> _type;
	private T _value = null;
	private String[] _attributes = null;
	private int _lengthWidth;
	private Endianess _endianess;
	private String _charset; // <--| "String" instead of "Charset" in order to be serializable

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, TransmissionMetrics, String[])}
	 * instead! The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aType The data structure's type.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ComplexTypeSegment( Class<T> aType, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aType The data structure's type.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( Class<T> aType, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aAttributes );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, TransmissionMetrics, String[])}
	 * instead! The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aValue The data structure instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ComplexTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The value from which to construct the complex type..
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aAttributes );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 * 
	 * @param aType The data structure's type.
	 */
	public ComplexTypeSegment( Class<T> aType ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according
	 * order.
	 * 
	 * @param aType The data structure's type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( Class<T> aType, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aAttributes );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 * 
	 * @param aValue The data structure instance.
	 */
	public ComplexTypeSegment( T aValue ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order.
	 * 
	 * @param aValue The value from which to construct the complex type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( T aValue, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aAttributes );
	}

	// ------------------------------------------------------------------------

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 * 
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public ComplexTypeSegment( Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, aLengthWidth, aEndianess, aCharset, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 * 
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), aType, null, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 * 
	 * @param aValue The data structure instance.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public ComplexTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, aLengthWidth, aEndianess, aCharset, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 * 
	 * @param aValue The value from which to construct the complex type..
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( ComplexTypeSegment.class.getSimpleName() ), null, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 */
	public ComplexTypeSegment( String aAlias, Class<T> aType ) {
		this( aAlias, aType, null, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( String aAlias, Class<T> aType, String... aAttributes ) {
		this( aAlias, aType, null, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aAttributes );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure instance.
	 */
	public ComplexTypeSegment( String aAlias, T aValue ) {
		this( aAlias, null, aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value from which to construct the complex type..
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( String aAlias, T aValue, String... aAttributes ) {
		this( aAlias, null, aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aAttributes );
	}

	// ------------------------------------------------------------------------

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public ComplexTypeSegment( String aAlias, Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( aAlias, aType, null, aLengthWidth, aEndianess, aCharset, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given {@link Class}
	 * for the according type T. The attributes of the given data structure are
	 * processed using the predefined set of attributes in their according order
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aType The data structure's type.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( String aAlias, Class<T> aType, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( aAlias, aType, null, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance. The attributes of the given data structure are processed all
	 * and in alphabetical order. For specifying a predefined set of attributes
	 * and their order, please invoke
	 * {@link #ComplexTypeSegment(Class, String[])} instead!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure instance.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public ComplexTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( aAlias, null, aValue, aLengthWidth, aEndianess, aCharset, null );
	}

	/**
	 * Constructs the {@link ComplexTypeSegment} from the given data structure
	 * instance.. The attributes of the given data structure are processed using
	 * the predefined set of attributes in their according order
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value from which to construct the complex type..
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public ComplexTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( aAlias, null, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	private ComplexTypeSegment( String aAlias, Class<T> aType, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String[] aAttributes ) {
		_alias = aAlias;
		_lengthWidth = aLengthWidth;
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();
		_endianess = aEndianess;
		_type = aType != null ? aType : (Class<T>) aValue.getClass();
		_value = aValue;
		_attributes = aAttributes;
		initComplexType();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _type;
	}

	/**
	 * Retrieves the attributes to process from the given data structure and the
	 * order in which the attributes are processed.
	 * 
	 * @return The attributes or null if all attributes are to be processed in
	 *         alphabetical order.
	 */
	public String[] getAttributes() {
		return _attributes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLengthWidth() {
		return _lengthWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset != null ? Charset.forName( _charset ) : TransmissionMetrics.DEFAULT_ENCODING;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final int theOffset = super.fromTransmission( aSequence, aOffset );
		_value = toValue();
		return theOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		super.receiveFrom( aInputStream, aReturnStream );
		_value = toValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getPayload() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PayloadTransmission<T> withPayload( T aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( T aValue ) {
		_value = aValue;
		initComplexType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", type=" + _type + ", value=" + _value + ", lengthWidth=" + _lengthWidth + ", endianess=" + _endianess + ", charset=" + _charset + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void initComplexType() {
		ClassStructMap theStructMap = new ClassStructMapImpl( _type, SimpleType.WRAPPER_TYPES );
		SimpleTypeMapBuilder theValueMap = new SimpleTypeMapBuilderImpl( _value );

		// Disable after testing |-->
		//	if ( _attributes == null || _attributes.length == 0 ) {
		//		_attributes = new String[theStructMap.size()];
		//		int i = 0;
		//		for ( String eKey : theStructMap.sortedKeys() ) {
		//			_attributes[i] = eKey;
		//			i++;
		//		}
		//	}
		// Disable after testing <--|

		if ( _attributes != null && _attributes.length != 0 ) {
			final ClassStructMapBuilder theStructMapBuilder = new ClassStructMapBuilderImpl();
			final SimpleTypeMapBuilder theValueMapBuilder = new SimpleTypeMapBuilderImpl();
			for ( String eAttribute : _attributes ) {
				theStructMapBuilder.put( eAttribute, theStructMap.get( eAttribute ) );
				theValueMapBuilder.insert( theValueMap.query( eAttribute ) );
			}
			theValueMap = theValueMapBuilder;
			theStructMap = theStructMapBuilder;
		}
		final Segment theSegment = toSegment( theStructMap, theValueMap );
		if ( theSegment != null ) {
			if ( theSegment instanceof SegmentComposite<?> ) {
				final SegmentComposite<?> theSegmentComposite = (SegmentComposite<?>) theSegment;
				_children = theSegmentComposite.getChildren();
			}
			else {
				_children = new Segment[] { theSegment };
			}
		}
	}

	private Segment toSegment( ClassStructMap aStructMap, SimpleTypeMap aValueMap ) {
		return toSegment( aStructMap, aValueMap, aStructMap.getRootPath() );
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Segment toSegment( ClassStructMap aStructMap, SimpleTypeMap aValueMap, String aPath ) {
		final List<Segment> theChildren = new ArrayList<>();
		Segment eChild;
		String ePath;
		for ( String eKey : aStructMap.children() ) {
			eChild = null;
			ePath = aStructMap.toPath( aPath, eKey );
			if ( aStructMap.isCompositeDir( eKey ) ) {
				final ClassStructMap theStructMap = aStructMap.getCompositeDir( eKey );
				final SimpleTypeMap theValueMap = aValueMap != null ? aValueMap.getDir( eKey ) : null;
				eChild = toSegment( theStructMap, theValueMap, ePath );
			}
			else if ( aStructMap.isCompositeArrayDir( eKey ) ) {
				final ClassStructMap theStructMap = aStructMap.getCompositeArrayDir( eKey );
				final Segment thePrototype = toSegment( theStructMap, null, "" );
				SegmentArraySection<Segment> theArraySegment = null;
				if ( aValueMap != null && aValueMap.isIndexDir() ) {
					final int[] theIndexes = aValueMap.getDirIndexes();
					if ( theIndexes != null && theIndexes.length != 0 ) {
						SimpleTypeMap eValueMap;
						Segment eSegment;
						final List<Segment> theList = new ArrayList<>();
						for ( int i : theIndexes ) {
							eValueMap = aValueMap.getDirAt( i );
							eSegment = toSegment( theStructMap, eValueMap, "" );
							theList.add( eSegment );
						}
						theArraySegment = new SegmentArraySection<>( aPath, new PrototypeFactory<Segment>( thePrototype ), theList );
					}
				}
				if ( theArraySegment == null ) {
					theArraySegment = new SegmentArraySection<>( aPath, new PrototypeFactory<Segment>( thePrototype ) );
				}
				eChild = new AllocSectionDecoratorSegment<Section>( theArraySegment, _lengthWidth, _endianess );
			}
			else if ( aStructMap.isSimpleArrayType( eKey ) ) {
				final Class<?> theType = aStructMap.getSimpleArrayType( eKey );
				eKey = eKey.endsWith( aStructMap.getArraySelector() ) ? aStructMap.toParentPath( eKey ) : eKey;
				final Object[] theValue = aValueMap != null ? ( aValueMap.isArray( eKey ) ? aValueMap.getArray( eKey ) : null ) : null;
				if ( Boolean.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new BooleanArraySection( aPath, TypeUtility.toArrayType( theValue, Boolean[].class ) ) : new BooleanArraySection( aPath ), _lengthWidth, _endianess );
				}
				else if ( Byte.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new ByteArraySection( aPath, TypeUtility.toArrayType( theValue, Byte[].class ) ) : new ByteArraySection( aPath ), _lengthWidth, _endianess );
				}
				else if ( Short.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new ShortArraySection( aPath, _endianess, TypeUtility.toArrayType( theValue, Short[].class ) ) : new ShortArraySection( aPath, _endianess ), _lengthWidth, _endianess );
				}
				else if ( Integer.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new IntArraySection( aPath, _endianess, TypeUtility.toArrayType( theValue, Integer[].class ) ) : new IntArraySection( aPath, _endianess ), _lengthWidth, _endianess );
				}
				else if ( Long.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new LongArraySection( aPath, _endianess, TypeUtility.toArrayType( theValue, Long[].class ) ) : new LongArraySection( aPath, _endianess ), _lengthWidth, _endianess );
				}
				else if ( Float.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new FloatArraySection( aPath, _endianess, TypeUtility.toArrayType( theValue, Float[].class ) ) : new FloatArraySection( aPath, _endianess ), _lengthWidth, _endianess );
				}
				else if ( Double.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new DoubleArraySection( aPath, _endianess, TypeUtility.toArrayType( theValue, Double[].class ) ) : new DoubleArraySection( aPath, _endianess ), _lengthWidth, _endianess );
				}
				else if ( Character.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new CharArraySection( aPath, getEncoding(), TypeUtility.toArrayType( theValue, Character[].class ) ) : new CharArraySection( aPath, getEncoding() ), _lengthWidth, _endianess );
				}
				else if ( String.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new StringArraySection( aPath, _lengthWidth, _endianess, getEncoding(), TypeUtility.toArrayType( theValue, String[].class ) ) : new StringArraySection( aPath, _lengthWidth, _endianess, getEncoding() ), _lengthWidth, _endianess );
				}
			}
			else if ( aStructMap.isSimpleType( eKey ) ) {
				final Class<?> theType = aStructMap.getSimpleType( eKey );
				final Object theValue = aValueMap != null ? aValueMap.get( eKey ) : null;
				if ( Boolean.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new BooleanSegment( ePath, (Boolean) theValue ) : new BooleanSegment( ePath );
				}
				else if ( Byte.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new ByteSegment( ePath, (Byte) theValue ) : new ByteSegment( ePath );
				}
				else if ( Short.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new ShortSegment( ePath, (Short) theValue, _endianess ) : new ShortSegment( ePath, _endianess );
				}
				else if ( Integer.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new IntSegment( ePath, (Integer) theValue, _endianess ) : new IntSegment( ePath, _endianess );
				}
				else if ( Long.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new LongSegment( ePath, (Long) theValue, _endianess ) : new LongSegment( ePath, _endianess );
				}
				else if ( Float.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new FloatSegment( ePath, (Float) theValue, _endianess ) : new FloatSegment( ePath, _endianess );
				}
				else if ( Double.class.isAssignableFrom( theType ) ) {
					eChild = theValue != null ? new DoubleSegment( ePath, (Double) theValue, _endianess ) : new DoubleSegment( ePath, _endianess );
				}
				else if ( Character.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new CharSection( ePath, (Character) theValue, getEncoding() ) : new CharSection( ePath, getEncoding() ), _lengthWidth, _endianess );
				}
				else if ( String.class.isAssignableFrom( theType ) ) {
					eChild = new AllocSectionDecoratorSegment<Section>( theValue != null ? new StringSection( ePath, (String) theValue, getEncoding() ) : new StringSection( ePath, null, getEncoding() ), _lengthWidth, _endianess );
				}
				else if ( theType.isEnum() ) {
					final Enum<?>[] theEnums = (Enum<?>[]) theType.getEnumConstants();
					Enum<?> theEnum = null;
					for ( Enum<?> eEnum : theEnums ) {
						if ( eEnum == theValue || eEnum.name().equals( theValue ) ) {
							theEnum = eEnum;
							break;
						}
					}
					eChild = new EnumSegment( ePath, theType, theEnum, _endianess );
				}
			}

			if ( eChild != null ) {
				theChildren.add( eChild );
			}
		}
		if ( theChildren.size() == 1 ) {
			return theChildren.get( 0 );
		}
		if ( theChildren.size() > 1 ) {
			return new SegmentComposite<>( theChildren );
		}
		return null;
	}

	/**
	 * Reconstructs the value from this {@link SegmentComposite} (being the
	 * super-class of the {@link ComplexTypeSegment}.
	 * 
	 * @return The accordingly constructed value.
	 */
	private T toValue() {
		final SimpleTypeMap theSimpleTypeMap = toSimpleTypeMap();
		return theSimpleTypeMap.toType( _type );
	}
}
