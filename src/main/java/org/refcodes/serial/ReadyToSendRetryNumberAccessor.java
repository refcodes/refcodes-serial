// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a RTS ("ready-to-send") retry number. A RTS retry
 * number is the overall number of retries to use when counting retries.
 */
public interface ReadyToSendRetryNumberAccessor {

	/**
	 * Retrieves the number of retries from the RTS ("ready-to-send") retry
	 * number. A RTS retry number is the overall number of retries to use when
	 * counting retries.
	 * 
	 * @return The number of retries stored by the RTS ("ready-to-send") retry
	 *         number.
	 */
	int getReadyToSendRetryNumber();

	/**
	 * Provides a mutator for a RTS ("ready-to-send") retry number. A RTS retry
	 * number is the overall number of retries to use when counting retries.
	 */
	public interface ReadyToSendRetryNumberMutator {

		/**
		 * Sets the number of retries for the RTS ("ready-to-send") retry
		 * number. A RTS retry number is the overall number of retries to use
		 * when counting retries.
		 * 
		 * @param aReadyToSendRetryNumber The number of retries to be stored by
		 *        the number of RTS ("ready-to-send") retry number.
		 */
		void setReadyToSendRetryNumber( int aReadyToSendRetryNumber );
	}

	/**
	 * Provides a builder method for a RTS ("ready-to-send") retry number
	 * returning the builder for applying multiple build operations. A RTS retry
	 * number is the overall number of retries to use when counting retries.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToSendRetryNumberBuilder<B extends ReadyToSendRetryNumberBuilder<B>> {

		/**
		 * Sets the number of retries for the RTS ("ready-to-send") retry
		 * number. A RTS retry number is the overall number of retries to use
		 * when counting retries.
		 * 
		 * @param aReadyToSendRetryNumber The number of retries to be stored by
		 *        the number of RTS ("ready-to-send") retry number.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToSendRetryNumber( int aReadyToSendRetryNumber );
	}

	/**
	 * Provides a RTS ("ready-to-send") retry number.A RTS ( "ready-to-send")
	 * retry number is the overall number of retries to use when counting
	 * retries.
	 */
	public interface ReadyToSendRetryNumberProperty extends ReadyToSendRetryNumberAccessor, ReadyToSendRetryNumberMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setReadyToSendRetryNumber(int)} and returns the very same
		 * value (getter). A RTS ("ready-to-send") retry number is the overall
		 * number of retries to use when counting retries.
		 * 
		 * @param aReadyToSendRetryNumber The integer to set (via
		 *        {@link #setReadyToSendRetryNumber(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letReadyToSendRetryNumber( int aReadyToSendRetryNumber ) {
			setReadyToSendRetryNumber( aReadyToSendRetryNumber );
			return aReadyToSendRetryNumber;
		}
	}
}
