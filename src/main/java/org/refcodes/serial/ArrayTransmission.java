// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.mixin.ArrayAccessor.ArrayBuilder;
import org.refcodes.mixin.ArrayAccessor.ArrayProperty;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMap.SimpleTypeMapBuilder;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;

/**
 * A {@link ArrayTransmission} is a {@link Transmission} with array
 * {@link Transmission} elements. When invoking {@link #toSimpleTypeMap()} the
 * array nature is preserved by indexing each element of the array with the
 * array index.
 *
 * @param <ARRAY> The type of the array {@link Transmission} elements.
 */
public interface ArrayTransmission<ARRAY extends Transmission> extends Transmission, ArrayProperty<ARRAY[]>, ArrayBuilder<ARRAY[], ArrayTransmission<ARRAY>>, AliasAccessor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Sequence toSequence() {
		final Sequence theSequence = new ByteArraySequence();
		if ( getArray() != null ) {
			for ( int i = 0; i < getArray().length; i++ ) {
				theSequence.append( getArray()[i].toSequence() );
			}
		}
		return theSequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap toSimpleTypeMap() {
		final SimpleTypeMapBuilder theBuilder = new SimpleTypeMapBuilderImpl();
		if ( getArray() != null && getArray().length != 0 ) {
			for ( int i = 0; i < getArray().length; i++ ) {
				theBuilder.insertTo( theBuilder.toPath( getAlias(), i ), getArray()[i].toSimpleTypeMap() );
			}
		}
		return theBuilder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SerialSchema toSchema() {
		SerialSchema[] theSchemas = null;
		if ( getArray() != null && getArray().length != 0 ) {
			theSchemas = new SerialSchema[getArray().length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = getArray()[i].toSchema();
			}
		}
		return new SerialSchema( getClass(), toSequence(), getLength(), "A parent segment containing an array of segment element.", theSchemas );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default int getLength() {
		int theLength = 0;
		if ( getArray() != null ) {
			for ( int i = 0; i < getArray().length; i++ ) {
				theLength += getArray()[i].getLength();
			}
		}
		return theLength;
	}
}
