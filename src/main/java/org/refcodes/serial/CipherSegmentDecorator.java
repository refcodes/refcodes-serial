// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import org.refcodes.numerical.Invertible;

/**
 * A {@link CipherSegmentDecorator} is a transmission applying an
 * {@link Invertible} on outgoing bytes (as of
 * {@link #transmitTo(java.io.OutputStream)} or the like) and the inverse
 * function on any incoming bytes.
 * 
 * @param <DECORATEE> The {@link Segment} type describing the {@link Segment}
 *        subclass to be enriched.s
 */
public class CipherSegmentDecorator<DECORATEE extends Segment> extends AbstractCipherTransmissionDecorator<DECORATEE> implements Segment {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CipherSegmentDecorator( DECORATEE aDecoratee, Cipher aCipher ) {
		super( aDecoratee, aCipher );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final SequenceInputStream theSequenceInputStream = new SequenceInputStream( aSequence, aOffset );
		try {
			receiveFrom( theSequenceInputStream );
		}
		catch ( IOException e ) {
			throw new TransmissionException( "Cannot receive transmission for sequence <" + aSequence.toHexString() + "> at offset <" + aOffset + ">!", e );
		}
		return theSequenceInputStream.getOffset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		final CipherInputStream theCipherInputStream = new CipherInputStream( aInputStream, _cipher );
		final CipherOutputStream theCipherReturnStream = aReturnStream != null ? new CipherOutputStream( aReturnStream, _cipher ) : null;
		_decoratee.receiveFrom( theCipherInputStream, theCipherReturnStream );

	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
}
