// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;

import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link ShortArraySection} is an implementation of a {@link Section}
 * carrying a short array as payload.
 */
public class ShortArraySection extends AbstractPayloadSection<short[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Endianess _endianess;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortArraySection( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( TransmissionMetrics aTransmissionMetrics, short... aValue ) {
		this( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( TransmissionMetrics aTransmissionMetrics, Short... aValue ) {
		this( aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, short... aValue ) {
		this( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Short... aValue ) {
		this( aAlias, aTransmissionMetrics.getEndianess(), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ShortArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 */
	public ShortArraySection() {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), TransmissionMetrics.DEFAULT_ENDIANESS, new short[0] );
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public ShortArraySection( Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), aEndianess, new short[0] );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link Section}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( short... aValue ) {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( Short... aValue ) {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( Endianess aEndianess, short... aValue ) {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), aEndianess, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( Endianess aEndianess, Short... aValue ) {
		this( CaseStyleBuilder.asCamelCase( ShortArraySection.class.getSimpleName() ), aEndianess, toPrimitiveArray( aValue ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ShortArraySection} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public ShortArraySection( String aAlias ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, new short[0] );
	}

	/**
	 * Constructs an empty {@link ShortArraySection} with the given
	 * {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public ShortArraySection( String aAlias, Endianess aEndianess ) {
		this( aAlias, aEndianess, new short[0] );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link Section}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, short... aValue ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation
	 * of the {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, Short... aValue ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENDIANESS, aValue );
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, Endianess aEndianess, short... aValue ) {
		super( aAlias, aValue );
		_endianess = aEndianess;
	}

	/**
	 * Constructs a {@link ShortArraySection} with the given short array payload
	 * and the given {@link Endianess} for the representation of the
	 * {@link ShortArraySection}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 * @param aValue The payload to be contained by the
	 *        {@link ShortArraySection}.
	 */
	public ShortArraySection( String aAlias, Endianess aEndianess, Short... aValue ) {
		super( aAlias, toPrimitiveArray( aValue ) );
		_endianess = aEndianess;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final byte[] theBytes = new byte[getPayload().length * Short.BYTES];
		byte[] eRecord;
		for ( short i = 0; i < getPayload().length; i++ ) {
			eRecord = _endianess.toBytes( getPayload()[i], Short.BYTES );
			for ( short j = 0; j < eRecord.length; j++ ) {
				theBytes[i * Short.BYTES + j] = eRecord[j];
			}
		}
		return new ByteArraySequence( theBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		if ( aLength % Short.BYTES != 0 ) {
			throw new TransmissionSequenceException( aSequence, "The length dedicated <" + aLength + "> does not match a multiple short length <" + Short.BYTES + "> for retrieving an short array from the given sequence at the offset <" + aOffset + ">!" );
		}
		final short[] theValues = new short[aLength / Short.BYTES];
		byte[] eRecord;
		for ( short i = 0; i < theValues.length; i++ ) {
			eRecord = aSequence.toBytes( aOffset + i * Short.BYTES, Short.BYTES );
			theValues[i] = _endianess.toShort( eRecord );
		}
		setPayload( theValues );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return getPayload() != null ? getPayload().length * Short.BYTES : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A body containing an short payload." );
		theSchema.put( ENDIANESS, _endianess );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + Arrays.toString( getPayload() ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShortArraySection withPayload( short[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #setPayload(Object)}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 */
	public void setPayload( Short[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #withPayload(short[])}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public ShortArraySection withPayload( Short[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static short[] toPrimitiveArray( Short[] aPayload ) {
		final short[] theResult = new short[aPayload.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aPayload[i];
		}
		return theResult;
	}
}
