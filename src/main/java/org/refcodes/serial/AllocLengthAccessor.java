// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a allocation length property.
 */
public interface AllocLengthAccessor {

	/**
	 * Retrieves the allocation length from the allocation length property.
	 * 
	 * @return The allocation length stored by the allocation length property.
	 */
	int getAllocLength();

	/**
	 * Provides a mutator for a allocation length property.
	 */
	public interface AllocLengthMutator {

		/**
		 * Sets the allocation length for the allocation length property.
		 * 
		 * @param aAllocLength The allocation length to be stored by the
		 *        allocation length property.
		 */
		void setAllocLength( int aAllocLength );
	}

	/**
	 * Provides a builder method for a allocation length property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AllocLengthBuilder<B extends AllocLengthBuilder<B>> {

		/**
		 * Sets the allocation length for the allocation length property.
		 * 
		 * @param aAllocLength The allocation length to be stored by the
		 *        allocation length property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAllocLength( int aAllocLength );
	}

	/**
	 * Provides a allocation length property.
	 */
	public interface AllocLengthProperty extends AllocLengthAccessor, AllocLengthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setAllocLength(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aAllocLength The integer to set (via
		 *        {@link #setAllocLength(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letAllocLength( int aAllocLength ) {
			setAllocLength( aAllocLength );
			return aAllocLength;
		}
	}
}
