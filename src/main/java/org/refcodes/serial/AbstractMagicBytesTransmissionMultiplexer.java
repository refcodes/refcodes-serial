// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.refcodes.mixin.ChildrenAccessor;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link AbstractMagicBytesTransmissionMultiplexer} dispatches a
 * transmission to one of the aggregated {@link Transmission} instances
 * depending on the magic number provided by the transmission. A transmission is
 * passed to each of the aggregated {@link Transmission} instances till one
 * {@link Transmission} accepts the transmission, e.g. until a
 * {@link Transmission} does not throw a {@link BadMagicBytesException}.
 * Attention: A {@link Transmission} throwing a {@link TransmissionException}
 * other than a {@link BadMagicBytesException} is considered to be responsible
 * for the transmission so that dispatching is *not* continued with the
 * succeeding {@link Transmission}! The last {@link Transmission} which was
 * responsible for a transmission's magic bytes will be the responsible
 * {@link Transmission} till another {@link Transmission} claims responsibility
 * for a transmsision's magic bytes. Initially the first {@link Transmission}
 * passed to this instance is the responsible {@link Transmission}.
 *
 * @param <CHILD> the generic type
 */
public abstract class AbstractMagicBytesTransmissionMultiplexer<CHILD extends Transmission> implements Transmission, Iterable<CHILD>, ChildrenAccessor<CHILD[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The responsible (if not specified otherwise) maximum limit of bytes that
	 * can be read from a given {@link InputStream} before the
	 * {@link InputStream} cannot be rolled back.
	 */
	public static final int DEFAULT_READ_LIMIT = 1024;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CHILD[] _children;
	protected CHILD _responsibility;
	protected int _readLimit;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link AbstractMagicBytesTransmissionMultiplexer} for
	 * sub-classes to use having the responsibility to set the children by
	 * themselves.
	 */
	protected AbstractMagicBytesTransmissionMultiplexer() {}

	/**
	 * Constructs a {@link AbstractMagicBytesTransmissionMultiplexer} containing
	 * the provided {@link Transmission} elements.
	 * 
	 * @param aSegments The {@link Transmission} elements being contained in
	 *        this instance.
	 */
	@SafeVarargs
	public AbstractMagicBytesTransmissionMultiplexer( CHILD... aSegments ) {
		this( DEFAULT_READ_LIMIT, aSegments );
	}

	/**
	 * Constructs a {@link AbstractMagicBytesTransmissionMultiplexer} containing
	 * the provided {@link Transmission} elements.
	 * 
	 * @param aSegments The {@link Transmission} elements being contained in
	 *        this instance.
	 */
	public AbstractMagicBytesTransmissionMultiplexer( Collection<CHILD> aSegments ) {
		this( aSegments, DEFAULT_READ_LIMIT );
	}

	/**
	 * Constructs a {@link AbstractMagicBytesTransmissionMultiplexer} containing
	 * the provided {@link Transmission} elements.
	 * 
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 * @param aSegments The {@link Transmission} elements being contained in
	 *        this instance.
	 */
	@SafeVarargs
	public AbstractMagicBytesTransmissionMultiplexer( int aReadLimit, CHILD... aSegments ) {
		_children = aSegments;
		_responsibility = _children[0];
		_readLimit = aReadLimit;
	}

	/**
	 * Constructs a {@link AbstractMagicBytesTransmissionMultiplexer} containing
	 * the provided {@link Transmission} elements.
	 * 
	 * @param aSegments The {@link Transmission} elements being contained in
	 *        this instance.
	 * @param aReadLimit the maximum limit of bytes that can be read from a
	 *        given {@link InputStream} before the {@link InputStream} cannot be
	 *        rolled back.
	 */
	public AbstractMagicBytesTransmissionMultiplexer( Collection<CHILD> aSegments, int aReadLimit ) {
		this( aReadLimit, toArray( aSegments ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the length of the responsible {@link Transmission}: The last
	 * {@link Transmission} which was responsible for a transmission's magic
	 * bytes will be the responsible {@link Transmission} till another
	 * {@link Transmission} claims responsibility for a transmsision's magic
	 * bytes. Initially the first {@link Transmission} passed to this instance
	 * is the responsible {@link Transmission}. {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _responsibility.getLength();
	}

	/**
	 * Returns the responsible {@link Transmission}'s transmission: The last
	 * {@link Transmission} which was responsible for a transmission's magic
	 * bytes will be the responsible {@link Transmission} till another
	 * {@link Transmission} claims responsibility for a transmsision's magic
	 * bytes. Initially the first {@link Transmission} passed to this instance
	 * is the responsible {@link Transmission}. {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _responsibility.toSequence();
	}

	/**
	 * Transmits to the responsible {@link Transmission}: The last
	 * {@link Transmission} which was responsible for a transmission's magic
	 * bytes will be the responsible {@link Transmission} till another
	 * {@link Transmission} claims responsibility for a transmsision's magic
	 * bytes. Initially the first {@link Transmission} passed to this instance
	 * is the responsible {@link Transmission}. {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_responsibility.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		if ( _children != null && _children.length != 0 ) {
			for ( var eTransmission : _children ) {
				eTransmission.reset();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getClass(), "The magic bytes multiplexer transmission passes transmissions to the segment responsible for the transmission's magic bytes." );
	}

	/**
	 * Returns the responsible {@link Transmission}'s {@link SimpleTypeMap}
	 * representation: The last {@link Transmission} which was responsible for a
	 * transmission's magic bytes will be the responsible {@link Transmission}
	 * till another {@link Transmission} claims responsibility for a
	 * transmsision's magic bytes. Initially the first {@link Transmission}
	 * passed to this instance is the responsible {@link Transmission}.
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _responsibility.toSimpleTypeMap();
	}

	/**
	 * Transmits to the responsible {@link Transmission}: The last
	 * {@link Transmission} which was responsible for a transmission's magic
	 * bytes will be the responsible {@link Transmission} till another
	 * {@link Transmission} claims responsibility for a transmsision's magic
	 * bytes. Initially the first {@link Transmission} passed to this instance
	 * is the responsible {@link Transmission}. {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream ) throws IOException {
		_responsibility.transmitTo( aOutputStream );
	}

	/**
	 * Transmits to the responsible {@link Transmission}: The last
	 * {@link Transmission} which was responsible for a transmission's magic
	 * bytes will be the responsible {@link Transmission} till another
	 * {@link Transmission} claims responsibility for a transmsision's magic
	 * bytes. Initially the first {@link Transmission} passed to this instance
	 * is the responsible {@link Transmission}. {@inheritDoc}
	 */
	@Override
	public void transmitTo( SerialTransceiver aSerialTransceiver ) throws IOException {
		_responsibility.transmitTo( aSerialTransceiver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<CHILD> iterator() {
		return Arrays.asList( _children ).iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CHILD[] getChildren() {
		return _children;
	}

	/**
	 * The last {@link Transmission} to which the dispatch was transferred, e.g.
	 * which matched the dispatch's magic bytes till another
	 * {@link Transmission} claims responsibility for a dispatch's magic bytes..
	 * 
	 * @return Returns the {@link Transmission} to which the dispatch was
	 *         transferred.
	 */
	public CHILD getCallee() {
		return _responsibility;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	private static <CHILD extends Transmission> CHILD[] toArray( Collection<CHILD> aSegments ) {
		CHILD[] theChildren;
		if ( aSegments != null && aSegments.size() != 0 ) {
			theChildren = (CHILD[]) new Transmission[aSegments.size()];
			theChildren = aSegments.toArray( theChildren );
		}
		else {
			theChildren = (CHILD[]) new Transmission[0];
		}
		return theChildren;
	}
}
