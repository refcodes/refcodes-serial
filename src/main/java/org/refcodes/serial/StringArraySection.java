// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.refcodes.factory.TypeFactory;
import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.serial.Section.SectionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link StringArraySection} is an implementation of a {@link Section}
 * carrying a {@link String} array as payload. Each {@link String} is
 * represented by a {@link StringSection} decorated by an
 * {@link AllocSectionDecoratorSegment}.
 */
public class StringArraySection implements SectionMixin, PayloadSection<String[]>, LengthWidthAccessor, EndianessAccessor, EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private SectionComposite<AllocSectionDecoratorSegment<StringSection>, StringSection> _arraySegment;
	private TypeFactory<StringSection> _segmentFactory;
	private TypeFactory<AllocSectionDecoratorSegment<StringSection>> _allocSegmentFactory;
	private int _lengthWidth;
	private Endianess _endianess;
	private String _charset = TransmissionMetrics.DEFAULT_ENCODING.name();
	private String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringArraySection( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link StringArraySection}.
	 */
	public StringArraySection( TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		this( aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link StringArraySection}.
	 */
	public StringArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, String... aValue ) {
		this( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding(), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link StringArraySection} using the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 */
	public StringArraySection() {
		this( CaseStyleBuilder.asCamelCase( StringArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( String... aPayload ) {
		this( CaseStyleBuilder.asCamelCase( StringArraySection.class.getSimpleName() ), aPayload );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, as well
	 * as the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 * 
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( Charset aCharset, String... aPayload ) {
		this( CaseStyleBuilder.asCamelCase( StringArraySection.class.getSimpleName() ), aCharset, aPayload );
	}

	/**
	 * Constructs a {@link StringArraySection} using containing the provided
	 * {@link StringSection} elements and using the provided {@link Segment}
	 * class for creating {@link Segment} instances.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for size values.
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aPayload ) {
		this( CaseStyleBuilder.asCamelCase( StringArraySection.class.getSimpleName() ), aLengthWidth, aEndianess, aCharset, aPayload );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link StringArraySection} using the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public StringArraySection( String aAlias ) {
		this( aAlias, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( String aAlias, String[] aPayload ) {
		this( aAlias, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aPayload );
	}

	/**
	 * Constructs a {@link StringArraySection} containing the provided payload
	 * and using the {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH}, as well
	 * as the {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( String aAlias, Charset aCharset, String... aPayload ) {
		this( aAlias, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, aCharset, aPayload );
	}

	/**
	 * Constructs a {@link StringArraySection} using containing the provided
	 * {@link StringSection} elements and using the provided {@link Segment}
	 * class for creating {@link Segment} instances.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for size values.
	 * @param aEndianess The {@link Endianess} to be used for size values.
	 * @param aCharset The {@link Charset} to use when encoding or decoding the
	 *        {@link String} instances.
	 * @param aPayload The {@link String} elements being contained in this
	 *        instance.
	 */
	public StringArraySection( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aPayload ) {
		_alias = aAlias;
		_lengthWidth = aLengthWidth;
		_endianess = aEndianess;
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();

		_segmentFactory = new TypeFactory<StringSection>() {
			@Override
			public StringSection create() {
				return new StringSection( aCharset );
			}

			@Override
			public Class<StringSection> getType() {
				return StringSection.class;
			}
		};

		_allocSegmentFactory = () -> new AllocSectionDecoratorSegment<>( aLengthWidth, aEndianess );

		setPayload( aPayload );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		_arraySegment.fromTransmission( aSequence, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException {
		_arraySegment.receiveFrom( aInputStream, aLength, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _arraySegment.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_arraySegment.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = _arraySegment.toSchema();
		theSchema.put( SerialSchema.ALIAS, _alias );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _arraySegment.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getPayload() {
		String[] theStrings = null;
		if ( _arraySegment != null ) {
			final StringSection[] theSegments = _arraySegment.getChildren();
			theStrings = new String[theSegments.length];
			for ( int i = 0; i < theSegments.length; i++ ) {
				theStrings[i] = theSegments[i].getPayload();
			}
		}
		return theStrings;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( String[] aValue ) {
		if ( aValue != null ) {
			_arraySegment = new SectionComposite<>( _allocSegmentFactory, _segmentFactory, toStringBodyArray( aValue, getEncoding() ) );
		}
		else {
			_arraySegment = new SectionComposite<>( _allocSegmentFactory, _segmentFactory );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringArraySection withPayload( String[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLengthWidth() {
		return _lengthWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset != null ? Charset.forName( _charset ) : TransmissionMetrics.DEFAULT_ENCODING;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return new SimpleTypeMapBuilderImpl().withInsertTo( _alias, getPayload() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link StringSection} array from the provided {@link String}
	 * array.
	 * 
	 * @param aArray The array of {@link String} elements.
	 * 
	 * @return The created {@link StringSection} array.
	 */
	private StringSection[] toStringBodyArray( String[] aArray, Charset aCharset ) {
		final StringSection[] theArray = new StringSection[aArray.length];
		for ( int i = 0; i < aArray.length; i++ ) {
			theArray[i] = new StringSection( aArray[i], aCharset );
		}
		return theArray;
	}
}
