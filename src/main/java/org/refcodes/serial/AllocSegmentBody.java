// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.io.BoundedInputStream;
import org.refcodes.serial.AllocLengthAccessor.AllocLengthProperty;

/**
 * An {@link AllocSegmentBody} provides an {@link Segment} with an allocation
 * length. An {@link AllocSegmentHead} provides the allocation length for a
 * {@link AllocSegmentBody}. Therefore the {@link AllocSegmentHead} references a
 * {@link AllocSegmentBody}. The {@link AllocSegmentHead} manages the
 * {@link Sequence} regarding the allocation length whereas the
 * {@link AllocSegmentBody} manages the decorated {@link Segment} by harnessing
 * the allocation length provided by the {@link AllocSegmentHead}.
 *
 * @param <DECORATEE> The type of the {@link Segment} decoratee.
 */
public class AllocSegmentBody<DECORATEE extends Section> extends AbstractTransmissionDecorator<DECORATEE> implements Segment, AllocLengthProperty {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the decorator with the given decoratee and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the decoratee's length a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the decoratee's length.
	 * 
	 * @param aDecoratee The decoratee used for this decorator.
	 */
	public AllocSegmentBody( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		if ( _value > 0 ) {
			_decoratee.fromTransmission( aSequence, aOffset, _value );
		}
		return aOffset + _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( _value > 0 ) {
			_decoratee.receiveFrom( new BoundedInputStream( aInputStream, _value ), _value, aReturnStream );
		}
	}

	/**
	 * Retrieves the allocated length.
	 * 
	 * @return The allocated length.
	 */
	@Override
	public int getAllocLength() {
		return _value;
	}

	/**
	 * Sets the allocated length.
	 * 
	 * @param aValue The allocated length.
	 */
	@Override
	public void setAllocLength( int aValue ) {
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getClass(), toSequence(), getLength(), "An allocation decorator body referencing a decoratee and managing the length of the decoratee (provided by the according head) in bytes.", _decoratee != null ? _decoratee.toSchema() : null );
	}
}
