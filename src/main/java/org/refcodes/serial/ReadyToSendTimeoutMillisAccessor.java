// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a RTS ("ready-to-send") timeout in milliseconds
 * property.
 */
public interface ReadyToSendTimeoutMillisAccessor {

	/**
	 * The RTS ("ready-to-send") timeout attribute in milliseconds.
	 * 
	 * @return An integer with the RTS ("ready-to-send") timeout in
	 *         milliseconds.
	 */
	long getReadyToSendTimeoutMillis();

	/**
	 * Provides a mutator for a RTS ("ready-to-send") timeout in milliseconds
	 * property.
	 */
	public interface ReadyToSendTimeoutMillisMutator {

		/**
		 * The RTS ("ready-to-send") timeout attribute in milliseconds.
		 * 
		 * @param aReadyToSendTimeoutMillis An integer with the RTS
		 *        ("ready-to-send") timeout in milliseconds.
		 */
		void setReadyToSendTimeoutMillis( long aReadyToSendTimeoutMillis );
	}

	/**
	 * Provides a builder method for a the RTS ("ready-to-send") timeout
	 * property returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToSendTimeoutMillisBuilder<B extends ReadyToSendTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the RTS ("ready-to-send") timeout property.
		 * 
		 * @param aReadyToSendTimeoutMillis The RTS ("ready-to-send") timeout in
		 *        milliseconds to be stored by the RTS timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToSendTimeoutMillis( long aReadyToSendTimeoutMillis );
	}

	/**
	 * Provides a RTS ("ready-to-send") timeout in milliseconds property.
	 */
	public interface ReadyToSendTimeoutMillisProperty extends ReadyToSendTimeoutMillisAccessor, ReadyToSendTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setReadyToSendTimeoutMillis(long)} and returns the very same
		 * value (getter).
		 * 
		 * @param aReadyToSendTimeoutMillis The long to set (via
		 *        {@link #setReadyToSendTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letReadyToSendTimeoutMillis( long aReadyToSendTimeoutMillis ) {
			setReadyToSendTimeoutMillis( aReadyToSendTimeoutMillis );
			return aReadyToSendTimeoutMillis;
		}
	}
}
