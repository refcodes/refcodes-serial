// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link Sequence} property.
 */
public interface SequenceAccessor {

	/**
	 * Retrieves the value from the {@link Sequence} property.
	 * 
	 * @return The {@link Sequence} stored by the {@link Sequence} property.
	 */
	Sequence getSequence();

	/**
	 * Provides a mutator for a segment property.
	 */
	public interface SequenceMutator {

		/**
		 * Sets the segment for the segment property.
		 * 
		 * @param aSequence The segment to be stored by the segment property.
		 */
		void setSequence( Sequence aSequence );
	}

	/**
	 * Provides a builder method for a segment property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SequenceBuilder<B extends SequenceBuilder<B>> {

		/**
		 * Sets the {@link Sequence} for the {@link Sequence} property.
		 * 
		 * @param aSequence The {@link Sequence} to be stored by the
		 *        {@link Sequence} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSequence( Sequence aSequence );
	}

	/**
	 * Provides a segment property.
	 */
	public interface SequenceProperty extends SequenceAccessor, SequenceMutator {

		/**
		 * Sets the given {@link Sequence} (setter) as of
		 * {@link #setSequence(Sequence)} and returns the very same
		 * {@link Sequence} (getter). This method stores and passes through the
		 * given {@link Sequence}, which is very useful for builder APIs.
		 * 
		 * @param aSequence The {@link Sequence} to set (via
		 *        {@link #setSequence(Sequence)}).
		 * 
		 * @return Returns {@link Sequence} passed for it to be used in
		 *         conclusive processing steps.
		 */
		default Sequence letSequence( Sequence aSequence ) {
			setSequence( aSequence );
			return aSequence;
		}
	}
}
