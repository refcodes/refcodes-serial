// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.serial.MagicBytesAccessor.MagicBytesProperty;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;

/**
 * Magic bytes are usually found (somewhere) at the beginning of a file or a
 * stream. A {@link AbstractMagicBytesTransmission} provides the base
 * functionality to manage magic bytes.
 */
public abstract class AbstractMagicBytesTransmission implements PayloadTransmission<byte[]>, TransmissionMixin, MagicBytesProperty, MagicBytesLengthAccessor, EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String MAGIC_BYTES = "MAGIC_BYTES";
	public static final String MAGIC_BYTES_TEXT = "MAGIC_BYTES_TEXT";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected byte[] _magicBytes = null;
	protected int _magicBytesLength;
	protected String _charset = TransmissionMetrics.DEFAULT_ENCODING.name();
	protected String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractMagicBytesTransmission( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		_magicBytesLength = aTransmissionMetrics.getMagicBytesLength();
		_alias = aAlias;
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractMagicBytesTransmission( String aAlias, String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		_magicBytes = aMagicBytes.getBytes( aTransmissionMetrics.getEncoding() );
		_magicBytesLength = _magicBytes != null ? _magicBytes.length : aTransmissionMetrics.getMagicBytesLength();
		_alias = aAlias;
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractMagicBytesTransmission( String aAlias, byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		_magicBytes = aMagicBytes;
		_magicBytesLength = _magicBytes != null ? _magicBytes.length : aTransmissionMetrics.getMagicBytesLength();
		_alias = aAlias;
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} for magic bytes of
	 * the given length.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public AbstractMagicBytesTransmission( String aAlias, int aMagicBytesLength ) {
		_magicBytesLength = aMagicBytesLength;
		_alias = aAlias;
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public AbstractMagicBytesTransmission( String aAlias, String aMagicBytes ) {
		this( aAlias, aMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public AbstractMagicBytesTransmission( String aAlias, String aMagicBytes, Charset aCharset ) {
		this( aAlias, aMagicBytes.getBytes( aCharset ) );
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();
	}

	/**
	 * Constructs an {@link AbstractMagicBytesTransmission} with the according
	 * magic bytes.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 */
	public AbstractMagicBytesTransmission( String aAlias, byte[] aMagicBytes ) {
		_magicBytes = aMagicBytes;
		_magicBytesLength = aMagicBytes.length;
		_alias = aAlias;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the magic bytes as of {@link #setMagicBytes(byte[])}. {@inheritDoc}
	 */
	@Override
	public void setPayload( byte[] aValue ) {
		_magicBytes = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * Returns the magic bytes as stored by this instance.
	 * 
	 * @return The according magic bytes.
	 */
	@Override
	public byte[] getMagicBytes() {
		return _magicBytes;
	}

	/**
	 * Sets the magic bytes to be stored by this instance.
	 * 
	 * @param aMagicBytes The according magic bytes to store.
	 */
	@Override
	public void setMagicBytes( byte[] aMagicBytes ) {
		_magicBytes = aMagicBytes;
		_magicBytesLength = aMagicBytes.length;
	}

	/**
	 * Sets the magic bytes from the provided {@link String} to be stored by
	 * this instance (uses the {@link TransmissionMetrics#DEFAULT_ENCODING}) for
	 * byte conversion).
	 * 
	 * @param aMagicBytes The String from which to copy the according magic
	 *        bytes from.
	 */
	@Override
	public void setMagicBytes( String aMagicBytes ) {
		setMagicBytes( aMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
	}

	/**
	 * Sets the magic bytes from the provided {@link String} to be stored by
	 * this instance.
	 * 
	 * @param aMagicBytes The String from which to copy the according magic
	 *        bytes from.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	@Override
	public void setMagicBytes( String aMagicBytes, Charset aCharset ) {
		setMagicBytes( aMagicBytes.getBytes( aCharset ) );
	}

	/**
	 * Retrieves the {@link String} representation of the given magic bytes
	 * using the latest provided {@link Charset} for conversion or the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING} in case no {@link Charset}
	 * has been provided.
	 * 
	 * @return The {@link String} representation of the magic bytes.
	 */
	public String toMagicBytesString() {
		return new String( _magicBytes, _charset != null ? getEncoding() : TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Retrieves the {@link String} representation of the given magic bytes
	 * using the provided {@link Charset} for conversion or the
	 * {@link TransmissionMetrics#DEFAULT_ENCODING} in case no {@link Charset}
	 * has been provided.
	 *
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 * 
	 * @return The {@link String} representation of the magic bytes.
	 */
	public String toMagicBytesString( String aCharset ) {
		return new String( _magicBytes, aCharset != null ? Charset.forName( aCharset ) : ( _charset != null ? getEncoding() : TransmissionMetrics.DEFAULT_ENCODING ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _magicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMagicBytesLength() {
		return _magicBytesLength;
	}

	/**
	 * Returns the magic bytes as of {@link #getMagicBytes()}. {@inheritDoc}
	 */
	@Override
	public byte[] getPayload() {
		return _magicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_magicBytes = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment representing magic bytes." );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES_TEXT, toMagicBytesString() );
		return theSchema;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset != null ? Charset.forName( _charset ) : TransmissionMetrics.DEFAULT_ENCODING;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", magicBytes=" + NumericalUtility.toHexString( ", ", _magicBytes ) + ", magicBytesLength=" + _magicBytesLength + ", charset=" + _charset + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode( _magicBytes );
		result = prime * result + _magicBytesLength;
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractMagicBytesTransmission other = (AbstractMagicBytesTransmission) obj;
		if ( !Arrays.equals( _magicBytes, other._magicBytes ) ) {
			return false;
		}
		if ( _magicBytesLength != other._magicBytesLength ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		final SimpleTypeMapBuilderImpl theBuilder = new SimpleTypeMapBuilderImpl();
		theBuilder.insertTo( _alias, _magicBytes );
		return theBuilder;
	}
}
