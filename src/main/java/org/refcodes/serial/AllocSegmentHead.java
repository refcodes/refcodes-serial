// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.mixin.BodyAccessor.BodyProperty;
import org.refcodes.numerical.Endianess;

/**
 * An {@link AllocSegmentHead} provides the allocation length for a
 * {@link AllocSegmentBody}. Therefore the {@link AllocSegmentHead} references a
 * {@link AllocSegmentBody}. The {@link AllocSegmentHead} manages the
 * {@link Sequence} regarding the allocation length whereas the
 * {@link AllocSegmentBody} manages the decorated {@link Segment} by harnessing
 * the allocation length provided by the {@link AllocSegmentHead}.
 */
public class AllocSegmentHead extends AbstractReferenceeLengthSegment<AllocSegmentBody<?>> implements BodyProperty<AllocSegmentBody<?>> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	protected AllocSegmentHead( TransmissionMetrics aTransmissionMetrics ) {
		super( aTransmissionMetrics );
	}

	/**
	 * Constructs the head with the given body. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aBody The body referenced by this head.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AllocSegmentHead( AllocSegmentBody<?> aBody, TransmissionMetrics aTransmissionMetrics ) {
		super( aBody, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the body's length.
	 */
	protected AllocSegmentHead() {}

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	protected AllocSegmentHead( Endianess aEndianess ) {
		super( aEndianess );
	}

	/**
	 * Constructs an empty head with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	protected AllocSegmentHead( int aLengthWidth, Endianess aEndianess ) {
		super( aLengthWidth, aEndianess );
	}

	/**
	 * Constructs an empty head with the given number of bytes used to specify
	 * the body's length and a {@link TransmissionMetrics#DEFAULT_ENDIANESS}
	 * endian representation of the body's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	protected AllocSegmentHead( int aLengthWidth ) {
		super( aLengthWidth );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs the head with the given body and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length and the provided {@link Endianess} representation of
	 * the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public AllocSegmentHead( AllocSegmentBody<?> aBody, Endianess aEndianess ) {
		super( aBody, aEndianess );
	}

	/**
	 * Constructs the head with the given body and with the given number of
	 * bytes used to specify the body's length and the provided
	 * {@link Endianess} representation of the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public AllocSegmentHead( AllocSegmentBody<?> aBody, int aLengthWidth, Endianess aEndianess ) {
		super( aBody, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the head with the given body and with the given number of
	 * bytes used to specify the body's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the body's length.
	 *
	 * @param aBody The body referenced by this head.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public AllocSegmentHead( AllocSegmentBody<?> aBody, int aLengthWidth ) {
		super( aBody, aLengthWidth );
	}

	/**
	 * Constructs the head with the given body and a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the body's length a {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian
	 * representation of the body's length.
	 * 
	 * @param aBody The body referenced by this head.
	 */
	public AllocSegmentHead( AllocSegmentBody<?> aBody ) {
		super( aBody );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		aOffset = super.fromTransmission( aSequence, aOffset );
		_referencee.setAllocLength( _allocLength );
		return aOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		super.receiveFrom( aInputStream, aReturnStream );
		_referencee.setAllocLength( _allocLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getClass(), toSequence(), getLength(), "An allocation decorator head referencing an according body managing the length of the decoratee of the body (referenced by the head) in bytes." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AllocSegmentBody<?> getBody() {
		return _referencee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBody( AllocSegmentBody<?> aBody ) {
		_referencee = aBody;
	}
}
