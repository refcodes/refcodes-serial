// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.OutputStream;

/**
 * The {@link SequenceOutputStream} constructs an {@link OutputStream} from a
 * {@link SerialTransmitter}.
 */
public class SequenceOutputStream extends OutputStream implements SequenceAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Sequence _sequence;
	boolean _isClosed = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Uses the provided {@link Sequence} to provide {@link OutputStream}
	 * functionality.
	 */
	public SequenceOutputStream() {
		_sequence = new ByteArraySequence();
	}

	/**
	 * Uses the provided {@link Sequence} to provide {@link OutputStream}
	 * functionality.
	 * 
	 * @param aSequence The {@link Sequence} to use.
	 */
	public SequenceOutputStream( Sequence aSequence ) {
		_sequence = aSequence;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_isClosed = true;
		super.close();
		// _sequence = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		if ( _isClosed ) {
			throw new IOException( "The stream has already been closed!" );
		}
		_sequence.append( aBytes, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int aByte ) throws IOException {
		if ( _isClosed ) {
			throw new IOException( "The stream has already been closed!" );
		}
		_sequence.append( (byte) aByte );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence getSequence() {
		return _sequence;
	}
}