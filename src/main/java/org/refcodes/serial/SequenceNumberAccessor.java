// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a sequence number property.
 */
public interface SequenceNumberAccessor {

	/**
	 * Retrieves the sequence number from the sequence number property.
	 * 
	 * @return The sequence number stored by the sequence number property.
	 */
	int getSequenceNumber();

	/**
	 * Provides a mutator for a sequence number property.
	 */
	public interface SequenceNumberMutator {

		/**
		 * Sets the sequence number for the sequence number property.
		 * 
		 * @param aSequenceNumber The sequence number to be stored by the
		 *        sequence number property.
		 */
		void setSequenceNumber( int aSequenceNumber );
	}

	/**
	 * Provides a builder method for a sequence number property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SequenceNumberBuilder<B extends SequenceNumberBuilder<B>> {

		/**
		 * Sets the sequence number for the sequence number property.
		 * 
		 * @param aSequenceNumber The sequence number to be stored by the
		 *        sequence number property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSequenceNumber( int aSequenceNumber );
	}

	/**
	 * Provides a sequence number property.
	 */
	public interface SequenceNumberProperty extends SequenceNumberAccessor, SequenceNumberMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setSequenceNumber(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aSequenceNumber The integer to set (via
		 *        {@link #setSequenceNumber(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letSequenceNumber( int aSequenceNumber ) {
			setSequenceNumber( aSequenceNumber );
			return aSequenceNumber;
		}
	}
}
