// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoTimeout;
import org.refcodes.io.SkipAvailableInputStream;
import org.refcodes.io.TimeoutInputStream;

/**
 * The {@link ReadyToReceiveSegmentDecorator} class implements a decorator
 * providing {@link ReadyToReceiveTransmission} functionality for a
 * {@link Segment}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public class ReadyToReceiveSegmentDecorator<DECORATEE extends Segment> extends AbstractReadyToReceiveTransmissionDecorator<DECORATEE> implements Segment, DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ReadyToReceiveSegmentDecorator( Builder<DECORATEE> aBuilder ) {
		this( aBuilder.decoratee, aBuilder.enquiryStandbyTimeInMs, aBuilder.readyToReceiveMagicBytes, aBuilder.readyToReceiveRetryNumber, aBuilder.readyToReceiveTimeoutInMs, aBuilder.readyToReceiveSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, byte aReadyToReceiveMagicBytes ) {
		super( aDecoratee, aReadyToReceiveMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		super( aDecoratee, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes, int aReadyToReceiveRetryNumber ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveRetryNumber );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes, long aReadyToReceiveTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte aReadyToReceiveMagicBytes ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToReceiveRetryNumber, long aReadyToReceiveTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveRetryNumber, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToReceiveTimeoutInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToReceiveTimeoutInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aReadyToReceiveTimeoutInMs, SegmentPackager aReadyToReceiveSegmentPackager ) {
		super( aDecoratee, aReadyToReceiveTimeoutInMs, aReadyToReceiveSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		super( aDecoratee, aEnquiryStandbyTimeInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ReadyToReceiveSegmentDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return _decoratee.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.receiveFrom( aInputStream, aReturnStream );
			return;
		}
		else {
			final RetryCounter theRetries = new RetryCounter( _readyToReceiveRetryNumber, _readyToReceiveTimeoutInMs );
			Exception eException = null;
			final TimeoutInputStream theTimeoutInputStream = SerialUtility.createTimeoutInputStream( aInputStream, _readyToReceiveTimeoutInMs );
			@SuppressWarnings("resource")
			final // Do not close me after done!
			SkipAvailableInputStream theSkipInputStream = new SkipAvailableInputStream( aInputStream, _readyToReceiveTimeoutInMs );
			while ( theRetries.nextRetry() ) {
				theSkipInputStream.skipAvailableWithin( IoTimeout.toTimeoutSleepLoopTimeInMs( _readyToReceiveTimeoutInMs ) ); // Clear anything before a RTS signal was sent
				eException = null;
				try {
					_readyToReceiveSegment.transmitTo( aReturnStream );
					_decoratee.receiveFrom( theTimeoutInputStream, aReturnStream );
					return;
				}
				catch ( Exception e ) {
					eException = e;
				}
			}
			if ( eException != null ) {
				throw new FlowControlRetryException( _readyToReceiveRetryNumber, _readyToReceiveTimeoutInMs, "Aborting after <" + _readyToReceiveRetryNumber + "> retries with a timeout for each retry of <" + _readyToReceiveTimeoutInMs + "> milliseconds: " + eException.getMessage(), eException );
			}
			else {
				throw new FlowControlRetryException( _readyToReceiveRetryNumber, _readyToReceiveTimeoutInMs, "Aborting after <" + _readyToReceiveRetryNumber + "> retries with a timeout for each retry of <" + _readyToReceiveTimeoutInMs + "> milliseconds." );
			}
		}
	}

	/**
	 * Creates builder to build {@link ReadyToReceiveSegmentDecorator}.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return created builder
	 */
	public static <DECORATEE extends Segment> Builder<DECORATEE> builder() {
		return new Builder<>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link ReadyToReceiveSegmentDecorator} instances .
	 *
	 * @param <DECORATEE> the generic type
	 */
	public static final class Builder<DECORATEE extends Segment> implements DecorateeBuilder<DECORATEE, Builder<DECORATEE>>, EnquiryStandbyTimeMillisBuilder<Builder<DECORATEE>>, ReadyToReceiveTimeoutMillisBuilder<Builder<DECORATEE>>, ReadyToReceiveMagicBytesBuilder<Builder<DECORATEE>>, ReadyToReceiveSegmentPackagerBuilder<Builder<DECORATEE>>, ReadyToReceiveRetryNumberBuilder<Builder<DECORATEE>> {

		private DECORATEE decoratee;
		private long enquiryStandbyTimeInMs = TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS;
		private long readyToReceiveTimeoutInMs = TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS;
		private byte[] readyToReceiveMagicBytes = TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES;
		private SegmentPackager readyToReceiveSegmentPackager;
		private int readyToReceiveRetryNumber = TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER;

		private Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withDecoratee( DECORATEE aDecoratee ) {
			decoratee = aDecoratee;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withEnquiryStandbyTimeMillis( long aEnquiryStandbyTimeInMs ) {
			enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutInMs ) {
			readyToReceiveTimeoutInMs = aReadyToReceiveTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToReceiveMagicBytes( byte[] aReadyToReceiveMagicBytes ) {
			readyToReceiveMagicBytes = aReadyToReceiveMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToReceiveSegmentPackager( SegmentPackager aReadyToReceiveSegmentPackager ) {
			readyToReceiveSegmentPackager = aReadyToReceiveSegmentPackager;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withReadyToReceiveRetryNumber( int aReadyToReceiveRetryNumber ) {
			readyToReceiveRetryNumber = aReadyToReceiveRetryNumber;
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return built class
		 */
		public ReadyToReceiveSegmentDecorator<DECORATEE> build() {
			return new ReadyToReceiveSegmentDecorator<>( this );
		}
	}
}
