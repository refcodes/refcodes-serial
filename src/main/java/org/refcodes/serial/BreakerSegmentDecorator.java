// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.mixin.Resetable;

/**
 * The {@link BreakerSegmentDecorator} is a {@link DecoratorSegment} build to
 * test error detection and error correction {@link Transmission}
 * implementations such as the {@link CrcSegmentDecorator} or the
 * {@link StopAndWaitSegmentDecorator}. To achieve this, the
 * {@link BreakerSegmentDecorator} can be configured to "break" deserialization
 * for a given number of times by intercepting into the according
 * {@link Transmission}'s methods and throwing a
 * {@link TransmissionSequenceException}.
 * 
 * @param <DECORATEE> The {@link Segment} type describing the {@link Segment}
 *        subclass to be "enriched" with breaking functionality.
 */
public class BreakerSegmentDecorator<DECORATEE extends Segment> extends AbstractTransmissionDecorator<DECORATEE> implements DecoratorSegment<DECORATEE>, Resetable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger( BreakerSegmentDecorator.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String BREAK_NUMBER = "BREAK_NUMBER";
	public static final String BREAK_COUNT = "BREAK_COUNT";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _breakNumber;
	protected int _breakCount = 0;
	protected long _startTime;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link BreakerSegmentDecorator}.
	 */
	protected BreakerSegmentDecorator() {}

	/**
	 * Constructs a {@link BreakerSegmentDecorator} instance with the given
	 * decoratee breaking deserialization of the decorated segments by the given
	 * number of times.fter the total number of breaking the decoratee has been
	 * reached, the decorator behaves transparent (it just delegates without
	 * breaking the decoratee any more). This is good to see if a retry
	 * mechanism works when using some kind of error correction segment.
	 * 
	 * @param aDecoratee The decoratee to be contained by this facade.
	 * @param aBreakNumber The number of times to break deserialization.
	 */
	public BreakerSegmentDecorator( DECORATEE aDecoratee, int aBreakNumber ) {
		super( aDecoratee );
		_breakNumber = aBreakNumber;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		if ( _breakCount == 0 ) {
			_startTime = System.currentTimeMillis();
		}
		if ( _breakCount < _breakNumber ) {
			_breakCount++;
			LOGGER.log( Level.INFO, getClass().getSimpleName() + ".fromTransmission(): Reached break <" + _breakCount + "> out of <" + _breakNumber + ">" + ( _breakCount == 1 ? "" : " after " + ( System.currentTimeMillis() - _startTime ) / 1000 + " seconds" ) + "..." );
			throw new TransmissionSequenceException( aSequence, "Breaking reception occurrence <" + _breakCount + "> out of <" + _breakNumber + "> occurrences." );
		}
		return _decoratee.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( _breakCount == 0 ) {
			_startTime = System.currentTimeMillis();
		}
		if ( _breakCount < _breakNumber ) {
			_breakCount++;
			LOGGER.log( Level.INFO, getClass().getSimpleName() + ".receiveFrom(): Reached break <" + _breakCount + "> out of <" + _breakNumber + ">" + ( _breakCount == 1 ? "" : " after " + ( System.currentTimeMillis() - _startTime ) / 1000 + " seconds" ) + "..." );
			throw new TransmissionException( "Breaking reception occurrence <" + _breakCount + "> out of <" + _breakNumber + "> occurrences." );
		}
		_decoratee.receiveFrom( aInputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A segment decorator for testing purpose enriching the encapsulated segment with a serialization breaker.", getDecoratee().toSchema() );
		theSchema.put( BREAK_NUMBER, _breakNumber );
		theSchema.put( BREAK_COUNT, _breakCount );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_breakCount = 0;
	}
}
