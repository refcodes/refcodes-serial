// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a acknowledge timeout in milliseconds property.
 */
public interface AcknowledgeTimeoutMillisAccessor {

	/**
	 * The acknowledge timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	long getAcknowledgeTimeoutMillis();

	/**
	 * Provides a mutator for a acknowledge timeout in milliseconds property.
	 */
	public interface AcknowledgeTimeoutMillisMutator {

		/**
		 * The acknowledge timeout attribute in milliseconds.
		 * 
		 * @param aAcknowledgeTimeoutMillis An integer with the acknowledge
		 *        timeout in milliseconds.
		 */
		void setAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutMillis );
	}

	/**
	 * Provides a builder method for the acknowledge timeout property returning
	 * the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AcknowledgeTimeoutMillisBuilder<B extends AcknowledgeTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the acknowledge timeout property.
		 * 
		 * @param aAcknowledgeTimeoutMillis The acknowledge timeout in
		 *        milliseconds to be stored by the acknowledge timeout property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutMillis );
	}

	/**
	 * Provides a acknowledge timeout in milliseconds property.
	 */
	public interface AcknowledgeTimeoutMillisProperty extends AcknowledgeTimeoutMillisAccessor, AcknowledgeTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setAcknowledgeTimeoutMillis(long)} and returns the very same
		 * value (getter).
		 * 
		 * @param aAcknowledgeTimeoutMillis The long to set (via
		 *        {@link #setAcknowledgeTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letAcknowledgeTimeoutMillis( long aAcknowledgeTimeoutMillis ) {
			setAcknowledgeTimeoutMillis( aAcknowledgeTimeoutMillis );
			return aAcknowledgeTimeoutMillis;
		}
	}
}
