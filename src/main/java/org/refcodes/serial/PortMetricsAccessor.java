// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link PortMetrics} property.
 * 
 * @param <PM> The actual {@link PortMetrics} type to use.
 */
public interface PortMetricsAccessor<PM extends PortMetrics> {

	/**
	 * Retrieves the {@link PortMetrics} from the {@link PortMetrics} property.
	 * 
	 * @return The {@link PortMetrics} stored by the {@link PortMetrics}
	 *         property.
	 */
	PM getPortMetrics();

	/**
	 * Provides a mutator for a {@link PortMetrics} property.
	 * 
	 * @param <PM> The actual {@link PortMetrics} type to use.
	 */
	public interface PortMetricsMutator<PM extends PortMetrics> {

		/**
		 * Sets the {@link PortMetrics} for the {@link PortMetrics} property.
		 * 
		 * @param aPortMetrics The {@link PortMetrics} to be stored by the
		 *        {@link PortMetrics} property.
		 */
		void setPortMetrics( PM aPortMetrics );
	}

	/**
	 * Provides a builder method for a {@link PortMetrics} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <PM> The actual {@link PortMetrics} type to use.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PortMetricsBuilder<PM extends PortMetrics, B extends PortMetricsBuilder<PM, B>> {

		/**
		 * Sets the {@link PortMetrics} for the {@link PortMetrics} property.
		 * 
		 * @param aPortMetrics The {@link PortMetrics} to be stored by the
		 *        {@link PortMetrics} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPortMetrics( PM aPortMetrics );
	}

	/**
	 * Provides a {@link PortMetrics} property.
	 * 
	 * @param <PM> The actual {@link PortMetrics} type to use.
	 */
	public interface PortMetricsProperty<PM extends PortMetrics> extends PortMetricsAccessor<PM>, PortMetricsMutator<PM> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setPortMetrics(PortMetrics)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPortMetrics The value to set (via
		 *        {@link #setPortMetrics(PortMetrics)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PM letPortMetrics( PM aPortMetrics ) {
			setPortMetrics( aPortMetrics );
			return aPortMetrics;
		}
	}
}
