// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.ConcatenateMode;

/**
 * Provides an accessor for a {@link ConcatenateMode} property.
 */
public interface SequenceNumberConcatenateModeAccessor {

	/**
	 * Retrieves the {@link ConcatenateMode} from the sequence number
	 * {@link ConcatenateMode} property.
	 * 
	 * @return The {@link ConcatenateMode} stored by the sequence number
	 *         {@link ConcatenateMode} property.
	 */
	ConcatenateMode getSequenceNumberConcatenateMode();

	/**
	 * Provides a mutator for a sequence number {@link ConcatenateMode}
	 * property.
	 */
	public interface SequenceNumberConcatenateModeMutator {

		/**
		 * Sets the {@link ConcatenateMode} for the {@link ConcatenateMode}
		 * property.
		 * 
		 * @param aSequenceNumberConcatenateMode The {@link ConcatenateMode} to
		 *        be stored by the sequence number {@link ConcatenateMode}
		 *        property.
		 */
		void setSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode );
	}

	/**
	 * Provides a builder method for a sequence number {@link ConcatenateMode}
	 * property returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SequenceNumberConcatenateModeBuilder<B extends SequenceNumberConcatenateModeBuilder<B>> {

		/**
		 * Sets the {@link ConcatenateMode} for the sequence number
		 * {@link ConcatenateMode} property.
		 * 
		 * @param aSequenceNumberConcatenateMode The {@link ConcatenateMode} to
		 *        be stored by the sequence number {@link ConcatenateMode}
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode );
	}

	/**
	 * Provides a sequence number {@link ConcatenateMode} property.
	 */
	public interface SequenceNumberConcatenateModeProperty extends SequenceNumberConcatenateModeAccessor, SequenceNumberConcatenateModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ConcatenateMode}
		 * (setter) as of
		 * {@link #setSequenceNumberConcatenateMode(ConcatenateMode)} and
		 * returns the very same value (getter).
		 * 
		 * @param aSequenceNumberConcatenateMode The {@link ConcatenateMode} to
		 *        set (via
		 *        {@link #setSequenceNumberConcatenateMode(ConcatenateMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ConcatenateMode letSequenceNumberConcatenateMode( ConcatenateMode aSequenceNumberConcatenateMode ) {
			setSequenceNumberConcatenateMode( aSequenceNumberConcatenateMode );
			return aSequenceNumberConcatenateMode;
		}
	}
}
