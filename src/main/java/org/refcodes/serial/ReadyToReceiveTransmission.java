// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * The {@link ReadyToReceiveTransmission} interface defines functionality for
 * achieving a RTR "software" handshake between a receiver having the active RTR
 * ("ready-to-receive") role and a transmitter being CTS ("clear-to-send") where
 * the transmitter waits for a RTR signal from the receiver (the receiver
 * signals to be ready for receiving data by issuing an RTR signal to the
 * transmitter): <code>         
 *                     TRANSMITTER       RECEIVER
 *                           |               |   
 * Wait till enquiry-timeout |               | Send RTR magic-bytes             
 * for RTR magic-bytes       |←-----RTR------| Try RTR retry-number of times
 *                           |               | Wait till RTR retry-timeout/retry
 * Send payload upon RTR     |               | 
 * magic-bytes or break-out  |----PAYLOAD---→| Receive payload
 * upon enquiry-timeout      |               |
 * </code> Handshake specific signals are only transmitted through the return
 * channel, the data channel is only used indirectly for handshaking by
 * transporting the payload data in response to the return channel signals. No
 * handshake specific signals are transmitted through the data channel, so in
 * case there is no return channel, the {@link ReadyToReceiveTransmission}
 * behaves invisible.
 */
public interface ReadyToReceiveTransmission extends EnquiryStandbyTimeMillisAccessor, ReadyToReceiveMagicBytesAccessor, ReadyToReceiveRetryNumberAccessor, ReadyToReceiveSegmentPackagerAccessor, ReadyToReceiveTimeoutMillisAccessor, Transmission {}
