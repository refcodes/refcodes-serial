// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.LengthAccessor;
import org.refcodes.mixin.OffsetAccessor;

/**
 * Thrown in case a given {@link Sequence} cannot be processed.
 */
public class TransmissionSequenceException extends TransmissionException implements SequenceAccessor, LengthAccessor, OffsetAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Sequence _sequence;
	private int _offset;
	private int _length;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, String message, Throwable cause ) {
		this( aSequence, 0, message, cause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, String message ) {
		this( aSequence, 0, message );

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, Throwable cause ) {
		this( aSequence, 0, cause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, String aMessage, Throwable aCause, String aErrorCode ) {
		this( aSequence, 0, aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, String aMessage, String aErrorCode ) {
		this( aSequence, 0, aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, Throwable aCause, String aErrorCode ) {
		this( aSequence, 0, aCause, aErrorCode );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, String message, Throwable cause ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, message, cause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, String message ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, message );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, Throwable cause ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, cause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, String aMessage, Throwable aCause, String aErrorCode ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, String aMessage, String aErrorCode ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, Throwable aCause, String aErrorCode ) {
		this( aSequence, aOffset, aSequence != null ? aSequence.getLength() : -1, aCause, aErrorCode );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, String message, Throwable cause ) {
		super( message, cause );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, String message ) {
		super( message );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, Throwable cause ) {
		super( cause );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aOffset The {@link Sequence} offset regarding the problem.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSequence The involved {@link Sequence}.
	 * @param aLength The length of the erroneous {@link Sequence}.
	 */
	public TransmissionSequenceException( Sequence aSequence, int aOffset, int aLength, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_sequence = aSequence;
		_offset = aOffset;
		_length = aLength;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence getSequence() {
		return _sequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getOffset() {
		return _offset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _length;
	}
}
