// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link AssertMagicBytesSegmentDecorator} extends the
 * {@link MagicBytesSegmentDecorator} and enforces the configured magic bytes to
 * match the received magic bytes (as of {@link #fromTransmission(Sequence)} and
 * {@link #receiveFrom(java.io.InputStream, java.io.OutputStream)} or the like).
 * In case the assertion of the configured magic bytes fails during receiving,
 * then a {@link BadMagicBytesException} or
 * {@link BadMagicBytesSequenceException} is thrown.
 * 
 * @param <DECORATEE> The {@link Segment} type describing the {@link Segment}
 *        subclass decoratee.
 */
public class AssertMagicBytesSegmentDecorator<DECORATEE extends Segment> extends MagicBytesSegmentDecorator<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( DECORATEE aDecoratee, byte... aMagicBytes ) {
		super( aDecoratee, aMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		super( aDecoratee, aMagicBytes, aCharset );
	}

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( DECORATEE aDecoratee, String aMagicBytes ) {
		super( aDecoratee, aMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, byte... aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aDecoratee, aMagicBytes, aCharset );
	}

	/**
	 * {@inheritDoc}
	 */
	public AssertMagicBytesSegmentDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte[] theMagicBytes = aSequence.toBytes( aOffset, _magicBytesLength );
		if ( !Arrays.equals( _magicBytes, theMagicBytes ) ) {
			throw new BadMagicBytesSequenceException( theMagicBytes, aSequence, "The received magic bytes " + VerboseTextBuilder.asString( theMagicBytes ) + " do not match the expected magic bytes " + VerboseTextBuilder.asString( _magicBytes ) + "!" );
		}
		return getDecoratee().fromTransmission( aSequence, aOffset + _magicBytesLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		byte[] theMagicBytes = new byte[_magicBytesLength];
		// Throw <BadMagicBytesException> as early as possible to prevent reading too many bytes |-->
		for ( int i = 0; i < _magicBytesLength; i++ ) {
			aInputStream.read( theMagicBytes, i, 1 );
			if ( _magicBytes[i] != theMagicBytes[i] ) {
				theMagicBytes = new ByteArraySequence( theMagicBytes, 0, i + 1 ).toBytes();
				throw new BadMagicBytesException( theMagicBytes, "The received magic bytes " + VerboseTextBuilder.asString( theMagicBytes ) + " do not match the expected magic bytes " + VerboseTextBuilder.asString( _magicBytes ) + "!" );
			}
		}
		// Throw <BadMagicBytesException> as early as possible to prevent reading too many bytes <--|
		getDecoratee().receiveFrom( aInputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment decorator asserting and prefixing the encapsulated segment with configured magic bytes." );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES_TEXT, toMagicBytesString() );
		return theSchema;
	}
}
