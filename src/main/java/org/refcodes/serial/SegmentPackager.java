// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.factory.Packager;

/**
 * The {@link SegmentPackager} interface defines methods to package (wrap) a
 * {@link Segment} e.g. with a {@link CrcSegmentDecorator}.
 */
public interface SegmentPackager extends Packager<Segment, Segment, RuntimeException> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	Segment toPackaged( Segment aUnpacked );

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Dummy implementation of a {@link SegmentPackager} just passing through
	 * provided {@link Segment} instances.
	 */
	public static class DummySegmentPackager implements SegmentPackager {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Segment toPackaged( Segment aUnpacked ) {
			return aUnpacked;
		}
	}
}
