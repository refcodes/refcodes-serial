// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.exception.TimeoutIOException;
import org.refcodes.io.SkipAvailableInputStream;
import org.refcodes.io.TimeoutInputStream;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * The {@link AbstractReadyToSendTransmissionDecorator} class implements a
 * decorator providing {@link ReadyToSendTransmission} functionality for a
 * {@link Transmission}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public abstract class AbstractReadyToSendTransmissionDecorator<DECORATEE extends Transmission> implements ReadyToSendTransmission, TransmissionMixin, DecorateeAccessor<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String RTS_MAGIC_BYTES = "RTS_MAGIC_BYTES";
	public static final String RTS_TIMEOUT_IN_MS = "RTS_TIMEOUT_IN_MS";
	public static final String RTS_RETRY_NUMBER = "RTS_RETRY_NUMBER";
	public static final String CTS_MAGIC_BYTES = "CTS_MAGIC_BYTES";
	public static final String CTS_TIMEOUT_IN_MS = "CTS_TIMEOUT_IN_MS";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected DECORATEE _decoratee = null;
	protected long _enquiryStandbyTimeInMs;
	protected long _clearToSendTimeoutInMs;
	protected byte[] _clearToSendMagicBytes;
	protected MagicBytesSegment _clearToSendMagicBytesSegment;
	protected Segment _clearToSendSegment;
	protected SegmentPackager _clearToSendSegmentPackager;
	protected long _readyToSendTimeoutInMs;
	protected byte[] _readyToSendMagicBytes;
	protected SegmentPackager _readyToSendSegmentPackager;
	protected int _readyToSendRetryNumber;
	protected MagicBytesSegment _readyToSendMagicBytesSegment;
	protected Segment _readyToSendSegment;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract ready to send transmission decorator.
	 */
	protected AbstractReadyToSendTransmissionDecorator() {}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according control-flow decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aDecoratee The decoratee to be wrapped by the control-flow
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		this( aDecoratee, aTransmissionMetrics.getEnquiryStandbyTimeMillis(), aTransmissionMetrics.getReadyToSendMagicBytes(), aTransmissionMetrics.getReadyToSendRetryNumber(), aTransmissionMetrics.getReadyToSendTimeoutMillis(), aTransmissionMetrics.getReadyToSendSegmentPackager(), aTransmissionMetrics.getClearToSendMagicBytes(), aTransmissionMetrics.getClearToSendTimeoutMillis(), aTransmissionMetrics.getClearToSendSegmentPackager() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        no timeout (blocking mode).
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToSendSegmentPackager, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, aReadyToSendRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, null, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, aReadyToSendRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToSendSegmentPackager, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, null, aClearToSendMagicBytes, aClearToSendTimeoutInMs, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );

	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, byte[] aClearToSendMagicBytes ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, null, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToSendSegmentPackager, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, byte[] aClearToSendMagicBytes ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, null, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, long aClearToSendTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, null, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, null );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, aReadyToSendRetryNumber, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_TIMEOUT_IN_MS, aReadyToSendSegmentPackager, aClearToSendMagicBytes, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_TIMEOUT_IN_MS, aClearToSendSegmentPackager );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_MAGIC_BYTES, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, TransmissionMetrics.DEFAULT_CLEAR_TO_SEND_MAGIC_BYTES, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, null, aClearToSendMagicBytes, aClearToSendTimeoutInMs, null );
	}

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, aEnquiryStandbyTimeInMs, aReadyToSendMagicBytes, TransmissionMetrics.DEFAULT_READY_TO_RECEIVE_RETRY_NUMBER, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );

	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        RTS ("ready-to-receive") to the transmitter.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ENQUIERY_STRANDBY_TIME_IN_MS, aReadyToSendMagicBytes, aReadyToSendRetryNumber, aReadyToSendTimeoutInMs, aReadyToSendSegmentPackager, aClearToSendMagicBytes, aClearToSendTimeoutInMs, aClearToSendSegmentPackager );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according flow-control decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the flow-control
	 *        decorator.
	 * @param aEnquiryStandbyTimeInMs The timeout in milliseconds to wait for an
	 *        RTS request (receiver) from the transmitter. A value of -1 denotes
	 *        the timeout being disabled.
	 * @param aReadyToSendMagicBytes The RTS characters to be used to signal a
	 *        RTS.
	 * @param aReadyToSendRetryNumber The number of retries sending an RTS over
	 *        the payload (data) channel.
	 * @param aReadyToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        CTS response (transmitter) from the receiver after signaling a RTS
	 *        ("ready-to-send") to the receiver.
	 * @param aReadyToSendSegmentPackager The RTS {@link SegmentPackager} for
	 *        packaging RTS responses.
	 * @param aClearToSendMagicBytes The CTS characters to be used to signal a
	 *        CTS.
	 * @param aClearToSendTimeoutInMs The timeout in milliseconds to wait for a
	 *        payload response (receiver) from the transmitter after signaling a
	 *        CTS ("ready-to-receive") to the transmitter.
	 * @param aClearToSendSegmentPackager The CTS {@link SegmentPackager} for
	 *        packaging CTS responses.
	 */
	public AbstractReadyToSendTransmissionDecorator( DECORATEE aDecoratee, long aEnquiryStandbyTimeInMs, byte[] aReadyToSendMagicBytes, int aReadyToSendRetryNumber, long aReadyToSendTimeoutInMs, SegmentPackager aReadyToSendSegmentPackager, byte[] aClearToSendMagicBytes, long aClearToSendTimeoutInMs, SegmentPackager aClearToSendSegmentPackager ) {
		_decoratee = aDecoratee;
		_enquiryStandbyTimeInMs = aEnquiryStandbyTimeInMs;
		_readyToSendMagicBytes = aReadyToSendMagicBytes;
		_readyToSendRetryNumber = aReadyToSendRetryNumber;
		_readyToSendTimeoutInMs = aReadyToSendTimeoutInMs;
		_readyToSendSegmentPackager = aReadyToSendSegmentPackager != null ? aReadyToSendSegmentPackager : new DummySegmentPackager();
		_readyToSendSegment = _readyToSendSegmentPackager.toPackaged( _readyToSendMagicBytesSegment = new MagicBytesSegment( _readyToSendMagicBytes ) );
		_clearToSendMagicBytes = aClearToSendMagicBytes;
		_clearToSendTimeoutInMs = aClearToSendTimeoutInMs;
		_clearToSendSegmentPackager = aClearToSendSegmentPackager != null ? aClearToSendSegmentPackager : new DummySegmentPackager();
		_clearToSendSegment = _clearToSendSegmentPackager.toPackaged( _clearToSendMagicBytesSegment = new MagicBytesSegment( _clearToSendMagicBytes ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.transmitTo( aOutputStream, aReturnStream );
		}
		else {
			final RetryCounter theRetries = new RetryCounter( _readyToSendRetryNumber, _readyToSendTimeoutInMs );
			Exception eException = null;
			@SuppressWarnings("resource")
			final // Do not close me after done!
			SkipAvailableInputStream theSkipReturnStream = new SkipAvailableInputStream( aReturnStream, _readyToSendTimeoutInMs );
			while ( theRetries.nextRetry() ) {
				theSkipReturnStream.skipAvailableExcept( _clearToSendSegment.getLength() );
				eException = null;
				try {
					_readyToSendSegment.transmitTo( aOutputStream );
					receiveCtsTransmitTo( aOutputStream, aReturnStream );
					return;
				}
				catch ( Exception e ) {
					eException = e;
				}
			}
			if ( eException != null ) {
				if ( eException instanceof FlowControlException ) {
					throw (FlowControlException) eException;
				}
				throw new FlowControlRetryException( _readyToSendRetryNumber, _readyToSendTimeoutInMs, "Aborting after <" + _readyToSendRetryNumber + "> retries with a timeout for each retry of <" + _readyToSendTimeoutInMs + "> milliseconds: " + eException.getMessage(), eException );
			}
			else {
				throw new FlowControlRetryException( _readyToSendRetryNumber, _readyToSendTimeoutInMs, "Aborting after <" + _readyToSendRetryNumber + "> retries with a timeout for each retry of <" + _readyToSendTimeoutInMs + "> milliseconds." );
			}
		}
	}

	/**
	 * Receive CTS transmit to.
	 *
	 * @param aOutputStream the output stream
	 * @param aReturnStream the return stream
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TransmissionException the transmission exception
	 * @throws FlowControlTimeoutException the flow control timeout exception
	 */
	protected void receiveCtsTransmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		long theReadyToSendTimeoutInMs = _readyToSendTimeoutInMs;
		long theBeginTimeInMs = System.currentTimeMillis();
		long ePassedTimeInMs;
		long eCurrentTimeInMs;
		final TimeoutInputStream theTimeoutReturnStream = SerialUtility.createTimeoutInputStream( aReturnStream, theReadyToSendTimeoutInMs );
		// @SuppressWarnings("resource") // Do not close me after done!
		// SkipAvailableInputStream theSkipReturnStream = new SkipAvailableInputStream( aReturnStream, _clearToSendTimeoutInMs );
		while ( theReadyToSendTimeoutInMs == -1 || theReadyToSendTimeoutInMs > 0 ) {
			// theSkipReturnStream.skipAvailableExcept( _clearToSendSegment.getLength() );
			try {
				_clearToSendSegment.receiveFrom( theTimeoutReturnStream );
			}
			catch ( TimeoutIOException e ) { // This is a serial exception now:
				throw new FlowControlTimeoutException( e.getTimeoutInMs(), "Encountered a timeout of <" + e.getTimeoutInMs() + "> ms while receiving from return stream <" + aReturnStream + ">!", e );

			}
			if ( Arrays.equals( _clearToSendMagicBytesSegment.getPayload(), _clearToSendMagicBytes ) ) {
				_decoratee.transmitTo( aOutputStream, aReturnStream );
				return;
			}
			else {
				if ( theReadyToSendTimeoutInMs != -1 ) {
					eCurrentTimeInMs = System.currentTimeMillis();
					ePassedTimeInMs = eCurrentTimeInMs - theBeginTimeInMs;
					theBeginTimeInMs = eCurrentTimeInMs;
					if ( ePassedTimeInMs < theReadyToSendTimeoutInMs ) {
						theReadyToSendTimeoutInMs -= ePassedTimeInMs;
					}
					else {
						throw new FlowControlTimeoutException( _clearToSendTimeoutInMs, "Failed after a timeout of <" + _clearToSendTimeoutInMs + "> as the current (transmitter) CTS byte <" + _clearToSendMagicBytesSegment.getPayload() + "> does not match the expected CTS byte <" + _clearToSendMagicBytes + ">." );
					}
				}
			}
		}
		throw new FlowControlTimeoutException( _clearToSendTimeoutInMs, "Failed after a timeout of <" + _clearToSendTimeoutInMs + "> as the current (received) RTS byte <" + _clearToSendMagicBytesSegment.getPayload() + "> does not match the expected RTS byte <" + _clearToSendMagicBytes + ">." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_clearToSendMagicBytesSegment.reset();
		_clearToSendSegment.reset();
		_decoratee.reset();
		_readyToSendMagicBytesSegment.reset();
		_readyToSendSegment.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A transmission decorator enriching the encapsulated transmission with \"Ready-to-send RTS\".", getDecoratee().toSchema() );
		theSchema.put( RTS_MAGIC_BYTES, getReadyToSendMagicBytes() );
		theSchema.put( RTS_TIMEOUT_IN_MS, getReadyToSendTimeoutMillis() );
		theSchema.put( RTS_RETRY_NUMBER, getReadyToSendRetryNumber() );
		theSchema.put( CTS_MAGIC_BYTES, getClearToSendMagicBytes() );
		theSchema.put( CTS_TIMEOUT_IN_MS, getClearToSendTimeoutMillis() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getEnquiryStandbyTimeMillis() {
		return _enquiryStandbyTimeInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadyToSendTimeoutMillis() {
		return _readyToSendTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getReadyToSendMagicBytes() {
		return _readyToSendMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getReadyToSendRetryNumber() {
		return _readyToSendRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getReadyToSendSegmentPackager() {
		return _readyToSendSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getClearToSendMagicBytes() {
		return _clearToSendMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getClearToSendTimeoutMillis() {
		return _clearToSendTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getClearToSendSegmentPackager() {
		return _clearToSendSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [segment=" + _decoratee + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractReadyToSendTransmissionDecorator<?> other = (AbstractReadyToSendTransmissionDecorator<?>) obj;
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}
}
