// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a transmission timeout in milliseconds property.
 */
public interface TransmissionTimeoutMillisAccessor {

	/**
	 * The transmission timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	long getTransmissionTimeoutMillis();

	/**
	 * Provides a mutator for a transmission timeout in milliseconds property.
	 */
	public interface TransmissionTimeoutMillisMutator {

		/**
		 * The transmission timeout attribute in milliseconds.
		 * 
		 * @param aTransmissionTimeoutMillis An integer with the transmission
		 *        timeout in milliseconds.
		 */
		void setTransmissionTimeoutMillis( long aTransmissionTimeoutMillis );
	}

	/**
	 * Provides a builder method for the transmission timeout property returning
	 * the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TransmissionTimeoutMillisBuilder<B extends TransmissionTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the transmission timeout property.
		 * 
		 * @param aTransmissionTimeoutMillis The transmission timeout in
		 *        milliseconds to be stored by the transmission timeout
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTransmissionTimeoutMillis( long aTransmissionTimeoutMillis );
	}

	/**
	 * Provides a transmission timeout in milliseconds property.
	 */
	public interface TransmissionTimeoutMillisProperty extends TransmissionTimeoutMillisAccessor, TransmissionTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setTransmissionTimeoutMillis(long)} and returns the very same
		 * value (getter).
		 * 
		 * @param aTransmissionTimeoutMillis The long to set (via
		 *        {@link #setTransmissionTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letTransmissionTimeoutMillis( long aTransmissionTimeoutMillis ) {
			setTransmissionTimeoutMillis( aTransmissionTimeoutMillis );
			return aTransmissionTimeoutMillis;
		}
	}
}
