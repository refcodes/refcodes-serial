// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.serial.Segment.SegmentMixin;

/**
 * A {@link ArraySegment} is a {@link Segment} with array {@link Segment}
 * elements as of {@link ArrayTransmission}.
 *
 * @param <SEGMENT> The type of the array {@link Segment} elements.
 */
public interface ArraySegment<SEGMENT extends Segment> extends SegmentMixin, Segment, ArrayTransmission<SEGMENT> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		int eOffset = aOffset;
		for ( int i = 0; i < getArray().length; i++ ) {
			eOffset = getArray()[i].fromTransmission( aSequence, eOffset );
		}
		return eOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		for ( int i = 0; i < getArray().length; i++ ) {
			getArray()[i].receiveFrom( aInputStream, aReturnStream );
		}
	}
}
