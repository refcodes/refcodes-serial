// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a ping magic bytes property.
 */
public interface PingMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the ping magic bytes property.
	 * 
	 * @return The magic bytes stored by the ping magic bytes property.
	 */
	byte[] getPingMagicBytes();

	/**
	 * Provides a mutator for a ping magic bytes property.
	 */
	public interface PingMagicBytesMutator {

		/**
		 * Sets the magic bytes for the ping magic bytes property.
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the ping magic
		 *        bytes property.
		 */
		void setPingMagicBytes( byte[] aPingMagicBytes );

		/**
		 * Sets the magic bytes for the ping magic bytes property. Uses
		 * {@link TransmissionMetrics#DEFAULT_ENCODING} for converting the
		 * {@link String} into a byte array.
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 */
		default void setPingMagicBytes( String aPingMagicBytes ) {
			setPingMagicBytes( aPingMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the ping magic bytes property.
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setPingMagicBytes( String aPingMagicBytes, Charset aEncoding ) {
			setPingMagicBytes( aPingMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a ping magic bytes property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PingMagicBytesBuilder<B extends PingMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the ping magic bytes property.
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the ping magic
		 *        bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPingMagicBytes( byte[] aPingMagicBytes );

		/**
		 * Sets the magic bytes for the ping magic bytes property.
		 *
		 * @param aPingMagicBytes The magic bytes to be stored by the magic
		 *        bytes property. Uses
		 *        {@link TransmissionMetrics#DEFAULT_ENCODING} for converting
		 *        the {@link String} into a byte array.
		 * 
		 * @return the b
		 */
		default B withPingMagicBytes( String aPingMagicBytes ) {
			return withPingMagicBytes( aPingMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the ping magic bytes property.
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withPingMagicBytes( String aPingMagicBytes, Charset aEncoding ) {
			return withPingMagicBytes( aPingMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a ping magic bytes property.
	 */
	public interface PingMagicBytesProperty extends PingMagicBytesAccessor, PingMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPingMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPingMagicBytes The character to set (via
		 *        {@link #setPingMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letPingMagicBytes( byte[] aPingMagicBytes ) {
			setPingMagicBytes( aPingMagicBytes );
			return aPingMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPingMagicBytes(byte[])} and returns the very same value
		 * (getter). Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 *
		 * @param aPingMagicBytes The character to set (via
		 *        {@link #setPingMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPingMagicBytes( String aPingMagicBytes ) {
			return letPingMagicBytes( aPingMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #letPingMagicBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPingMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPingMagicBytes( String aPingMagicBytes, Charset aEncoding ) {
			setPingMagicBytes( aPingMagicBytes, aEncoding );
			return aPingMagicBytes;
		}
	}
}
