// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * he {@link DynamicTypeTransmission} represents a {@link Transmission} which's
 * internal raw data is created from provided types at runtime and which's
 * internal raw data is used to create provided types at runtime. Therefore
 * internally it just consists of a {@link Sequence} which is created if
 * necessary from a provided type or from which a provided type is instantiated
 * if necessary.
 *
 */
public interface DynamicTypeTransmission extends Transmission {

	/**
	 * Constructs an instance for the given type T. The attributes of the given
	 * data structure are processed all and in alphabetical order. For
	 * specifying a predefined set of attributes and their order, please invoke
	 * {@link #fromType(Object, String...)} instead!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * 
	 * @return The according type constructed from the
	 *         {@link DynamicTypeSection}'s {@link Sequence}.
	 * 
	 * @throws IllegalArgumentException thrown in case the
	 *         {@link DynamicTypeSection}'s {@link Sequence} does not match the
	 *         provided type.
	 */
	<T> T toType( Class<T> aType );

	/**
	 * Constructs an instance for the given type T. . The attributes of the
	 * given data structure are processed using the predefined set of attributes
	 * in their according order
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aType The data structure's type.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 * 
	 * @return The according type constructed from the
	 *         {@link DynamicTypeSection}'s {@link Sequence}.
	 * 
	 * @throws IllegalArgumentException thrown in case the
	 *         {@link DynamicTypeSection}'s {@link Sequence} does not match the
	 *         provided type.
	 */
	<T> T toType( Class<T> aType, String... aAttributes );

	/**
	 * Constructs the {@link DynamicTypeSection}'s {@link Sequence} from the
	 * given type's instance . The attributes of the given data structure are
	 * processed all and in alphabetical order. For specifying a predefined set
	 * of attributes and their order, please invoke
	 * {@link #fromType(Object, String[])} instead!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 */
	<T> void fromType( T aValue );

	/**
	 * Constructs an instance for the given type T. . The attributes of the
	 * given data structure are processed using the predefined set of attributes
	 * in their according order.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	<T> void fromType( T aValue, String... aAttributes );

}