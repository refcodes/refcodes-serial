// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.numerical.NumericalUtility;

/**
 * Magic bytes are usually found (somewhere) at the beginning of a file or a
 * stream. A {@link AbstractMagicBytesTransmissionDecorator} decorates a
 * decoratee decoratee and prefixes this decoratee instance with given magic
 * bytes.
 * 
 * @param <DECORATEE> The decoratee type describing the decoratee subclass
 *        decoratee.
 */
public abstract class AbstractMagicBytesTransmissionDecorator<DECORATEE extends Transmission> extends AbstractMagicBytesTransmission implements Transmission, DecorateeAccessor<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected DECORATEE _decoratee;
	protected String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractMagicBytesTransmissionDecorator( String aAlias, DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aTransmissionMetrics );
		_alias = aAlias;
		_decoratee = aDecoratee;
	}

	// -------------------------------------------------------------------------

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public AbstractMagicBytesTransmissionDecorator( String aAlias, DECORATEE aDecoratee, int aMagicBytesLength ) {
		super( aAlias, aMagicBytesLength );
		_decoratee = aDecoratee;
	}

	/**
	 * Enriches the provided decoratee with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public AbstractMagicBytesTransmissionDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		super( aAlias, aMagicBytes );
		_decoratee = aDecoratee;
	}

	/**
	 * Enriches the provided decoratee with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public AbstractMagicBytesTransmissionDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aMagicBytes, aCharset );
		_decoratee = aDecoratee;
	}

	/**
	 * Enriches the provided decoratee with the given magic bytes being
	 * prefixed.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 */
	public AbstractMagicBytesTransmissionDecorator( String aAlias, DECORATEE aDecoratee, byte[] aMagicBytes ) {
		super( aAlias, aMagicBytes );
		_decoratee = aDecoratee;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _magicBytes ).withAppend( _decoratee.toSequence() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _magicBytesLength + _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( byte[] aValue ) {
		_magicBytes = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		// !!! DO NOT CLEAR SUPER'S MAGIC BYTES !!! 
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", segment=" + _decoratee + ", magicBytes=" + NumericalUtility.toHexString( ", ", _magicBytes ) + ", magicBytesLength=" + _magicBytesLength + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( !super.equals( obj ) ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractMagicBytesTransmissionDecorator<?> other = (AbstractMagicBytesTransmissionDecorator<?>) obj;
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A transmission decorator prefixing the encapsulated transmission with magic bytes.", _decoratee.toSchema() );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES, toMagicBytesString() );
		return theSchema;
	}
}
