// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

/**
 * Provides an accessor for a handshake magic bytes property.
 */
public interface HandshakeMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the handshake magic bytes property.
	 * 
	 * @return The magic bytes stored by the handshake magic bytes property.
	 */
	byte[] getHandshakeMagicBytes();

	/**
	 * Provides a mutator for a handshake magic bytes property.
	 */
	public interface HandshakeMagicBytesMutator {

		/**
		 * Sets the magic bytes for the handshake magic bytes property.
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the
		 *        handshake magic bytes property.
		 */
		void setHandshakeMagicBytes( byte[] aHandshakeMagicBytes );

		/**
		 * Sets the magic bytes for the handshake magic bytes property. Uses
		 * {@link TransmissionMetrics#DEFAULT_ENCODING} for converting the
		 * {@link String} into a byte array.
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 */
		default void setHandshakeMagicBytes( String aHandshakeMagicBytes ) {
			setHandshakeMagicBytes( aHandshakeMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the handshake magic bytes property.
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 */
		default void setHandshakeMagicBytes( String aHandshakeMagicBytes, Charset aEncoding ) {
			setHandshakeMagicBytes( aHandshakeMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a builder method for a handshake magic bytes property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HandshakeMagicBytesBuilder<B extends HandshakeMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the handshake magic bytes property.
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the
		 *        handshake magic bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHandshakeMagicBytes( byte[] aHandshakeMagicBytes );

		/**
		 * Sets the magic bytes for the handshake magic bytes property.
		 *
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the magic
		 *        bytes property. Uses
		 *        {@link TransmissionMetrics#DEFAULT_ENCODING} for converting
		 *        the {@link String} into a byte array.
		 * 
		 * @return the b
		 */
		default B withHandshakeMagicBytes( String aHandshakeMagicBytes ) {
			return withHandshakeMagicBytes( aHandshakeMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}

		/**
		 * Sets the magic bytes for the handshake magic bytes property.
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withHandshakeMagicBytes( String aHandshakeMagicBytes, Charset aEncoding ) {
			return withHandshakeMagicBytes( aHandshakeMagicBytes.getBytes( aEncoding ) );
		}
	}

	/**
	 * Provides a handshake magic bytes property.
	 */
	public interface HandshakeMagicBytesProperty extends HandshakeMagicBytesAccessor, HandshakeMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setHandshakeMagicBytes(byte[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aHandshakeMagicBytes The character to set (via
		 *        {@link #setHandshakeMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letHandshakeMagicBytes( byte[] aHandshakeMagicBytes ) {
			setHandshakeMagicBytes( aHandshakeMagicBytes );
			return aHandshakeMagicBytes;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setHandshakeMagicBytes(byte[])} and returns the very same
		 * value (getter). Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 *
		 * @param aHandshakeMagicBytes The character to set (via
		 *        {@link #setHandshakeMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letHandshakeMagicBytes( String aHandshakeMagicBytes ) {
			return letHandshakeMagicBytes( aHandshakeMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #letHandshakeMagicBytes(byte[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aHandshakeMagicBytes The magic bytes to be stored by the magic
		 *        bytes property.
		 * @param aEncoding The string's bytes are converted using the given
		 *        {@link Charset}.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letHandshakeMagicBytes( String aHandshakeMagicBytes, Charset aEncoding ) {
			setHandshakeMagicBytes( aHandshakeMagicBytes, aEncoding );
			return aHandshakeMagicBytes;
		}
	}
}
