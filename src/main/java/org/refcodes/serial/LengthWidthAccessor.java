// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a length width (in bytes) property.
 */
public interface LengthWidthAccessor {

	/**
	 * Retrieves the length width (in bytes) from the length width (in bytes)
	 * property.
	 * 
	 * @return The length width (in bytes) stored by the length width (in bytes)
	 *         property.
	 */
	int getLengthWidth();

	/**
	 * Provides a mutator for a length width (in bytes) property.
	 */
	public interface LengthWidthMutator {

		/**
		 * Sets the length width (in bytes) for the length width (in bytes)
		 * property.
		 * 
		 * @param aLengthWidth The length width (in bytes) to be stored by the
		 *        length width (in bytes) property.
		 */
		void setLengthWidth( int aLengthWidth );
	}

	/**
	 * Provides a builder method for a length width (in bytes) property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LengthWidthBuilder<B extends LengthWidthBuilder<B>> {

		/**
		 * Sets the length width (in bytes) for the length width (in bytes)
		 * property.
		 * 
		 * @param aLengthWidth The length width (in bytes) to be stored by the
		 *        length width (in bytes) property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLengthWidth( int aLengthWidth );
	}

	/**
	 * Provides a length width (in bytes) property.
	 */
	public interface LengthWidthProperty extends LengthWidthAccessor, LengthWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setLengthWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aLengthWidth The integer to set (via
		 *        {@link #setLengthWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letLengthWidth( int aLengthWidth ) {
			setLengthWidth( aLengthWidth );
			return aLengthWidth;
		}
	}
}
