// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.numerical.Endianess;

/**
 * The {@link AbstractLengthDecoratorSegment} decorates a decoratee with a
 * length prefix.
 *
 * @param <DECORATEE> the generic type
 */
public abstract class AbstractLengthDecoratorSegment<DECORATEE extends Transmission> extends AbstractReferenceeLengthSegment<DECORATEE> implements DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	protected AbstractLengthDecoratorSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( aTransmissionMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	public AbstractLengthDecoratorSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	protected AbstractLengthDecoratorSegment() {}

	/**
	 * {@inheritDoc}
	 */
	protected AbstractLengthDecoratorSegment( Endianess aEndianess ) {
		super( aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	protected AbstractLengthDecoratorSegment( int aLengthWidth ) {
		super( aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	protected AbstractLengthDecoratorSegment( int aLengthWidth, Endianess aEndianess ) {
		super( aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public AbstractLengthDecoratorSegment( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	/**
	 * {@inheritDoc}
	 */
	public AbstractLengthDecoratorSegment( DECORATEE aDecoratee, Endianess aEndianess ) {
		super( aDecoratee, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public AbstractLengthDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth ) {
		super( aDecoratee, aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public AbstractLengthDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth, Endianess aEndianess ) {
		super( aDecoratee, aLengthWidth, aEndianess );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		if ( _referencee == null ) {
			_allocLength = 0;
			return new ByteArraySequence( _endianess.toBytes( _allocLength, _lengthWidth ) );
		}

		// Assertion only enabled during testing |-->  
		assert _referencee.toSequence().getLength() == _referencee.getLength() : "Bad length determination: Sequence length <" + _referencee.toSequence().getLength() + "> does not match the segment length <" + _referencee.getLength() + ">";
		// Assertion only enabled during testing <--|

		_allocLength = _referencee.getLength();
		return new ByteArraySequence( _endianess.toBytes( _allocLength, _lengthWidth ) ).withAppend( _referencee.toSequence() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		final int theLength = _referencee == null ? 0 : _referencee.getLength();
		final byte[] theBuffer = _endianess.toUnsignedBytes( theLength, _lengthWidth );
		aOutputStream.write( theBuffer );
		if ( _referencee != null ) {
			_referencee.transmitTo( aOutputStream, aReturnStream );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _lengthWidth + ( _referencee != null ? _referencee.getLength() : 0 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _referencee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "An allocation decorator referencing a decoratee and prefixing the length of the decoratee in bytes.", _referencee != null ? _referencee.toSchema() : null );
		theSchema.put( ENDIANESS, _endianess );
		theSchema.put( ALLOC_LENGTH_WIDTH, _lengthWidth );
		theSchema.put( ALLOC_LENGTH, _allocLength );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [endianess=" + _endianess + ", lengthWidth=" + _lengthWidth + ", allocLength=" + _allocLength + ", segment=" + _referencee + "]";
	}
}
