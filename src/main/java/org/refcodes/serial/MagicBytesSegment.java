// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * Magic bytes are usually found (somewhere) at the beginning of a file or a
 * stream. A {@link MagicBytesSegment} decorates a {@link Segment} decoratee and
 * prefixes this {@link Segment} instance with given magic bytes.
 */
public class MagicBytesSegment extends AbstractMagicBytesTransmission implements SegmentMixin, Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegment( String aAlias, String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegment( String aAlias, byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegment( String aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes, aTransmissionMetrics );
	}

	/**
	 * Constructs an according instance for magic bytes of the given length. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aMagicBytes The bytes to be stored by this instance as magic
	 *        bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSegment( byte[] aMagicBytes, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according instance with magic bytes of the given length.
	 *
	 * @param aMagicBytesLength The length of the magic bytes sequence..
	 */
	public MagicBytesSegment( int aMagicBytesLength ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytesLength );
	}

	/**
	 * Constructs an according instance with the according magic bytes
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSegment( String aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes );
	}

	/**
	 * Constructs an according instance with the according magic bytes
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSegment( String aMagicBytes, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes, aCharset );
	}

	/**
	 * Constructs an according instance with the according magic bytes.
	 * 
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 */
	public MagicBytesSegment( byte... aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSegment.class.getSimpleName() ), aMagicBytes );
	}

	// --------------------------------------------------------------------------

	/**
	 * Constructs an according instance with magic bytes of the given length.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytesLength The length of the magic bytes sequence..
	 */
	public MagicBytesSegment( String aAlias, int aMagicBytesLength ) {
		super( aAlias, aMagicBytesLength );
	}

	/**
	 * Constructs an according instance with the according magic bytes
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSegment( String aAlias, String aMagicBytes ) {
		super( aAlias, aMagicBytes );
	}

	/**
	 * Constructs an according instance with the according magic bytes
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSegment( String aAlias, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aMagicBytes, aCharset );
	}

	/**
	 * Constructs an according instance with the according magic bytes.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aMagicBytes The magic bytes to be stored by this instance.
	 */
	public MagicBytesSegment( String aAlias, byte... aMagicBytes ) {
		super( aAlias, aMagicBytes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		_magicBytes = aSequence.toBytes( aOffset, _magicBytesLength );
		return aOffset += _magicBytesLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _magicBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _magicBytesLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MagicBytesSegment withPayload( byte[] aValue ) {
		setPayload( aValue );
		return this;
	}
}
