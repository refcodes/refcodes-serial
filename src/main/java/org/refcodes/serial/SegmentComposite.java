// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;

import org.refcodes.serial.Segment.SegmentMixin;

/**
 * The {@link SegmentComposite} is a {@link Segment} composed of multiple child
 * {@link Segment} elements.
 * 
 * @param <CHILD> The type of the child elements to be contained in this
 *        instance.
 */
public class SegmentComposite<CHILD extends Segment> extends AbstractTransmissionComposite<CHILD> implements Segment, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public SegmentComposite( Collection<CHILD> aSegments ) {
		if ( aSegments != null && aSegments.size() != 0 ) {
			_children = (CHILD[]) new Segment[aSegments.size()];
			_children = aSegments.toArray( _children );
		}
		else {
			_children = (CHILD[]) new Segment[0];
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SafeVarargs
	public SegmentComposite( CHILD... aSegments ) {
		super( aSegments );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		int eOffset = aOffset;
		for ( int i = 0; i < getChildren().length; i++ ) {
			eOffset = getChildren()[i].fromTransmission( aSequence, eOffset );
		}
		return eOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		for ( int i = 0; i < getChildren().length; i++ ) {
			_children[i].receiveFrom( aInputStream, aReturnStream );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		SerialSchema[] theSchemas = null;
		if ( _children != null && _children.length != 0 ) {
			theSchemas = new SerialSchema[_children.length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = _children[i].toSchema();
			}
		}
		return new SerialSchema( getClass(), toSequence(), getLength(), "A body containing a composite segment as payload.", theSchemas );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode( _children );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final SegmentComposite<?> other = (SegmentComposite<?>) obj;
		if ( !Arrays.equals( _children, other._children ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [children=" + Arrays.toString( _children ) + "]";
	}
}
