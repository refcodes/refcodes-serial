// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoTimeout;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.io.SkipAvailableInputStream;
import org.refcodes.io.TimeoutInputStream;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * The {@link AbstractStopAndWaitTransmissionDecorator} class implements a
 * decorator providing {@link StopAndWaitTransmission} functionality for a
 * {@link Transmission}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public abstract class AbstractStopAndWaitTransmissionDecorator<DECORATEE extends Transmission> extends AbstractErrorCorrectionTransmissionDecorator<DECORATEE> implements StopAndWaitTransmission, EndianessAccessor, SequenceNumberAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ACK_MAGIC_BYTES = "ACK_MAGIC_BYTES";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected byte[] _acknowledgeMagicBytes;
	protected int _sequenceNumber;
	protected int _sequenceNumberWidth;
	protected int _sequenceNumberInitValue;
	protected ConcatenateMode _sequenceNumberConcatenateMode;
	protected Endianess _endianess;
	protected SegmentPackager _acknowledgeSegmentPackager;
	protected Segment _acknowledgeSegment;
	protected NumberSegment _acknowledgeSequenceNumberSegment;
	protected MagicBytesSegment _acknowledgeMagicBytesSegment;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract stop and wait transmission decorator.
	 */
	protected AbstractStopAndWaitTransmissionDecorator() {}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		this( aDecoratee, aTransmissionMetrics.getSequenceNumberInitValue(), aTransmissionMetrics.getSequenceNumberWidth(), aTransmissionMetrics.getSequenceNumberConcatenateMode(), aTransmissionMetrics.getAcknowledgeMagicBytes(), aTransmissionMetrics.getAcknowledgeRetryNumber(), aTransmissionMetrics.getAcknowledgeTimeoutMillis(), aTransmissionMetrics.toAckSegmentPackager(), aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, (SegmentPackager) null, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		this( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAckSegmentPackager The ACK {@link SegmentPackager} for packaging
	 *        ACK responses.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, SegmentPackager aAckSegmentPackager ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, aAckSegmentPackager, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager The ACK {@link SegmentPackager} for packaging
	 *        ACK responses.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager The ACK {@link SegmentPackager} for packaging
	 *        ACK responses.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, new CrcSegmentPackager( aCrcChecksumConcatenateMode ), TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, new CrcSegmentPackager( aCrcAlgorithm ), TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		this( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aEndianess ), aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode ), TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, (SegmentPackager) null, aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess ), aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode ), TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE, TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		this( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, new CrcSegmentPackager( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess ), aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according stop-and-wait decorator instance wrapping the
	 * given decoratee.
	 *
	 * @param aDecoratee The decoratee to be wrapped by the stop-and-wait
	 *        decorator.
	 * @param aSequenceNumberInitValue The initial sequence number from where to
	 *        start counting the blocks.
	 * @param aSequenceNumberWidth The width (in bytes) to be used for sequence
	 *        number values.
	 * @param aSequenceNumberConcatenateMode The mode of concatenation to use
	 *        when creating a {@link Sequence} from this {@link Transmission}
	 *        and the decorated {@link Transmission}.
	 * @param aAcknowledgeMagicBytes The ACK character(s) to be used by the
	 *        return channel to transmit an ACK (acknowledge) response after
	 *        successful receiving a transmission.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 * @param aAckSegmentPackager The ACK {@link SegmentPackager} for packaging
	 *        ACK responses.
	 * @param aEndianess The {@link Endianess} to use for integer (double)
	 *        numbers and the like.
	 */
	public AbstractStopAndWaitTransmissionDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		super( aDecoratee, aAckRetryNumber, aAckTimeoutInMs );
		_acknowledgeMagicBytes = aAcknowledgeMagicBytes;
		_acknowledgeSegmentPackager = aAckSegmentPackager;
		_sequenceNumberInitValue = aSequenceNumberInitValue;
		_sequenceNumber = aSequenceNumberInitValue != -1 ? aSequenceNumberInitValue : 0;
		_sequenceNumberWidth = aSequenceNumberWidth;
		_sequenceNumberConcatenateMode = aSequenceNumberConcatenateMode;
		_endianess = aEndianess;
		_acknowledgeSegmentPackager = aAckSegmentPackager != null ? aAckSegmentPackager : new DummySegmentPackager();
		_acknowledgeSegment = _acknowledgeSegmentPackager.toPackaged( new SegmentComposite<>( _acknowledgeMagicBytesSegment = new MagicBytesSegment( _acknowledgeMagicBytes ), _acknowledgeSequenceNumberSegment = new NumberSegment( _sequenceNumberWidth, _endianess ) ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.transmitTo( aOutputStream, aReturnStream );
		}
		else {
			// Sequence number |-->
			final byte[] theSequenceBytes = _endianess.toUnsignedBytes( _sequenceNumber, _sequenceNumberWidth );
			// Sequence number <--|

			byte[] eAck;
			int eSequenceNumber;

			Exception eException = null;
			final RetryCounter theRetries = new RetryCounter( getAcknowledgeRetryNumber(), getAcknowledgeTimeoutMillis() );
			final TimeoutInputStream theTimeoutInputStream = SerialUtility.createTimeoutInputStream( aReturnStream, _acknowledgeTimeoutInMs );
			@SuppressWarnings("resource")
			final // Do not close me after done!
			SkipAvailableInputStream theSkipReturnStream = new SkipAvailableInputStream( aReturnStream, _acknowledgeTimeoutInMs );
			while ( theRetries.nextRetry() ) {
				eException = null;
				try {
					switch ( _sequenceNumberConcatenateMode ) {
					case APPEND -> {
						_decoratee.transmitTo( aOutputStream, aReturnStream );
						aOutputStream.write( theSequenceBytes );
					}
					case PREPEND -> {
						aOutputStream.write( theSequenceBytes );
						_decoratee.transmitTo( aOutputStream, aReturnStream );
					}
					default -> throw new UnhandledEnumBugException( _sequenceNumberConcatenateMode );
					}

					// Ack |--> 
					_acknowledgeSegment.receiveFrom( theTimeoutInputStream );
					eAck = _acknowledgeMagicBytesSegment.getPayload();
					eSequenceNumber = _acknowledgeSequenceNumberSegment.getPayload().intValue();
					if ( Arrays.equals( eAck, _acknowledgeMagicBytes ) && eSequenceNumber == _sequenceNumber ) {
						_sequenceNumber++;
						return;
					}
					// Ack <--|
				}
				catch ( Exception e ) {
					eException = e;
				}
				if ( theRetries.hasNextRetry() ) {
					try {
						theSkipReturnStream.skipAvailableWithin( IoTimeout.toTimeoutSleepLoopTimeInMs( _acknowledgeTimeoutInMs ) );
					}
					catch ( IOException ignore ) {}
				}
			}
			if ( eException != null ) {
				throw new FlowControlRetryException( _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, "Aborting after <" + getAcknowledgeRetryNumber() + "> retries with a timeout for each retry of <" + getAcknowledgeTimeoutMillis() + "> milliseconds: " + eException.getMessage(), eException );
			}
			else {
				throw new FlowControlRetryException( _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, "Aborting after <" + getAcknowledgeRetryNumber() + "> retries with a timeout for each retry of <" + getAcknowledgeTimeoutMillis() + "> milliseconds." );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		_acknowledgeMagicBytesSegment.reset();
		_acknowledgeSegment.reset();
		_acknowledgeSequenceNumberSegment.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A segment decorator enriching the encapsulated segment with \"Stop-and-wait ARQ\".", getDecoratee().toSchema() );
		theSchema.put( ACK_RETRY_NUMBER, getAcknowledgeRetryNumber() );
		theSchema.put( ACK_TIMEOUT_IN_MS, getAcknowledgeTimeoutMillis() );
		theSchema.put( ACK_MAGIC_BYTES, getAcknowledgeMagicBytes() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSequenceNumber() {
		return _sequenceNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getAcknowledgeRetryNumber() {
		return _acknowledgeRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getAcknowledgeTimeoutMillis() {
		return _acknowledgeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getAcknowledgeMagicBytes() {
		return _acknowledgeMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SegmentPackager getAcknowledgeSegmentPackager() {
		return _acknowledgeSegmentPackager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [segment=" + _decoratee + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractStopAndWaitTransmissionDecorator<?> other = (AbstractStopAndWaitTransmissionDecorator<?>) obj;
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Reads a sequence number from the {@link InputStream}.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        sequence number.
	 * 
	 * @return The accordingly read sequence number.
	 * 
	 * @throws IOException thrown in case of I/O problems.
	 */
	protected long readSequenceNumber( InputStream aInputStream ) throws IOException {
		final byte[] theSequenceBytes = new byte[_sequenceNumberWidth];
		final int theLength = aInputStream.read( theSequenceBytes, 0, theSequenceBytes.length );
		if ( theLength != theSequenceBytes.length ) {
			throw new FlowControlException( "While reading the sequence number, unexpected number of bytes received <" + theLength + "> of the number of bytes expected <" + theSequenceBytes.length + ">." );
		}
		return _endianess.toLong( theSequenceBytes );
	}
}
