// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.InputStream;

import org.refcodes.mixin.InputStreamAccessor.InputStreamBuilder;

/**
 * Provides an accessor for an input return stream property.
 */
public interface InputReturnStreamAccessor {

	/**
	 * Retrieves the input return stream from the input return stream property.
	 * 
	 * @return The input return stream stored by the input return stream
	 *         property.
	 */
	InputStream getReturnStream();

	/**
	 * Provides a mutator for an input return stream property.
	 */
	public interface InputReturnStreamMutator {

		/**
		 * Sets the input return stream for the input return stream property.
		 * 
		 * @param aReturnStream The input return stream to be stored by the
		 *        input stream property.
		 */
		void setReturnStream( InputStream aReturnStream );
	}

	/**
	 * Provides a mutator for an input return stream property.
	 * 
	 * @param <B> The builder which implements the {@link InputStreamBuilder}.
	 */
	public interface InputReturnStreamBuilder<B extends InputReturnStreamBuilder<?>> {

		/**
		 * Sets the input return stream to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aReturnStream The input return stream to be stored by the
		 *        input stream property.
		 * 
		 * @return This {@link InputReturnStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withReturnStream( InputStream aReturnStream );
	}

	/**
	 * Provides an input return stream property.
	 */
	public interface InputReturnStreamProperty extends InputReturnStreamAccessor, InputReturnStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link InputStream}
		 * (setter) as of {@link #setReturnStream(InputStream)} and returns the
		 * very same value (getter).
		 * 
		 * @param aReturnStream The {@link InputStream} to set (via
		 *        {@link #setReturnStream(InputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InputStream letReturnStream( InputStream aReturnStream ) {
			setReturnStream( aReturnStream );
			return aReturnStream;
		}
	}
}
