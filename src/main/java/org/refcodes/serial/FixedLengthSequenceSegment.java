// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link FixedLengthSequenceSegment} is a {@link Segment} representing a
 * {@link Sequence} of a fixed length (as of the arguments passed to one of its
 * constructors).
 */
public class FixedLengthSequenceSegment implements PayloadSegment<Sequence>, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Sequence _sequence;
	protected String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aSequenceSize The size of the {@link Sequence}.
	 */
	public FixedLengthSequenceSegment( int aSequenceSize ) {
		this( CaseStyleBuilder.asCamelCase( FixedLengthSequenceSegment.class.getSimpleName() ), aSequenceSize );
	}

	/**
	 * Constructs the {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequenceSize The size of the {@link Sequence}.
	 */
	public FixedLengthSequenceSegment( String aAlias, int aSequenceSize ) {
		this( aAlias, new ByteArraySequence( aSequenceSize ) );
	}

	/**
	 * Constructs the {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link FixedLengthSequenceSegment}.
	 */
	public FixedLengthSequenceSegment( Sequence aSequence ) {
		this( CaseStyleBuilder.asCamelCase( FixedLengthSequenceSegment.class.getSimpleName() ), aSequence );
	}

	/**
	 * Constructs the {@link FixedLengthSequenceSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this
	 *        {@link Segment}.
	 * @param aSequence The {@link Sequence} representing this
	 *        {@link FixedLengthSequenceSegment}.
	 */
	public FixedLengthSequenceSegment( String aAlias, Sequence aSequence ) {
		_alias = aAlias;
		_sequence = aSequence;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _sequence.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _sequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		if ( aSequence.getLength() < aOffset + _sequence.getLength() ) {
			throw new TransmissionSequenceException( aSequence, "The provided sequence length <" + aSequence.getLength() + "> is not sufficient to retrieve a sequence with length <" + _sequence.getLength() + "> at offset <" + aOffset + ">!" );
		}
		_sequence.replace( aSequence.toSequence( aOffset, _sequence.getLength() ) );
		return aOffset + _sequence.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_sequence.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment consisting of a sequence." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PayloadTransmission<Sequence> withPayload( Sequence aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence getPayload() {
		return _sequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( Sequence aValue ) {
		_sequence = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return new SimpleTypeMapBuilderImpl().withPut( _alias, _sequence != null ? _sequence.toBytes() : null );
	}
}
