// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link BooleanSegment} is an implementation of a {@link Segment} carrying
 * in boolean value as value.
 */
public class BooleanSegment extends AbstractPayloadSegment<Boolean> implements Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final int BYTES = 1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link BooleanSegment}.
	 */
	public BooleanSegment() {
		this( CaseStyleBuilder.asCamelCase( BooleanSegment.class.getSimpleName() ) );
	}

	/**
	 * Constructs a {@link BooleanSegment} with the given boolean payload.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link BooleanSegment}.
	 */
	public BooleanSegment( Boolean aValue ) {
		this( CaseStyleBuilder.asCamelCase( BooleanSegment.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs an empty {@link BooleanSegment}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public BooleanSegment( String aAlias ) {
		this( aAlias, false );
	}

	/**
	 * Constructs a {@link BooleanSegment} with the given boolean payload.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link BooleanSegment}.
	 */
	public BooleanSegment( String aAlias, Boolean aValue ) {
		super( aAlias, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( new byte[] { getPayload() == true ? (byte) 1 : (byte) 0 } );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final boolean theBoolean = aSequence.getByteAt( aOffset ) == 1;
		setPayload( theBoolean );
		return aOffset + BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "" + getPayload(), "A segment containing an boolean payload." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BooleanSegment withPayload( Boolean aValue ) {
		setPayload( aValue );
		return this;
	}
}
