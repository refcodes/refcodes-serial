// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.io.BoundedInputStream;
import org.refcodes.numerical.Endianess;

/**
 * An {@link LengthSegmentDecoratorSegment} enriches a {@link Segment} with an
 * allocation declaration being prefixed to the enriched {@link Segment}.
 *
 * @param <DECORATEE> The type of the {@link Segment} decoratee.
 */
public class LengthSegmentDecoratorSegment<DECORATEE extends Segment> extends AbstractLengthDecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public LengthSegmentDecoratorSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public LengthSegmentDecoratorSegment( DECORATEE aDecoratee, Endianess aEndianess ) {
		super( aDecoratee, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegmentDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth, Endianess aEndianess ) {
		super( aDecoratee, aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegmentDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth ) {
		super( aDecoratee, aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegmentDecoratorSegment( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		aOffset = super.fromTransmission( aSequence, aOffset );
		if ( _allocLength > 0 ) {
			_referencee.fromTransmission( aSequence, aOffset );
		}
		return aOffset + _allocLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		super.receiveFrom( aInputStream, aReturnStream );
		if ( _allocLength > 0 ) {
			_referencee.receiveFrom( new BoundedInputStream( aInputStream, _allocLength ), aReturnStream );
		}
	}
}
