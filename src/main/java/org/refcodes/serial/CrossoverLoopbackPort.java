// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.concurrent.ExecutorService;

/**
 * The {@link CrossoverLoopbackPort} is an in-memory implementation of a
 * {@link Port} which loops its output directly to the "connected"
 * {@link LoopbackPort} counterpart ("other end of the wire"). This is suitable
 * for cases using a some kind of frequent handshake between a transmitter and a
 * receiver on the same line where we have to simulate a bidirectional in-memory
 * communication between two ports.
 */
public class CrossoverLoopbackPort extends LoopbackPort {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CrossoverLoopbackPort}.
	 * 
	 * @param aCounterpartPort The port's counterpart (other end of the line).
	 */
	public CrossoverLoopbackPort( LoopbackPort aCounterpartPort ) {
		this( aCounterpartPort.getAlias(), aCounterpartPort, null );
	}

	/**
	 * Constructs a {@link CrossoverLoopbackPort}.
	 *
	 * @param aAlias The alias to use for this port.
	 * @param aCounterpartPort The port's counterpart (other end of the line).
	 */
	public CrossoverLoopbackPort( String aAlias, LoopbackPort aCounterpartPort ) {
		this( aAlias, aCounterpartPort, null );
	}

	/**
	 * Constructs a {@link CrossoverLoopbackPort}.
	 * 
	 * @param aCounterpartPort The port's counterpart (other end of the line).
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        invoking asynchronously working methods.
	 */
	public CrossoverLoopbackPort( LoopbackPort aCounterpartPort, ExecutorService aExecutorService ) {
		this( aCounterpartPort.getAlias(), aCounterpartPort, aExecutorService );
	}

	/**
	 * Constructs a {@link CrossoverLoopbackPort}.
	 * 
	 * @param aAlias The alias to use for this port.
	 * @param aCounterpartPort The port's counterpart (other end of the line).
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        invoking asynchronously working methods.
	 */
	public CrossoverLoopbackPort( String aAlias, LoopbackPort aCounterpartPort, ExecutorService aExecutorService ) {
		super( aAlias, aExecutorService );
		aCounterpartPort.setCrossoverPort( this );
		setCrossoverPort( aCounterpartPort );
	}
}
