// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import org.refcodes.exception.BugException;
import org.refcodes.numerical.BijectiveFunction;
import org.refcodes.numerical.InverseFunction;

/**
 * An abstract implementation of bijective and inverse functions applied to the
 * decoratee's transmitting and receiving methods.
 * 
 * @param <DECORATEE> The {@link Transmission} type describing the
 *        {@link Transmission} subclass to be enriched.s
 */
public abstract class AbstractCipherTransmissionDecorator<DECORATEE extends Transmission> extends AbstractTransmissionDecorator<DECORATEE> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Cipher _cipher;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Cipher} transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param aDecoratee The decorator having applied the {@link Cipher}'s
	 *        {@link BijectiveFunction} to encode and the {@link Cipher}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aCipher The {@link Cipher} providing the {@link BijectiveFunction}
	 *        to encode and the {@link Cipher}'s {@link InverseFunction} to
	 *        decode any data being delegated.
	 */
	public AbstractCipherTransmissionDecorator( DECORATEE aDecoratee, Cipher aCipher ) {
		super( aDecoratee );
		_cipher = aCipher;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		final CipherOutputStream theCipherOutputStream = new CipherOutputStream( aOutputStream, _cipher );
		final CipherInputStream theCipherInputStream = aReturnStream != null ? new CipherInputStream( aReturnStream, _cipher ) : null;
		_decoratee.transmitTo( theCipherOutputStream, theCipherInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final SequenceOutputStream theSequenceInputStream = new SequenceOutputStream();
		try {
			transmitTo( theSequenceInputStream );
		}
		catch ( IOException e ) {
			throw new BugException( "Encountered an I/O related bug which cannot occur as we merely use streams in memory!", e );
		}
		return theSequenceInputStream.getSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getClass(), toSequence(), getLength(), "A transmission decorator enriching the encapsulated transmission with \"Encryption\" as well as \"Decryption\" functionality (cypher).", getDecoratee().toSchema() );
	}
}
