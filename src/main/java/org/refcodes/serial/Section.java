// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The {@link Section} interface enables an implementing type to provide a
 * {@link Sequence} representation of itself or to transmit its {@link Sequence}
 * representation through an {@link OutputStream} (with an optional feedback
 * {@link InputStream}) as of {@link #transmitTo(OutputStream, InputStream)} and
 * to initialize with a {@link Sequence} representation for itself or to receive
 * a {@link Sequence} representation for itself through an {@link InputStream}
 * (with an optional feedback {@link OutputStream}) as of
 * {@link #receiveFrom(InputStream, int, OutputStream)}. A {@link Section} does
 * not have a predictable length and it cannot (by itself) determine itself's
 * length from a provided {@link InputStream} or {@link Sequence}, therefore a
 * length has to be provided alongside an {@link InputStream} or
 * {@link Sequence}.
 */
public interface Section extends Transmission {

	/**
	 * (Re-)initializes this instance with the the given byte array data.
	 * 
	 * @param aChunk The byte array data from which to (re-)initialize this
	 *        instance.
	 * @param aLength The length of data assigned by the byte array.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default void fromTransmission( byte[] aChunk, int aLength ) throws TransmissionException {
		fromTransmission( new ByteArraySequence( aChunk ), aLength );
	}

	/**
	 * (Re-)initializes this instance with the the given {@link Sequence} data.
	 * 
	 * @param aSequence The {@link Sequence} data from which to (re-)initialize
	 *        this instance.
	 * @param aLength The length of data assigned by the {@link Sequence}.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default void fromTransmission( Sequence aSequence, int aLength ) throws TransmissionException {
		fromTransmission( aSequence, 0, aLength );
	}

	/**
	 * (Re-)initializes this instance with the the given byte array data.
	 * 
	 * @param aChunk The byte array data from which to (re-)initialize this
	 *        instance.
	 * @param aOffset The offset where to start processing the provided byte
	 *        array.
	 * @param aLength The length of data assigned by the byte array.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default void fromTransmission( byte[] aChunk, int aOffset, int aLength ) throws TransmissionException {
		fromTransmission( new ByteArraySequence( aChunk ), aOffset, aLength );
	}

	/**
	 * (Re-)initializes this instance with the the given {@link Sequence} data.
	 * 
	 * @param aSequence The {@link Sequence} data from which to (re-)initialize
	 *        this instance.
	 * @param aOffset The offset where to start processing the provided
	 *        {@link Sequence}.
	 * @param aLength The length of data assigned by the {@link Sequence}.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException;

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link SerialTransceiver}'s
	 * {@link InputStream}. Implementations providing error correction methods
	 * use the provided {@link SerialTransceiver}'s feedback
	 * {@link OutputStream} to do some sort of "stop-and-wait ARQ" or apply
	 * similar methods to ensure correctness of the received data. This is a
	 * convenience method (actually the
	 * {@link #receiveFrom(InputStream, int, OutputStream )} method is invoked).
	 * Override {@link #receiveFrom(InputStream, int, OutputStream)} for your
	 * custom receiving functionality.
	 * 
	 * @param aSerialTransceiver The {@link SerialTransceiver} providing the
	 *        {@link InputStream} where to read this instance's {@link Sequence}
	 *        from and providing the {@link OutputStream} being the feedback
	 *        channel to handle "stop-and-wait ARQ".
	 * @param aLength The length of data assigned by the byte array.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given {@link InputStream}
	 *         bytes cannot be processed.
	 */
	default void receiveFrom( SerialTransceiver aSerialTransceiver, int aLength ) throws IOException {
		receiveFrom( aSerialTransceiver.getInputStream(), aLength, aSerialTransceiver.getOutputStream() );
	}

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link InputStream}. This is a
	 * convenience method in case there is no feedback {@link OutputStream}
	 * available (actually the
	 * {@link #receiveFrom(InputStream, int, OutputStream )} method is invoked
	 * with <code>null</code> for the feedback {@link OutputStream}). Override
	 * {@link #receiveFrom(InputStream, int, OutputStream)} for your custom
	 * receiving functionality.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        instance's (re-)initialization {@link Sequence} from.
	 * @param aLength The length of data assigned by the byte array.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given {@link InputStream}
	 *         bytes cannot be processed.
	 */
	default void receiveFrom( InputStream aInputStream, int aLength ) throws IOException {
		receiveFrom( aInputStream, aLength, null );
	}

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link InputStream}. Implementations
	 * providing error correction methods use the provided feedback
	 * {@link OutputStream} to do some sort of "stop-and-wait ARQ" or apply
	 * similar methods to ensure correctness of the received data.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        instance's (re-)initialization {@link Sequence} from.
	 * @param aLength The length of data assigned by the byte array.
	 * @param aReturnStream An {@link OutputStream} being the return channel to
	 *        handle "stop-and-wait ARQ" or the like in case of a bidirectional
	 *        connection. Can be null in case we have a unidirectional
	 *        connection.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given {@link InputStream}
	 *         bytes cannot be processed.
	 */
	void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException;

	// /////////////////////////////////////////////////////////////////////////
	// MIXINS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default implementation of the {@link Section} interface providing an
	 * implementation of the
	 * {@link #receiveFrom(InputStream, int, OutputStream)} method using the
	 * {@link #fromTransmission(Sequence, int)} method or the
	 * {@link #fromTransmission(Sequence, int, int)} method..
	 */
	public interface SectionMixin extends TransmissionMixin, Section {

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Default implementation harnessing the
		 * {@link #fromTransmission(Sequence, int)} method. {@inheritDoc}
		 */
		@Override
		default void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException {
			final byte[] theChunk = Transmission.fromInputStream( aInputStream, aLength );
			fromTransmission( theChunk, theChunk.length );
			if ( theChunk.length != getLength() ) {
				throw new TransmissionException( "The length <" + aLength + "> to process from the input stream is not equal to the actually resulting length <" + getLength() + ">." );
			}
		}
	}
}
