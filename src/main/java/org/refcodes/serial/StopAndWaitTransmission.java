// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * The {@link StopAndWaitTransmission} is the most simple implementation of the
 * ARQ (Automatic repeat request) protocol. Taken from the book "Serial
 * Programming": "... The sender sends a packet, then waits a little for an ACK.
 * As soon as it gets the ACK, it immediately sends the next packet. If the
 * sender doesn't hear the ACK in time, it starts over from the beginning,
 * sending the same packet again, until it does get an ACK ..." See
 * "https://en.wikibooks.org/wiki/Serial_Programming".
 */
public interface StopAndWaitTransmission extends ErrorCorrectionTransmission, AcknowledgeMagicBytesAccessor, AcknowledgeSegmentPackagerAccessor {

	/**
	 * Returns the ACK bytes to be used by the return channel to transmit an ACK
	 * (acknowledge) response after successful receiving a transmission.
	 * 
	 * @return The ACK character.
	 */
	@Override
	byte[] getAcknowledgeMagicBytes();

}
