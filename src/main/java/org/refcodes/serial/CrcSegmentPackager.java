// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.AbstractCrcMixin;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.Endianess;

/**
 * A {@link CrcSegmentPackager} enriches a {@link Segment} with a CRC checksum.
 */
public class CrcSegmentPackager extends AbstractCrcMixin implements SegmentPackager {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum. The configuration attributes
	 * are taken from the {@link TransmissionMetrics} configuration object,
	 * though only those attributes are supported which are also supported by
	 * the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public CrcSegmentPackager( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getCrcAlgorithm(), aTransmissionMetrics.getCrcChecksumConcatenateMode(), aTransmissionMetrics.getChecksumValidationMode(), aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm ) {
		this( aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		this( aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, aChecksumValidationMode, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		this( aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		this( aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 */
	public CrcSegmentPackager( ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( TransmissionMetrics.DEFAULT_CRC_ALGORITHM, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( aCrcAlgorithm, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum (using
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 *
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		this( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 *
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public CrcSegmentPackager( ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( TransmissionMetrics.DEFAULT_CRC_ALGORITHM, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aCrcAlgorithm, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according {@link CrcSegmentPackager} for packaging and
	 * extracting of packets with a CRC checksum.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        concatenating the CRC checksum with the transmission's
	 *        {@link Sequence}.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public CrcSegmentPackager( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Segment toPackaged( Segment aUnpacked ) {
		return new CrcSegmentDecorator<Segment>( aUnpacked, _crcAlgorithm, _crcChecksumConcatenateMode, _checksumValidationMode );
	}
}
