// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.ChildrenAccessor;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMap.SimpleTypeMapBuilder;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;

/**
 * A {@link TransmissionComposite} is a {@link Transmission} with child
 * {@link Transmission} elements.
 *
 * @param <CHILD> The type of the child {@link Transmission} elements.
 */
public interface TransmissionComposite<CHILD extends Transmission> extends Transmission, ChildrenAccessor<CHILD[]> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Sequence toSequence() {
		final Sequence theSequence = new ByteArraySequence();
		if ( getChildren() != null ) {
			CHILD eChild;
			Sequence eTransmission;
			for ( int i = 0; i < getChildren().length; i++ ) {
				eChild = getChildren()[i];
				eTransmission = eChild.toSequence();
				theSequence.append( eTransmission );
			}
		}
		return theSequence;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap toSimpleTypeMap() {
		final SimpleTypeMapBuilder theBuilder = new SimpleTypeMapBuilderImpl();
		if ( getChildren() != null && getChildren().length != 0 ) {
			for ( int i = 0; i < getChildren().length; i++ ) {
				theBuilder.insert( getChildren()[i].toSimpleTypeMap() );
			}
		}
		return theBuilder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SerialSchema toSchema() {
		SerialSchema[] theSchemas = null;
		if ( getChildren() != null && getChildren().length != 0 ) {
			theSchemas = new SerialSchema[getChildren().length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = getChildren()[i].toSchema();
			}
		}
		return new SerialSchema( getClass(), toSequence(), getLength(), "A parent segment containing child elements.", theSchemas );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default int getLength() {
		int theLength = 0;
		if ( getChildren() != null ) {
			for ( int i = 0; i < getChildren().length; i++ ) {
				theLength += getChildren()[i].getLength();
			}
		}
		return theLength;
	}
}
