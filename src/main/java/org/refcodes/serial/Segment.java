// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The {@link Segment} interface enables an implementing type to provide a
 * {@link Sequence} representation of itself or to transmit its {@link Sequence}
 * representation through an {@link OutputStream} (with an optional feedback
 * {@link InputStream}) as of {@link #transmitTo(OutputStream, InputStream)} and
 * to initialize with a {@link Sequence} representation for itself or to receive
 * a {@link Sequence} representation for itself through an {@link InputStream}
 * (with an optional feedback {@link OutputStream}) as of
 * {@link #receiveFrom(InputStream, OutputStream)}. A {@link Segment} has a
 * predictable length or it can (by itself) determine itself's length.
 */
public interface Segment extends Transmission {

	/**
	 * (Re-)initializes this instance with the the given byte array data.
	 * 
	 * @param aChunk The byte array data from which to (re-)initialize this
	 *        instance.
	 * 
	 * @return The index after the last offset into the given byte array
	 *         processed by this method.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default int fromTransmission( byte[] aChunk ) throws TransmissionException {
		return fromTransmission( new ByteArraySequence( aChunk ) );
	}

	/**
	 * (Re-)initializes this instance with the the given {@link Sequence} data.
	 * 
	 * @param aSequence The {@link Sequence} data from which to (re-)initialize
	 *        this instance.
	 * 
	 * @return The index after the last offset into the given {@link Sequence}
	 *         processed by this method.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default int fromTransmission( Sequence aSequence ) throws TransmissionException {
		return fromTransmission( aSequence, 0 );
	}

	/**
	 * (Re-)initializes this instance with the the given byte array data.
	 * 
	 * @param aChunk The byte array data from which to (re-)initialize this
	 *        instance.
	 * @param aOffset The offset where to start processing the provided byte
	 *        array.
	 * 
	 * @return The index after the last offset into the given byte array
	 *         processed by this method.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	default int fromTransmission( byte[] aChunk, int aOffset ) throws TransmissionException {
		return fromTransmission( new ByteArraySequence( aChunk ), aOffset );
	}

	/**
	 * (Re-)initializes this instance with the the given {@link Sequence} data.
	 * 
	 * @param aSequence The {@link Sequence} data from which to (re-)initialize
	 *        this instance.
	 * @param aOffset The offset where to start processing the provided
	 *        {@link Sequence}.
	 * 
	 * @return The index after the last offset into the given {@link Sequence}
	 *         processed by this method.
	 * 
	 * @throws TransmissionException thrown in case a given {@link Sequence}
	 *         cannot be processed.
	 */
	int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException;

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link InputStream}. This is a
	 * convenience method in case there is no feedback {@link OutputStream}
	 * available (actually the {@link #receiveFrom(InputStream, OutputStream )}
	 * method is invoked with <code>null</code> for the feedback
	 * {@link OutputStream}). Override
	 * {@link #receiveFrom(InputStream, OutputStream)} for your custom receiving
	 * functionality.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        instance's (re-)initialization {@link Sequence} from.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given transmission cannot
	 *         be processed.
	 */
	default void receiveFrom( InputStream aInputStream ) throws IOException {
		receiveFrom( aInputStream, null );
	}

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link SerialTransceiver}'s
	 * {@link InputStream}. Implementations providing error correction methods
	 * use the provided {@link SerialTransceiver}'s feedback
	 * {@link OutputStream} to do some sort of "stop-and-wait ARQ" or apply
	 * similar methods to ensure correctness of the received data. This is a
	 * convenience method (actually the
	 * {@link #receiveFrom(InputStream, OutputStream )} method is invoked).
	 * Override {@link #receiveFrom(InputStream, OutputStream)} for your custom
	 * receiving functionality.
	 * 
	 * @param aSerialTransceiver The {@link SerialTransceiver} providing the
	 *        {@link InputStream} where to read this instance's {@link Sequence}
	 *        from and providing the {@link OutputStream} being the feedback
	 *        channel to handle "stop-and-wait ARQ".
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given transmission cannot
	 *         be processed.
	 */
	default void receiveFrom( SerialTransceiver aSerialTransceiver ) throws IOException {
		receiveFrom( aSerialTransceiver.getInputStream(), aSerialTransceiver.getOutputStream() );
	}

	/**
	 * (Re-)initializes this instance by receiving the according
	 * {@link Sequence} from the given {@link InputStream}. Implementations
	 * providing error correction methods use the provided feedback
	 * {@link OutputStream} to do some sort of "stop-and-wait ARQ" or apply
	 * similar methods to ensure correctness of the received data.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        instance's (re-)initialization {@link Sequence} from.
	 * @param aReturnStream An {@link OutputStream} being the return channel to
	 *        handle "stop-and-wait ARQ" or the like in case of a bidirectional
	 *        connection. Can be null in case we have a unidirectional
	 *        connection.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 * @throws TransmissionException thrown in case a given transmission cannot
	 *         be processed.
	 */
	void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException;

	// /////////////////////////////////////////////////////////////////////////
	// MIXINS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default implementation of the {@link Segment} interface providing an
	 * implementation of the {@link #receiveFrom(InputStream, OutputStream)}
	 * method using the {@link #fromTransmission(Sequence)} method or the
	 * {@link #fromTransmission(Sequence, int)} method.
	 */
	public interface SegmentMixin extends TransmissionMixin, Segment {

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Default implementation harnessing the
		 * {@link #fromTransmission(Sequence)} method. {@inheritDoc}
		 */
		@Override
		default void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
			final byte[] theChunk = Transmission.fromInputStream( aInputStream, getLength() );
			fromTransmission( theChunk );
		}
	}
}
