// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoTimeout;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.io.SkipAvailableInputStream;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.ConcatenateModeAccessor.ConcatenateModeBuilder;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.ChecksumValidationModeAccessor.ChecksumValidationModeBuilder;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.CrcAlgorithmAccessor.CrcAlgorithmBuilder;
import org.refcodes.numerical.Endianess;
import org.refcodes.serial.SegmentPackager.DummySegmentPackager;
import org.refcodes.serial.SequenceNumberInitValueAccessor.SequenceNumberInitValueBuilder;
import org.refcodes.serial.SequenceNumberWidthAccessor.SequenceNumberWidthBuilder;

/**
 * The {@link StopAndWaitSegmentDecorator} class implements a decorator
 * providing {@link StopAndWaitTransmission} functionality for a
 * {@link Segment}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public class StopAndWaitSegmentDecorator<DECORATEE extends Segment> extends AbstractStopAndWaitTransmissionDecorator<DECORATEE> implements Segment, DecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private StopAndWaitSegmentDecorator( Builder<DECORATEE> aBuilder ) {
		this( aBuilder.decoratee, aBuilder.sequenceNumberInitValue, aBuilder.sequenceNumberWidth, aBuilder.sequenceNumberConcatenateMode, aBuilder.acknowledgeMagicBytes, aBuilder.ackRetryNumber, aBuilder.ackTimeoutInMs, aBuilder.toAckSegmentPackager(), aBuilder.endianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		super( aDecoratee, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		super( aDecoratee, aCrcChecksumConcatenateMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		super( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		super( aDecoratee, aCrcAlgorithm );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aCrcChecksumConcatenateMode, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aCrcAlgorithm, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, int aSequenceNumberInitValue, int aSequenceNumberWidth, ConcatenateMode aSequenceNumberConcatenateMode, byte[] aAcknowledgeMagicBytes, int aAckRetryNumber, long aAckTimeoutInMs, SegmentPackager aAckSegmentPackager, Endianess aEndianess ) {
		super( aDecoratee, aSequenceNumberInitValue, aSequenceNumberWidth, aSequenceNumberConcatenateMode, aAcknowledgeMagicBytes, aAckRetryNumber, aAckTimeoutInMs, aAckSegmentPackager, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, SegmentPackager aAckSegmentPackager ) {
		super( aDecoratee, aAckSegmentPackager );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	public StopAndWaitSegmentDecorator( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return _decoratee.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		if ( aReturnStream == null ) {
			_decoratee.receiveFrom( aInputStream, aReturnStream );
			return;
		}
		else {
			final RetryCounter theRetries = new RetryCounter( _acknowledgeRetryNumber, _acknowledgeTimeoutInMs );
			Exception eException = null;
			long eSequenceNumber;
			@SuppressWarnings("resource")
			final // Do not close me after done!
			SkipAvailableInputStream theSkipInputStream = new SkipAvailableInputStream( aInputStream, _acknowledgeTimeoutInMs );
			while ( theRetries.nextRetry() ) {
				eException = null;
				try {
					switch ( _sequenceNumberConcatenateMode ) {
					case APPEND -> {
						_decoratee.receiveFrom( aInputStream, aReturnStream );
						eSequenceNumber = readSequenceNumber( aInputStream );
					}
					case PREPEND -> {
						eSequenceNumber = readSequenceNumber( aInputStream );
						_decoratee.receiveFrom( aInputStream, aReturnStream );
					}
					default -> throw new UnhandledEnumBugException( _sequenceNumberConcatenateMode );
					}

					if ( ( _sequenceNumberInitValue == -1 && _sequenceNumber == 0 ) || eSequenceNumber == _sequenceNumber ) {
						// Ack |-->
						_acknowledgeSequenceNumberSegment.setPayload( eSequenceNumber );
						_acknowledgeSegment.transmitTo( aReturnStream );
						_sequenceNumber++;
						// Ack <--|
						return;
					}
				}
				catch ( Exception e ) {
					eException = e;
				}
				if ( theRetries.hasNextRetry() ) {
					try {
						theSkipInputStream.skipAvailableTillSilenceFor( IoTimeout.toSenseTimeoutTimeframeInMs( _acknowledgeTimeoutInMs ) );
					}
					catch ( IOException ignore ) {}
				}
			}
			if ( eException != null ) {
				throw new FlowControlRetryException( _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, "Aborting after <" + getAcknowledgeRetryNumber() + "> retries with a timeout for each retry of <" + getAcknowledgeTimeoutMillis() + "> milliseconds: " + eException.getMessage(), eException );
			}
			else {
				throw new FlowControlRetryException( _acknowledgeRetryNumber, _acknowledgeTimeoutInMs, "Aborting after <" + getAcknowledgeRetryNumber() + "> retries with a timeout for each retry of <" + getAcknowledgeTimeoutMillis() + "> milliseconds." );
			}
		}
	}

	/**
	 * Creates builder to build {@link StopAndWaitSegmentDecorator}.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 * 
	 * @return The created builder.
	 */
	public static <DECORATEE extends Segment> Builder<DECORATEE> builder() {
		return new Builder<>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link StopAndWaitSegmentDecorator} instances.
	 * 
	 * @param <DECORATEE> The decoratee type describing the according subclass
	 *        to be enriched.
	 */
	public static final class Builder<DECORATEE extends Segment> implements AcknowledgeRetryNumberBuilder<Builder<DECORATEE>>, AcknowledgeTimeoutMillisBuilder<Builder<DECORATEE>>, DecorateeBuilder<DECORATEE, Builder<DECORATEE>>, AcknowledgeMagicBytesBuilder<Builder<DECORATEE>>, SequenceNumberWidthBuilder<Builder<DECORATEE>>, SequenceNumberInitValueBuilder<Builder<DECORATEE>>, ConcatenateModeBuilder<Builder<DECORATEE>>, ChecksumValidationModeBuilder<Builder<DECORATEE>>, CrcAlgorithmBuilder<Builder<DECORATEE>>, EndianessBuilder<Builder<DECORATEE>>, AcknowledgeSegmentPackagerBuilder<Builder<DECORATEE>> {

		private DECORATEE decoratee = null;
		private int ackRetryNumber = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER;
		private long ackTimeoutInMs = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS;
		private byte[] acknowledgeMagicBytes = TransmissionMetrics.DEFAULT_ACKNOWLEDGE_MAGIC_BYTES;
		private int sequenceNumberWidth = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_WIDTH;
		private int sequenceNumberInitValue = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_INIT_VALUE;
		private ConcatenateMode sequenceNumberConcatenateMode = TransmissionMetrics.DEFAULT_SEQUENCE_NUMBER_CONCATENATE_MODE;
		private Endianess endianess = TransmissionMetrics.DEFAULT_ENDIANESS;
		private CrcAlgorithm crcAlgorithm = null;
		private final ConcatenateMode crcChecksumConcatenateMode = null;
		private ChecksumValidationMode crcChecksumValidationMode = null;
		private SegmentPackager ackSegmentPackager = null;

		private Builder() {}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeRetryNumber( int aAcknowledgeRetryNumber ) {
			ackRetryNumber = aAcknowledgeRetryNumber;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeTimeoutMillis( long aAckTimeoutInMs ) {
			ackTimeoutInMs = aAckTimeoutInMs;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withDecoratee( DECORATEE aDecoratee ) {
			decoratee = aDecoratee;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeMagicBytes( byte[] aAcknowledgeMagicBytes ) {
			acknowledgeMagicBytes = aAcknowledgeMagicBytes;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withSequenceNumberWidth( int aSequenceNumberWidth ) {
			sequenceNumberWidth = aSequenceNumberWidth;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			sequenceNumberInitValue = aSequenceNumberInitValue;
			return this;
		}

		/**
		 * {@inheritDoc}}
		 */
		@Override
		public Builder<DECORATEE> withConcatenateMode( ConcatenateMode aConcatenateMode ) {
			sequenceNumberConcatenateMode = aConcatenateMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			crcAlgorithm = aCrcAlgorithm;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode ) {
			crcChecksumValidationMode = aChecksumValidationMode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<DECORATEE> withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			ackSegmentPackager = aAcknowledgeSegmentPackager;
			return this;
		}

		/**
		 * Inferences the ACK {@link SegmentPackager}. In case one is available
		 * as of {@link #getAcknowledgeSegmentPackager()}, then that is
		 * returned. Else CRC settings are evaluated and if possible sufficient
		 * CRC settings are available, a {@link CrcSegmentPackager} is returned.
		 * If there are no sufficient CRC settings, then a
		 * {@link DummySegmentPackager} is returned.
		 * 
		 * @return An interferenced {@link SegmentPackager} as of the instance's
		 *         properties.
		 */
		SegmentPackager toAckSegmentPackager() {
			if ( ackSegmentPackager != null ) {
				return ackSegmentPackager;
			}
			if ( crcAlgorithm != null || crcChecksumValidationMode != null ) {
				final CrcAlgorithm theCrcAlgorithm = crcAlgorithm != null ? crcAlgorithm : TransmissionMetrics.DEFAULT_CRC_ALGORITHM;
				final ChecksumValidationMode theCrcChecksumValidationMode = crcChecksumValidationMode != null ? crcChecksumValidationMode : TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE;
				final ConcatenateMode theCrcChecksumConcatenateMode = crcChecksumConcatenateMode != null ? crcChecksumConcatenateMode : TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE;
				final Endianess theEndianess = endianess != null ? endianess : TransmissionMetrics.DEFAULT_ENDIANESS;
				return new CrcSegmentPackager( theCrcAlgorithm, theCrcChecksumConcatenateMode, theCrcChecksumValidationMode, theEndianess );
			}
			return new DummySegmentPackager();
		}

		/**
		 * Returns the {@link StopAndWaitSegmentDecorator} instance build
		 * according to the {@link Builder} configuration.
		 * 
		 * @return The accordingly configured
		 *         {@link StopAndWaitSegmentDecorator}.
		 */
		public StopAndWaitSegmentDecorator<DECORATEE> build() {
			return new StopAndWaitSegmentDecorator<>( this );
		}
	}
}
