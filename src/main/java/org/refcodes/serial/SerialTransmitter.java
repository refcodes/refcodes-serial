// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

import org.refcodes.component.LinkComponent.LinkAutomaton;
import org.refcodes.io.BytesTransmitter;

/**
 * A {@link SerialTransmitter} is used to write data to a serial port or the
 * like.
 */
public interface SerialTransmitter extends BytesTransmitter, SegmentTransmitter, LinkAutomaton {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void transmitBytes( byte[] aBytes ) throws IOException {
		transmitSequence( new ByteArraySequence( aBytes ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void transmitBytes( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		transmitSequence( new ByteArraySequence( aBytes, aOffset, aLength ) );
	}

	/**
	 * Transmits a {@link Sequence}.
	 * 
	 * @param aSequence The {@link Sequence} containing the data to be send.
	 * @param aOffset The offset from where to take the chunk data.
	 * @param aLength The number of bytes to take, beginning at the provided
	 *        offset.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         sending
	 */
	default void transmitSequence( Sequence aSequence, int aOffset, int aLength ) throws IOException {

		if ( aOffset == 0 && aLength == aSequence.getLength() ) {
			transmitSequence( aSequence );
		}
		else {
			transmitBytes( aSequence.toBytes( aOffset, aLength ) );
		}
	}

	/**
	 * Transmits a {@link Sequence} (and blocks this thread) till all it's data
	 * has been sent.
	 * 
	 * @param aSequence The {@link Sequence} containing the data to be send.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         sending
	 */
	void transmitSequence( Sequence aSequence ) throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <SEGMENT extends Segment> void transmitSegment( SEGMENT aSegment ) throws IOException {
		aSegment.transmitTo( getOutputStream(), null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void flush() throws IOException {}
}
