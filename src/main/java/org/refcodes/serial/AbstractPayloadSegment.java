// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.util.Arrays;

import org.refcodes.serial.Segment.SegmentMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapBuilderImpl;

/**
 * An abstract implementation of a {@link Segment} with payload.
 * 
 * @param <T> The type of the segment's payload.
 */
public abstract class AbstractPayloadSegment<T> implements PayloadSegment<T>, SegmentMixin {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected T _payload = null;
	protected String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Empty constructor fur sub-classes.
	 */
	protected AbstractPayloadSegment() {}

	/**
	 * Constructs an empty {@link Segment} to be initialized via
	 * {@link #fromTransmission(Sequence)} or
	 * {@link #fromTransmission(Sequence, int)}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public AbstractPayloadSegment( String aAlias ) {
		_alias = aAlias;
	}

	/**
	 * Constructs a {@link Segment} instance with the given value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the {@link Segment}.
	 */
	public AbstractPayloadSegment( String aAlias, T aValue ) {
		_alias = aAlias;
		_payload = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( T aValue ) {
		_payload = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getPayload() {
		return _payload;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + ( _payload == null ? null : ( _payload.getClass().isArray() ? Arrays.toString( (Object[]) _payload ) : _payload ) ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _payload == null ) ? 0 : ( ( _payload.getClass().isArray() ) ? Arrays.hashCode( (Object[]) _payload ) : _payload.hashCode() ) );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractPayloadSegment<?> other = (AbstractPayloadSegment<?>) obj;
		if ( _payload == null ) {
			if ( other._payload != null ) {
				return false;
			}
		}
		else if ( _payload.getClass().isArray() ) {
			// boolean[]:
			if ( _payload.getClass().getComponentType().equals( Boolean.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Boolean.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (boolean[]) _payload, (boolean[]) other._payload ) ) {
					return false;
				}
			}
			// byte[]:
			else if ( _payload.getClass().getComponentType().equals( Byte.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Byte.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (byte[]) _payload, (byte[]) other._payload ) ) {
					return false;
				}
			}
			// char[]:
			else if ( _payload.getClass().getComponentType().equals( Character.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Character.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (char[]) _payload, (char[]) other._payload ) ) {
					return false;
				}
			}
			// int[]:
			else if ( _payload.getClass().getComponentType().equals( Integer.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Integer.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (int[]) _payload, (int[]) other._payload ) ) {
					return false;
				}
			}
			// long[]:
			else if ( _payload.getClass().getComponentType().equals( Long.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Long.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (long[]) _payload, (long[]) other._payload ) ) {
					return false;
				}
			}
			// float[]:
			else if ( _payload.getClass().getComponentType().equals( Float.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Float.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (float[]) _payload, (float[]) other._payload ) ) {
					return false;
				}
			}
			// double[]:
			else if ( _payload.getClass().getComponentType().equals( Double.TYPE ) ) {
				if ( !other._payload.getClass().getComponentType().equals( Double.TYPE ) ) {
					return false;
				}
				if ( !Arrays.equals( (double[]) _payload, (double[]) other._payload ) ) {
					return false;
				}
			}
			// Object[]
			else if ( !Arrays.equals( (Object[]) _payload, (Object[]) other._payload ) ) {
				return false;
			}
		}
		else if ( !_payload.equals( other._payload ) ) {
			return false;
		}
		return true;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return new SimpleTypeMapBuilderImpl().withPut( _alias, _payload );
	}
}
