// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a packet {@link SegmentPackager} property.
 */
public interface PacketSegmentPackagerAccessor {

	/**
	 * Retrieves the {@link SegmentPackager} from the packet
	 * {@link SegmentPackager} property.
	 * 
	 * @return The {@link SegmentPackager} stored by the packet
	 *         {@link SegmentPackager} property.
	 */
	SegmentPackager getPacketSegmentPackager();

	/**
	 * Provides a mutator for a packet {@link SegmentPackager} property.
	 */
	public interface PacketSegmentPackagerMutator {

		/**
		 * Sets the {@link SegmentPackager} for the packet
		 * {@link SegmentPackager} property.
		 * 
		 * @param aPacketSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the packet {@link SegmentPackager} property.
		 */
		void setPacketSegmentPackager( SegmentPackager aPacketSegmentPackager );
	}

	/**
	 * Provides a builder method for a packet {@link SegmentPackager} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PacketSegmentPackagerBuilder<B extends PacketSegmentPackagerBuilder<B>> {

		/**
		 * Sets the {@link SegmentPackager} for the packet
		 * {@link SegmentPackager} property.
		 * 
		 * @param aPacketSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the packet {@link SegmentPackager} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPacketSegmentPackager( SegmentPackager aPacketSegmentPackager );
	}

	/**
	 * Provides a packet {@link SegmentPackager} property.
	 */
	public interface PacketSegmentPackagerProperty extends PacketSegmentPackagerAccessor, PacketSegmentPackagerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SegmentPackager}
		 * (setter) as of {@link #setPacketSegmentPackager(SegmentPackager)} and
		 * returns the very same value (getter).
		 * 
		 * @param aPacketSegmentPackager The {@link SegmentPackager} to set (via
		 *        {@link #setPacketSegmentPackager(SegmentPackager)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SegmentPackager letPacketSegmentPackager( SegmentPackager aPacketSegmentPackager ) {
			setPacketSegmentPackager( aPacketSegmentPackager );
			return aPacketSegmentPackager;
		}
	}
}
