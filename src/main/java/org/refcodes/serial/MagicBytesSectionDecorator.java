// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.refcodes.serial.Section.SectionMixin;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * Magic bytes are usually found (somewhere) at the beginning of a file or a
 * stream. A {@link MagicBytesSectionDecorator} decorates a {@link Section}
 * decoratee and prefixes this {@link Section} instance with given magic bytes.
 * 
 * @param <DECORATEE> The {@link Section} type describing the {@link Section}
 *        subclass decoratee.
 */
public class MagicBytesSectionDecorator<DECORATEE extends Section> extends AbstractMagicBytesTransmissionDecorator<DECORATEE> implements SectionMixin, DecoratorSection<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Enriches the provided decoratee with magic bytes of the given length to
	 * be prefixed. The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The decoratee which to be prefixed with magic bytes.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public MagicBytesSectionDecorator( String aAlias, DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aDecoratee, aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * Enriches the provided {@link Section} with magic bytes of the given
	 * length to be prefixed.
	 * 
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public MagicBytesSectionDecorator( DECORATEE aDecoratee, int aMagicBytesLength ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSectionDecorator.class.getSimpleName() ), aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSectionDecorator( DECORATEE aDecoratee, String aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSectionDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 * 
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSectionDecorator( DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSectionDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed.
	 * 
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 */
	public MagicBytesSectionDecorator( DECORATEE aDecoratee, byte[] aMagicBytes ) {
		this( CaseStyleBuilder.asCamelCase( MagicBytesSectionDecorator.class.getSimpleName() ), aDecoratee, aMagicBytes );
	}

	// -------------------------------------------------------------------------

	/**
	 * Enriches the provided {@link Section} with magic bytes of the given
	 * length to be prefixed.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytesLength The length of the magic bytes sequence.
	 */
	public MagicBytesSectionDecorator( String aAlias, DECORATEE aDecoratee, int aMagicBytesLength ) {
		super( aAlias, aDecoratee, aMagicBytesLength );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes (uses the
	 *        {@link TransmissionMetrics#DEFAULT_ENCODING}) for byte
	 *        conversion).
	 */
	public MagicBytesSectionDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with magic bytes being prefixed
	 * (retrieved from the given {@link String}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The {@link String} to be stored by this instance as
	 *        magic bytes.
	 * @param aCharset The {@link Charset} to use when converting the
	 *        {@link String} to a byte array.
	 */
	public MagicBytesSectionDecorator( String aAlias, DECORATEE aDecoratee, String aMagicBytes, Charset aCharset ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	/**
	 * Enriches the provided {@link Section} with the given magic bytes being
	 * prefixed.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aDecoratee The {@link Section} which is to be prefixed with magic
	 *        bytes.
	 * @param aMagicBytes The magic bytes to be prefixed.
	 */
	public MagicBytesSectionDecorator( String aAlias, DECORATEE aDecoratee, byte[] aMagicBytes ) {
		super( aAlias, aDecoratee, aMagicBytes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		_magicBytes = aSequence.toBytes( aOffset, _magicBytesLength );
		aOffset += _magicBytesLength;
		aLength -= _magicBytesLength;
		getDecoratee().fromTransmission( aSequence, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, int aLength, OutputStream aReturnStream ) throws IOException {
		_magicBytes = ( _magicBytes != null && _magicBytes.length != _magicBytesLength ) ? new byte[_magicBytesLength] : _magicBytes;
		aInputStream.read( _magicBytes );
		getDecoratee().receiveFrom( aInputStream, aLength - _magicBytesLength, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MagicBytesSectionDecorator<DECORATEE> withPayload( byte[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A section decorator prefixing the encapsulated section with magic bytes.", _decoratee.toSchema() );
		theSchema.put( MAGIC_BYTES, getMagicBytes() );
		theSchema.put( MAGIC_BYTES_TEXT, toMagicBytesString() );
		return theSchema;
	}
}
