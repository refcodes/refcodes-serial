// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link LongSegment} is an implementation of a {@link Segment} carrying in
 * long value as payload.
 */
public class LongSegment extends AbstractPayloadSegment<Long> implements Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";
	public static final int BYTES = Long.BYTES;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Endianess _endianess;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongSegment( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongSegment( Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public LongSegment( String aAlias, Long aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link LongSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 */
	public LongSegment() {
		this( CaseStyleBuilder.asCamelCase( LongSegment.class.getSimpleName() ) );
	}

	/**
	 * Constructs an empty {@link LongSegment} with the given {@link Endianess}.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongSegment( Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( LongSegment.class.getSimpleName() ), aEndianess );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aValue The payload to be contained by the {@link LongSegment}.
	 */
	public LongSegment( Long aValue ) {
		this( CaseStyleBuilder.asCamelCase( LongSegment.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link LongSegment}'s value (payload).
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongSegment( Long aValue, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( LongSegment.class.getSimpleName() ), aValue, aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link LongSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public LongSegment( String aAlias ) {
		this( aAlias, 0L, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an empty {@link LongSegment} with the given {@link Endianess}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongSegment( String aAlias, Endianess aEndianess ) {
		this( aAlias, 0L, aEndianess );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link LongSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by the {@link LongSegment}.
	 */
	public LongSegment( String aAlias, Long aValue ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link LongSegment} with the given long payload and the
	 * given {@link Endianess} for the representation of the
	 * {@link LongSegment}'s value (payload).
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link LongSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public LongSegment( String aAlias, Long aValue, Endianess aEndianess ) {
		super( aAlias, aValue );
		_endianess = aEndianess;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _endianess.toBytes( getPayload(), BYTES ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, BYTES );
		setPayload( _endianess.toLong( theRecord ) );
		return aOffset + BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = 0L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), Long.toString( getPayload() ), "A body containing an long payload." );
		theSchema.put( ENDIANESS, _endianess );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LongSegment withPayload( Long aValue ) {
		setPayload( aValue );
		return this;
	}
}
