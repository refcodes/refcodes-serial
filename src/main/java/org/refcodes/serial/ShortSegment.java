// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link ShortSegment} is an implementation of a {@link Segment} carrying
 * in short value as payload.
 */
public class ShortSegment extends AbstractPayloadSegment<Short> implements Segment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ENDIANESS = "ENDIANESS";
	public static final int BYTES = Short.BYTES;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Endianess _endianess;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortSegment( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortSegment( Short aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link DoubleArraySection}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public ShortSegment( String aAlias, Short aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aValue, aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ShortSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 */
	public ShortSegment() {
		this( CaseStyleBuilder.asCamelCase( ShortSegment.class.getSimpleName() ) );
	}

	/**
	 * Constructs an empty {@link ShortSegment} with the given {@link Endianess}
	 * endian representation of the {@link ShortSegment}'s value.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 */
	public ShortSegment( Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( ShortSegment.class.getSimpleName() ), aEndianess );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 */
	public ShortSegment( Short aValue ) {
		this( CaseStyleBuilder.asCamelCase( ShortSegment.class.getSimpleName() ), aValue );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 */
	public ShortSegment( Short aValue, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( ShortSegment.class.getSimpleName() ), aValue, aEndianess );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link ShortSegment} with a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public ShortSegment( String aAlias ) {
		this( aAlias, (short) 0, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an empty {@link ShortSegment} with the given {@link Endianess}
	 * endian representation of the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 */
	public ShortSegment( String aAlias, Endianess aEndianess ) {
		this( aAlias, (short) 0, aEndianess );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the {@link ShortSegment}'s value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 */
	public ShortSegment( String aAlias, Short aValue ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link ShortSegment} with the given short payload and the
	 * given {@link Endianess} for the representation of the {@link Segment}'s
	 * value.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The value (payload) to be contained by the
	 *        {@link ShortSegment}.
	 * @param aEndianess The {@link Endianess} to be used for the payload
	 *        values.
	 */
	public ShortSegment( String aAlias, Short aValue, Endianess aEndianess ) {
		super( aAlias, aValue );
		_endianess = aEndianess;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( _endianess.toBytes( getPayload(), BYTES ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		final byte[] theRecord = aSequence.toBytes( aOffset, BYTES );
		setPayload( _endianess.toShort( theRecord ) );
		return aOffset + BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return BYTES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_payload = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "" + getPayload(), "A body containing an short payload." );
		theSchema.put( ENDIANESS, _endianess );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShortSegment withPayload( Short aValue ) {
		setPayload( aValue );
		return this;
	}
}
