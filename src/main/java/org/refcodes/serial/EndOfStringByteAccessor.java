// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a end of string byte property.
 */
public interface EndOfStringByteAccessor {

	/**
	 * Retrieves the end of string byte from the end of string byte property.
	 * 
	 * @return The end of string byte stored by the end of string byte property.
	 */
	byte getEndOfStringByte();

	/**
	 * Provides a mutator for a end of string byte property.
	 */
	public interface EndOfStringByteMutator {

		/**
		 * Sets the end of string byte for the end of string byte property.
		 * 
		 * @param aEndOfStringByte The end of string byte to be stored by the
		 *        end of string byte property.
		 */
		void setEndOfStringByte( byte aEndOfStringByte );
	}

	/**
	 * Provides a builder method for a end of string byte property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface EndOfStringByteBuilder<B extends EndOfStringByteBuilder<B>> {

		/**
		 * Sets the end of string byte for the end of string byte property.
		 * 
		 * @param aEndOfStringByte The end of string byte to be stored by the
		 *        end of string byte property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withEndOfStringByte( byte aEndOfStringByte );
	}

	/**
	 * Provides a end of string byte property.
	 */
	public interface EndOfStringByteProperty extends EndOfStringByteAccessor, EndOfStringByteMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setEndOfStringByte(byte)} and returns the very same value
		 * (getter).
		 * 
		 * @param aEndOfStringByte The integer to set (via
		 *        {@link #setEndOfStringByte(byte)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte letEndOfStringByte( byte aEndOfStringByte ) {
			setEndOfStringByte( aEndOfStringByte );
			return aEndOfStringByte;
		}
	}
}
