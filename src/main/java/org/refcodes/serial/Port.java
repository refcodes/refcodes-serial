// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.component.ConnectionOpenable.ConnectionOpenBuilder;
import org.refcodes.component.Openable.OpenBuilder;
import org.refcodes.mixin.AliasAccessor;
import org.refcodes.textual.NumberSuffixComparatorSingleton;

/**
 * A {@link Port} enriches a {@link SerialTransceiver} with {@link PortMetrics}
 * describing the {@link SerialTransceiver}'s physical properties.
 * 
 * @param <PM> The actual {@link PortMetrics} type to use.
 */
public interface Port<PM extends PortMetrics> extends SerialTransceiver, PortMetricsAccessor<PM>, AliasAccessor, OpenBuilder<Port<PM>>, ConnectionOpenBuilder<PM, Port<PM>>, ConnectionAutomaton<PM>, Comparable<Port<?>> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default int compareTo( Port<?> aPort ) {
		return NumberSuffixComparatorSingleton.getInstance().compare( getAlias(), aPort.getAlias() );
	}
}
