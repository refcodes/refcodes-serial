// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link Transmission} property (as of the decorator
 * pattern).
 *
 * @param <T> The type of the {@link Transmission} to be used.
 */
public interface TransmissionAccessor<T extends Transmission> {

	/**
	 * Retrieves the value from the {@link Transmission} property.
	 * 
	 * @return The {@link Transmission} stored by the {@link Transmission}
	 *         property.
	 */
	T getTransmission();

	/**
	 * Provides a mutator for a {@link Transmission} property.
	 * 
	 * @param <T> The type of the {@link Transmission} property.
	 */
	public interface TransmissionMutator<T extends Transmission> {

		/**
		 * Sets the {@link Transmission} for the {@link Transmission} property.
		 * 
		 * @param aTransmission The {@link Transmission} to be stored by the
		 *        {@link Transmission} property.
		 */
		void setTransmission( T aTransmission );
	}

	/**
	 * Provides a builder method for a {@link Transmission} property returning
	 * the builder for applying multiple build operations.
	 *
	 * @param <T> the {@link Transmission} type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TransmissionBuilder<T, B extends TransmissionBuilder<T, B>> {

		/**
		 * Sets the {@link Transmission} for the {@link Transmission} property.
		 * 
		 * @param aTransmission The {@link Transmission} to be stored by the
		 *        {@link Transmission} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTransmission( T aTransmission );
	}

	/**
	 * Provides a {@link Transmission} property.
	 * 
	 * @param <T> The type of the {@link Transmission} property.
	 */
	public interface TransmissionProperty<T extends Transmission> extends TransmissionAccessor<T>, TransmissionMutator<T> {

		/**
		 * Sets the given {@link Transmission} (setter) as of
		 * {@link #setTransmission(Transmission)} and returns the very same
		 * {@link Transmission} (getter). This method stores and passes through
		 * the given {@link Transmission}, which is very useful for builder
		 * APIs.
		 * 
		 * @param aTransmission The {@link Transmission} to set (via
		 *        {@link #setTransmission(Transmission)}).
		 * 
		 * @return Returns {@link Transmission} passed for it to be used in
		 *         conclusive processing steps.
		 */
		default T letTransmission( T aTransmission ) {
			setTransmission( aTransmission );
			return aTransmission;
		}
	}
}
