// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

/**
 * A daemon handling {@link SerialTransmitter#doTransmitSegment(Segment)}
 * asynchronous calls.
 *
 * @param <SEGMENT> The type of the {@link Segment} in question.
 */
public class TransmitSegmentResultDaemon<SEGMENT extends Segment> implements Runnable {

	private final SegmentResult<SEGMENT> _segmentResult;
	private final SerialTransceiver _serialTransceiver;

	/**
	 * Instantiates a new transmit segment result daemon.
	 *
	 * @param aSegment the segment
	 * @param aSerialTransceiver the serial transceiver
	 */
	public TransmitSegmentResultDaemon( SEGMENT aSegment, SerialTransceiver aSerialTransceiver ) {
		_segmentResult = new SegmentResult<>( aSegment );
		_serialTransceiver = aSerialTransceiver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void run() {
		try {
			_segmentResult.getSegment().transmitTo( _serialTransceiver );
			_segmentResult.notifyResult();
			synchronized ( _segmentResult ) {
				_segmentResult.notifyAll();
			}
		}
		catch ( IOException e ) {
			_segmentResult.setException( e );
			_segmentResult.notifyResult();
			synchronized ( _segmentResult ) {
				_segmentResult.notifyAll();
			}
		}
	}

	/**
	 * Gets the segment result.
	 *
	 * @return the segment result
	 */
	public SegmentResult<SEGMENT> getSegmentResult() {
		return _segmentResult;
	}
}