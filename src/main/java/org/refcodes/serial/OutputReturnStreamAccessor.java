// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.OutputStream;

/**
 * Provides an accessor for an output return stream property.
 */
public interface OutputReturnStreamAccessor {

	/**
	 * Retrieves the output return stream from the output return stream
	 * property.
	 * 
	 * @return The output return stream stored by the output return stream
	 *         property.
	 */
	OutputStream getReturnStream();

	/**
	 * Provides a mutator for an output return stream property.
	 */
	public interface OutputReturnStreamMutator {

		/**
		 * Sets the output return stream for the output return stream property.
		 * 
		 * @param aReturnStream The output return stream to be stored by the
		 *        output return stream property.
		 */
		void setReturnStream( OutputStream aReturnStream );
	}

	/**
	 * Provides a mutator for an output return stream property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link OutputReturnStreamBuilder}.
	 */
	public interface OutputReturnStreamBuilder<B extends OutputReturnStreamBuilder<?>> {

		/**
		 * Sets the output return stream to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aReturnStream The output return stream to be stored by the
		 *        output return stream property.
		 * 
		 * @return This {@link OutputReturnStreamBuilder} instance to continue
		 *         configuration.
		 */
		B withReturnStream( OutputStream aReturnStream );
	}

	/**
	 * Provides an output return stream property.
	 */
	public interface OutputReturnStreamProperty extends OutputReturnStreamAccessor, OutputReturnStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link OutputStream}
		 * (setter) as of {@link #setReturnStream(OutputStream)} and returns the
		 * very same value (getter).
		 * 
		 * @param aReturnStream The {@link OutputStream} to set (via
		 *        {@link #setReturnStream(OutputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default OutputStream letReturnStream( OutputStream aReturnStream ) {
			setReturnStream( aReturnStream );
			return aReturnStream;
		}
	}
}
