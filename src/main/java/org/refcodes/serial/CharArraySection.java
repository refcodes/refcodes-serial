// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link CharArraySection} is an implementation of a {@link PayloadSection}
 * carrying a char array as payload.
 */
public class CharArraySection extends AbstractPayloadSection<char[]> implements EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _charset; // <--| "String" instead of "Charset" in order to be serializable

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public CharArraySection( TransmissionMetrics aTransmissionMetrics ) {
		this( aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 */
	public CharArraySection( TransmissionMetrics aTransmissionMetrics, char... aValue ) {
		this( aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 */
	public CharArraySection( TransmissionMetrics aTransmissionMetrics, Character... aValue ) {
		this( aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public CharArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 */
	public CharArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, char... aValue ) {
		this( aAlias, aTransmissionMetrics.getEncoding(), aValue );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aValue The payload to be contained by the
	 *        {@link CharArraySection}.
	 */
	public CharArraySection( String aAlias, TransmissionMetrics aTransmissionMetrics, Character... aValue ) {
		this( aAlias, aTransmissionMetrics.getEncoding(), aValue );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link CharArraySection}. Uses UTF-16 by default for
	 * encoding.
	 */
	public CharArraySection() {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ) );
	}

	/**
	 * Constructs an empty {@link CharArraySection}.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 */
	public CharArraySection( Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ), aCharset );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( char[] aPayload ) {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ), aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( Character[] aPayload ) {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ), aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( Charset aCharset, char... aPayload ) {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ), aCharset, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( Charset aCharset, Character... aPayload ) {
		this( CaseStyleBuilder.asCamelCase( CharArraySection.class.getSimpleName() ), aCharset, toPrimitiveArray( aPayload ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an empty {@link CharArraySection}. Uses UTF-16 by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public CharArraySection( String aAlias ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENCODING, new char[0] );
	}

	/**
	 * Constructs an empty {@link CharArraySection}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 */
	public CharArraySection( String aAlias, Charset aCharset ) {
		this( aAlias, aCharset, new char[0] );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( String aAlias, char[] aPayload ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENCODING, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( String aAlias, Character[] aPayload ) {
		this( aAlias, TransmissionMetrics.DEFAULT_ENCODING, aPayload );
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( String aAlias, Charset aCharset, char... aPayload ) {
		super( aAlias, aPayload );
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();
	}

	/**
	 * Constructs a {@link CharArraySection} with the given char array payload.
	 * Uses {@link TransmissionMetrics#DEFAULT_ENCODING} by default for
	 * encoding.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The charset to be used for encoding.
	 * @param aPayload The payload to be contained by the
	 *        {@link PayloadSection}.
	 */
	public CharArraySection( String aAlias, Charset aCharset, Character... aPayload ) {
		super( aAlias, toPrimitiveArray( aPayload ) );
		_charset = aCharset != null ? aCharset.name() : TransmissionMetrics.DEFAULT_ENCODING.name();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CharArraySection withPayload( char[] aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return new ByteArraySequence( new String( getPayload() ).getBytes( getEncoding() ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromTransmission( Sequence aSequence, int aOffset, int aLength ) throws TransmissionException {
		final char[] theRecord;
		theRecord = new String( aSequence.toBytes( aOffset, aLength ), getEncoding() ).toCharArray();
		setPayload( theRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return toSequence().getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), new String( getPayload() ), "A body containing a char array payload." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", value=" + Arrays.toString( getPayload() ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset != null ? Charset.forName( _charset ) : TransmissionMetrics.DEFAULT_ENCODING;
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #setPayload(Object)}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 */
	public void setPayload( Character[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
	}

	/**
	 * Convenience method to convert the array of wrapper types into its
	 * counterpart with primitive types before invoking
	 * {@link #withPayload(char[])}.
	 * 
	 * @param aPayload The payload with the wrapper types.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public CharArraySection withPayload( Character[] aPayload ) {
		setPayload( toPrimitiveArray( aPayload ) );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static char[] toPrimitiveArray( Character[] aPayload ) {
		final char[] theResult = new char[aPayload.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aPayload[i];
		}
		return theResult;
	}
}
