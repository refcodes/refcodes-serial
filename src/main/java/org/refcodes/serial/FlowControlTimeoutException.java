// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.mixin.TimeoutMillisAccessor;

/**
 * Thrown in case a flow control failed due t a timeout.
 */
public class FlowControlTimeoutException extends FlowControlException implements TimeoutMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final long _timeoutInMs;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, String aMessage ) {
		super( aMessage );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aTimeoutMillis The timeout in milliseconds causing the exception.
	 */
	public FlowControlTimeoutException( long aTimeoutMillis, Throwable aCause ) {
		super( aCause );
		_timeoutInMs = aTimeoutMillis;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	@Override
	public long getTimeoutMillis() {
		return _timeoutInMs;
	}
}
