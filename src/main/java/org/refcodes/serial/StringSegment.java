// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.mixin.ValueAccessor.ValueBuilder;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor.EndianessBuilder;
import org.refcodes.serial.LengthWidthAccessor.LengthWidthBuilder;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link StringSegment} is an implementation of a {@link Section} carrying
 * a {@link String} as payload. By default, if not otherwise specified, the
 * {@link TransmissionMetrics#DEFAULT_ENCODING} encoding is used for encoding
 * and decoding {@link String} instances.
 */
public class StringSegment implements PayloadSegment<String>, EncodingAccessor<Charset> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private AllocSectionDecoratorSegment<StringSection> _allocSectionDecoratorSegment;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private StringSegment( Builder aBuilder ) {
		this( aBuilder.alias, aBuilder.value, aBuilder.encoding, aBuilder.lengthWidth, aBuilder.endianess );
	}

	/**
	 * Instantiates a new string segment.
	 */
	public StringSegment() {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSegment( TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aValue The payload to be contained by the {@link StringSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSegment( String aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs an according instance from the given configuration. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this instance.
	 * @param aValue The payload to be contained by the {@link StringSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public StringSegment( String aAlias, String aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aValue, aTransmissionMetrics.getEncoding(), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 */
	public StringSegment( String aValue ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 * 
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public StringSegment( Charset aEncoding ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 */
	public StringSegment( String aValue, Charset aEncoding ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 */
	public StringSegment( String aAlias, String aValue ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 */
	public StringSegment( String aAlias, String aValue, Charset aEncoding ) {
		this( aAlias, aValue, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an empty allocation value with a width of
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} bytes used to specify
	 * the decoratee's length and the provided {@link Endianess} representation
	 * of the decoratee's length.
	 * 
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the decoratee's length and a
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} endian representation of
	 * the decoratee's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public StringSegment( int aLengthWidth ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an empty allocation value with the given number of bytes used
	 * to specify the decoratee's length and the provided {@link Endianess}
	 * representation of the decoratee's length.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aValue, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( Charset aEncoding, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aValue, Charset aEncoding, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, Endianess aEndianess ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENCODING, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, Charset aEncoding, Endianess aEndianess ) {
		this( aAlias, aValue, aEncoding, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String}payload*
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.**
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */

	public StringSegment( String aValue, int aLengthWidth ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public StringSegment( Charset aEncoding, int aLengthWidth ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, aEncoding, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public StringSegment( String aValue, Charset aEncoding, int aLengthWidth ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, aEncoding, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, int aLengthWidth ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, Charset aEncoding, int aLengthWidth ) {
		this( aAlias, aValue, aEncoding, aLengthWidth, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aValue, int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with a payload expected to be encoded
	 * with the given {@link Charset}.
	 *
	 * @param aEncoding The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), null, aEncoding, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aValue, Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( StringSegment.class.getSimpleName() ), aValue, aEncoding, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, int aLengthWidth, Endianess aEndianess ) {
		this( aAlias, aValue, TransmissionMetrics.DEFAULT_ENCODING, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs a {@link StringSegment} with the given {@link String} payload
	 * being encoded with the given {@link Charset}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The payload to be contained by this {@link StringSegment}.
	 * @param aEncoding The {@link Charset} to be used for encoding the
	 *        {@link String}.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for length values.
	 */
	public StringSegment( String aAlias, String aValue, Charset aEncoding, int aLengthWidth, Endianess aEndianess ) {
		_allocSectionDecoratorSegment = new AllocSectionDecoratorSegment<>( new StringSection( aAlias, aValue, aEncoding ), aLengthWidth, aEndianess );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _allocSectionDecoratorSegment.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return _allocSectionDecoratorSegment.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		_allocSectionDecoratorSegment.receiveFrom( aInputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _allocSectionDecoratorSegment.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_allocSectionDecoratorSegment.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_allocSectionDecoratorSegment.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), getPayload(), "Wrapper segment for an allocation segment containing a string section payload.", _allocSectionDecoratorSegment.toSchema() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringSegment withPayload( String aValue ) {
		setPayload( aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _allocSectionDecoratorSegment.getDecoratee().getEncoding();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _allocSectionDecoratorSegment.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _allocSectionDecoratorSegment.getDecoratee().getAlias();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPayload() {
		return _allocSectionDecoratorSegment.getDecoratee().getPayload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPayload( String aPayload ) {
		_allocSectionDecoratorSegment.getDecoratee().setPayload( aPayload );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + getAlias() + ", value=" + ( getPayload() == null ? null : getPayload() ) + "]";
	}

	/**
	 * Creates builder to build {@link StringSegment}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link StringSegment} instances.
	 */
	public static final class Builder implements AliasBuilder<Builder>, ValueBuilder<String, Builder>, EncodingBuilder<Charset, Builder>, LengthWidthBuilder<Builder>, EndianessBuilder<Builder> {

		private String alias;
		private String value;
		private Charset encoding;
		private int lengthWidth;
		private Endianess endianess;

		private Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAlias( String aAlias ) {
			alias = aAlias;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withValue( String aValue ) {
			value = aValue;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEncoding( Charset aEncoding ) {
			encoding = aEncoding;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLengthWidth( int aLengthWidth ) {
			lengthWidth = aLengthWidth;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEndianess( Endianess aEndianess ) {
			endianess = aEndianess;
			return this;
		}

		/**
		 * Builds the accordingly configured {@link StringSegment} instance.
		 *
		 * @return The {@link StringSegment} being configured.
		 */
		public StringSegment build() {
			return new StringSegment( this );
		}
	}
}
