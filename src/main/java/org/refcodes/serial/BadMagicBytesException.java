// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * A {@link BadMagicBytesException} denotes bad (unexpected) magic bytes
 * encountered while processing a transmission.
 */
public class BadMagicBytesException extends TransmissionException implements MagicBytesAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final byte[] _magicBytes;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_magicBytes = aMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_magicBytes = aMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, String message, Throwable cause ) {
		super( message, cause );
		_magicBytes = aMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, String message ) {
		super( message );
		_magicBytes = aMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_magicBytes = aMagicBytes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMagicBytes The according bad magic bytes.
	 */
	public BadMagicBytesException( byte[] aMagicBytes, Throwable cause ) {
		super( cause );
		_magicBytes = aMagicBytes;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getMagicBytes() {
		return _magicBytes;
	}
}
