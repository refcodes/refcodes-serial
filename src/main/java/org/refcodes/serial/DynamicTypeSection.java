// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * The {@link DynamicTypeSection} represents a {@link Section} which's internal
 * raw data is created from provided types at runtime and which's internal raw
 * data is used to create provided types at runtime. Therefore internally it
 * just consists of a {@link Sequence} which is created if necessary from a
 * provided type or from which a provided type is instantiated if necessary.
 */
public class DynamicTypeSection extends SequenceSection implements Section, LengthWidthAccessor, EndianessAccessor, EncodingAccessor<Charset>, DynamicTypeTransmission {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Charset _charset;
	private Endianess _endianess;
	private int _lengthWidth;
	private ComplexTypeSegment<?> _segment = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 */
	public DynamicTypeSection() {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSection( Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, aCharset );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public DynamicTypeSection( int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aLengthWidth, aEndianess, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSection( int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public DynamicTypeSection( String aAlias ) {
		this( aAlias, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSection( String aAlias, Charset aCharset ) {
		this( aAlias, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, aCharset );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public DynamicTypeSection( String aAlias, int aLengthWidth, Endianess aEndianess ) {
		this( aAlias, aLengthWidth, aEndianess, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSection( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		super( aAlias );
		_lengthWidth = aLengthWidth;
		_endianess = aEndianess;
		_charset = aCharset;
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue ) {
		super( aAlias );
		fromType( aValue );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, Charset aCharset ) {
		this( aAlias, aCharset );
		fromType( aValue );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, Charset aCharset, String... aAttributes ) {
		this( aAlias, aCharset );
		fromType( aValue, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess ) {
		this( aAlias, aLengthWidth, aEndianess );
		fromType( aValue );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( aAlias, aLengthWidth, aEndianess, aCharset );
		fromType( aValue );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( aAlias, aLengthWidth, aEndianess, aCharset );
		fromType( aValue, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		this( aAlias, aLengthWidth, aEndianess );
		fromType( aValue, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, String... aAttributes ) {
		super( aAlias );
		fromType( aValue, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
		fromType( aValue );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		this( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
		fromType( aValue, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public DynamicTypeSection( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		this( aAlias, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 */
	public <T> DynamicTypeSection( T aValue ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSection( T aValue, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, aCharset );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( T aValue, Charset aCharset, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, aCharset, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public <T> DynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aLengthWidth, aEndianess, TransmissionMetrics.DEFAULT_ENCODING );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aLengthWidth, aEndianess, aCharset );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aLengthWidth, aEndianess, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( T aValue, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, TransmissionMetrics.DEFAULT_LENGTH_WIDTH, TransmissionMetrics.DEFAULT_ENDIANESS, TransmissionMetrics.DEFAULT_ENCODING, aAttributes );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public <T> DynamicTypeSection( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSection( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aValue, aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	/**
	 * Constructs the {@link DynamicTypeSection} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public DynamicTypeSection( TransmissionMetrics aTransmissionMetrics ) {
		this( CaseStyleBuilder.asCamelCase( DynamicTypeSection.class.getSimpleName() ), aTransmissionMetrics.getLengthWidth(), aTransmissionMetrics.getEndianess(), aTransmissionMetrics.getEncoding() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> void fromType( T aValue ) {
		_segment = new ComplexTypeSegment<>( aValue, _lengthWidth, _endianess, getEncoding() );
		_sequence = _segment.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> void fromType( T aValue, String... aAttributes ) {
		final ComplexTypeSegment<T> theSegment = new ComplexTypeSegment<>( aValue, _lengthWidth, _endianess, getEncoding(), aAttributes );
		_sequence = theSegment.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _charset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLengthWidth() {
		return _lengthWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_segment.reset();
		_sequence.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getAlias(), getClass(), toSequence(), getLength(), "A segment containing a sequence as intermediate payload retrieved from a type and used for retrieving types after transmission." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _segment != null ? _segment.toSimpleTypeMap() : super.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( Class<T> aType ) {
		final ComplexTypeSegment<T> theSegment = new ComplexTypeSegment<>( aType, _lengthWidth, _endianess, getEncoding() );
		try {
			final int theOffset = theSegment.fromTransmission( _sequence );
			if ( theOffset != _sequence.getLength() ) {
				throw new IllegalArgumentException( "Cannot create instance of type <" + aType.getName() + "> as the processed length <" + theOffset + "> does not match the sequence's <" + _sequence.toHexString() + "> length <" + _sequence.getLength() + ">!" );
			}
		}
		catch ( TransmissionException e ) {
			throw new IllegalArgumentException( "Cannot create instance of type <" + aType.getName() + "> for sequence <" + _sequence.toHexString() + ">!", e );
		}
		return theSegment.getPayload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( Class<T> aType, String... aAttributes ) {
		final ComplexTypeSegment<T> theSegment = new ComplexTypeSegment<>( aType, _lengthWidth, _endianess, getEncoding(), aAttributes );
		try {
			final int theOffset = theSegment.fromTransmission( _sequence );
			if ( theOffset != _sequence.getLength() ) {
				throw new IllegalArgumentException( "Cannot create instance of type <" + aType.getName() + "> as the processed length <" + theOffset + "> does not match the sequence's <" + _sequence.toHexString() + "> length <" + _sequence.getLength() + ">!" );
			}
		}
		catch ( TransmissionException e ) {
			throw new IllegalArgumentException( "Cannot create instance of type <" + aType.getName() + "> for sequence <" + _sequence.toHexString() + ">!", e );
		}
		return theSegment.getPayload();
	}
}
