// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Provides an accessor for a magic byte array property.
 */
public interface MagicBytesAccessor extends org.refcodes.mixin.MagicBytesAccessor {

	/**
	 * Returns the magic bytes (as of {@link #getMagicBytes()}) {@link String}
	 * being UTF-8 encoded.
	 * 
	 * @return The according magic bytes {@link String}.
	 */
	default String toMagicBytes() {
		return new String( getMagicBytes(), StandardCharsets.UTF_8 );
	}

	/**
	 * Returns the magic bytes (as of {@link #getMagicBytes()}) as an
	 * accordingly encoded {@link String}.
	 * 
	 * @param aCharset The {@link Charset} to use for encoding.
	 * 
	 * @return The accordingly encoded magic bytes {@link String}.
	 */
	default String toMagicBytes( Charset aCharset ) {
		return new String( getMagicBytes(), aCharset );
	}

	/**
	 * Provides a mutator for a magic byte array property.
	 */
	public interface MagicBytesMutator extends org.refcodes.mixin.MagicBytesAccessor.MagicBytesMutator {

		/**
		 * {@inheritDoc} Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 */
		@Override
		default void setMagicBytes( String aMagicBytes ) {
			setMagicBytes( aMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}
	}

	/**
	 * Provides a builder method for a magic byte array property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MagicBytesBuilder<B extends MagicBytesBuilder<B>> extends org.refcodes.mixin.MagicBytesAccessor.MagicBytesBuilder<B> {

		/**
		 * {@inheritDoc} Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 */
		@Override
		default B withMagicBytes( String aMagicBytes ) {
			return withMagicBytes( aMagicBytes.getBytes( TransmissionMetrics.DEFAULT_ENCODING ) );
		}
	}

	/**
	 * Provides a magic byte array property.
	 */
	public interface MagicBytesProperty extends MagicBytesAccessor, MagicBytesMutator, org.refcodes.mixin.MagicBytesAccessor.MagicBytesProperty {

		/**
		 * {@inheritDoc} Uses {@link TransmissionMetrics#DEFAULT_ENCODING} for
		 * converting the {@link String} into a byte array.
		 */
		@Override
		default String letMagicBytes( String aMagicBytes ) {
			return letMagicBytes( aMagicBytes, TransmissionMetrics.DEFAULT_ENCODING );
		}
	}
}
