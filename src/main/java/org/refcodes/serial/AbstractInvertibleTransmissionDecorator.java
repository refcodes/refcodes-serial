// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.exception.BugException;
import org.refcodes.io.BijectiveOutputStream;
import org.refcodes.io.InverseInputStream;
import org.refcodes.numerical.BijectiveFunction;
import org.refcodes.numerical.InverseFunction;
import org.refcodes.numerical.Invertible;

/**
 * An abstract implementation of bijective and inverse functions applied to the
 * decoratee's transmitting and receiving methods.
 * 
 * @param <DECORATEE> The {@link Transmission} type describing the
 *        {@link Transmission} subclass to be enriched.s
 */
public abstract class AbstractInvertibleTransmissionDecorator<DECORATEE extends Transmission> extends AbstractTransmissionDecorator<DECORATEE> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected BijectiveFunction<Byte, Byte> _bijectiveFunction;
	protected InverseFunction<Byte, Byte> _inverseFunction;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param aDecoratee The decorator having applied the {@link Invertible}'s
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aInvertible The {@link Invertible} providing the
	 *        {@link BijectiveFunction} to encode and the {@link Invertible}'s
	 *        {@link InverseFunction} to decode any data being delegated.
	 */
	public AbstractInvertibleTransmissionDecorator( DECORATEE aDecoratee, Invertible<Byte, Byte> aInvertible ) {
		super( aDecoratee );
		_bijectiveFunction = aInvertible;
		_inverseFunction = aInvertible;
	}

	/**
	 * Constructs an invertible transmission decorator applying bijective and
	 * inverse functions upon the delegated methods.
	 * 
	 * @param aDecoratee The decorator having applied the
	 *        {@link BijectiveFunction} to encode and the
	 *        {@link InverseFunction} to decode any data being delegated.
	 * @param aBijectiveFunction The {@link BijectiveFunction} to encode any
	 *        data being delegated.
	 * @param aInverseFunction The {@link InverseFunction} to decode any data
	 *        being delegated.
	 */
	public AbstractInvertibleTransmissionDecorator( DECORATEE aDecoratee, BijectiveFunction<Byte, Byte> aBijectiveFunction, InverseFunction<Byte, Byte> aInverseFunction ) {
		super( aDecoratee );
		_bijectiveFunction = aBijectiveFunction;
		_inverseFunction = aInverseFunction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		final BijectiveOutputStream theBijectiveOutputStream = new BijectiveOutputStream( aOutputStream, _bijectiveFunction );
		final InverseInputStream theInverseInputStream = aReturnStream != null ? new InverseInputStream( aReturnStream, _inverseFunction ) : null;
		_decoratee.transmitTo( theBijectiveOutputStream, theInverseInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final SequenceOutputStream theSequenceInputStream = new SequenceOutputStream();
		try {
			transmitTo( theSequenceInputStream );
		}
		catch ( IOException e ) {
			throw new BugException( "Encountered an I/O related bug which cannot occur as we merely use streams in memory!", e );
		}
		return theSequenceInputStream.getSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return new SerialSchema( getClass(), toSequence(), getLength(), "A transmission decorator enriching the encapsulated transmission with \"Bijectivce-Function\" as well as \"Inverse-Function\" functionality.", getDecoratee().toSchema() );
	}
}
