// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoRetryCount;
import org.refcodes.mixin.LengthAccessor;
import org.refcodes.mixin.Resetable;
import org.refcodes.schema.Schemable;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link Transmission} interface enables an implementing type to provide a
 * {@link Sequence} representation of itself or to transmit its {@link Sequence}
 * representation through an {@link OutputStream} (with an optional feedback
 * {@link InputStream}) as of {@link #transmitTo(OutputStream, InputStream)}.
 * Usually, an implementation of the {@link Transmission} interface actually
 * either implements the {@link Segment} interface or the {@link Section}
 * interface which both share the {@link Transmission} super-interface. (this
 * interface is {@link Serializable} in order to achieve a deep copy the easy
 * way which is required by the {@link ComplexTypeSegment} and the
 * {@link FixedSegmentArraySection}).
 */
public interface Transmission extends LengthAccessor, Resetable, Serializable, Schemable {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the overall length of this {@link Transmission}. In case of
	 * nested {@link Transmission} instances, all length values from all
	 * sub-segments are accumulated to the result as well.
	 * 
	 * @return The (overall) length of the {@link Transmission} (including any
	 *         sub-segments).
	 */
	@Override
	int getLength();

	/**
	 * Provides the {@link Sequence} representation of this
	 * {@link Transmission}. In case of nested {@link Transmission} instances,
	 * all {@link Sequence} representations from all sub-segments are
	 * accumulated to the result as well. Caution, the Transmission (or its
	 * nested {@link Transmission} instances) may be backed by the returned
	 * {@link Sequence}.
	 * 
	 * @return The according instance's {@link Sequence}.
	 */
	Sequence toSequence();

	/**
	 * Transmits the {@link Sequence} representing the implementing type's
	 * instance to the given {@link OutputStream}. This is a convenience method
	 * in case there is no feedback {@link InputStream} available (actually the
	 * {@link #transmitTo(OutputStream, InputStream)} method is invoked with
	 * <code>null</code> for the feedback {@link InputStream}). Override
	 * {@link #transmitTo(OutputStream, InputStream)} for your custom
	 * transmitting functionality.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write this
	 *        instance's {@link Sequence} to.
	 * 
	 * @throws IOException thrown in case writing data to the
	 *         {@link OutputStream} caused problems.
	 */
	default void transmitTo( OutputStream aOutputStream ) throws IOException {
		transmitTo( aOutputStream, null );
	}

	/**
	 * Transmits the {@link Sequence} representing the implementing type's
	 * instance to the given {@link SerialTransceiver}'s {@link OutputStream}.
	 * Implementations providing error correction methods use the provided
	 * {@link SerialTransceiver}'s feedback {@link InputStream} to do some sort
	 * of "stop-and-wait ARQ" or apply similar methods to ensure correctness of
	 * the transmitted data.This is a convenience method (actually the
	 * {@link #transmitTo(OutputStream, InputStream)} method is invoked).
	 * Override {@link #transmitTo(OutputStream, InputStream)} for your custom
	 * transmitting functionality.
	 * 
	 * @param aSerialTransceiver The {@link SerialTransceiver} providing the
	 *        {@link OutputStream} where to write this instance's
	 *        {@link Sequence} to and providing the {@link InputStream} being
	 *        the return channel to handle "stop-and-wait ARQ".
	 * 
	 * @throws IOException thrown in case writing data to the
	 *         {@link OutputStream} caused problems.
	 */
	default void transmitTo( SerialTransceiver aSerialTransceiver ) throws IOException {
		transmitTo( aSerialTransceiver.getOutputStream(), aSerialTransceiver.getInputStream() );
	}

	/**
	 * Transmits the {@link Sequence} representing the implementing type's
	 * instance to the given {@link OutputStream}. Implementations providing
	 * error correction methods use the provided feedback {@link InputStream} to
	 * do some sort of "stop-and-wait ARQ" or apply similar methods to ensure
	 * correctness of the transmitted data.
	 * 
	 * @param aOutputStream The {@link OutputStream} where to write this
	 *        instance's {@link Sequence} to.
	 * @param aReturnStream An {@link InputStream} being the return channel to
	 *        handle "stop-and-wait ARQ" or the like in case of a bidirectional
	 *        connection. Can be null in case we have a unidirectional
	 *        connection.
	 * 
	 * @throws IOException thrown in case writing data to the
	 *         {@link OutputStream} caused problems.
	 */
	void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException;

	/**
	 * Resets any dynamic data (e.g. values such as payloads or checksums) and
	 * must not(!) reset any configurations or settings required to produce the
	 * dynamic data (e.g. the {@link TransmissionMetrics}).
	 */
	@Override
	void reset();

	/**
	 * {@inheritDoc}
	 */
	@Override
	SerialSchema toSchema();

	/**
	 * Returns the {@link SimpleTypeMap} representation of this
	 * {@link Transmission}. In case this Transmission has {@link Transmission}
	 * children, then the children are queried as well and contained in the
	 * resulting {@link SimpleTypeMap}. The aliases of the according
	 * {@link Transmission} instances represent the resulting path to a
	 * {@link Transmission}'s final simple type.
	 * 
	 * @return The {@link SimpleTypeMap} representing this {@link Transmission}
	 *         and (if any) its children, with the according aliases forming the
	 *         paths to the {@link Transmission}'s values.
	 */
	SimpleTypeMap toSimpleTypeMap();

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the number of bytes from the given {@link InputStream} by also
	 * trying to read the required number of bytes in chunks (in case not all
	 * bytes can be read in one take).
	 * 
	 * @param aInputStream The byte array data from which to retrieve the bytes.
	 * @param aLength The number of bytes to be retrieved.
	 * 
	 * @return An array containing the number of bytes.
	 * 
	 * @throws IOException thrown in case reading data from the
	 *         {@link InputStream} caused problems.
	 */
	static byte[] fromInputStream( InputStream aInputStream, int aLength ) throws IOException {
		final byte[] theChunk = new byte[aLength];
		int theProcessedLength = 0;
		int theRemainingLength = aLength;
		final RetryCounter theRetries = new RetryCounter( IoRetryCount.NORM.getValue() );
		int eRead = 0;
		while ( theProcessedLength < aLength && theRetries.nextRetry() ) {
			try {
				eRead = aInputStream.read( theChunk, theProcessedLength, theRemainingLength );
			}
			catch ( IOException e ) {
				throw new IOException( "Can only retrieve <" + theProcessedLength + "> number of bytes of the expected <" + aLength + "> number of bytes!", e );
			}
			eRead = eRead < 0 ? 0 : eRead;
			theRemainingLength -= eRead;
			theProcessedLength += eRead;
		}

		if ( theProcessedLength < aLength ) {
			throw new IOException( "Can only retrieve <" + theProcessedLength + "> of the expected <" + aLength + "> number of bytes after <" + theRetries.getRetryCount() + "> retries!" );
		}

		return theChunk;
	}

	// /////////////////////////////////////////////////////////////////////////
	// MIXINS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default implementation of the {@link Transmission} interface providing an
	 * implementation of the {@link #transmitTo(OutputStream, InputStream)}
	 * method using the {@link #toSequence()} method.
	 */
	public interface TransmissionMixin extends Transmission {

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Default implementation harnessing the {@link #toSequence()} method.
		 * {@inheritDoc}
		 */
		@Override
		default void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
			final Sequence theSequence = toSequence();
			for ( byte eByte : theSequence ) {
				aOutputStream.write( eByte );
			}
			aOutputStream.flush();
		}
	}
}
