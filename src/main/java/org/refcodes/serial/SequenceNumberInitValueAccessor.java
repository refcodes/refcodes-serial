// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a sequence number initial value property.
 */
public interface SequenceNumberInitValueAccessor {

	/**
	 * Retrieves the sequence number initial value from the sequence number
	 * initial value property.
	 * 
	 * @return The sequence number initial value stored by the sequence number
	 *         initial value property.
	 */
	int getSequenceNumberInitValue();

	/**
	 * Provides a mutator for a sequence number initial value property.
	 */
	public interface SequenceNumberInitValueMutator {

		/**
		 * Sets the sequence number initial value for the sequence number
		 * initial value property.
		 * 
		 * @param aSequenceNumberInitValue The sequence number initial value to
		 *        be stored by the sequence number initial value property.
		 */
		void setSequenceNumberInitValue( int aSequenceNumberInitValue );
	}

	/**
	 * Provides a builder method for a sequence number initial value property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SequenceNumberInitValueBuilder<B extends SequenceNumberInitValueBuilder<B>> {

		/**
		 * Sets the sequence number initial value for the sequence number
		 * initial value property.
		 * 
		 * @param aSequenceNumberInitValue The sequence number initial value to
		 *        be stored by the sequence number initial value property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSequenceNumberInitValue( int aSequenceNumberInitValue );
	}

	/**
	 * Provides a sequence number initial value property.
	 */
	public interface SequenceNumberInitValueProperty extends SequenceNumberInitValueAccessor, SequenceNumberInitValueMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setSequenceNumberInitValue(int)} and returns the very same
		 * value (getter).
		 * 
		 * @param aSequenceNumberInitValue The integer to set (via
		 *        {@link #setSequenceNumberInitValue(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letSequenceNumberInitValue( int aSequenceNumberInitValue ) {
			setSequenceNumberInitValue( aSequenceNumberInitValue );
			return aSequenceNumberInitValue;
		}
	}
}
