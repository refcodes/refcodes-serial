// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a IO heuristics TTL (time to live) in milliseconds
 * property.
 */
public interface IoHeuristicsTimeToLiveMillisAccessor {

	/**
	 * The IO heuristics TTL (time to live) attribute in milliseconds.
	 * 
	 * @return An integer with the in milliseconds.
	 */
	long getIoHeuristicsTimeToLiveMillis();

	/**
	 * Provides a mutator for a IO heuristics TTL (time to live) in milliseconds
	 * property.
	 */
	public interface IoHeuristicsTimeToLiveMillisMutator {

		/**
		 * The IO heuristics TTL (time to live) attribute in milliseconds.
		 * 
		 * @param aIoHeuristicsTimeToLiveMillis An integer with the IO
		 *        heuristics TTL (time to live) in milliseconds.
		 */
		void setIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveMillis );
	}

	/**
	 * Provides a builder method for the IO heuristics TTL (time to live)
	 * property returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface IoHeuristicsTimeToLiveMillisBuilder<B extends IoHeuristicsTimeToLiveMillisBuilder<B>> {

		/**
		 * Sets the number for the IO heuristics TTL (time to live) property.
		 * 
		 * @param aIoHeuristicsTimeToLiveMillis The IO heuristics TTL (time to
		 *        live) in milliseconds to be stored by the IO heuristics TTL
		 *        (time to live) property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveMillis );
	}

	/**
	 * Provides a IO heuristics TTL (time to live) in milliseconds property.
	 */
	public interface IoHeuristicsTimeToLiveMillisProperty extends IoHeuristicsTimeToLiveMillisAccessor, IoHeuristicsTimeToLiveMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setIoHeuristicsTimeToLiveMillis(long)} and returns the very
		 * same value (getter).
		 * 
		 * @param aIoHeuristicsTimeToLiveMillis The long to set (via
		 *        {@link #setIoHeuristicsTimeToLiveMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letIoHeuristicsTimeToLiveMillis( long aIoHeuristicsTimeToLiveMillis ) {
			setIoHeuristicsTimeToLiveMillis( aIoHeuristicsTimeToLiveMillis );
			return aIoHeuristicsTimeToLiveMillis;
		}
	}
}
