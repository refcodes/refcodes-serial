// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a RTR ("ready-to-receive") timeout in milliseconds
 * property.
 */
public interface ReadyToReceiveTimeoutMillisAccessor {

	/**
	 * The RTR ("ready-to-receive") timeout attribute in milliseconds.
	 * 
	 * @return An integer with the RTR ("ready-to-receive") timeout in
	 *         milliseconds.
	 */
	long getReadyToReceiveTimeoutMillis();

	/**
	 * Provides a mutator for a RTR ("ready-to-receive") timeout in milliseconds
	 * property.
	 */
	public interface ReadyToReceiveTimeoutMillisMutator {

		/**
		 * The RTR ("ready-to-receive") timeout attribute in milliseconds.
		 * 
		 * @param aReadyToReceiveTimeoutMillis An integer with the RTR
		 *        ("ready-to-receive") timeout in milliseconds.
		 */
		void setReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutMillis );
	}

	/**
	 * Provides a builder method for a the RTR ("ready-to-receive") timeout
	 * property returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToReceiveTimeoutMillisBuilder<B extends ReadyToReceiveTimeoutMillisBuilder<B>> {

		/**
		 * Sets the number for the RTR ("ready-to-receive") timeout property.
		 * 
		 * @param aReadyToReceiveTimeoutMillis The RTR ("ready-to-receive")
		 *        timeout in milliseconds to be stored by the RTR timeout
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutMillis );
	}

	/**
	 * Provides a RTR ("ready-to-receive") timeout in milliseconds property.
	 */
	public interface ReadyToReceiveTimeoutMillisProperty extends ReadyToReceiveTimeoutMillisAccessor, ReadyToReceiveTimeoutMillisMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setReadyToReceiveTimeoutMillis(long)} and returns the very
		 * same value (getter).
		 * 
		 * @param aReadyToReceiveTimeoutMillis The long to set (via
		 *        {@link #setReadyToReceiveTimeoutMillis(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letReadyToReceiveTimeoutMillis( long aReadyToReceiveTimeoutMillis ) {
			setReadyToReceiveTimeoutMillis( aReadyToReceiveTimeoutMillis );
			return aReadyToReceiveTimeoutMillis;
		}
	}
}
