// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.numerical.Endianess;
import org.refcodes.textual.CaseStyleBuilder;

/**
 * Convenience class for the {@link NumberSegment} using
 * {@link TransmissionMetrics} sequence number related properties to configure
 * the {@link SequenceNumberSegment} (being an accordingly configured
 * {@link NumberSegment}).
 */
public class SequenceNumberSegment extends NumberSegment {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 */
	public SequenceNumberSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( SequenceNumberSegment.class.getSimpleName() ), aTransmissionMetrics.getSequenceNumberWidth(), (long) aTransmissionMetrics.getSequenceNumberInitValue(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link SequenceNumberSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 */
	public SequenceNumberSegment( Long aSequenceNumberInitValue, TransmissionMetrics aTransmissionMetrics ) {
		super( CaseStyleBuilder.asCamelCase( SequenceNumberSegment.class.getSimpleName() ), aTransmissionMetrics.getSequenceNumberWidth(), aSequenceNumberInitValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 */
	public SequenceNumberSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aTransmissionMetrics.getSequenceNumberWidth(), (long) aTransmissionMetrics.getSequenceNumberInitValue(), aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link SequenceNumberSegment}.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} used to
	 *        configure the {@link SequenceNumberSegment}.
	 */
	public SequenceNumberSegment( String aAlias, Long aSequenceNumberInitValue, TransmissionMetrics aTransmissionMetrics ) {
		super( aAlias, aTransmissionMetrics.getSequenceNumberWidth(), aSequenceNumberInitValue, aTransmissionMetrics.getEndianess() );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public SequenceNumberSegment( int aSequenceNumberWidth, Endianess aEndianess ) {
		super( CaseStyleBuilder.asCamelCase( SequenceNumberSegment.class.getSimpleName() ), aSequenceNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public SequenceNumberSegment( int aSequenceNumberWidth, Long aSequenceNumberInitValue, Endianess aEndianess ) {
		super( CaseStyleBuilder.asCamelCase( SequenceNumberSegment.class.getSimpleName() ), aSequenceNumberWidth, aSequenceNumberInitValue, aEndianess );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public SequenceNumberSegment( String aAlias, int aSequenceNumberWidth, Endianess aEndianess ) {
		super( aAlias, aSequenceNumberWidth, aEndianess );
	}

	/**
	 * Constructs a {@link SequenceNumberSegment} with the given
	 * {@link TransmissionMetrics}. Only those properties of the
	 * {@link TransmissionMetrics} are considered which are also supported by
	 * the other constructors.
	 * 
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aSequenceNumberWidth The number of bytes to be occupied by the
	 *        number.
	 * @param aSequenceNumberInitValue The value (payload) to be contained by
	 *        the {@link NumberSegment}.
	 * @param aEndianess The {@link Endianess} to be used for payload values.
	 */
	public SequenceNumberSegment( String aAlias, int aSequenceNumberWidth, Long aSequenceNumberInitValue, Endianess aEndianess ) {
		super( aAlias, aSequenceNumberWidth, aSequenceNumberInitValue, aEndianess );
	}
}
