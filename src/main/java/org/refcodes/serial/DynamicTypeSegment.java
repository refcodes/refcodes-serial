// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.refcodes.mixin.EncodingAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.EndianessAccessor;
import org.refcodes.struct.SimpleTypeMap;

/**
 * The {@link DynamicTypeSegment} represents a {@link Segment} which's internal
 * raw data is created from provided types at runtime and which's internal raw
 * data is used to create provided types at runtime. It is implemented by
 * decorating a {@link DynamicTypeSection} with an
 * {@link AllocSectionDecoratorSegment}.
 */
public class DynamicTypeSegment implements Segment, LengthWidthAccessor, EndianessAccessor, EncodingAccessor<Charset>, DynamicTypeTransmission {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final AllocSectionDecoratorSegment<DynamicTypeSection> _allocSection;
	private final DynamicTypeSection _dynamicTypeSection;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 */
	public DynamicTypeSegment() {
		_dynamicTypeSection = new DynamicTypeSection();
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSegment( Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection();
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public DynamicTypeSegment( int aLengthWidth, Endianess aEndianess ) {
		_dynamicTypeSection = new DynamicTypeSection( aLengthWidth, aEndianess );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties.
	 * 
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSegment( int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aEndianess, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}).
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 */
	public DynamicTypeSegment( String aAlias ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSegment( String aAlias, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public DynamicTypeSegment( String aAlias, int aLengthWidth, Endianess aEndianess ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aLengthWidth, aEndianess );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties.
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public DynamicTypeSegment( String aAlias, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aLengthWidth, aEndianess, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, Charset aCharset, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aCharset, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aLengthWidth, aEndianess, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aLengthWidth, aEndianess, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aTransmissionMetrics );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( String aAlias, T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aValue, aTransmissionMetrics, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 *
	 * @param aAlias The alias which identifies the content of this segment.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public DynamicTypeSegment( String aAlias, TransmissionMetrics aTransmissionMetrics ) {
		_dynamicTypeSection = new DynamicTypeSection( aAlias, aTransmissionMetrics );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 */
	public <T> DynamicTypeSegment( T aValue ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSegment( T aValue, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given
	 * {@link CharSection}, as well as with the
	 * {@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH} and the
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS}. It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( T aValue, Charset aCharset, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aCharset, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 */
	public <T> DynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aLengthWidth, aEndianess );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 */
	public <T> DynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aCharset );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aCharset The {@link Charset} to be used for encoding and decoding
	 *        {@link String} instances.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, Charset aCharset, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aCharset, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties as
	 * well as with the {@link TransmissionMetrics#DEFAULT_ENCODING}. It is
	 * pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aLengthWidth The width (in bytes) to be used for length values.
	 * @param aEndianess The {@link Endianess} to be used for (length) values.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( T aValue, int aLengthWidth, Endianess aEndianess, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aLengthWidth, aEndianess, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aLengthWidth, aEndianess );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the default properties
	 * ({@link TransmissionMetrics#DEFAULT_LENGTH_WIDTH},
	 * {@link TransmissionMetrics#DEFAULT_ENDIANESS} as well as
	 * {@link TransmissionMetrics#DEFAULT_ENCODING}). It is pre-initialized with
	 * the given value as of {@link #fromType(Object, String...)}.
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( T aValue, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of {@link #fromType(Object)}.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public <T> DynamicTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aTransmissionMetrics );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. It
	 * is pre-initialized with the given value as of
	 * {@link #fromType(Object, String...)}. The configuration attributes are
	 * taken from the {@link TransmissionMetrics} configuration object, though
	 * only those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param <T> The type of the data structure representing the body.
	 * @param aValue The data structure's value.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 * @param aAttributes The attributes or null if all attributes are to be
	 *        processed in alphabetical order.
	 */
	public <T> DynamicTypeSegment( T aValue, TransmissionMetrics aTransmissionMetrics, String... aAttributes ) {
		_dynamicTypeSection = new DynamicTypeSection( aValue, aTransmissionMetrics, aAttributes );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	/**
	 * Constructs the {@link DynamicTypeSegment} with the given properties. The
	 * configuration attributes are taken from the {@link TransmissionMetrics}
	 * configuration object, though only those attributes are supported which
	 * are also supported by the other constructors!
	 * 
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public DynamicTypeSegment( TransmissionMetrics aTransmissionMetrics ) {
		_dynamicTypeSection = new DynamicTypeSection( aTransmissionMetrics );
		_allocSection = new AllocSectionDecoratorSegment<>( _dynamicTypeSection, aTransmissionMetrics );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _allocSection.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _allocSection.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		_allocSection.transmitTo( aOutputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_allocSection.reset();
		_dynamicTypeSection.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		return _allocSection.toSchema();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _dynamicTypeSection.toSimpleTypeMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( Class<T> aType ) {
		return _dynamicTypeSection.toType( aType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( Class<T> aType, String... aAttributes ) {
		return _dynamicTypeSection.toType( aType, aAttributes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> void fromType( T aValue ) {
		_dynamicTypeSection.fromType( aValue );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> void fromType( T aValue, String... aAttributes ) {
		_dynamicTypeSection.fromType( aValue, aAttributes );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Charset getEncoding() {
		return _dynamicTypeSection.getEncoding();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _dynamicTypeSection.getEndianess();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLengthWidth() {
		return _dynamicTypeSection.getLengthWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return _allocSection.fromTransmission( aSequence, aOffset );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		receiveFrom( aInputStream, aReturnStream );
	}
}
