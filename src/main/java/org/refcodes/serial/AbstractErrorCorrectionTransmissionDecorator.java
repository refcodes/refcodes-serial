// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoTimeout;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * The {@link AbstractErrorCorrectionTransmissionDecorator} class implements a
 * decorator providing base functionality for an error correction
 * {@link Transmission}.
 *
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched.
 */
public abstract class AbstractErrorCorrectionTransmissionDecorator<DECORATEE extends Transmission> implements ErrorCorrectionTransmission, TransmissionMixin, DecorateeAccessor<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ACK_TIMEOUT_IN_MS = "ACK_TIMEOUT_IN_MS";
	public static final String ACK_RETRY_NUMBER = "ACK_RETRY_NUMBER";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _acknowledgeRetryNumber;
	protected long _acknowledgeTimeoutInMs;
	protected DECORATEE _decoratee = null; // Either referencee or decoratee!

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractErrorCorrectionTransmissionDecorator}
	 * instance with the given decoratee. The configuration attributes are taken
	 * from the {@link TransmissionMetrics} configuration object, though only
	 * those attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aSegment The decoratee or referencee to be contained by this
	 *        facade.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractErrorCorrectionTransmissionDecorator( DECORATEE aSegment, TransmissionMetrics aTransmissionMetrics ) {
		this( aSegment, aTransmissionMetrics.getAcknowledgeRetryNumber(), aTransmissionMetrics.getAcknowledgeTimeoutMillis() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Hook constructor to be used by sub-classes.
	 */
	protected AbstractErrorCorrectionTransmissionDecorator() {
		this( null, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS );
	}

	/**
	 * Constructs a {@link AbstractErrorCorrectionTransmissionDecorator}
	 * instance with the given decoratee. Takes a retry count of
	 * {@link IoRetryCount#notify()} by default and a timeout detecting a retry
	 * situation of {@link IoTimeout#MIN}.
	 * 
	 * @param aDecoratee The decoratee to be contained by this facade.
	 */
	public AbstractErrorCorrectionTransmissionDecorator( DECORATEE aDecoratee ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_RETRY_NUMBER, TransmissionMetrics.DEFAULT_ACKNOWLEDGE_TIMEOUT_IN_MS );
	}

	/**
	 * Constructs a {@link AbstractErrorCorrectionTransmissionDecorator}
	 * instance with the given decoratee.
	 * 
	 * @param aSegment The decoratee or referencee to be contained by this
	 *        facade.
	 * @param aAckRetryNumber The number of retries waiting for an ACK from the
	 *        return channel.
	 * @param aAckTimeoutInMs The timeout in milliseconds to pend till the next
	 *        retry.
	 */
	public AbstractErrorCorrectionTransmissionDecorator( DECORATEE aSegment, int aAckRetryNumber, long aAckTimeoutInMs ) {
		_decoratee = aSegment;
		_acknowledgeTimeoutInMs = aAckTimeoutInMs;
		_acknowledgeRetryNumber = aAckRetryNumber;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getAcknowledgeRetryNumber() {
		return _acknowledgeRetryNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getAcknowledgeTimeoutMillis() {
		return _acknowledgeTimeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		return _decoratee.toSequence();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A transmission decorator enriching the encapsulated transmission with \"Stop-and-wait ARQ\".", getDecoratee().toSchema() );
		theSchema.put( ACK_RETRY_NUMBER, getAcknowledgeRetryNumber() );
		theSchema.put( ACK_TIMEOUT_IN_MS, getAcknowledgeTimeoutMillis() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [segment=" + _decoratee + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractErrorCorrectionTransmissionDecorator<?> other = (AbstractErrorCorrectionTransmissionDecorator<?>) obj;
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}
}
