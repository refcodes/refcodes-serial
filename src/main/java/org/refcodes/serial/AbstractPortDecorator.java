// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.io.TimeoutInputStream;

/**
 * The {@link AbstractPortDecorator} decorates a {@link Port} for sub-classes to
 * enrich {@link Port} functionality.
 * 
 * @param <PM> The actual {@link PortMetrics} type to use.
 */
public class AbstractPortDecorator<PM extends PortMetrics> implements Port<PM> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Port<PM> _port;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link Port} with the according functionality.
	 * 
	 * @param aPort The {@link Port} to be decorated.
	 */
	public AbstractPortDecorator( Port<PM> aPort ) {
		_port = aPort;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void receiveSegment( SEGMENT aSegment ) throws IOException {
		_port.receiveSegment( aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void transmitSegment( SEGMENT aSegment ) throws IOException {
		_port.transmitSegment( aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitBytes( byte[] aBytes, int aOffset, int aLength ) throws IOException {
		_port.transmitBytes( aBytes, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputStream getOutputStream() {
		return _port.getOutputStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void receiveSegmentWithin( long aTimeoutMillis, SEGMENT aSegment ) throws IOException {
		_port.receiveSegmentWithin( aTimeoutMillis, aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence receiveSequenceWithin( long aTimeoutMillis, int aLength ) throws IOException {
		return _port.receiveSequenceWithin( aTimeoutMillis, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void onReceiveSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException {
		_port.onReceiveSegment( aSegment, aSegmentConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> SegmentResult<SEGMENT> onReceiveSegment( SEGMENT aSegment ) throws IOException {
		return _port.onReceiveSegment( aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence receiveSequence( int aLength ) throws IOException {
		return _port.receiveSequence( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_port.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeUnchecked() {
		_port.closeUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeQuietly() {
		_port.closeQuietly();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeIn( int aCloseMillis ) {
		_port.closeIn( aCloseMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _port.getAlias();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_port.open();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void openUnchecked() {
		_port.openUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip( long aLength ) throws IOException {
		return _port.skip( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getInputStream() {
		return _port.getInputStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TimeoutInputStream getInputStream( long aTimeoutMillis ) {
		return _port.getInputStream( aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveAllBytes() throws IOException {
		return _port.receiveAllBytes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveBytes( int aLength ) throws IOException {
		return _port.receiveBytes( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByteWithin( long aTimeoutMillis ) throws IOException {
		return _port.receiveByteWithin( aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveBytesWithin( long aTimeoutMillis, int aLength ) throws IOException {
		return _port.receiveBytesWithin( aTimeoutMillis, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveBytesWithin( long aTimeoutMillis, byte[] aBuffer, int aOffset, int aLength ) throws IOException {
		_port.receiveBytesWithin( aTimeoutMillis, aBuffer, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_port.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _port.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _port.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasAvailable() throws IOException {
		return _port.hasAvailable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _port.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PM getPortMetrics() {
		return _port.getPortMetrics();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> void doTransmitSegment( SEGMENT aSegment, SegmentConsumer<SEGMENT> aSegmentConsumer ) throws IOException {
		_port.doTransmitSegment( aSegment, aSegmentConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SEGMENT extends Segment> SegmentResult<SEGMENT> doTransmitSegment( SEGMENT aSegment ) throws IOException {
		return _port.doTransmitSegment( aSegment );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitSequence( Sequence aSequence, int aOffset, int aLength ) throws IOException {
		_port.transmitSequence( aSequence, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitSequence( Sequence aSequence ) throws IOException {
		_port.transmitSequence( aSequence );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _port.isClosable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _port.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flushUnchecked() {
		_port.flushUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFlushable() {
		return _port.isFlushable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailable() throws IOException {
		_port.skipAvailable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableWithin( long aSkipTimeSpanInMs ) throws IOException {
		_port.skipAvailableWithin( aSkipTimeSpanInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableWithin( long aSkipTimeSpanInMs, long aTimeoutMillis ) throws IOException {
		_port.skipAvailableWithin( aSkipTimeSpanInMs, aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableTill( long aSkipTimeSpanInMs ) throws IOException {
		_port.skipAvailableTill( aSkipTimeSpanInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableTillSilenceFor( long aSilenceTimeSpanInMs ) throws IOException {
		_port.skipAvailableTillSilenceFor( aSilenceTimeSpanInMs );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableTillSilenceFor( long aSilenceTimeSpanInMs, long aTimeoutMillis ) throws IOException {
		_port.skipAvailableTillSilenceFor( aSilenceTimeSpanInMs, aTimeoutMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void skipAvailableExcept( int aLength ) throws IOException {
		_port.skipAvailableExcept( aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByte() throws IOException {
		return _port.receiveByte();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveBytes( byte[] aBuffer, int aOffset, int aLength ) throws IOException {
		_port.receiveBytes( aBuffer, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable() {
		return _port.isOpenable();
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public void transmitByte( byte aByte ) throws IOException {
	//		_port.transmitByte( aByte );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public void transmitAllBytes( byte[] aBytes ) throws IOException {
	//		_port.transmitAllBytes( aBytes );
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Port<PM> withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Port<PM> withOpenUnchecked() {
		openUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Port<PM> withOpen( PM aPortMetrics ) throws IOException {
		open( aPortMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Port<PM> withOpenUnchecked( PM aPortMetrics ) {
		openUnchecked( aPortMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( PM aPortMetrics ) throws IOException {
		_port.open( aPortMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void openUnchecked( PM aPortMetrics ) {
		_port.openUnchecked( aPortMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( PM aPortMetrics ) {
		return _port.isOpenable( aPortMetrics );
	}
}
