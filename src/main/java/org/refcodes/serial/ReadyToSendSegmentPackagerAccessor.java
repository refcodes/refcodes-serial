// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link SegmentPackager} RTS ("ready-to-send")
 * property.
 */
public interface ReadyToSendSegmentPackagerAccessor {

	/**
	 * Retrieves the {@link SegmentPackager} RTS ("ready-to-send") property.
	 * 
	 * @return The {@link SegmentPackager} stored by the RTS ("ready-to-send")
	 *         {@link SegmentPackager} property.
	 */
	SegmentPackager getReadyToSendSegmentPackager();

	/**
	 * Provides a mutator for a {@link SegmentPackager} RTS ("ready-to-send")
	 * property.
	 */
	public interface ReadyToSendSegmentPackagerMutator {

		/**
		 * Sets the {@link SegmentPackager} RTS ("ready-to-send") property.
		 * 
		 * @param aReadyToSendSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} RTS ("ready-to-send")
		 *        property.
		 */
		void setReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager );
	}

	/**
	 * Provides a builder method for a {@link SegmentPackager} RTS
	 * ("ready-to-send") property returning the builder for applying multiple
	 * build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToSendSegmentPackagerBuilder<B extends ReadyToSendSegmentPackagerBuilder<B>> {

		/**
		 * Sets the {@link SegmentPackager} RTS ("ready-to-send") property.
		 * 
		 * @param aReadyToSendSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} RTS ("ready-to-send")
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager );
	}

	/**
	 * Provides a {@link SegmentPackager} RTS ("ready-to-send") property.
	 */
	public interface ReadyToSendSegmentPackagerProperty extends ReadyToSendSegmentPackagerAccessor, ReadyToSendSegmentPackagerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SegmentPackager}
		 * (setter) as of
		 * {@link #setReadyToSendSegmentPackager(SegmentPackager)} and returns
		 * the very same value (getter).
		 * 
		 * @param aReadyToSendSegmentPackager The {@link SegmentPackager} to set
		 *        (via {@link #setReadyToSendSegmentPackager(SegmentPackager)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SegmentPackager letReadyToSendSegmentPackager( SegmentPackager aReadyToSendSegmentPackager ) {
			setReadyToSendSegmentPackager( aReadyToSendSegmentPackager );
			return aReadyToSendSegmentPackager;
		}
	}
}
