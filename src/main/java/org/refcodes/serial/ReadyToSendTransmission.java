// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * The {@link ReadyToSendTransmission} interface defines functionality for
 * achieving an RTS "software" handshake between a transmitter having the active
 * RTS ("ready-to-send") role and a receiver confirming being CTS
 * ("clear-to-send") where the receiver waits for a RTS signal from the
 * transmitter (the transmitter signals to be ready for sending data by issuing
 * an RTS signal to the receiver). Upon the receiver's CTS ("clear-to-send")
 * acknowledgement, the transmitter starts sending: <code>
 *                              TRANSMITTER       RECEIVER
 *                                   |               |   
 * Send RTS magic-bytes              |               | Wait till enquiry-timeout           
 * Try RTS retry-number of times     |------RTS-----→| for RTR magic-bytes      
 * Wait till RTS retry-timeout/retry |               | 
 *                                   |               |
 *                                   |               | Send CTS magic-bytes             
 * Send payload upon CTS             |←-----CTS------| Wait till CTS timeout
 * magic-bytes or break-out          |               | 
 * upon RTS retry-timeout x retries  |----PAYLOAD---→| Receive payload
 *                                   |               |
 * </code>
 */
public interface ReadyToSendTransmission extends EnquiryStandbyTimeMillisAccessor, ReadyToSendMagicBytesAccessor, ReadyToSendRetryNumberAccessor, ReadyToSendSegmentPackagerAccessor, ReadyToSendTimeoutMillisAccessor, ClearToSendMagicBytesAccessor, ClearToSendTimeoutMillisAccessor, ClearToSendSegmentPackagerAccessor, Transmission {}
