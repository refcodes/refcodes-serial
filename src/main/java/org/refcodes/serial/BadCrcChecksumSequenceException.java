// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Thrown in case a {@link Sequence} CRC checksum did not match the according
 * calculated CRC checksum.
 */
public class BadCrcChecksumSequenceException extends TransmissionSequenceException {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final long _attachedCrcChecksum;
	private final long _calculatedCrcChecksum;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, String aMessage, String aErrorCode ) {
		super( aSequence, aMessage, aErrorCode );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aSequence, aMessage, aCause, aErrorCode );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, String message, Throwable cause ) {
		super( aSequence, message, cause );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, String message ) {
		super( aSequence, message );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, Throwable aCause, String aErrorCode ) {
		super( aSequence, aCause, aErrorCode );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aAttachedCrcChecksum The CRC checksum being attached to the
	 *        {@link Sequence} data.
	 * @param aCalculatedCrcChecksum The CRC checksum being calculated from the
	 *        {@link Sequence} data.
	 */
	public BadCrcChecksumSequenceException( long aAttachedCrcChecksum, long aCalculatedCrcChecksum, Sequence aSequence, Throwable cause ) {
		super( aSequence, cause );
		_attachedCrcChecksum = aAttachedCrcChecksum;
		_calculatedCrcChecksum = aCalculatedCrcChecksum;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the CRC checksum being attached the {@link Sequence} data.
	 * 
	 * @return The according CRC checksum.
	 */
	public long getAttachedCrcChecksum() {
		return _attachedCrcChecksum;
	}

	/**
	 * Returns the CRC checksum being calculated from the {@link Sequence} data.
	 *
	 * @return The according CRC checksum.
	 */
	public long getCalculatedCrcChecksum() {
		return _calculatedCrcChecksum;
	}
}
