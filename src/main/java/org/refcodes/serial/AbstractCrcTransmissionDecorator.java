// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.mixin.DecorateeAccessor;
import org.refcodes.numerical.AbstractCrcMixin;
import org.refcodes.numerical.ChecksumValidationMode;
import org.refcodes.numerical.CrcAlgorithm;
import org.refcodes.numerical.CrcChecksumAccessor;
import org.refcodes.numerical.Endianess;
import org.refcodes.serial.Transmission.TransmissionMixin;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.struct.SimpleTypeMapImpl;

/**
 * A {@link AbstractCrcTransmissionDecorator} wraps a {@link Transmission}
 * instance and enriches the {@link Transmission} with a CRC checksum.
 * 
 * @param <DECORATEE> The decoratee type describing the according subclass to be
 *        enriched with a CRC checksum.
 */
public abstract class AbstractCrcTransmissionDecorator<DECORATEE extends Transmission> extends AbstractCrcMixin implements Transmission, TransmissionMixin, DecorateeAccessor<DECORATEE>, CrcChecksumAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String CRC_ENDIANESS = "CRC_ENDIANESS";
	public static final String CRC_CHECKSUM = "CRC_CHECKSUM";
	public static final String CRC_CHECKSUM_HEX = "CRC_CHECKSUM_HEX";
	public static final String CRC_ALGORITHM = "CRC_ALGORITHM";
	public static final String CRC_BYTE_WIDTH = "CRC_BYTE_WIDTH";
	public static final String CRC_CHECKSUM_CONCATENATION_MODE = "CRC_CHECKSUM_CONCATENATION_MODE";
	public static final String CRC_CHECKSUM_LITTLE_ENDIAN_BYTES = "CRC_CHECKSUM_LITTLE_ENDIAN_BYTES";
	public static final String CRC_CHECKSUM_BIG_ENDIAN_BYTES = "CRC_CHECKSUM_BIG_ENDIAN_BYTES";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected DECORATEE _decoratee;
	protected long _crcChecksum = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}.
	 * The configuration attributes are taken from the
	 * {@link TransmissionMetrics} configuration object, though only those
	 * attributes are supported which are also supported by the other
	 * constructors!
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aTransmissionMetrics The {@link TransmissionMetrics} to be used
	 *        for configuring this instance.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		this( aDecoratee, aTransmissionMetrics.getCrcAlgorithm(), aTransmissionMetrics.getCrcChecksumConcatenateMode(), aTransmissionMetrics.getChecksumValidationMode(), aTransmissionMetrics.getEndianess() );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm ) {
		this( aDecoratee, aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode ) {
		this( aDecoratee, aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, aChecksumValidationMode, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}.
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, Endianess aEndianess ) {
		this( aDecoratee, aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}.
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		this( aDecoratee, aCrcAlgorithm, TransmissionMetrics.DEFAULT_CRC_CHECKSUM_CONCATENATE_MODE, aChecksumValidationMode, aEndianess );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_CRC_ALGORITHM, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, TransmissionMetrics.DEFAULT_CRC_ALGORITHM, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode ) {
		this( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}
	 * (using {@link TransmissionMetrics#DEFAULT_ENDIANESS} by default).
	 *
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode ) {
		this( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, TransmissionMetrics.DEFAULT_ENDIANESS );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}.
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, Endianess aEndianess ) {
		this( aDecoratee, aCrcAlgorithm, aCrcChecksumConcatenateMode, TransmissionMetrics.DEFAULT_CHECKSUM_VALIDATION_MODE, aEndianess );
	}

	/**
	 * Constructs an according instance wrapping the given {@link Transmission}.
	 * 
	 * @param aDecoratee The {@link Transmission} to be wrapped.
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation for
	 *        concatenating the decoratee's {@link Sequence} with the according
	 *        CRC checksum.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcTransmissionDecorator( DECORATEE aDecoratee, CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		super( aCrcAlgorithm, aCrcChecksumConcatenateMode, aChecksumValidationMode, aEndianess );
		_decoratee = aDecoratee;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Sequence toSequence() {
		final Sequence theSequence = _decoratee.toSequence();
		_crcChecksum = theSequence.toCrcChecksum( _crcAlgorithm );
		final byte[] theCrcBytes = getEndianess().toUnsignedBytes( _crcChecksum, _crcAlgorithm.getCrcWidth() );
		return theSequence.withConcatenate( _crcChecksumConcatenateMode, theCrcBytes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmitTo( OutputStream aOutputStream, InputStream aReturnStream ) throws IOException {
		final Sequence theSequence = _decoratee.toSequence();
		_crcChecksum = theSequence.toCrcChecksum( _crcAlgorithm );
		final byte[] theCrcBytes = getEndianess().toUnsignedBytes( _crcChecksum, _crcAlgorithm.getCrcWidth() );
		switch ( _crcChecksumConcatenateMode ) {
		case APPEND -> {
			_decoratee.transmitTo( aOutputStream, aReturnStream );
			aOutputStream.write( theCrcBytes );
		}
		case PREPEND -> {
			aOutputStream.write( theCrcBytes );
			_decoratee.transmitTo( aOutputStream, aReturnStream );
		}
		default -> throw new UnhandledEnumBugException( _crcChecksumConcatenateMode );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLength() {
		return _crcAlgorithm.getCrcWidth() + _decoratee.getLength();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DECORATEE getDecoratee() {
		return _decoratee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getCrcChecksum() {
		if ( _crcChecksum == -1 ) {
			synchronized ( this ) {
				if ( _crcChecksum == -1 ) {
					final Sequence theSequence = _decoratee.toSequence();
					_crcChecksum = theSequence.toCrcChecksum( _crcAlgorithm );
				}
			}
		}
		return _crcChecksum;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_crcChecksum = -1;
		_decoratee.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SerialSchema toSchema() {
		// Generate CRC checksum first if not done already |-->
		final long theChecksum = getCrcChecksum();
		// Generate CRC checksum first if not done already <--|
		final SerialSchema theSchema = new SerialSchema( getClass(), toSequence(), getLength(), "A segment decorator enriching the encapsulated segment with a CRC checksum.", getDecoratee().toSchema() );
		theSchema.put( CRC_CHECKSUM, theChecksum );
		theSchema.put( getEndianess() == Endianess.BIG ? CRC_CHECKSUM_BIG_ENDIAN_BYTES : CRC_CHECKSUM_LITTLE_ENDIAN_BYTES, getEndianess().toBytes( theChecksum, getCrcAlgorithm().getCrcWidth() ) );
		theSchema.put( CRC_CHECKSUM_HEX, getEndianess().toBytes( theChecksum, getCrcAlgorithm().getCrcWidth() ) );
		theSchema.put( CRC_ALGORITHM, getCrcAlgorithm() );
		theSchema.put( CRC_BYTE_WIDTH, getCrcAlgorithm().getCrcWidth() );
		theSchema.put( CRC_ENDIANESS, getEndianess() );
		theSchema.put( CRC_CHECKSUM_CONCATENATION_MODE, getCrcChecksumConcatenateMode() );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _crcChecksumConcatenateMode == null ) ? 0 : _crcChecksumConcatenateMode.hashCode() );
		result = prime * result + ( ( _crcAlgorithm == null ) ? 0 : _crcAlgorithm.hashCode() );
		result = prime * result + (int) ( _crcChecksum ^ ( _crcChecksum >>> 32 ) );
		result = prime * result + ( ( _checksumValidationMode == null ) ? 0 : _checksumValidationMode.hashCode() );
		result = prime * result + ( ( _endianess == null ) ? 0 : _endianess.hashCode() );
		result = prime * result + ( ( _decoratee == null ) ? 0 : _decoratee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractCrcTransmissionDecorator<?> other = (AbstractCrcTransmissionDecorator<?>) obj;
		if ( _crcChecksumConcatenateMode != other._crcChecksumConcatenateMode ) {
			return false;
		}
		if ( _crcAlgorithm == null ) {
			if ( other._crcAlgorithm != null ) {
				return false;
			}
		}
		else if ( !_crcAlgorithm.equals( other._crcAlgorithm ) ) {
			return false;
		}
		if ( _crcChecksum != other._crcChecksum ) {
			return false;
		}
		if ( _checksumValidationMode != other._checksumValidationMode ) {
			return false;
		}
		if ( _endianess != other._endianess ) {
			return false;
		}
		if ( _decoratee == null ) {
			if ( other._decoratee != null ) {
				return false;
			}
		}
		else if ( !_decoratee.equals( other._decoratee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [crcAlgorithm=" + _crcAlgorithm + ", segment=" + _decoratee + ", crcChecksum=" + getCrcChecksum() + ", crcMode=" + _checksumValidationMode + ", endianess=" + _endianess + ", concatenateMode=" + _crcChecksumConcatenateMode + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap toSimpleTypeMap() {
		return _decoratee != null ? _decoratee.toSimpleTypeMap() : new SimpleTypeMapImpl();
	}
}
