// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a magic bytes length property.
 */
public interface MagicBytesLengthAccessor {

	/**
	 * Retrieves the magic bytes length from the magic bytes magic bytes length
	 * property.
	 * 
	 * @return The magic bytes length stored by the magic bytes magic bytes
	 *         length property.
	 */
	int getMagicBytesLength();

	/**
	 * Provides a mutator for a magic bytes length property.
	 */
	public interface MagicBytesLengthMutator {

		/**
		 * Sets the magic bytes length for the magic bytes magic bytes length
		 * property.
		 * 
		 * @param aMagicBytesLength The magic bytes length to be stored by the
		 *        magic bytes length property.
		 */
		void setMagicBytesLength( int aMagicBytesLength );
	}

	/**
	 * Provides a builder method for a magic bytes length property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MagicBytesLengthBuilder<B extends MagicBytesLengthBuilder<B>> {

		/**
		 * Sets the magic bytes length for the magic bytes magic bytes length
		 * property.
		 * 
		 * @param aMagicBytesLength The magic bytes length to be stored by the
		 *        magic bytes length property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMagicBytesLength( int aMagicBytesLength );
	}

	/**
	 * Provides a magic bytes length property.
	 */
	public interface MagicBytesLengthProperty extends MagicBytesLengthAccessor, MagicBytesLengthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setMagicBytesLength(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aMagicBytesLength The integer to set (via
		 *        {@link #setMagicBytesLength(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letMagicBytesLength( int aMagicBytesLength ) {
			setMagicBytesLength( aMagicBytesLength );
			return aMagicBytesLength;
		}
	}
}
