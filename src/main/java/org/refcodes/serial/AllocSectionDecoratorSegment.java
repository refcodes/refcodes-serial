// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.io.BoundedInputStream;
import org.refcodes.numerical.Endianess;

/**
 * An {@link AllocSectionDecoratorSegment} enriches an {@link Section} with an
 * allocation declaration being prefixed to the enriched {@link Section}.
 *
 * @param <DECORATEE> The type of the {@link Section} decoratee.
 */
public class AllocSectionDecoratorSegment<DECORATEE extends Section> extends AbstractLengthDecoratorSegment<DECORATEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc} (hook for the {@link SectionComposite})
	 */
	protected AllocSectionDecoratorSegment() {}

	/**
	 * {@inheritDoc}
	 */
	protected AllocSectionDecoratorSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( aTransmissionMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	public AllocSectionDecoratorSegment( DECORATEE aDecoratee, TransmissionMetrics aTransmissionMetrics ) {
		super( aDecoratee, aTransmissionMetrics );
	}

	/**
	 * {@inheritDoc} (hook for the {@link SectionComposite})
	 */
	protected AllocSectionDecoratorSegment( Endianess aEndianess ) {
		super( aEndianess );
	}

	/**
	 * {@inheritDoc} (hook for the {@link SectionComposite})
	 */
	protected AllocSectionDecoratorSegment( int aLengthWidth, Endianess aEndianess ) {
		super( aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc} (hook for the {@link SectionComposite})
	 */
	protected AllocSectionDecoratorSegment( int aLengthWidth ) {
		super( aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public AllocSectionDecoratorSegment( DECORATEE aDecoratee, Endianess aEndianess ) {
		super( aDecoratee, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public AllocSectionDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth, Endianess aEndianess ) {
		super( aDecoratee, aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public AllocSectionDecoratorSegment( DECORATEE aDecoratee, int aLengthWidth ) {
		super( aDecoratee, aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public AllocSectionDecoratorSegment( DECORATEE aDecoratee ) {
		super( aDecoratee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		aOffset = super.fromTransmission( aSequence, aOffset );
		if ( _allocLength >= 0 ) {
			_referencee.fromTransmission( aSequence, aOffset, _allocLength );
		}
		return aOffset + _allocLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		super.receiveFrom( aInputStream, aReturnStream );
		if ( _allocLength >= 0 ) {
			_referencee.receiveFrom( new BoundedInputStream( aInputStream, _allocLength ), _allocLength, aReturnStream );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook and package local setter for the {@link SectionComposite} class.
	 * 
	 * @param aDecoratee The decoratee to be set.
	 */
	protected void setDecoratee( DECORATEE aDecoratee ) {
		_referencee = aDecoratee;
	}
}
