// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a RTS ("ready-to-send") magic bytes property.
 */
public interface ReadyToSendMagicBytesAccessor {

	/**
	 * Retrieves the magic bytes from the RTS ("ready-to-send") magic bytes
	 * property.
	 * 
	 * @return The magic bytes stored by the RTS ("ready-to-send") magic bytes
	 *         property.
	 */
	byte[] getReadyToSendMagicBytes();

	/**
	 * Provides a mutator for a RTS ("ready-to-send") magic bytes property.
	 */
	public interface ReadyToSendMagicBytesMutator {

		/**
		 * Sets the magic bytes for the RTS ("ready-to-send") magic bytes
		 * property.
		 * 
		 * @param aReadyToSendMagicBytes The magic bytes to be stored by the RTS
		 *        ("ready-to-send") magic bytes property.
		 */
		void setReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes );
	}

	/**
	 * Provides a builder method for a RTS ("ready-to-send") magic bytes
	 * property returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ReadyToSendMagicBytesBuilder<B extends ReadyToSendMagicBytesBuilder<B>> {

		/**
		 * Sets the magic bytes for the RTS ("ready-to-send") magic bytes
		 * property.
		 * 
		 * @param aReadyToSendMagicBytes The magic bytes to be stored by the RTS
		 *        ("ready-to-send") magic bytes property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes );
	}

	/**
	 * Provides a RTS ("ready-to-send") magic bytes property.
	 */
	public interface ReadyToSendMagicBytesProperty extends ReadyToSendMagicBytesAccessor, ReadyToSendMagicBytesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setReadyToSendMagicBytes(byte[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aReadyToSendMagicBytes The character to set (via
		 *        {@link #setReadyToSendMagicBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letReadyToSendMagicBytes( byte[] aReadyToSendMagicBytes ) {
			setReadyToSendMagicBytes( aReadyToSendMagicBytes );
			return aReadyToSendMagicBytes;
		}
	}
}
