// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.util.UUID;

/**
 * The {@link LoopbackPortTestBench} implements the {@link PortTestBench} for
 * the {@link LoopbackPort} type.
 */
public class LoopbackPortTestBench implements PortTestBench {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final LoopbackPort _port1;
	private final LoopbackPort _port2;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public LoopbackPortTestBench() {
		_port1 = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() );
		_port2 = new CrossoverLoopbackPort( _port1 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPorts() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackPort getReceiverPort() throws IOException {
		return _port1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackPort getTransmitterPort() throws IOException {
		return _port2;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_port1.open();
		_port2.open();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		try {
			_port1.close();
		}
		finally {
			_port2.close();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitShortestForPortCatchUp() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitShortForPortCatchUp() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForPortCatchUp() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitLongForPortCatchUp() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitLongestForPortCatchUp() {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForPortCatchUp( long aSleepTimeMillis ) {
		try {
			Thread.sleep( aSleepTimeMillis );
		}
		catch ( InterruptedException ignore ) {}
	}
}
