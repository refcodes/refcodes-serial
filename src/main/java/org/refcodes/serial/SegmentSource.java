// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;

/**
 * The {@link SegmentSource} is used to send {@link Segment} instances in a
 * unified way.
 */
@FunctionalInterface
public interface SegmentSource {
	/**
	 * Transmits a {@link Segment} (and blocks this thread) till all it's
	 * {@link Sequence} data (as of {@link Segment#toSequence()}) has been sent.
	 * 
	 * @param <SEGMENT> The {@link Segment} type describing the {@link Segment}
	 *        subclass used.
	 * @param aSegment The {@link Segment}'s data to be sent.
	 * 
	 * @throws IOException thrown in case of I/O issues (e.g. a timeout) while
	 *         sending.
	 */
	<SEGMENT extends Segment> void transmitSegment( SEGMENT aSegment ) throws IOException;

}
