// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.refcodes.mixin.ReferenceeAccessor.ReferenceeProperty;
import org.refcodes.numerical.Endianess;

/**
 * An {@link LengthSegment} enriches a {@link Segment} with an allocation
 * declaration computed from the referenced {@link Transmission}.
 *
 * @param <REFERENCEE> The type of the {@link Transmission} referencee.
 */
public class LengthSegment<REFERENCEE extends Transmission> extends AbstractReferenceeLengthSegment<REFERENCEE> implements ReferenceeProperty<REFERENCEE> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( TransmissionMetrics aTransmissionMetrics ) {
		super( aTransmissionMetrics );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment() {}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( Endianess aEndianess ) {
		super( aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( int aLengthWidth, Endianess aEndianess ) {
		super( aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( int aLengthWidth ) {
		super( aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( REFERENCEE aReferencee, Endianess aEndianess ) {
		super( aReferencee, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( REFERENCEE aReferencee, int aLengthWidth, Endianess aEndianess ) {
		super( aReferencee, aLengthWidth, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( REFERENCEE aReferencee, int aLengthWidth ) {
		super( aReferencee, aLengthWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( REFERENCEE aReferencee ) {
		super( aReferencee );
	}

	/**
	 * {@inheritDoc}
	 */
	public LengthSegment( REFERENCEE aReferencee, TransmissionMetrics aTransmissionMetrics ) {
		super( aReferencee, aTransmissionMetrics );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int fromTransmission( Sequence aSequence, int aOffset ) throws TransmissionException {
		return super.fromTransmission( aSequence, aOffset );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receiveFrom( InputStream aInputStream, OutputStream aReturnStream ) throws IOException {
		super.receiveFrom( aInputStream, aReturnStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public REFERENCEE getReferencee() {
		return _referencee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReferencee( REFERENCEE aDecoratee ) {
		_referencee = aDecoratee;
	}
}
