// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link SegmentPackager} ACK property.
 */
public interface AcknowledgeSegmentPackagerAccessor {

	/**
	 * Retrieves the {@link SegmentPackager} ACK property.
	 * 
	 * @return The {@link SegmentPackager} stored by the ACK
	 *         {@link SegmentPackager} property.
	 */
	SegmentPackager getAcknowledgeSegmentPackager();

	/**
	 * Provides a mutator for a {@link SegmentPackager} ACK property.
	 */
	public interface AcknowledgeSegmentPackagerMutator {

		/**
		 * Sets the {@link SegmentPackager} ACK property.
		 * 
		 * @param aAcknowledgeSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} ACK property.
		 */
		void setAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager );
	}

	/**
	 * Provides a builder method for a {@link SegmentPackager} ACK property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AcknowledgeSegmentPackagerBuilder<B extends AcknowledgeSegmentPackagerBuilder<B>> {

		/**
		 * Sets the {@link SegmentPackager} ACK property.
		 * 
		 * @param aAcknowledgeSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} ACK property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager );
	}

	/**
	 * Provides a {@link SegmentPackager} ACK property.
	 */
	public interface AcknowledgeSegmentPackagerProperty extends AcknowledgeSegmentPackagerAccessor, AcknowledgeSegmentPackagerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SegmentPackager}
		 * (setter) as of
		 * {@link #setAcknowledgeSegmentPackager(SegmentPackager)} and returns
		 * the very same value (getter).
		 * 
		 * @param aAcknowledgeSegmentPackager The {@link SegmentPackager} to set
		 *        (via {@link #setAcknowledgeSegmentPackager(SegmentPackager)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SegmentPackager letAcknowledgeSegmentPackager( SegmentPackager aAcknowledgeSegmentPackager ) {
			setAcknowledgeSegmentPackager( aAcknowledgeSegmentPackager );
			return aAcknowledgeSegmentPackager;
		}
	}
}
