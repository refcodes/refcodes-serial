// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

/**
 * Provides an accessor for a {@link SegmentPackager} CTS ("clear-to-send")
 * property.
 */
public interface ClearToSendSegmentPackagerAccessor {

	/**
	 * Retrieves the {@link SegmentPackager} CTS ("clear-to-send") property.
	 * 
	 * @return The {@link SegmentPackager} stored by the CTS ("clear-to-send")
	 *         {@link SegmentPackager} property.
	 */
	SegmentPackager getClearToSendSegmentPackager();

	/**
	 * Provides a mutator for a {@link SegmentPackager} CTS ("clear-to-send")
	 * property.
	 */
	public interface ClearToSendSegmentPackagerMutator {

		/**
		 * Sets the {@link SegmentPackager} CTS ("clear-to-send") property.
		 * 
		 * @param aClearToSendSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} CTS ("clear-to-send")
		 *        property.
		 */
		void setClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager );
	}

	/**
	 * Provides a builder method for a {@link SegmentPackager} CTS
	 * ("clear-to-send") property returning the builder for applying multiple
	 * build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ClearToSendSegmentPackagerBuilder<B extends ClearToSendSegmentPackagerBuilder<B>> {

		/**
		 * Sets the {@link SegmentPackager} CTS ("clear-to-send") property.
		 * 
		 * @param aClearToSendSegmentPackager The {@link SegmentPackager} to be
		 *        stored by the {@link SegmentPackager} CTS ("clear-to-send")
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager );
	}

	/**
	 * Provides a {@link SegmentPackager} CTS ("clear-to-send") property.
	 */
	public interface ClearToSendSegmentPackagerProperty extends ClearToSendSegmentPackagerAccessor, ClearToSendSegmentPackagerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SegmentPackager}
		 * (setter) as of
		 * {@link #setClearToSendSegmentPackager(SegmentPackager)} and returns
		 * the very same value (getter).
		 * 
		 * @param aClearToSendSegmentPackager The {@link SegmentPackager} to set
		 *        (via {@link #setClearToSendSegmentPackager(SegmentPackager)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SegmentPackager letClearToSendSegmentPackager( SegmentPackager aClearToSendSegmentPackager ) {
			setClearToSendSegmentPackager( aClearToSendSegmentPackager );
			return aClearToSendSegmentPackager;
		}
	}
}
