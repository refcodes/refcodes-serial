// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;

public class SequenceTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testConstruct() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testOutputStream1() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theValues = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final ByteArraySequence theSequence = new ByteArraySequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final OutputStream theOutputStream = theSequence.getOutputStream();
		for ( byte theValue : theValues ) {
			theOutputStream.write( theValue );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		assertArrayEquals( theValues, theSequence.toBytes() );
	}

	@Test
	public void testOutputStream2() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theValues = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final ByteArraySequence theSequence = new ByteArraySequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final OutputStream theOutputStream = theSequence.getOutputStream();
		theOutputStream.write( theValues );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		assertArrayEquals( theValues, theSequence.toBytes() );
	}

	@Test
	public void testInputStream1() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final InputStream theInputStream = theSequence.getInputStream();
		int eRead;
		int i = 0;
		final byte[] theBytes = theSequence.toBytes();
		final byte[] theStreamBytes = new byte[theSequence.getLength()];
		while ( ( eRead = theInputStream.read() ) != -1 ) {
			theStreamBytes[i] = (byte) eRead;
			i++;
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theStreamBytes ) );
		}
		assertArrayEquals( theBytes, theStreamBytes );
	}

	@Test
	public void testInputStream2() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final InputStream theInputStream = theSequence.getInputStream();
		final byte[] theBytes = theSequence.toBytes();
		final byte[] theStreamBytes = new byte[theSequence.getLength()];
		theInputStream.read( theStreamBytes, 0, theInputStream.available() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theStreamBytes ) );
		}
		assertArrayEquals( theBytes, theStreamBytes );
	}

	@Test
	public void testInputStream3() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final InputStream theInputStream = theSequence.getInputStream();
		final byte[] theBytes = theSequence.toBytes( 0, 10 );
		final byte[] theStreamBytes = new byte[10];
		final int theRead = theInputStream.read( theStreamBytes, 0, 10 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theStreamBytes ) );
		}
		assertArrayEquals( theBytes, theStreamBytes );
		assertEquals( theRead, 10 );
	}

	@Test
	public void testInputStream4() throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final InputStream theInputStream = theSequence.getInputStream();
		final byte[] theBytes = theSequence.toBytes();
		final byte[] theStreamBytes = new byte[theSequence.getLength()];
		final int theRead = theInputStream.read( theStreamBytes, 0, 20 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theStreamBytes ) );
		}
		assertArrayEquals( theBytes, theStreamBytes );
		assertEquals( theRead, theBytes.length );
	}

	@Test
	public void testAppend() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence();
		theSequence.append( theNumbers );
		theSequence.append( theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testPrepend() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers );
		theSequence.prepend( theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 97, 98, 99 }, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testToTruncateTail() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final ByteArraySequence theTruncated = theSequence.toTruncateTail( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theTruncated );
		}
		String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theTruncated.getChunks() ) );
		theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testToTruncateHead() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final ByteArraySequence theTruncated = theSequence.toTruncateHead( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theTruncated );
		}
		String theExpected = "{ { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theTruncated.getChunks() ) );
		theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail1() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail2() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 2 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail3() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 3 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail4() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 4 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 1, 2, 3, 4, 5, 6, 7, 8 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail5() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 13 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{}";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateTail6() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateTail( 14 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{}";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead1() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead2() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 9 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 9 }, { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead3() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 10 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 97, 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead4() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 11 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 98, 99 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead5() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 13 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{}";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testTruncateHead6() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final ByteArraySequence theSequence = new ByteArraySequence( theNumbers, theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.truncateHead( 14 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{}";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testWrite1() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final ByteArraySequence theSequence = new ByteArraySequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.overwrite( 1, theNumbers );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testWrite2() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final ByteArraySequence theSequence = new ByteArraySequence( theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.overwrite( 1, theNumbers );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 97, 0, 1 }, { 2, 3, 4, 5, 6, 7, 8, 9 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	@Test
	public void testWrite3() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "### " + Execution.getCallerStackTraceElement().getMethodName() + " ###\n" );
		}
		final byte[] theChars = new byte[] { 'a', 'b', 'c' };
		final byte[] theNumbers = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final ByteArraySequence theSequence = new ByteArraySequence( theChars );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		theSequence.overwrite( 5, theNumbers );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			printSequence( theSequence );
		}
		final String theExpected = "{ { 97, 98, 99 }, { 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } }";
		assertEquals( theExpected, VerboseTextBuilder.asString( theSequence.getChunks() ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void printSequence( ByteArraySequence theSequence ) {
		System.out.println( theSequence.toString() );
		System.out.println( VerboseTextBuilder.asString( theSequence.getChunks() ) );
		System.out.println();
	}
}
