package org.refcodes.serial;

import static org.refcodes.serial.SerialSugar.*;

import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.NumericalUtility;

public class Example {

	public static void main( String[] args ) throws TransmissionException {
		example1();
		// example2();
		// example3();
	}

	public static void example1() throws TransmissionException {
		final BooleanSegment theFlag;
		final IntSegment theValue;
		final StringSection theString;

		// @formatter:off
		final Segment theSegment = crcPrefixSegment(
			segmentComposite(
				theFlag = booleanSegment( true ),
				theValue = intSegment( 5161, Endianess.LITTLE ),
				allocSegment( 
					theString = stringSection("Hello world!"), 2, Endianess.LITTLE
				)
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on

		// Create the bytes representation:
		final byte[] theBytes = theSegment.toSequence().toBytes();
		System.out.println( NumericalUtility.toHexString( " ", theBytes ) );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Change the payload and create the bytes representation again:
		theFlag.setPayload( false );
		final byte[] theOtherBytes = theSegment.toSequence().toBytes();
		System.out.println( NumericalUtility.toHexString( " ", theOtherBytes ) );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Restore the data structure from original bytes representation:
		theSegment.fromTransmission( theBytes );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Print our data structure's schema:
		final SerialSchema theSchema = theSegment.toSchema();
		System.out.println( theSchema );
	}

	public static void example2() throws TransmissionException {

		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withLengthWidth( 2 ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).build();
		final BooleanSegment theFlag;
		final IntSegment theValue;
		final StringSection theString;

		// @formatter:off
		final Segment theSegment = crcPrefixSegment(
			segmentComposite(
				theFlag = booleanSegment( true ),
				theValue = intSegment( 5161, theMetrics ),
				allocSegment( 
					theString = stringSection("Hello world!"), theMetrics
				)
			), theMetrics
		);
		// @formatter:on

		// Create the bytes representation:
		final byte[] theBytes = theSegment.toSequence().toBytes();
		System.out.println( NumericalUtility.toHexString( " ", theBytes ) );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Change the payload and create the bytes representation again:
		theFlag.setPayload( false );
		final byte[] theOtherBytes = theSegment.toSequence().toBytes();
		System.out.println( NumericalUtility.toHexString( " ", theOtherBytes ) );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Restore the data structure from original bytes representation:
		theSegment.fromTransmission( theBytes );
		System.out.println( theFlag.getPayload() );
		System.out.println( theValue.getPayload() );
		System.out.println( theString.getPayload() );

		// Print our data structure's schema:
		System.out.println( theSegment.toSchema() );
	}

	public static void example3() throws TransmissionException {
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withLengthWidth( 2 ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).build();

		final Sensor[] theSensors = new Sensor[] { new Sensor( "SensorA", 103343 ), new Sensor( "SensorB", 22109 ), new Sensor( "SensorC", 313773 ) };
		final ComplexTypeSegment<Sensor[]> theSegment = complexTypeSegment( theSensors, theMetrics );
		final byte[] theBytes = theSegment.toSequence().toBytes();
		System.out.println( NumericalUtility.toHexString( " ", theBytes ) );

		// Restore the byte representation back to the data structure:
		final ComplexTypeSegment<Sensor[]> theOtherSegment = complexTypeSegment( Sensor[].class, theMetrics );
		theOtherSegment.fromTransmission( theBytes );
		final Sensor[] theOtherSensors = theOtherSegment.getPayload();
		System.out.println( theOtherSensors[0].getName() + "=" + theOtherSensors[0].getPayload() );
		System.out.println( theOtherSensors[1].getName() + "=" + theOtherSensors[1].getPayload() );
		System.out.println( theOtherSensors[2].getName() + "=" + theOtherSensors[2].getPayload() );

		// Print our data structure's schema:
		System.out.println( theSegment.toSchema() );
	}

	public static class Sensor {

		public int _value;
		public String _name;

		public Sensor() {}

		public Sensor( String aName, int aValue ) {
			_name = aName;
			_value = aValue;
		}

		public String getName() {
			return _name;
		}

		public int getPayload() {
			return _value;
		}
	}
}
