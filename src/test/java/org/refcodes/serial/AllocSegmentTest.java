// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.runtime.SystemProperty;

public class AllocSegmentTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAllocSegmentDecorator() throws TransmissionException {
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				// @formatter:off
				final AllocSectionDecoratorSegment<StringSection> theSegment = allocSegment(
					stringSection(Text.ARECIBO_MESSAGE.toString()), eLengthWidth, eEndianess
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				// @formatter:off
				final Segment theOtherSegment = allocSegment(
					stringSection(), eLengthWidth, eEndianess
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				assertEquals( theSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
			}
		}
	}

	@Test
	public void testLengthSegment() throws TransmissionException {
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				final LengthSegment<Segment> theLen;
				// @formatter:off
				final Segment theSegment = crcPrefixSegment(					//    2 bytes
					segmentComposite(
						theLen = segmentLength(eLengthWidth, eEndianess),		//  + x bytes
								theLen.letReferencee( segmentComposite(
								booleanSegment( true ),							//  + 1 byte
								intSegment(5161 ),								//  + 4 bytes
								allocSegment(									//  + 4 bytes
									stringSection("Hello World!")				// + 12 bytes
								)
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);                                                              // = 23 + x bytes
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				final int theLength = theSegment.getLength();
				assertEquals( 23 + eLengthWidth, theLength );
				assertEquals( 23 + eLengthWidth, theSequence.getLength() );
				final LengthSegment<Segment> theOtherLen;
				// @formatter:off
				final Segment theOtherSegment = crcPrefixSegment(
					segmentComposite(
						theOtherLen = segmentLength(eLengthWidth, eEndianess),
							theOtherLen.letReferencee( segmentComposite(
								booleanSegment( true ),
								intSegment(), 
								allocSegment( 
									stringSection()
								)
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				assertEquals( theSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
				assertEquals( 23 + eLengthWidth, theOtherSegment.getLength() );
			}
		}
	}

	@Test
	public void testAllocHeadBodySegmentDecorator1() throws TransmissionException {
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				final AllocSegmentHead theLen;
				// @formatter:off
				final Segment theSegment = crcPrefixSegment(                    		//    2 bytes
					segmentComposite(
						theLen = allocSegmentHead(eLengthWidth, eEndianess),    //  + x bytes
						theLen.letBody( 
							allocSegmentBody( 
								stringSection("Hello World!")                   // + 12 bytes
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);                                                              // = 14 + x bytes
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				assertEquals( 14 + eLengthWidth, theSegment.getLength() );
				assertEquals( 14 + eLengthWidth, theSequence.getLength() );
				final AllocSegmentHead theOtherLen;
				// @formatter:off
				final Segment theOtherSegment = crcPrefixSegment(
					segmentComposite(
						theOtherLen = allocSegmentHead(eLengthWidth, eEndianess),
						theOtherLen.letBody( 
							allocSegmentBody( 
								stringSection()
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				assertEquals( theOtherSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
				assertEquals( 14 + eLengthWidth, theOtherSegment.getLength() );
			}
		}
	}

	@Test
	public void testAllocHeadBodySegmentDecorator2() throws TransmissionException {
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				final AllocSegmentHead theLen;
				// @formatter:off
				final Segment theSegment = crcPrefixSegment(
					segmentComposite(
						theLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment( true ),
						intSegment( 5161 ), 
						theLen.letBody(
							allocSegmentBody( 
								stringSection(Text.ARECIBO_MESSAGE.toString())
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				final AllocSegmentHead theOtherLen;
				// @formatter:off
				final Segment theOtherSegment = crcPrefixSegment(
					segmentComposite(
						theOtherLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment(),
						intSegment(), 
						theOtherLen.letBody( 
							allocSegmentBody( 
								stringSection()
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				assertEquals( theOtherSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
			}
		}
	}
}
