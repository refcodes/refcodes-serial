// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.alt.chaos.ChaosKey;
import org.refcodes.security.ext.chaos.ChaosProvider;
import org.refcodes.serial.TestFixures.WeatherData;

public class CipherTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 15;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled
	@Test
	public void testCipherSimple() throws TransmissionException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theSegment = lengthSegment( 
			allocSegment( 
				cipherSection( 
					stringSection("Hello World!"),toCipher( "TopSecret123!")
				)
			)
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theOtherSegment = lengthSegment( 
				allocSegment( 
					cipherSection( 
						stringSection(),toCipher( "TopSecret123!")
					)
				)
			);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	@Disabled
	@Test
	public void testCipherSimpleFail() throws TransmissionException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theSegment = lengthSegment( 
			allocSegment( 
				cipherSection( 
					stringSection("Hello World!"),toCipher( "TopSecret123!")
				)
			)
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theOtherSegment = lengthSegment( 
				allocSegment( 
					cipherSection( 
						stringSection(), toCipher( "NotSecret123!")
					)
				)
			);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertNotEquals( theSegment, theOtherSegment );
	}

	@Disabled
	@Test
	public void testCipherComplex() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				ComplexTypeSegment<WeatherData> theReceiverSegment;
				for ( int i = 0; i < LOOPS; i++ ) {
					final String eSecret = "TopSecret" + ( 'A' + i );
					final Cipher eCipher;
					final Segment theSenderSeg = readyToSendSegment( cipherSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ), eCipher = toCipher( eSecret ) ) );
					final Segment theReceiverSeg = readyToSendSegment( cipherSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), eCipher ) );
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						try {
							System.out.println( "Expected [" + eCipher + "]= " + theResult.getResult() );
						}
						catch ( BadCrcChecksumException e ) {
							System.out.println( "Unexpected exceptio = " + e.getMessage() );
							fail( "Not Expecting a <" + BadCrcChecksumException.class.getSimpleName() + "> instead of a resul!t" );
						}
					}
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@Disabled
	@Test
	public void testCipherComplexFail() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final Segment theSenderSeg = cipherSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ), toCipher( "TopSecret123!" ) );
				final Segment theReceiverSeg = cipherSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), toCipher( "NotSecret123!" ) );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
				theSenderSeg.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					try {
						System.out.println( "Result = " + theResult.getResult() );
						fail( "Expecting a <" + BadCrcChecksumException.class.getSimpleName() + "> instead of a resul!t" );
					}
					catch ( BadCrcChecksumException expected ) {
						System.out.println( "Expected = " + expected.getMessage() );
					}
				}
				final WeatherData theReceiverData = theReceiverSegment.getPayload();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theReceiverData );
				}
				assertNotEquals( theSenderData, theReceiverData );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	public static Cipher toCipher( String aSecret ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		final int theIndex = Security.addProvider( new ChaosProvider() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Provider eProvider : Security.getProviders() ) {
				System.out.println( eProvider.getName() + " - " + eProvider.getInfo() );
			}
		}
		assertTrue( theIndex >= 0 );
		final Cipher c = Cipher.getInstance( ChaosKey.PROVIDER_NAME, new ChaosProvider() );
		final SecretKey key = new ChaosKey( aSecret );
		c.init( Cipher.ENCRYPT_MODE, key );
		return c;
		//	ChaosCipher theCipher = new ChaosCipher();
		//	SecretKey key = new ChaosKey( aSecret );
		//	theCipher.init( Cipher.ENCRYPT_MODE, key );
		//	return theCipher;
	}
}
