// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.runtime.SystemProperty;

public class FileTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String PASSWORDS_TXT = "passwords.txt";
	private static final String LF = SystemProperty.LINE_SEPARATOR.getValue();
	private static final String EOB = LF + "-----------------------" + LF;
	private static final String SHORT_TEXT = Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB + Text.ARECIBO_MESSAGE.getText() + EOB;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testFileSection1() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final TransmissionMetrics theMetrics = new TransmissionMetrics();
				Segment theSendSegment = crcSegment( allocSegment( stringSection( SHORT_TEXT ) ), theMetrics );
				final File theTempFile = File.createTempFile( getClass().getName(), ".tmp" );
				theTempFile.deleteOnExit();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Before: Temp file = " + theTempFile.getAbsolutePath() );
					System.out.println( "Before: Temp file size = " + theTempFile.length() );
				}
				Segment theReceiveSegment = crcSegment( allocSegment( new FileSection( theTempFile ) ), theMetrics );
				SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiveSegment );
				final long start = System.currentTimeMillis();
				theTransmitPort.transmitSegment( theSendSegment );
				final long durationInSecs = ( System.currentTimeMillis() - start ) / 1000;
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Duration in Seconds = " + durationInSecs );
					System.out.println( "Bytes/S = " + ( durationInSecs != 0 ? ( theReceiveSegment.getLength() / durationInSecs ) : "~" ) );
					System.out.println( "After: Temp file = " + theTempFile.getAbsolutePath() );
					System.out.println( "After: Temp file size = " + theTempFile.length() );
					System.out.println( theReceiveSegment.toSchema() );
				}
				// Assert |-->
				final String[] theReferenceText = SHORT_TEXT.split( LF );
				int i = 0;
				try ( Scanner theScanner = new Scanner( theTempFile, theMetrics.getEncoding().name() ) ) {
					theScanner.useDelimiter( LF );
					String eFileLine;
					while ( theScanner.hasNext() ) {
						eFileLine = theScanner.next();
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "\"" + eFileLine + "\" [" + i + "]" );
						}
						assertEquals( theReferenceText[i], eFileLine );
						i++;
					}
				}
				assertEquals( theReferenceText.length, i );
				// Assert <--|

				// |<------------------------------------------------------------->|

				final StringSection theStringSection;
				theSendSegment = crcSegment( allocSegment( fileSection( theTempFile ) ), theMetrics );
				theReceiveSegment = crcSegment( allocSegment( theStringSection = stringSection() ), theMetrics );
				theResult = theReceiverPort.onReceiveSegment( theReceiveSegment );
				theTransmitPort.transmitSegment( theSendSegment );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theStringSection.getPayload() );
				}
				assertEquals( SHORT_TEXT, theStringSection.getPayload() );
			}
		}
	}

	@Test
	public void testFileSection2() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final File theTempFile = File.createTempFile( getClass().getName(), ".tmp" );
				theTempFile.deleteOnExit();
				final String theBasePath = theTempFile.getParent();
				final TransmissionMetrics theMetrics = new TransmissionMetrics();
				Segment theSendSegment = crcSegment( segmentComposite( allocSegment( stringSection( PASSWORDS_TXT ) ), allocSegment( stringSection( SHORT_TEXT ) ) ), theMetrics );
				final StringSection theFilenameSection;
				final FileSection theFileSection;
				Segment theReceiveSegment = crcSegment( segmentComposite( allocSegment( theFilenameSection = stringSection() ), allocSegment( theFileSection = fileSection( theBasePath, theFilenameSection::getPayload ) ) ), theMetrics );
				SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiveSegment );
				theTransmitPort.transmitSegment( theSendSegment );
				theResult.waitForResult();
				final File thePayloadFile = theFileSection.toPayloadFile();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( thePayloadFile.getAbsolutePath() );
				}
				thePayloadFile.deleteOnExit();
				assertTrue( thePayloadFile.getAbsolutePath().endsWith( PASSWORDS_TXT ) );

				// |<------------------------------------------------------------->|

				theSendSegment = crcSegment( segmentComposite( allocSegment( stringSection( PASSWORDS_TXT ) ), allocSegment( fileSection( thePayloadFile ) ) ), theMetrics );
				final StringSection theStringSection;
				theReceiveSegment = crcSegment( segmentComposite( allocSegment( stringSection() ), allocSegment( theStringSection = stringSection() ) ), theMetrics );
				theResult = theReceiverPort.onReceiveSegment( theReceiveSegment );
				theTransmitPort.transmitSegment( theSendSegment );
				theResult.waitForResult();
				//	if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				//		System.out.println( theStringSection.getPayload() );
				//	}
				assertEquals( SHORT_TEXT, theStringSection.getPayload() );
			}
		}
	}
}
