// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.numerical.Endianess;
import org.refcodes.runtime.SystemProperty;

public class DataTypesTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTransmitAsciizSegment1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final AsciizSegment theSegment = asciizSegment( "Hallo Welt!" );
				final AsciizSegment theOtherSegment = asciizSegment();
				final SegmentResult<AsciizSegment> theResult = theReceiverPort.onReceiveSegment( theOtherSegment );
				theSegment.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "The payload = " + theSegment.getPayload() );
					System.out.println( "The other payload = " + theOtherSegment.getPayload() );
				}
				assertEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// ASCIIZ:
	// -------------------------------------------------------------------------

	@Test
	public void testAsciizSegment1() throws TransmissionException {
		final AsciizSegment theSegment = asciizSegment( "Hallo Welt!" );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 12, theSegment.getLength() );
		final AsciizSegment theOtherSegment = asciizSegment();
		assertEquals( 1, theOtherSegment.getLength() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 12, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizSegment2() throws TransmissionException {
		final AsciizSegment theSegment = asciizSegment();
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 1, theSegment.getLength() );
		final AsciizSegment theOtherSegment = asciizSegment();
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 1, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizSegment3() throws TransmissionException {
		final AsciizSegment theSegment = asciizSegment();
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 1, theSegment.getLength() );
		final AsciizSegment theOtherSegment = asciizSegment();
		assertEquals( 1, theOtherSegment.getLength() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 1, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizSegment4() throws TransmissionException {
		final IntSegment theIntSegment;
		final AsciizSegment theAsciizSegment1;
		final AsciizSegment theAsciizSegment2;
		final FloatSegment theFloatSegment;
		final Segment theSegment = segmentComposite( theIntSegment = intSegment( 2 ), theAsciizSegment1 = asciizSegment( "Hello world!" ), theAsciizSegment2 = asciizSegment( "Hallo Welt!" ), theFloatSegment = floatSegment( 3.14F ) );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 33, theSegment.getLength() );
		final IntSegment theOtherIntSegment;
		final AsciizSegment theOtherAsciizSegment1;
		final AsciizSegment theOtherAsciizSegment2;
		final FloatSegment theOtherFloatSegment;
		final Segment theOtherSegment = segmentComposite( theOtherIntSegment = intSegment(), theOtherAsciizSegment1 = asciizSegment(), theOtherAsciizSegment2 = asciizSegment(), theOtherFloatSegment = floatSegment() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theIntSegment.getPayload(), theOtherIntSegment.getPayload() );
		assertEquals( theAsciizSegment1.getPayload(), theOtherAsciizSegment1.getPayload() );
		assertEquals( theAsciizSegment2.getPayload(), theOtherAsciizSegment2.getPayload() );
		assertEquals( theFloatSegment.getPayload(), theOtherFloatSegment.getPayload() );
		assertEquals( 33, theOtherSegment.getLength() );
	}

	// -------------------------------------------------------------------------
	// ASCIIZ array:
	// -------------------------------------------------------------------------

	@Test
	public void testAsciizArraySegment1() throws TransmissionException {
		final AsciizArraySegment theSegment = asciizArraySegment( "Hallo Welt!" );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 16, theSegment.getLength() );
		final AsciizArraySegment theOtherSegment = asciizArraySegment();
		assertEquals( 4, theOtherSegment.getLength() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertArrayEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 16, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment2() throws TransmissionException {
		final AsciizArraySegment theSegment = asciizArraySegment( "Hallo Welt!", "Hello world!" );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 29, theSegment.getLength() );
		final AsciizArraySegment theOtherSegment = asciizArraySegment();
		assertEquals( 4, theOtherSegment.getLength() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertArrayEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 29, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment3() throws TransmissionException {
		final AsciizArraySegment theSegment = asciizArraySegment();
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 4, theSegment.getLength() );
		final AsciizArraySegment theOtherSegment = asciizArraySegment();
		assertEquals( 4, theOtherSegment.getLength() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertArrayEquals( theSegment.getPayload(), theOtherSegment.getPayload() );
		assertEquals( 4, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment4() throws TransmissionException {
		final IntSegment theIntSegment;
		final AsciizArraySegment theAsciizArraySegment;
		final FloatSegment theFloatSegment;
		final Segment theSegment = segmentComposite( theIntSegment = intSegment( 2 ), theAsciizArraySegment = asciizArraySegment( "Hello world!" ), theFloatSegment = floatSegment( 3.14F ) );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 25, theSegment.getLength() );
		final IntSegment theOtherIntSegment;
		final AsciizArraySegment theOtherAsciizArraySegment;
		final FloatSegment theOtherFloatSegment;
		final Segment theOtherSegment = segmentComposite( theOtherIntSegment = intSegment(), theOtherAsciizArraySegment = asciizArraySegment(), theOtherFloatSegment = floatSegment() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theIntSegment.getPayload(), theOtherIntSegment.getPayload() );
		assertArrayEquals( theAsciizArraySegment.getPayload(), theOtherAsciizArraySegment.getPayload() );
		assertEquals( theFloatSegment.getPayload(), theOtherFloatSegment.getPayload() );
		assertEquals( 25, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment5() throws TransmissionException {
		final IntSegment theIntSegment;
		final AsciizArraySegment theAsciizArraySegment;
		final FloatSegment theFloatSegment;
		final Segment theSegment = segmentComposite( theIntSegment = intSegment( 2 ), theAsciizArraySegment = asciizArraySegment( "Hello world!", "Hallo Welt!" ), theFloatSegment = floatSegment( 3.14F ) );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 37, theSegment.getLength() );
		final IntSegment theOtherIntSegment;
		final AsciizArraySegment theOtherAsciizArraySegment;
		final FloatSegment theOtherFloatSegment;
		final Segment theOtherSegment = segmentComposite( theOtherIntSegment = intSegment(), theOtherAsciizArraySegment = asciizArraySegment(), theOtherFloatSegment = floatSegment() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theIntSegment.getPayload(), theOtherIntSegment.getPayload() );
		assertArrayEquals( theAsciizArraySegment.getPayload(), theOtherAsciizArraySegment.getPayload() );
		assertEquals( theFloatSegment.getPayload(), theOtherFloatSegment.getPayload() );
		assertEquals( 37, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment6() throws TransmissionException {
		final IntSegment theIntSegment;
		final AsciizArraySegment theAsciizArraySegment;
		final FloatSegment theFloatSegment;
		final Segment theSegment = segmentComposite( theIntSegment = intSegment( 2 ), theAsciizArraySegment = asciizArraySegment(), theFloatSegment = floatSegment( 3.14F ) );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 12, theSegment.getLength() );
		final IntSegment theOtherIntSegment;
		final AsciizArraySegment theOtherAsciizArraySegment;
		final FloatSegment theOtherFloatSegment;
		final Segment theOtherSegment = segmentComposite( theOtherIntSegment = intSegment(), theOtherAsciizArraySegment = asciizArraySegment(), theOtherFloatSegment = floatSegment() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theIntSegment.getPayload(), theOtherIntSegment.getPayload() );
		assertArrayEquals( theAsciizArraySegment.getPayload(), theOtherAsciizArraySegment.getPayload() );
		assertEquals( theFloatSegment.getPayload(), theOtherFloatSegment.getPayload() );
		assertEquals( 12, theOtherSegment.getLength() );
	}

	@Test
	public void testAsciizArraySegment7() throws TransmissionException {
		final IntSegment theIntSegment;
		final AsciizArraySegment theAsciizArraySegment;
		final FloatSegment theFloatSegment;
		final Segment theSegment = segmentComposite( theIntSegment = intSegment( 2 ), theAsciizArraySegment = asciizArraySegment( 1, Endianess.BIG, "Hello world!", "Hallo Welt!" ), theFloatSegment = floatSegment( 3.14F ) );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		assertEquals( 34, theSegment.getLength() );
		final IntSegment theOtherIntSegment;
		final AsciizArraySegment theOtherAsciizArraySegment;
		final FloatSegment theOtherFloatSegment;
		final Segment theOtherSegment = segmentComposite( theOtherIntSegment = intSegment(), theOtherAsciizArraySegment = asciizArraySegment( 1, Endianess.BIG ), theOtherFloatSegment = floatSegment() );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theIntSegment.getPayload(), theOtherIntSegment.getPayload() );
		assertArrayEquals( theAsciizArraySegment.getPayload(), theOtherAsciizArraySegment.getPayload() );
		assertEquals( theFloatSegment.getPayload(), theOtherFloatSegment.getPayload() );
		assertEquals( 34, theOtherSegment.getLength() );
	}

	// -------------------------------------------------------------------------
	// Boolean:
	// -------------------------------------------------------------------------

	@Test
	public void testBooleanSegment() throws TransmissionException {
		for ( boolean eBool : new boolean[] { true, false } ) {
			// @formatter:off
			final LengthSegmentDecoratorSegment<BooleanSegment> theSegment = lengthSegment( 
				booleanSegment( eBool )
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
				System.out.println( "AbstractSchema = " + theSegment.toSchema() );
			}
			// @formatter:off
			final LengthSegmentDecoratorSegment<BooleanSegment> theOtherSegment = lengthSegment( 
					booleanSegment()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
				System.out.println( "Other schema = " + theOtherSegment.toSchema() );
			}
			assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	// -------------------------------------------------------------------------
	// Boolean array:
	// -------------------------------------------------------------------------

	@Test
	public void testBooleanArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random boolean digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<BooleanArraySection> theSegment = allocSegment(  
				booleanArraySection( toRndBooleanArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
				System.out.println( "AbstractSchema = " + theSegment.toSchema() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<BooleanArraySection> theOtherSegment = allocSegment(  
				booleanArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
				System.out.println( "Other schema = " + theOtherSegment.toSchema() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private boolean[] toRndBooleanArray( int aLength ) {
		final boolean[] theResult = new boolean[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = Math.random() < 0.5;
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// Byte:
	// -------------------------------------------------------------------------

	@Test
	public void testByteSegment() throws TransmissionException {
		for ( int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++ ) {
			// @formatter:off
			final LengthSegmentDecoratorSegment<ByteSegment> theSegment = lengthSegment( 
				byteSegment( (byte) i )
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
				System.out.println( "AbstractSchema = " + theSegment.toSchema() );
			}
			// @formatter:off
			final LengthSegmentDecoratorSegment<ByteSegment> theOtherSegment = lengthSegment( 
				byteSegment()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
				System.out.println( "Other schema = " + theOtherSegment.toSchema() );
			}
			assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	// -------------------------------------------------------------------------
	// Byte array:
	// -------------------------------------------------------------------------

	@Test
	public void testByteArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random byte digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<ByteArraySection> theSegment = allocSegment(  
				byteArraySection( toRndByteArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
				System.out.println( "AbstractSchema = " + theSegment.toSchema() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<ByteArraySection> theOtherSegment = allocSegment(  
				byteArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
				System.out.println( "Other schema = " + theOtherSegment.toSchema() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private byte[] toRndByteArray( int aLength ) {
		final byte[] theResult = new byte[aLength];
		RND.nextBytes( theResult );
		return theResult;
	}

	@Test
	public void testFixedLengthByteArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random byte digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
				fixedSegmentArraySection( toRndByteSegmentArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
				fixedSegmentArraySection( ByteSegment.class )
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
			final AllocSectionDecoratorSegment<ByteArraySection> theReferenceSegment = allocSegment(  
				byteArraySection()
			);
			// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private ByteSegment[] toRndByteSegmentArray( int aLength ) {
		final ByteSegment[] theByteSegmentArray = new ByteSegment[aLength];
		final byte[] theBytes = toRndByteArray( aLength );
		for ( int i = 0; i < theByteSegmentArray.length; i++ ) {
			theByteSegmentArray[i] = new ByteSegment( theBytes[i] );
		}
		return theByteSegmentArray;
	}

	// -------------------------------------------------------------------------
	// Short:
	// -------------------------------------------------------------------------

	@Test
	public void testShortSegment() throws TransmissionException {
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				// @formatter:off
				final LengthSegmentDecoratorSegment<ShortSegment> theSegment = lengthSegment( 
					shortSegment( (short) i, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<ShortSegment> theOtherSegment = lengthSegment( 
					shortSegment( eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// Short array:
	// -------------------------------------------------------------------------

	@Test
	public void testShortArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random short digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<ShortArraySection> theSegment = allocSegment(  
				shortArraySection( toRndShortArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<ShortArraySection> theOtherSegment = allocSegment(  
				shortArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private short[] toRndShortArray( int aLength ) {
		final short[] theResult = new short[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = (short) ( RND.nextBoolean() ? RND.nextFloat() * Short.MAX_VALUE : RND.nextFloat() * Short.MIN_VALUE );
		}
		return theResult;
	}

	@Test
	public void testFixedLengthShortArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random short digits ----------" );
			}
			// @formatter:off
				final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
					fixedSegmentArraySection( toRndShortSegmentArray(i))
				);
				// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
				final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
					fixedSegmentArraySection( ShortSegment.class )
				);
				// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
				final AllocSectionDecoratorSegment<ShortArraySection> theReferenceSegment = allocSegment(  
					shortArraySection()
				);
				// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private ShortSegment[] toRndShortSegmentArray( int aLength ) {
		final ShortSegment[] theShortSegmentArray = new ShortSegment[aLength];
		final short[] theShorts = toRndShortArray( aLength );
		for ( int i = 0; i < theShortSegmentArray.length; i++ ) {
			theShortSegmentArray[i] = new ShortSegment( theShorts[i] );
		}
		return theShortSegmentArray;
	}

	// -------------------------------------------------------------------------
	// Integer:
	// -------------------------------------------------------------------------

	@Test
	public void testIntSegment() throws TransmissionException {
		int l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Integer.MIN_VALUE : ( i == Short.MAX_VALUE ? Integer.MAX_VALUE : i ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<IntSegment> theSegment = lengthSegment( 
					intSegment( l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<IntSegment> theOtherSegment = lengthSegment( 
					intSegment( eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// Integer array:
	// -------------------------------------------------------------------------

	@Test
	public void testIntArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random int digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<IntArraySection> theSegment = allocSegment(  
				intArraySection( toRndIntArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<IntArraySection> theOtherSegment = allocSegment(  
				intArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private int[] toRndIntArray( int aLength ) {
		final int[] theResult = new int[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = RND.nextInt();
		}
		return theResult;
	}

	@Test
	public void testFixedLengthIntArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random int digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
				fixedSegmentArraySection( toRndIntSegmentArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
				fixedSegmentArraySection( IntSegment.class )
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
			final AllocSectionDecoratorSegment<IntArraySection> theReferenceSegment = allocSegment(  
				intArraySection()
			);
			// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private IntSegment[] toRndIntSegmentArray( int aLength ) {
		final IntSegment[] theIntSegmentArray = new IntSegment[aLength];
		final int[] theIntegers = toRndIntArray( aLength );
		for ( int i = 0; i < theIntSegmentArray.length; i++ ) {
			theIntSegmentArray[i] = new IntSegment( theIntegers[i] );
		}
		return theIntSegmentArray;
	}

	// -------------------------------------------------------------------------
	// Long:
	// -------------------------------------------------------------------------

	@Test
	public void testLongSegment() throws TransmissionException {
		long l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Long.MIN_VALUE : ( i == Short.MAX_VALUE ? Long.MAX_VALUE : i ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<LongSegment> theSegment = lengthSegment( 
					longSegment( l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<LongSegment> theOtherSegment = lengthSegment( 
					longSegment( eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// Long array:
	// -------------------------------------------------------------------------

	@Test
	public void testLongArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random long digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<LongArraySection> theSegment = allocSegment(  
				longArraySection( toRndLongArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<LongArraySection> theOtherSegment = allocSegment(  
				longArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private long[] toRndLongArray( int aLength ) {
		final long[] theResult = new long[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = RND.nextLong();
		}
		return theResult;
	}

	@Test
	public void testFixedLengthLongArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random long digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
				fixedSegmentArraySection( toRndLongSegmentArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
				fixedSegmentArraySection( LongSegment.class )
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
			final AllocSectionDecoratorSegment<LongArraySection> theReferenceSegment = allocSegment(  
				longArraySection()
			);
			// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private LongSegment[] toRndLongSegmentArray( int aLength ) {
		final LongSegment[] theLongSegmentArray = new LongSegment[aLength];
		final long[] theLongs = toRndLongArray( aLength );
		for ( int i = 0; i < theLongSegmentArray.length; i++ ) {
			theLongSegmentArray[i] = new LongSegment( theLongs[i] );
		}
		return theLongSegmentArray;
	}

	// -------------------------------------------------------------------------
	// Float:
	// -------------------------------------------------------------------------

	@Test
	public void testFloatSegment() throws TransmissionException {
		float l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Float.MIN_VALUE : ( i == Short.MAX_VALUE ? Float.MAX_VALUE : i * (float) Math.PI ) );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.print( "Endianess = " + eEndianess + ", value= " );
					System.out.printf( "%f%n", l );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theSegment = lengthSegment( 
					floatSegment( l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theOtherSegment = lengthSegment( 
					floatSegment( eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other sequence (hex) = " + theOtherSegment.toSequence().toHexString( ", " ) );
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "theSegment.value = " + theSegment.getDecoratee().getPayload() );
					System.out.println( "theOtherSegment.value = " + theOtherSegment.getDecoratee().getPayload() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// Float array:
	// -------------------------------------------------------------------------

	@Test
	public void testFloatArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random float digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FloatArraySection> theSegment = allocSegment(  
				floatArraySection( toRndFloatArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FloatArraySection> theOtherSegment = allocSegment(  
				floatArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private float[] toRndFloatArray( int aLength ) {
		final float[] theResult = new float[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = RND.nextBoolean() ? RND.nextFloat() * Float.MAX_VALUE : RND.nextFloat() * Float.MIN_VALUE * -1;
		}
		return theResult;
	}

	@Test
	public void testFixedLengthFloatArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random float digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
				fixedSegmentArraySection( toRndFloatSegmentArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
				fixedSegmentArraySection( FloatSegment.class )
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
			final AllocSectionDecoratorSegment<FloatArraySection> theReferenceSegment = allocSegment(  
				floatArraySection()
			);
			// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private FloatSegment[] toRndFloatSegmentArray( int aLength ) {
		final FloatSegment[] theFloatSegmentArray = new FloatSegment[aLength];
		final float[] theFloats = toRndFloatArray( aLength );
		for ( int i = 0; i < theFloatSegmentArray.length; i++ ) {
			theFloatSegmentArray[i] = new FloatSegment( theFloats[i] );
		}
		return theFloatSegmentArray;
	}

	// -------------------------------------------------------------------------
	// Double:
	// -------------------------------------------------------------------------

	@Test
	public void testDoubleSegment() throws TransmissionException {
		double l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Double.MIN_VALUE : ( i == Short.MAX_VALUE ? Double.MAX_VALUE : i * Math.PI ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<DoubleSegment> theSegment = lengthSegment( 
					doubleSegment( l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<DoubleSegment> theOtherSegment = lengthSegment( 
					doubleSegment( eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// Double array:
	// -------------------------------------------------------------------------

	@Test
	public void testDoubleArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random double digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<DoubleArraySection> theSegment = allocSegment(  
				doubleArraySection( toRndDoubleArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<DoubleArraySection> theOtherSegment = allocSegment(  
				doubleArraySection()
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
		}
	}

	private double[] toRndDoubleArray( int aLength ) {
		final double[] theResult = new double[aLength];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = RND.nextBoolean() ? RND.nextDouble() * Double.MAX_VALUE : RND.nextDouble() * Double.MIN_VALUE * -1;
		}
		return theResult;
	}

	@Test
	public void testFixedLengthDoubleArray() throws TransmissionException {
		for ( int i = 0; i < 1024; i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
				System.out.println( "---------- Testing for <" + i + "> random double digits ----------" );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theSegment = allocSegment(  
				fixedSegmentArraySection( toRndDoubleSegmentArray(i))
			);
			// @formatter:on
			final Sequence theSequence = theSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
				System.out.println( "Transmission = " + theSegment.toString() );
			}
			// @formatter:off
			final AllocSectionDecoratorSegment<FixedSegmentArraySection<?>> theOtherSegment = allocSegment(  
				fixedSegmentArraySection( DoubleSegment.class )
			);
			// @formatter:on
			theOtherSegment.fromTransmission( theSequence );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Other segment = " + theOtherSegment.toString() );
			}
			assertArrayEquals( theSegment.getDecoratee().getArray(), theOtherSegment.getDecoratee().getArray() );
			// -----------------------------------------------------------------
			// @formatter:off
			final AllocSectionDecoratorSegment<DoubleArraySection> theReferenceSegment = allocSegment(  
				doubleArraySection()
			);
			// @formatter:on
			theReferenceSegment.fromTransmission( theOtherSegment.toSequence() );
			final Sequence theReferenceSequence = theReferenceSegment.toSequence();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Reference sequence (hex) = " + theReferenceSequence.toHexString( ", " ) );
				System.out.println( "Reference segment = " + theReferenceSegment.toString() );
			}
			assertArrayEquals( theSegment.toSequence().toBytes(), theReferenceSegment.toSequence().toBytes() );
		}
	}

	private DoubleSegment[] toRndDoubleSegmentArray( int aLength ) {
		final DoubleSegment[] theDoubleSegmentArray = new DoubleSegment[aLength];
		final double[] theDoubles = toRndDoubleArray( aLength );
		for ( int i = 0; i < theDoubleSegmentArray.length; i++ ) {
			theDoubleSegmentArray[i] = new DoubleSegment( theDoubles[i] );
		}
		return theDoubleSegmentArray;
	}

	// -------------------------------------------------------------------------
	// String:
	// -------------------------------------------------------------------------

	@Test
	public void testStringSegment() throws TransmissionException {
		// @formatter:off
		final AllocSectionDecoratorSegment<StringSection> theSegment = allocSegment( 
			stringSection("Hallo Welt!")
		);
		// @formatter:on
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "AllocationBodyFacade (hex) = " + theSequence.toHexString() );
			System.out.println( "AllocationBodyFacade = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		// @formatter:off
		final AllocSectionDecoratorSegment<StringSection> theOtherSegment = allocSegment( 
			stringSection()
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
	}

	// -------------------------------------------------------------------------
	// String array:
	// -------------------------------------------------------------------------

	@Test
	public void testStringArrayBody() throws TransmissionException {
		// @formatter:off
		final AllocSectionDecoratorSegment<StringArraySection> theSegment = allocSegment( 
			stringArraySection("Hallo Welt!", "Hello World!", "สวัสดีจักรวาล!")
		);
		// @formatter:on
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
			System.out.println( "Transmission = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		// @formatter:off
		final AllocSectionDecoratorSegment<StringArraySection> theOtherSegment = allocSegment( 
			stringArraySection()
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		assertArrayEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
	}

	// -------------------------------------------------------------------------
	// Number:
	// -------------------------------------------------------------------------

	@Test
	public void testNumberSegment1() throws TransmissionException {
		long l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Integer.MIN_VALUE : ( i == Short.MAX_VALUE ? Integer.MAX_VALUE : i ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theSegment = lengthSegment( 
					numberSegment( Integer.BYTES, l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theOtherSegment = lengthSegment( 
						numberSegment( Integer.BYTES, eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	@Test
	public void testNumberSegment2() throws TransmissionException {
		long l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Long.MIN_VALUE : ( i == Short.MAX_VALUE ? Long.MAX_VALUE : i ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theSegment = lengthSegment( 
					numberSegment( Long.BYTES, l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theOtherSegment = lengthSegment( 
						numberSegment( Long.BYTES, eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	@Test
	public void testNumberSegment3() throws TransmissionException {
		long l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Integer.MIN_VALUE : ( i == Short.MAX_VALUE ? Integer.MAX_VALUE : i ) );
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theSegment = lengthSegment( 
					numberSegment( Integer.BYTES + 1, l, eEndianess )
				);
				// @formatter:on
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<NumberSegment> theOtherSegment = lengthSegment( 
						numberSegment( Integer.BYTES + 1, eEndianess )
				);
				// @formatter:on
				theOtherSegment.fromTransmission( theSequence );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------
	// EdgeCase:
	// -------------------------------------------------------------------------

	@Disabled
	@Test
	public void testEdgeCase1() throws TransmissionException {
		final int i = -128;
		// @formatter:off
		final LengthSegmentDecoratorSegment<ShortSegment> theSegment = lengthSegment( 
			shortSegment( (short) i )
		);
		// @formatter:on
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
			System.out.println( "Transmission = " + theSegment.toString() );
		}
		// @formatter:off
		final LengthSegmentDecoratorSegment<ShortSegment> theOtherSegment = lengthSegment( 
			shortSegment()
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other sequence (hex) = " + theOtherSegment.toSequence().toHexString( ", " ) );
			System.out.println( "Other segment = " + theOtherSegment.toString() );
		}
		assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
	}

	@Test
	public void testEdgeCase2() throws TransmissionException {
		final float l = -78131.414063F;
		final Endianess theEndianess = Endianess.LITTLE;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.print( "Endianess = " + theEndianess + ", value= " );
			System.out.printf( "%f%n", l );
		}
		// @formatter:off
		final LengthSegmentDecoratorSegment<FloatSegment> theSegment = lengthSegment( 
			floatSegment( l, theEndianess )
		);
		// @formatter:on
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
			System.out.println( "Transmission = " + theSegment.toString() );
		}
		// @formatter:off
		final LengthSegmentDecoratorSegment<FloatSegment> theOtherSegment = lengthSegment( 
			floatSegment( theEndianess )
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other sequence (hex) = " + theOtherSegment.toSequence().toHexString( ", " ) );
			System.out.println( "Other segment = " + theOtherSegment.toString() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.print( "theSegment.value = " );
			System.out.printf( "%f%n", theSegment.getDecoratee().getPayload() );
			System.out.print( "theOtherSegment.value = " );
			System.out.printf( "%f%n", theOtherSegment.getDecoratee().getPayload() );
		}
		assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
	}
}
