// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class DynamicTypeTest extends TestFixures {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testDynamicTypeSection1() throws TransmissionException {
		final int[] theValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theValues ) );
		}
		// @formatter:off
		final Segment theSegment = allocSegment(
			dynamicTypeSection( theValues )
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		final DynamicTypeSection theOtherDynamicSection;
		// @formatter:off
		final Segment theOtherSegment = allocSegment(
			theOtherDynamicSection = dynamicTypeSection()
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		final int[] theOtherValues = theOtherDynamicSection.toType( int[].class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theOtherValues ) );
		}
		assertArrayEquals( theValues, theOtherValues );
	}

	@Test
	public void testDynamicTypeSection2() throws TransmissionException {
		final Values theValues = new Values();
		theValues._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theValues._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theValues._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theValues );
		}
		final DynamicTypeSegment theDynamicTypeSegment = dynamicTypeSegment( theValues );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDynamicTypeSegment.toSchema() );
		}
		final Sequence theSequence = theDynamicTypeSegment.toSequence();
		final DynamicTypeSegment theOtherDynamicSegment = dynamicTypeSegment();
		theOtherDynamicSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherDynamicSegment.toSchema() );
		}
		final Values theOtherValues = theOtherDynamicSegment.toType( Values.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValues );
		}
		assertEquals( theValues, theOtherValues );
	}

	@Test
	public void testDynamicTypeSegment1() throws TransmissionException {
		final int[] theValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theValues ) );
		}
		final DynamicTypeSegment theDynamicTypeSegment = dynamicTypeSegment( theValues );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDynamicTypeSegment.toSchema() );
		}
		final Sequence theSequence = theDynamicTypeSegment.toSequence();
		final DynamicTypeSegment theOtherDynamicSegment = dynamicTypeSegment();
		theOtherDynamicSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherDynamicSegment.toSchema() );
		}
		final int[] theOtherValues = theOtherDynamicSegment.toType( int[].class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theOtherValues ) );
		}
		assertArrayEquals( theValues, theOtherValues );
	}

	@Test
	public void testDynamicTypeSegment2() throws TransmissionException {
		final Values theValues = new Values();
		theValues._booleans = new boolean[] { true, false, true, false, true, false, true, false, true, false };
		theValues._bytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._chars = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		theValues._shorts = new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._longs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._floats = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._doubles = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		theValues._strings = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theValues );
		}
		// @formatter:off
		final Segment theSegment = allocSegment(
			dynamicTypeSection( theValues )
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		final DynamicTypeSection theOtherDynamicSection;
		// @formatter:off
		final Segment theOtherSegment = allocSegment(
			theOtherDynamicSection = dynamicTypeSection()
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		final Values theOtherValues = theOtherDynamicSection.toType( Values.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValues );
		}
		assertEquals( theValues, theOtherValues );
	}
}
