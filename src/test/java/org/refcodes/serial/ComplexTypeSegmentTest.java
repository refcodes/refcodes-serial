// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.textual.CaseStyleBuilder;

public class ComplexTypeSegmentTest extends TestFixures {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEdgeCase1() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final String theValue = "Hallo Welt!";
		final ComplexTypeSegment<String> theSegment = new ComplexTypeSegment<>( theValue );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<String> theOtherSegment = new ComplexTypeSegment<>( String.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final String theOtherValue = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValue );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theValue, theOtherValue );
		assertEquals( theSequence, theOtherSequence );
		assertNotSame( theValue, theOtherValue );
	}

	enum ReadySteadyGo {
		READY, STEADY, GO
	}

	@Test
	public void testEdgeCase2() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final ReadySteadyGo theValue = ReadySteadyGo.STEADY;
		final ComplexTypeSegment<ReadySteadyGo> theSegment = new ComplexTypeSegment<>( theValue );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<ReadySteadyGo> theOtherSegment = new ComplexTypeSegment<>( ReadySteadyGo.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final ReadySteadyGo theOtherValue = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValue );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theValue, theOtherValue );
		assertEquals( theSequence, theOtherSequence );
		assertSame( theValue, theOtherValue );
	}

	@Test
	public void testComplexType1() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final int[] theValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
		final ComplexTypeSegment<int[]> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<int[]> theOtherSegment = new ComplexTypeSegment<>( int[].class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final int[] theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theOtherValues ) );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertArrayEquals( theValues, theOtherValues );
		assertEquals( theSequence, theOtherSequence );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testComplexType2() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Values theValues = createValues();
		final ComplexTypeSegment<Values> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Values> theOtherSegment = new ComplexTypeSegment<>( Values.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Values theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValues );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testComplexType3() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final SensorValues theSensorValues = createSensorValues();
		final ComplexTypeSegment<SensorValues> theSegment = new ComplexTypeSegment<>( theSensorValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<SensorValues> theOtherSegment = new ComplexTypeSegment<>( SensorValues.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final SensorValues theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherValues );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theSensorValues, theOtherValues );
		assertNotSame( theSensorValues, theOtherSegment );
	}

	@Test
	public void testComplexType4() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Wrapper theWrapper = createWrapper();
		final ComplexTypeSegment<Wrapper> theSegment = new ComplexTypeSegment<>( theWrapper );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Wrapper> theOtherSegment = new ComplexTypeSegment<>( Wrapper.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Wrapper theOtherWrapper = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherWrapper );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theWrapper, theOtherWrapper );
		assertNotSame( theWrapper, theOtherWrapper );
	}

	@Test
	public void testComplexType5() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Sensors theSensors = createSensors();
		final ComplexTypeSegment<Sensors> theSegment = new ComplexTypeSegment<>( theSensors );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Sensors> theOtherSegment = new ComplexTypeSegment<>( Sensors.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sensors theOtherSensors = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSensors );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theSensors, theOtherSensors );
		assertNotSame( theSensors, theOtherSensors );
	}

	@Disabled("Under construction")
	@Test
	public void testComplexType6() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Sensors[] theArray = new Sensors[] { createSensors(), createSensors(), createSensors(), createSensors(), createSensors() };
		final ComplexTypeSegment<Sensors[]> theSegment = new ComplexTypeSegment<>( theArray );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Sensors[]> theOtherSegment = new ComplexTypeSegment<>( Sensors[].class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sensors[] theOtherArray = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherArray );
		}
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		assertEquals( theSequence, theOtherSequence );
		assertArrayEquals( theArray, theOtherArray );
		assertNotSame( theArray, theOtherArray );
	}

	@Test
	public void testCompositeArrayType1() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Sensors[] theValues = new Sensors[] { createSensors(), createSensors(), createSensors() };
		final ComplexTypeSegment<Sensors[]> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Sensors[]> theOtherSegment = new ComplexTypeSegment<>( Sensors[].class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final Sensors[] theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Sensors eValue : theOtherValues ) {
				System.out.println( eValue );
			}
		}
		assertEquals( theSequence, theOtherSequence );
		assertArrayEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testCompositeArrayType2() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final SensorArray theValues = new SensorArray( new Sensor[] { new Sensor( "Sensor1", 1 ), new Sensor( "Sensor2", 22 ), new Sensor( "Sensor3", 333 ) } );
		final ComplexTypeSegment<SensorArray> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<SensorArray> theOtherSegment = new ComplexTypeSegment<>( SensorArray.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final SensorArray theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Sensor eValue : theOtherValues.getSensors() ) {
				System.out.println( eValue );
			}
		}
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testCompositeArrayType3() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final SensorArray theValues[] = new SensorArray[] { new SensorArray( new Sensor[] { new Sensor( "Sensor1", 1 ), new Sensor( "Sensor2", 22 ), new Sensor( "Sensor3", 333 ) } ), new SensorArray( new Sensor[] { new Sensor( "Sensor4", 4 ), new Sensor( "Sensor5", 55 ), new Sensor( "Sensor6", 666 ) } ), new SensorArray( new Sensor[] { new Sensor( "Sensor7", 7 ), new Sensor( "Sensor8", 88 ), new Sensor( "Sensor9", 999 ) } ) };
		final ComplexTypeSegment<SensorArray[]> theSegment = new ComplexTypeSegment<>( theValues );
		printSegment( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<SensorArray[]> theOtherSegment = new ComplexTypeSegment<>( SensorArray[].class );
		theOtherSegment.fromTransmission( theSequence );
		printSegment( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final SensorArray[] theOtherValues = theOtherSegment.getPayload();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( SensorArray eArray : theOtherValues ) {
				for ( Sensor eValue : eArray.getSensors() ) {
					System.out.println( eValue );
				}
			}
		}
		assertEquals( theSequence, theOtherSequence );
		assertArrayEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testCompositeArrayType4() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final SensorValues theValues = createSensorValues();
		final ComplexTypeSegment<SensorValues> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<SensorValues> theOtherSegment = new ComplexTypeSegment<>( SensorValues.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final SensorValues theOtherValues = theOtherSegment.getPayload();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testCompositeArrayType5() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Values theValues = createValues();
		final ComplexTypeSegment<Values> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Values> theOtherSegment = new ComplexTypeSegment<>( Values.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final Values theOtherValues = theOtherSegment.getPayload();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testCompositeArrayType6() throws TransmissionException {
		printTestName( Execution.toMethodName() );
		final Wrapper theValues = createWrapper();
		final ComplexTypeSegment<Wrapper> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		final ComplexTypeSegment<Wrapper> theOtherSegment = new ComplexTypeSegment<>( Wrapper.class );
		theOtherSegment.fromTransmission( theSequence );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final Sequence theOtherSequence = theOtherSegment.toSequence();
		final Wrapper theOtherValues = theOtherSegment.getPayload();
		assertEquals( theSequence, theOtherSequence );
		assertEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	@Test
	public void testComplexTypeStream() throws IOException {
		printTestName( Execution.toMethodName() );
		final SensorArray theValues[] = new SensorArray[] { new SensorArray( new Sensor[] { new Sensor( "Sensor1", 1 ), new Sensor( "Sensor2", 22 ), new Sensor( "Sensor3", 333 ) } ), new SensorArray( new Sensor[] { new Sensor( "Sensor4", 4 ), new Sensor( "Sensor5", 55 ), new Sensor( "Sensor6", 666 ) } ), new SensorArray( new Sensor[] { new Sensor( "Sensor7", 7 ), new Sensor( "Sensor8", 88 ), new Sensor( "Sensor9", 999 ) } ) };
		final ComplexTypeSegment<SensorArray[]> theSegment = new ComplexTypeSegment<>( theValues );
		printSchema( theSegment );
		printSimpleTypeMap( theSegment );
		final LoopbackPort theTransceiver = new LoopbackPort( "loop0" ).withOpen();
		theSegment.transmitTo( theTransceiver.getOutputStream() );
		final ComplexTypeSegment<SensorArray[]> theOtherSegment = new ComplexTypeSegment<>( SensorArray[].class );
		theOtherSegment.receiveFrom( theTransceiver.getInputStream() );
		printSchema( theOtherSegment );
		printSimpleTypeMap( theOtherSegment );
		final SensorArray[] theOtherValues = theOtherSegment.getPayload();
		assertArrayEquals( theValues, theOtherValues );
		assertNotSame( theValues, theOtherValues );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void printTestName( String aMethodName ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
			System.out.println( "[---------- " + CaseStyleBuilder.asSnakeCase( aMethodName ) + " ----------]" );
			System.out.println();
		}
	}

	private void printSimpleTypeMap( Transmission aSegment ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final SimpleTypeMap theSimpleTypeMap = aSegment.toSimpleTypeMap();
			for ( String eKey : theSimpleTypeMap.sortedKeys() ) {
				System.out.println( eKey + " = " + theSimpleTypeMap.get( eKey ) );
			}
			System.out.println();
		}
	}

	private void printSchema( Transmission aSegment ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aSegment.toSchema() );
			System.out.println();
		}
	}

	private void printSegment( Transmission aSegment ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final SimpleTypeMap theSimpleTypeMap = aSegment.toSimpleTypeMap();
			for ( String eKey : theSimpleTypeMap.sortedKeys() ) {
				System.out.println( eKey + " = " + theSimpleTypeMap.get( eKey ) );
			}
			System.out.println();
		}
	}
}
