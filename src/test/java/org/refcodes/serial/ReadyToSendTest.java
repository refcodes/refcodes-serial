// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.TestFixures.WeatherData;
import org.refcodes.textual.VerboseTextBuilder;

public class ReadyToSendTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 3;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testReadyToSendSegmentDecorator1() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final Segment theSenderSeg = readyToSendSegment( stopAndWaitSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ) );
				final Segment theReceiverSeg = readyToSendSegment( stopAndWaitSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testReadyToSendSegmentDecorator1Breaker() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final BreakerSegmentDecorator<?> theBreaker;
				final Segment theSenderSeg = readyToSendSegment( stopAndWaitSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ) );
				final Segment theReceiverSeg = readyToSendSegment( stopAndWaitSegment( theBreaker = breakerSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), 3 ) ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					theBreaker.reset();
				}
			}
		}
	}

	@Test
	public void testReadyToSendSegmentDecorator2() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final Segment theSenderSeg = readyToSendSegment( stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 16 ).build() );
				final Segment theReceiverSeg = readyToSendSegment( stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 16 ).build() );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testReadyToSendSegmentDecorator3() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final Segment theSenderSeg = readyToSendSegment( stopAndWaitSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ), 5000 );
				final Segment theReceiverSeg = readyToSendSegment( stopAndWaitSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) ), 5000 );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theTransmitPort.doTransmitSegment( theSenderSeg );
					thePortTestBench.waitForPortCatchUp( 400 );
					theReceiverPort.receiveSegment( theReceiverSeg );
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					theResult.waitForResult();
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void testReadyToSendSegmentDecorator4() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final Segment theSenderSeg = readyToSendSegmentBuilder().withDecoratee( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ).withReadyToSendRetryNumber( 2 ).withReadyToSendTimeoutMillis( 200 ).build();
				for ( int i = 0; i < LOOPS; i++ ) {
					try {
						theTransmitPort.transmitSegment( theSenderSeg );
						fail( "Expected a <" + FlowControlTimeoutException.class.getSimpleName() + ">!" );
					}
					catch ( FlowControlTimeoutException e ) {
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( e.toMessage() );
							// e.printStackTrace();
						}
						/* expected */
					}
				}
			}
		}
	}

	@Test
	public void testReadyToSendSegmentDecorator5() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String[] theSenderData = { "This", "is", "not", "a", "love", "song" };
				final StringArraySection theReceiverSection;
				final Segment theSenderSeg = allocSegment( readyToSendSection( ( crcPrefixSection( stringArraySection( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ) ) );
				final Segment theReceiverSeg = allocSegment( readyToSendSection( ( crcPrefixSection( theReceiverSection = stringArraySection(), CrcStandard.CRC_16_CCITT_FALSE ) ) ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					theReceiverSection.setPayload( null );
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final String[] theReceiverData = theReceiverSection.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					assertArrayEquals( theSenderData, theReceiverData );
				}
			}
		}
	}
}
