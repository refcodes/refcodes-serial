// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.mixin.ConcatenateMode;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.TestFixures.WeatherData;
import org.refcodes.textual.VerboseTextBuilder;

public class PacketStreamTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 3;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unused")
	@Test
	@Disabled
	public void testPackeStream() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final int theBlockSize = 64;
				final PacketInputStream thePacketIn = PacketInputStream.builder().withInputStream( theReceiverPort.getInputStream() ).withBlockSize( theBlockSize ).build();
				final PacketOutputStream thePacketOut = PacketOutputStream.builder().withOutputStream( theReceiverPort.getOutputStream() ).withBlockSize( theBlockSize ).build();
				final byte[] theOutBytes = "Hallo Welt!".getBytes();
				final byte[] theInBytes = new byte[theOutBytes.length];
				final Thread t = new Thread( new Runnable() {
					@Override
					public void run() {
						int eRead;
						int i = 0;
						try {
							while ( ( eRead = thePacketIn.read() ) != -1 && i < theOutBytes.length ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									System.out.println( i + " - " + (char) eRead );
								}
								theInBytes[i] = (byte) eRead;
								i++;
							}
							if ( SystemProperty.LOG_TESTS.isEnabled() ) {
								System.out.println( "EOT = " + eRead );
								// assertEquals( -1, eRead );
							}
							synchronized ( this ) {
								notifyAll();
							}
						}
						catch ( IOException e ) {
							System.out.println( e.getClass().getSimpleName() + ": " + e.getMessage() );
						}
					}
				} );
				t.start();
				thePacketOut.write( theOutBytes, 0, 3 );
				thePacketOut.flush();
				thePacketOut.write( theOutBytes, 3, theOutBytes.length - 3 );
				thePacketOut.flush();
				synchronized ( this ) {
					wait( 500 );
				}
				assertArrayEquals( theOutBytes, theInBytes );
			}
		}
	}

	@Test
	public void testSegmentTransmitReceive1() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 7 ) );
		}
		final StringSection theOtherStringSection = new StringSection();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
			System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
		}
		assertEquals( theStringSection, theOtherStringSection );
	}

	@Test
	public void testSegmentTransmitReceive1Crc() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).build();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 9 ) );
		}
		final StringSection theOtherStringSection = new StringSection();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
			System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
		}
		assertEquals( theStringSection, theOtherStringSection );
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive1() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 256 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringSection theOtherStringSection = new StringSection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
					System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
				}
				assertEquals( theStringSection, theOtherStringSection );
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive1Crc() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 256 ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringSection theOtherStringSection = new StringSection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
					System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
				}
				assertEquals( theStringSection, theOtherStringSection );
			}
		}
	}

	@Test
	public void testSegmentTransmitReceive2() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 7 ) );
		}
		final StringArraySection theOtherStringSection = new StringArraySection();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
			System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
	}

	@Test
	public void testSegmentTransmitReceive2Crc() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).build();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 9 ) );
		}
		final StringArraySection theOtherStringSection = new StringArraySection();
		final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
			System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive2() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 0 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringArraySection theOtherStringSection = new StringArraySection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
					System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
				}
				assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentTransmitReceive2Crc() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
				final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 0 ).withBlockSize( 5 ).build();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> thePacketSection = stopAndWaitPacketStreamSegment( allocSegment( theStringSection ), theMetrics );
				final StringArraySection theOtherStringSection = new StringArraySection();
				final StopAndWaitPacketStreamSegmentDecorator<Segment> theOtherPacketSection = stopAndWaitPacketStreamSegment( allocSegment( theOtherStringSection ), theMetrics );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theOtherPacketSection );
				thePacketSection.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
					System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
				}
				assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
			}
		}
	}

	// -------------------------------------------------------------------------

	@Test
	public void testSectionTransmitReceive1() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 0 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
		final StopAndWaitPacketStreamSectionDecorator<StringSection> thePacketSection = stopAndWaitPacketStreamSection( theStringSection, theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 7 ) );
		}
		final StringSection theOtherStringSection = new StringSection();
		final StopAndWaitPacketStreamSectionDecorator<StringSection> theOtherPacketSection = stopAndWaitPacketStreamSection( theOtherStringSection, theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream, thePacketSection.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
			System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
		}
		assertEquals( theStringSection, theOtherStringSection );
	}

	@Test
	public void testSectionTransmitReceive1Crc() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringSection theStringSection = new StringSection( Text.ARECIBO_MESSAGE.getText() );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).build();
		final StopAndWaitPacketStreamSectionDecorator<StringSection> thePacketSection = stopAndWaitPacketStreamSection( theStringSection, theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 9 ) );
		}
		final StringSection theOtherStringSection = new StringSection();
		final StopAndWaitPacketStreamSectionDecorator<StringSection> theOtherPacketSection = stopAndWaitPacketStreamSection( theOtherStringSection, theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream, thePacketSection.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: \"" + theStringSection.getPayload() + "\"" );
			System.out.println( "Other string section: \"" + theOtherStringSection.getPayload() + "\"" );
		}
		assertEquals( theStringSection, theOtherStringSection );
	}

	@Test
	public void testSectionTransmitReceive2() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).withAcknowledgeSegmentPackager( dummySegmentPackager() ).withPacketSegmentPackager( dummySegmentPackager() ).build();
		final StopAndWaitPacketStreamSectionDecorator<StringArraySection> thePacketSection = stopAndWaitPacketStreamSection( theStringSection, theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 7 ) );
		}
		final StringArraySection theOtherStringSection = new StringArraySection();
		final StopAndWaitPacketStreamSectionDecorator<StringArraySection> theOtherPacketSection = stopAndWaitPacketStreamSection( theOtherStringSection, theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream, thePacketSection.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
			System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
	}

	@Test
	public void testSectionTransmitReceive2Crc() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final StringArraySection theStringSection = new StringArraySection( "AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "" );
		final TransmissionMetrics theMetrics = TransmissionMetrics.builder().withEndianess( Endianess.LITTLE ).withCrcAlgorithm( CrcStandard.CRC_16_CCITT_FALSE ).withCrcChecksumConcatenateMode( ConcatenateMode.PREPEND ).withSequenceNumberWidth( 2 ).withSequenceNumberInitValue( 511 ).withBlockSize( 5 ).build();
		final StopAndWaitPacketStreamSectionDecorator<StringArraySection> thePacketSection = stopAndWaitPacketStreamSection( theStringSection, theMetrics );
		thePacketSection.transmitTo( theOutputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( NumericalUtility.toHexString( theOutputStream.toByteArray(), ":", 9 ) );
		}
		final StringArraySection theOtherStringSection = new StringArraySection();
		final StopAndWaitPacketStreamSectionDecorator<StringArraySection> theOtherPacketSection = stopAndWaitPacketStreamSection( theOtherStringSection, theMetrics );
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theOutputStream.toByteArray() );
		theOtherPacketSection.receiveFrom( theInputStream, thePacketSection.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String section: " + VerboseTextBuilder.asString( theStringSection.getPayload() ) );
			System.out.println( "Other string section: " + VerboseTextBuilder.asString( theOtherStringSection.getPayload() ) );
		}
		assertArrayEquals( theStringSection.getPayload(), theOtherStringSection.getPayload() );
	}

	@Test
	public void testStopAndWaitPacketStreamSegmentDecorator() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverComplexSegment;
				final Segment theSenderSeg = stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 8 ).build();
				final Segment theReceiverSeg = stopAndWaitPacketStreamSegmentBuilder().withDecoratee( crcPrefixSegment( theReceiverComplexSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) ).withBlockSize( 8 ).build();
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverComplexSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					try {
						Thread.sleep( 1000 );
					}
					catch ( InterruptedException e ) {}
				}
			}
		}
	}

	@Test
	public void testEdgeCase1() throws IOException, InterruptedException {
		final ByteArrayOutputStream theByteOut = new ByteArrayOutputStream();
		final int theBlockSize = 6;
		final PacketOutputStream thePacketOut = PacketOutputStream.builder().withOutputStream( theByteOut ).withBlockSize( theBlockSize ).build();
		final Sequence theSeqOut = new ByteArraySequence( new byte[] { 1, 2, -1, 4, -1, 0 } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence:" );
			System.out.println( NumericalUtility.toHexString( ":", 10, theSeqOut.toBytes() ) );
		}
		theSeqOut.writeTo( thePacketOut );
		thePacketOut.flush();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ByteArrayOutputStream:" );
			System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theByteOut.toByteArray() ) );
		}
		final ByteArrayInputStream theByteIn = new ByteArrayInputStream( theByteOut.toByteArray() );
		try ( PacketInputStream thePacketIn = PacketInputStream.builder().withInputStream( theByteIn ).withBlockSize( theBlockSize ).build() ) {
			final Sequence theInSeq = new ByteArraySequence( thePacketIn );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "ByteArrayInputStream:" );
				System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theInSeq.toBytes() ) );
			}
			assertArrayEquals( theSeqOut.toBytes(), theInSeq.toBytes() );
		}
	}

	@Test
	public void testEdgeCase2() throws IOException, InterruptedException {
		final ByteArrayOutputStream theByteOut = new ByteArrayOutputStream();
		final int theBlockSize = 1;
		final PacketOutputStream thePacketOut = PacketOutputStream.builder().withOutputStream( theByteOut ).withBlockSize( theBlockSize ).build();
		final Sequence theSeqOut = new ByteArraySequence( new byte[] { 1, 2, -1, 4, -1, 0 } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence:" );
			System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theSeqOut.toBytes() ) );
		}
		theSeqOut.writeTo( thePacketOut );
		thePacketOut.flush();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ByteArrayOutputStream:" );
			System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theByteOut.toByteArray() ) );
		}
		final ByteArrayInputStream theByteIn = new ByteArrayInputStream( theByteOut.toByteArray() );
		try ( PacketInputStream thePacketIn = PacketInputStream.builder().withInputStream( theByteIn ).withBlockSize( theBlockSize ).build() ) {
			final Sequence theInSeq = new ByteArraySequence( thePacketIn );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "ByteArrayInputStream:" );
				System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theInSeq.toBytes() ) );
			}
			assertArrayEquals( theSeqOut.toBytes(), theInSeq.toBytes() );
		}
	}

	@Test
	public void testEdgeCase3() throws IOException, InterruptedException {
		final ByteArrayOutputStream theByteOut = new ByteArrayOutputStream();
		final int theBlockSize = 3;
		final PacketOutputStream thePacketOut = PacketOutputStream.builder().withOutputStream( theByteOut ).withBlockSize( theBlockSize ).build();
		final Sequence theSeqOut = new ByteArraySequence( new byte[] { 1, 2, -1, 4, -1, 0 } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence:" );
			System.out.println( NumericalUtility.toHexString( ":", 10, theSeqOut.toBytes() ) );
		}
		theSeqOut.writeTo( thePacketOut );
		thePacketOut.flush();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ByteArrayOutputStream:" );
			System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theByteOut.toByteArray() ) );
		}
		final ByteArrayInputStream theByteIn = new ByteArrayInputStream( theByteOut.toByteArray() );
		try ( PacketInputStream thePacketIn = PacketInputStream.builder().withInputStream( theByteIn ).withBlockSize( theBlockSize ).build() ) {
			final Sequence theInSeq = new ByteArraySequence( thePacketIn );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "ByteArrayInputStream:" );
				System.out.println( NumericalUtility.toHexString( ":", 10, theInSeq.toBytes() ) );
			}
			assertArrayEquals( theSeqOut.toBytes(), theInSeq.toBytes() );
		}
	}

	@Test
	public void testEdgeCase4() throws IOException, InterruptedException {
		final ByteArrayOutputStream theByteOut = new ByteArrayOutputStream();
		final int theBlockSize = 10;
		final PacketOutputStream thePacketOut = PacketOutputStream.builder().withOutputStream( theByteOut ).withBlockSize( theBlockSize ).build();
		final Sequence theSeqOut = new ByteArraySequence( new byte[] { 1, 2, -1, 4, -1, 0 } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence:" );
			System.out.println( NumericalUtility.toHexString( ":", 10, theSeqOut.toBytes() ) );
		}
		theSeqOut.writeTo( thePacketOut );
		thePacketOut.flush();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ByteArrayOutputStream:" );
			System.out.println( NumericalUtility.toHexString( ":", thePacketOut.getPacketSize(), theByteOut.toByteArray() ) );
		}
		final ByteArrayInputStream theByteIn = new ByteArrayInputStream( theByteOut.toByteArray() );
		try ( PacketInputStream thePacketIn = PacketInputStream.builder().withInputStream( theByteIn ).withBlockSize( theBlockSize ).build() ) {
			final Sequence theInSeq = new ByteArraySequence( thePacketIn );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "ByteArrayInputStream:" );
				System.out.println( NumericalUtility.toHexString( ":", 10, theInSeq.toBytes() ) );
			}
			assertArrayEquals( theSeqOut.toBytes(), theInSeq.toBytes() );
		}
	}
}
