// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Invertible;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.TestFixures.WeatherData;

public class InvertibleTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 15;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testInvertibleSimple() throws TransmissionException {
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theSegment = lengthSegment( 
			allocSegment( 
				invertibleSection( 
					stringSection("Hello World!"), new InvertibleXor( (byte) 0xFE )
				)
			)
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theOtherSegment = lengthSegment( 
				allocSegment( 
					invertibleSection( 
						stringSection(), new InvertibleXor( (byte) 0xFE )
					)
				)
			);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	@Test
	public void testInvertibleSimpleFail() throws TransmissionException {
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theSegment = lengthSegment( 
			allocSegment( 
				invertibleSection( 
					stringSection("Hello World!"), new InvertibleXor( (byte) 0xFE )
				)
			)
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final LengthSegmentDecoratorSegment<?> theOtherSegment = lengthSegment( 
				allocSegment( 
					invertibleSection( 
						stringSection(), new InvertibleXor( (byte) 0xFF )
					)
				)
			);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertNotEquals( theSegment, theOtherSegment );
	}

	@Test
	public void testInvertibleComplex() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final InvertibleXor theInvertibleXor;
				final Segment theSenderSeg = readyToSendSegment( invertibleSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ), theInvertibleXor = new InvertibleXor( (byte) 0x10 ) ) );
				final Segment theReceiverSeg = readyToSendSegment( invertibleSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), theInvertibleXor ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						try {
							System.out.println( "Expected [" + theInvertibleXor.bijectionXor + "]= " + theResult.getResult() );
						}
						catch ( BadCrcChecksumException e ) {
							System.out.println( "Unexpected exceptio = " + e.getMessage() );
							fail( "Not Expecting a <" + BadCrcChecksumException.class.getSimpleName() + "> instead of a resul!t" );
						}
					}
					final WeatherData theReceiverData = theReceiverSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					theInvertibleXor.bijectionXor++;
					theInvertibleXor.inversionXor = theInvertibleXor.bijectionXor;
				}
			}
		}
	}

	@Test
	public void testInvertibleComplexFail() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverSegment;
				final Segment theSenderSeg = invertibleSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ), new InvertibleXor( (byte) 0x10 ) );
				final Segment theReceiverSeg = invertibleSegment( crcPrefixSegment( theReceiverSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), new InvertibleXor( (byte) 0xC3 ) );
				final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
				theSenderSeg.transmitTo( theTransmitPort );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					try {
						System.out.println( "Result = " + theResult.getResult() );
						fail( "Expecting a <" + BadCrcChecksumException.class.getSimpleName() + "> instead of a resul!t" );
					}
					catch ( BadCrcChecksumException expected ) {
						System.out.println( "Expected = " + expected.getMessage() );
					}
				}
				final WeatherData theReceiverData = theReceiverSegment.getPayload();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( theReceiverData );
				}
				assertNotEquals( theSenderData, theReceiverData );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	class InvertibleXor implements Invertible<Byte, Byte> {
		private byte bijectionXor;
		private byte inversionXor;

		public InvertibleXor( byte aXor ) {
			bijectionXor = aXor;
			inversionXor = aXor;
		}

		public InvertibleXor( byte aBijectionXor, byte aInversionXor ) {
			bijectionXor = aBijectionXor;
			inversionXor = aInversionXor;
		}

		@Override
		public Byte applyBijection( Byte aValue ) {
			return (byte) ( aValue ^ bijectionXor );
		}

		@Override
		public Byte applyInversion( Byte aValue ) {
			return (byte) ( aValue ^ inversionXor );
		}
	}
}
