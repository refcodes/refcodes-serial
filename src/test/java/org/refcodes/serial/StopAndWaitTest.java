// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.serial.TestFixures.WeatherData;

public class StopAndWaitTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOOPS = 3;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testStopAndWaitSegmentDecorator1() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverComplexSegment;
				final Segment theSenderSeg = stopAndWaitSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) );
				final Segment theReceiverSeg = stopAndWaitSegment( crcPrefixSegment( theReceiverComplexSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverComplexSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testStopAndWaitSegmentDecorator2() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final WeatherData theSenderData = TestFixures.createWeatherData();
				final ComplexTypeSegment<WeatherData> theReceiverComplexSegment;
				final BreakerSegmentDecorator<?> theBreaker;
				final Segment theSenderSeg = stopAndWaitSegment( crcPrefixSegment( complexTypeSegment( theSenderData ), CrcStandard.CRC_16_CCITT_FALSE ) );
				final Segment theReceiverSeg = stopAndWaitSegment( theBreaker = breakerSegment( crcPrefixSegment( theReceiverComplexSegment = complexTypeSegment( WeatherData.class ), CrcStandard.CRC_16_CCITT_FALSE ), 3 ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final WeatherData theReceiverData = theReceiverComplexSegment.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					theBreaker.reset();
				}
			}
		}
	}

	@Test
	public void testStopAndWaitSectionDecorator1() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String theSenderData = "The weather is good this morning. Temperature is about 20 degrees, some rain, much sun.";
				final StringSection theReceiverSection;
				final Segment theSenderSeg = crcPrefixSegment( allocSegment( stopAndWaitSection( stringSection( theSenderData ) ) ), CrcStandard.CRC_16_CCITT_FALSE );
				final Segment theReceiverSeg = crcPrefixSegment( allocSegment( stopAndWaitSection( theReceiverSection = stringSection() ) ), CrcStandard.CRC_16_CCITT_FALSE );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final String theReceiverData = theReceiverSection.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testStopAndWaitSectionDecorator2() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String theSenderData = "The weather is good this morning. Temperature is about 20 degrees, some rain, much sun.";
				final StringSection theReceiverSection;
				final BreakerSectionDecorator<?> theBreaker;
				final Segment theSenderSeg = crcPrefixSegment( allocSegment( stopAndWaitSection( stringSection( theSenderData ) ) ), CrcStandard.CRC_16_CCITT_FALSE );
				final Segment theReceiverSeg = crcPrefixSegment( allocSegment( stopAndWaitSection( theBreaker = breakerSection( theReceiverSection = stringSection(), 3 ) ) ), CrcStandard.CRC_16_CCITT_FALSE );
				for ( int i = 0; i < LOOPS; i++ ) {
					final SegmentResult<Segment> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					theResult.waitForResult();
					final String theReceiverData = theReceiverSection.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( theReceiverData );
					}
					assertEquals( theSenderData, theReceiverData );
					theBreaker.reset();
				}
			}
		}
	}
}
