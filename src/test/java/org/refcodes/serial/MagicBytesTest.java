// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.refcodes.exception.TimeoutIOException;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.SimpleTypeMap;
import org.refcodes.textual.VerboseTextBuilder;

public class MagicBytesTest extends AbstractPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int WAIT_TIMEOUT = 5000;
	private static final int LOOPS = 15;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testMagicBytesSegmentDispatcher1() throws TransmissionException {
		final StringSegment theString1;
		final StringSegment theString2;
		final StringSegment theString3;
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes1 = assertMagicBytesSegment( theString1 = stringSegment(), "A00".getBytes() );
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes2 = assertMagicBytesSegment( theString2 = stringSegment(), "A01".getBytes() );
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes3 = assertMagicBytesSegment( theString3 = stringSegment(), "B02".getBytes() );
		final MagicBytesSegmentMultiplexer theMultiplexer = new MagicBytesSegmentMultiplexer( theMagicBytes1, theMagicBytes2, theMagicBytes3 );
		final MagicBytesSegmentDecorator<StringSegment> theTransmission = magicBytesSegment( stringSegment( "A01!" ), "A01".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new String( theMagicBytes1.getPayload() ) + ": " + theString1.getPayload() );
			System.out.println( new String( theMagicBytes2.getPayload() ) + ": " + theString2.getPayload() );
			System.out.println( new String( theMagicBytes3.getPayload() ) + ": " + theString3.getPayload() );
		}
		assertNull( theString1.getPayload() );
		assertEquals( "A01!", theString2.getPayload() );
		assertNull( theString3.getPayload() );
	}

	@Test
	public void testMagicBytesSegmentDispatcher2() throws TransmissionException {
		final StringSegment theString1;
		final StringSegment theString2;
		final StringSegment theString3;
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes1 = assertMagicBytesSegment( theString1 = stringSegment(), "A00".getBytes() );
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes2 = assertMagicBytesSegment( theString2 = stringSegment(), "A01".getBytes() );
		final MagicBytesSegmentDecorator<StringSegment> theMagicBytes3 = assertMagicBytesSegment( theString3 = stringSegment(), "B02".getBytes() );
		final MagicBytesSegmentMultiplexer theMultiplexer = new MagicBytesSegmentMultiplexer( theMagicBytes1, theMagicBytes2, theMagicBytes3 );
		MagicBytesSegmentDecorator<StringSegment> theTransmission = magicBytesSegment( stringSegment( "A00!" ), "A00".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence() );
		theTransmission = magicBytesSegment( stringSegment( "A01!" ), "A01".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence() );
		theTransmission = magicBytesSegment( stringSegment( "B02!" ), "B02".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new String( theMagicBytes1.getPayload() ) + ": " + theString1.getPayload() );
			System.out.println( new String( theMagicBytes2.getPayload() ) + ": " + theString2.getPayload() );
			System.out.println( new String( theMagicBytes3.getPayload() ) + ": " + theString3.getPayload() );
		}
		assertEquals( "A00!", theString1.getPayload() );
		assertEquals( "A01!", theString2.getPayload() );
		assertEquals( "B02!", theString3.getPayload() );
	}

	@Test
	public void testMagicBytesSegmentDispatcher3() throws IOException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringSegment theString1;
				final StringSegment theString2;
				final StringSegment theString3;
				final MagicBytesSegmentDecorator<StringSegment> theMagicBytes1 = assertMagicBytesSegment( theString1 = stringSegment(), "A00".getBytes() );
				final MagicBytesSegmentDecorator<StringSegment> theMagicBytes2 = assertMagicBytesSegment( theString2 = stringSegment(), "A01".getBytes() );
				final MagicBytesSegmentDecorator<StringSegment> theMagicBytes3 = assertMagicBytesSegment( theString3 = stringSegment(), "B02".getBytes() );
				final MagicBytesSegmentMultiplexer theMultiplexer = new MagicBytesSegmentMultiplexer( theMagicBytes1, theMagicBytes2, theMagicBytes3 );
				final MagicBytesSegmentDecorator<StringSegment> theTransmission = magicBytesSegment( stringSegment( "A01!" ), "A01".getBytes() );
				theTransmitPort.transmitSegment( theTransmission );
				final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theMultiplexer );
				theResult.waitForResult();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( new String( theMagicBytes1.getPayload() ) + ": " + theString1.getPayload() );
					System.out.println( new String( theMagicBytes2.getPayload() ) + ": " + theString2.getPayload() );
					System.out.println( new String( theMagicBytes3.getPayload() ) + ": " + theString3.getPayload() );
				}
				assertNull( theString1.getPayload() );
				assertEquals( "A01!", theString2.getPayload() );
				assertNull( theString3.getPayload() );
			}
		}
	}

	@Test
	public void testMagicBytesSectionDispatcher1() throws TransmissionException {
		final StringSection theString1;
		final StringSection theString2;
		final StringSection theString3;
		final MagicBytesSectionDecorator<StringSection> theMagicBytes1 = assertMagicBytesSection( theString1 = stringSection(), "A00".getBytes() );
		final MagicBytesSectionDecorator<StringSection> theMagicBytes2 = assertMagicBytesSection( theString2 = stringSection(), "A01".getBytes() );
		final MagicBytesSectionDecorator<StringSection> theMagicBytes3 = assertMagicBytesSection( theString3 = stringSection(), "B02".getBytes() );
		final MagicBytesSectionMultiplexer theMultiplexer = new MagicBytesSectionMultiplexer( theMagicBytes1, theMagicBytes2, theMagicBytes3 );
		final MagicBytesSectionDecorator<StringSection> theTransmission = magicBytesSection( stringSection( "A01!" ), "A01".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence(), theTransmission.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new String( theMagicBytes1.getPayload() ) + ": " + theString1.getPayload() );
			System.out.println( new String( theMagicBytes2.getPayload() ) + ": " + theString2.getPayload() );
			System.out.println( new String( theMagicBytes3.getPayload() ) + ": " + theString3.getPayload() );
		}
		assertNull( theString1.getPayload() );
		assertEquals( "A01!", theString2.getPayload() );
		assertNull( theString3.getPayload() );
	}

	@Test
	public void testMagicBytesSectionDispatcher2() throws TransmissionException {
		final StringSection theString1;
		final StringSection theString2;
		final StringSection theString3;
		final MagicBytesSectionDecorator<StringSection> theMagicBytes1 = assertMagicBytesSection( theString1 = stringSection(), "A00".getBytes() );
		final MagicBytesSectionDecorator<StringSection> theMagicBytes2 = assertMagicBytesSection( theString2 = stringSection(), "A01".getBytes() );
		final MagicBytesSectionDecorator<StringSection> theMagicBytes3 = assertMagicBytesSection( theString3 = stringSection(), "B02".getBytes() );
		final MagicBytesSectionMultiplexer theMultiplexer = new MagicBytesSectionMultiplexer( theMagicBytes1, theMagicBytes2, theMagicBytes3 );
		MagicBytesSectionDecorator<StringSection> theTransmission = magicBytesSection( stringSection( "A00!" ), "A00".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence(), theTransmission.getLength() );
		theTransmission = magicBytesSection( stringSection( "A01!" ), "A01".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence(), theTransmission.getLength() );
		theTransmission = magicBytesSection( stringSection( "B02!" ), "B02".getBytes() );
		theMultiplexer.fromTransmission( theTransmission.toSequence(), theTransmission.getLength() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new String( theMagicBytes1.getPayload() ) + ": " + theString1.getPayload() );
			System.out.println( new String( theMagicBytes2.getPayload() ) + ": " + theString2.getPayload() );
			System.out.println( new String( theMagicBytes3.getPayload() ) + ": " + theString3.getPayload() );
		}
		assertEquals( "A00!", theString1.getPayload() );
		assertEquals( "A01!", theString2.getPayload() );
		assertEquals( "B02!", theString3.getPayload() );
	}

	@Test
	public void testMagicBytesPlain() throws TransmissionException {
		final MagicBytesSegment theSegment = magicBytesSegment( "FEED C0DE", StandardCharsets.US_ASCII );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		final MagicBytesSegment theOtherSegment = magicBytesSegment( 9 );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	// -------------------------------------------------------------------------

	@Test
	public void testAssertMagicBytesPlainPass() throws TransmissionException {
		final AssertMagicBytesSegment theSegment = assertMagicBytesSegment( "FEED C0DE", StandardCharsets.US_ASCII );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		final AssertMagicBytesSegment theOtherSegment = assertMagicBytesSegment( "FEED C0DE", StandardCharsets.US_ASCII );
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	// -------------------------------------------------------------------------

	@Test
	public void testAssertMagicBytesPlainFail() throws TransmissionException {
		final AssertMagicBytesSegment theSegment = assertMagicBytesSegment( "FEED C0DE", StandardCharsets.US_ASCII );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		final AssertMagicBytesSegment theOtherSegment = assertMagicBytesSegment( "BAD", StandardCharsets.US_ASCII );
		try {
			theOtherSegment.fromTransmission( theSequence );
			fail( "Expected a <BadMagicBytesException> here!" );
		}
		catch ( BadMagicBytesException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expected: " + e.getMessage() );
			}
		}
	}

	// -------------------------------------------------------------------------

	@Test
	public void testMagicBytesComposite() throws TransmissionException {
		// @formatter:off
		final SegmentComposite<MagicBytesSegment> theSegment = segmentComposite(
			magicBytesSegment( "FEED C0DE", StandardCharsets.US_ASCII ),
			magicBytesSegment(  new byte[] {-64, -34, -2, -19} ) // C0 DE FE ED = CODE FEED
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final SegmentComposite<MagicBytesSegment> theOtherSegment = segmentComposite(
			magicBytesSegment( 9 ),
			magicBytesSegment( 4 )
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	@Test
	public void testMagicBytesSimple() throws TransmissionException {
		// @formatter:off
		final LengthSegmentDecoratorSegment<MagicBytesSegmentDecorator<?>> theSegment = lengthSegment( 
			magicBytesSegment( 
				allocSegment( 
					stringSection("Hello World!")
				), new byte[] {-64, -34, -2, -19} // C0 DE FE ED = CODE FEED
			)
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final LengthSegmentDecoratorSegment<MagicBytesSegmentDecorator<?>> theOtherSegment = lengthSegment( 
			magicBytesSegment( 
				allocSegment( 
					stringSection()
				), 4
			)
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
	}

	@Test
	public void testMagicBytesComplex() throws TransmissionException {
		// @formatter:off
		final CrcSegmentDecorator<LengthSegmentDecoratorSegment<?>> theSegment = crcPrefixSegment(
			lengthSegment(
				magicBytesSegment( 
					allocSegment( 
						stringSection("Hello World!")
					), new byte[] {-64, -34, -2, -19} // C0 DE FE ED = CODE FEED
				) 
			), CrcStandard.CRC_16_X25
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theSegment.toSchema() );
		}
		final Sequence theSequence = theSegment.toSequence();
		// @formatter:off
		final CrcSegmentDecorator<LengthSegmentDecoratorSegment<?>> theOtherSegment = crcPrefixSegment(
			lengthSegment(
				magicBytesSegment( 
					allocSegment( 
						stringSection()
					), 4
				) 
			), CrcStandard.CRC_16_X25
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theOtherSegment.toSchema() );
		}
		assertEquals( theSegment, theOtherSegment );
		theSequence.setByteAt( 0, (byte) 65 );
		try {
			theOtherSegment.fromTransmission( theSequence );
			fail( "Expected an exception of type <BadCrcChecksumSequenceException>!" );
		}
		catch ( BadCrcChecksumSequenceException expected ) {
			// expected!
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final SimpleTypeMap theSimpleTypeMap = theSegment.toSimpleTypeMap();
			for ( String eKey : theSimpleTypeMap.sortedKeys() ) {
				System.out.println( eKey + " = " + theSimpleTypeMap.get( eKey ) );
			}
		}
	}

	// -------------------------------------------------------------------------

	@Test
	public void testAssertMagicBytesPass() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theReceiverPayloadSec;
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = segmentComposite( assertMagicBytesSegment( "MZ" ), allocSegment( stringArraySection( thePayload ) ) );
				final Segment theReceiverSeg = segmentComposite( assertMagicBytesSegment( "MZ" ), allocSegment( theReceiverPayloadSec = stringArraySection() ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					theReceiverPayloadSec.setPayload( null );
					final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					// DEBUG |-->
					try {
						theResult.waitForResult( WAIT_TIMEOUT );
					}
					catch ( TimeoutIOException e ) {
						System.err.println( e.toMessage() );
						try {
							System.err.println( theResult.getResult() );
							final String[] theReceiverData = theReceiverPayloadSec.getPayload();
							System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
						}
						catch ( TimeoutIOException ignore ) {}
						throw e;
					}
					// DEBUG <--|
					final String[] theReceiverData = theReceiverPayloadSec.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					assertArrayEquals( thePayload, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testAssertMagicBytesFail() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = segmentComposite( assertMagicBytesSegment( "MZ" ), allocSegment( stringArraySection( thePayload ) ) );
				final StringArraySection theReceiverPayloadSec;
				final Segment theReceiverSeg = segmentComposite( assertMagicBytesSegment( "P2P" ), allocSegment( theReceiverPayloadSec = stringArraySection() ) );
				final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
				theSenderSeg.transmitTo( theTransmitPort );
				// DEBUG |-->
				try {
					theResult.waitForResult( WAIT_TIMEOUT );
				}
				catch ( TimeoutIOException e ) {
					System.err.println( e.toMessage() );
					try {
						System.err.println( theResult.getResult() );
						final String[] theReceiverData = theReceiverPayloadSec.getPayload();
						System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					catch ( TimeoutIOException ignore ) {}
					throw e;
				}
				// DEBUG <--|
				try {
					theResult.getResult();
					fail( "Expected a <BadMagicBytesException> here!" );
				}
				catch ( BadMagicBytesException e ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Expected: " + e.getMessage() );
					}
				}
			}
		}
	}

	// -------------------------------------------------------------------------

	@Test
	public void testAssertMagicBytesSegmentPass() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theReceiverPayloadSec;
				final StringArraySection theSenderPayloadSec;
				String[] ePayload;
				final Segment theSenderSeg = assertMagicBytesSegment( allocSegment( theSenderPayloadSec = stringArraySection() ), new byte[] { -1, -2 } );
				final Segment theReceiverSeg = assertMagicBytesSegment( allocSegment( theReceiverPayloadSec = stringArraySection() ), new byte[] { -1, -2 } );
				for ( int i = 0; i < LOOPS; i++ ) {
					ePayload = new String[] { "Hello", "World", "!!!", Integer.toString( i ) };
					theSenderPayloadSec.setPayload( ePayload );
					theReceiverPayloadSec.setPayload( null );
					final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					// DEBUG |-->
					try {
						theResult.waitForResult( WAIT_TIMEOUT );
					}
					catch ( TimeoutIOException e ) {
						System.err.println( e.toMessage() );
						try {
							System.err.println( theResult.getResult() );
							final String[] theReceiverData = theReceiverPayloadSec.getPayload();
							System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
						}
						catch ( TimeoutIOException ignore ) {}
						throw e;
					}
					// DEBUG <--|
					final String[] theReceiverData = theReceiverPayloadSec.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					assertArrayEquals( ePayload, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testAssertMagicBytesSegmentFail() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = assertMagicBytesSegment( allocSegment( stringArraySection( thePayload ) ), new byte[] { -1, -2 } );
				final StringArraySection theReceiverPayloadSec;
				final Segment theReceiverSeg = assertMagicBytesSegment( allocSegment( theReceiverPayloadSec = stringArraySection() ), new byte[] { -1, -5, 127 } );
				final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
				theSenderSeg.transmitTo( theTransmitPort );
				// DEBUG |-->
				try {
					theResult.waitForResult( WAIT_TIMEOUT );
				}
				catch ( TimeoutIOException e ) {
					System.err.println( e.toMessage() );
					try {
						System.err.println( theResult.getResult() );
						final String[] theReceiverData = theReceiverPayloadSec.getPayload();
						System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					catch ( TimeoutIOException ignore ) {}
					throw e;
				}
				// DEBUG <--|
				try {
					theResult.getResult();
					fail( "Expected a <BadMagicBytesException> here!" );
				}
				catch ( BadMagicBytesException e ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Expected: " + e.getMessage() );
					}
				}
			}
		}
	}

	// -------------------------------------------------------------------------

	@Test
	public void testAssertMagicBytesSectionPass() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final StringArraySection theReceiverPayloadSec;
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = allocSegment( assertMagicBytesSection( stringArraySection( thePayload ), "MZ" ) );
				final Segment theReceiverSeg = allocSegment( assertMagicBytesSection( theReceiverPayloadSec = stringArraySection(), "MZ" ) );
				for ( int i = 0; i < LOOPS; i++ ) {
					theReceiverPayloadSec.setPayload( null );
					final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
					theSenderSeg.transmitTo( theTransmitPort );
					// DEBUG |-->
					try {
						theResult.waitForResult( WAIT_TIMEOUT );
					}
					catch ( TimeoutIOException e ) {
						System.err.println( e.toMessage() );
						try {
							System.err.println( theResult.getResult() );
							final String[] theReceiverData = theReceiverPayloadSec.getPayload();
							System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
						}
						catch ( TimeoutIOException ignore ) {}
						throw e;
					}
					// DEBUG <--|
					final String[] theReceiverData = theReceiverPayloadSec.getPayload();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					assertArrayEquals( thePayload, theReceiverData );
				}
			}
		}
	}

	@Test
	public void testAssertMagicBytesSectionFail() throws IOException, InterruptedException {
		try ( PortTestBench thePortTestBench = createPortTestBench() ) {
			if ( thePortTestBench.hasPorts() ) {
				final Port<?> theTransmitPort = thePortTestBench.getTransmitterPort().withOpen();
				final Port<?> theReceiverPort = thePortTestBench.getReceiverPort().withOpen();
				final String[] thePayload = { "Hello", "World", "!!!" };
				final Segment theSenderSeg = allocSegment( assertMagicBytesSection( stringArraySection( thePayload ), "MZ" ) );
				final StringArraySection theReceiverPayloadSec;
				final Segment theReceiverSeg = allocSegment( assertMagicBytesSection( theReceiverPayloadSec = stringArraySection(), "FEEDC0DE" ) );
				final SegmentResult<?> theResult = theReceiverPort.onReceiveSegment( theReceiverSeg );
				theSenderSeg.transmitTo( theTransmitPort );
				// DEBUG |-->
				try {
					theResult.waitForResult( WAIT_TIMEOUT );
				}
				catch ( TimeoutIOException e ) {
					System.err.println( e.toMessage() );
					try {
						System.err.println( theResult.getResult() );
						final String[] theReceiverData = theReceiverPayloadSec.getPayload();
						System.err.println( VerboseTextBuilder.asString( theReceiverData ) );
					}
					catch ( TimeoutIOException ignore ) {}
					throw e;
				}
				// DEBUG <--|
				try {
					theResult.getResult();
					fail( "Expected a <BadMagicBytesException> here!" );
				}
				catch ( BadMagicBytesException e ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Expected: " + e.getMessage() );
					}
				}
			}
		}
	}
}
