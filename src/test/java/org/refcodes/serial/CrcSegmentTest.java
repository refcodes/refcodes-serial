// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;
import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.runtime.SystemProperty;

public class CrcSegmentTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Test crc segment facade.
	 *
	 * @throws TransmissionException the transmission exception
	 */
	@Test
	public void testCrcSegmentFacade() throws TransmissionException {
		// @formatter:off
		final Segment theSegment = crcPrefixSegment( 
			allocSegment( 
				stringSection("Hello World!")
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
			System.out.println( "Transmission = " + theSegment.toString() );
			System.out.println( "AbstractSchema = " + theSegment.toSchema() );
		}
		// @formatter:off
		final Segment theOtherSegment = crcPrefixSegment(
				allocSegment( 
				stringSection()
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		theOtherSegment.fromTransmission( theSequence );
		assertEquals( theSegment, theOtherSegment );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
			System.out.println( "Other schema = " + theOtherSegment.toSchema() );
		}
		// BadCrcChecksumSequenceException:
		try {
			theSequence.setByteAt( 1, (byte) 1 ); // First 4 Bytes = length
			theOtherSegment.fromTransmission( theSequence );
			fail( "Expected a <BadCrcChecksumSequenceException> here!" );
		}
		catch ( BadCrcChecksumSequenceException expected ) { /* expected */ }
	}

	@Test
	public void testCrcBody() throws TransmissionException {
		final LengthSegmentDecoratorSegment<?> theSegment = new LengthSegmentDecoratorSegment<>( new IntSegment( 5161 ) );
		final CrcSegmentDecorator<Segment> theCrcHeader = new CrcSegmentDecorator<>( theSegment, CrcStandard.CRC_16_CCITT_FALSE );
		final long theCrcChecksum = theCrcHeader.getCrcChecksum();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Checksum for payload size <" + theSegment.getAllocLength() + "> = " + theCrcChecksum );
		}
		Sequence theSequence = theCrcHeader.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "To sequence = " + theSequence.toHexString() );
		}
		theCrcHeader.fromTransmission( theSequence );
		theSequence = theCrcHeader.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "From sequence = " + theSequence.toHexString() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other checksum for payload size <" + theSegment.getAllocLength() + "> = " + theCrcChecksum );
		}
	}

	@Test
	public void testCrcHeader1() throws TransmissionException {
		final String thePayload = "Hello World!";
		// @formatter:off
		final CrcSegmentDecorator<?> theSegment = crcPrefixSegment(
			segmentComposite(
				magicBytesSegment( (byte) 0xC0, (byte) 0xDE, (byte) 0xFE, (byte) 0xED ),
				allocSegment( 
					stringSection(thePayload)
				)
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Transmission = " + theSegment.toString() );
		}
		// @formatter:off
		final CrcSegmentDecorator<?> theOtherSegment = crcPrefixSegment(
			segmentComposite(
				magicBytesSegment( (byte) 0xC0, (byte) 0xDE, (byte) 0xFE, (byte) 0xED ),
				intSegment(
					thePayload.length() + 1
				)
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
		}
		assertNotEquals( theSegment.getCrcChecksum(), theOtherSegment.getCrcChecksum() );
	}

	@Test
	public void testCrcHeader2() throws TransmissionException {
		final String thePayload = "Hello World!";
		// @formatter:off
		final CrcSegmentDecorator<?> theSegment = crcPrefixSegment(
			segmentComposite(
				magicBytesSegment( (byte) 0xC0, (byte) 0xDE, (byte) 0xFE, (byte) 0xED ),
				allocSegment( 
					stringSection(thePayload)
				)
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Transmission = " + theSegment.toString() );
		}
		// @formatter:off
		final CrcSegmentDecorator<?> theOtherSegment = crcPrefixSegment(
			segmentComposite(
				magicBytesSegment( (byte) 0xC0, (byte) 0xDE, (byte) 0xFE, (byte) 0xED ),
				intSegment(
					thePayload.length()
				)
			), CrcStandard.CRC_16_CCITT_FALSE
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other segment = " + theOtherSegment.toString() );
		}
		assertNotEquals( theSegment.getCrcChecksum(), theOtherSegment.getCrcChecksum() );
	}
}
