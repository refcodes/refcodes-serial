// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.serial;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.serial.SerialSugar.*;
import java.io.IOException;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.refcodes.numerical.CrcStandard;
import org.refcodes.numerical.Endianess;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.runtime.SystemProperty;

public class LoopbackPortTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSegmentComposition() throws IOException {
		final LoopbackPort thePort = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() ).withOpen();
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.put( "/sensor/0/name", "temp01" );
		theBuilder.putFloat( "/sensor/0/value", 12.37F );
		theBuilder.put( "/sensor/1/name", "temp01" );
		theBuilder.putFloat( "/sensor/1/value", 8.71F );
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				final AllocSegmentHead theLen;
				// @formatter:off
				final Segment theSegment = crcPrefixSegment(
					segmentComposite(
						theLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment( true ),
						intSegment( 5161 ), 
						theLen.letBody(
							allocSegmentBody( 
								new PropertiesSection( theBuilder )
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				thePort.transmitSegment( theSegment );
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				final AllocSegmentHead theOtherLen;
				// @formatter:off
				final Segment theOtherSegment = crcPrefixSegment(
					segmentComposite(
						theOtherLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment(),
						intSegment(), 
						theOtherLen.letBody( 
							allocSegmentBody( 
									new PropertiesSection( )
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				thePort.receiveSegment( theOtherSegment );
				assertEquals( theOtherSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
			}
		}
	}

	@Test
	public void testFloatSegment() throws IOException {
		final LoopbackPort thePort = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() ).withOpen();
		float l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Float.MIN_VALUE : ( i == Short.MAX_VALUE ? Float.MAX_VALUE : i * (float) Math.PI ) );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Endianess = <" + eEndianess + ">, l = <" + l + "> ----------" );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theSegment = lengthSegment( 
					floatSegment( l, eEndianess )
				);
				// @formatter:on
				thePort.transmitSegment( theSegment );
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theOtherSegment = lengthSegment( 
					floatSegment( eEndianess )
				);
				// @formatter:on
				thePort.receiveSegment( theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	@Test
	public void testCrossoverLoopbackPort1() throws IOException {
		final LoopbackPort theTransmitter = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() ).withOpen();
		final LoopbackPort theReceiver = new CrossoverLoopbackPort( theTransmitter ).withOpen();
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.put( "/sensor/0/name", "temp01" );
		theBuilder.putFloat( "/sensor/0/value", 12.37F );
		theBuilder.put( "/sensor/1/name", "temp01" );
		theBuilder.putFloat( "/sensor/1/value", 8.71F );
		for ( int eLengthWidth = 2; eLengthWidth <= 8; eLengthWidth++ ) {
			for ( Endianess eEndianess : Endianess.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Length width = <" + eLengthWidth + ">, endianess = <" + eEndianess + "> ----------" );
				}
				final AllocSegmentHead theLen;
				// @formatter:off
				final Segment theSegment = crcPrefixSegment(
					segmentComposite(
						theLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment( true ),
						intSegment( 5161 ), 
						theLen.letBody(
							allocSegmentBody( 
								new PropertiesSection( theBuilder )
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				theTransmitter.transmitSegment( theSegment );
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString() );
					System.out.println( "Transmission = " + theSegment.toString() );
					System.out.println( "AbstractSchema = " + theSegment.toSchema() );
				}
				final AllocSegmentHead theOtherLen;
				// @formatter:off
				final Segment theOtherSegment = crcPrefixSegment(
					segmentComposite(
						theOtherLen = allocSegmentHead(eLengthWidth, eEndianess),
						booleanSegment(),
						intSegment(), 
						theOtherLen.letBody( 
							allocSegmentBody( 
									new PropertiesSection( )
							)
						)
					), CrcStandard.CRC_16_CCITT_FALSE
				);
				// @formatter:on
				theReceiver.receiveSegment( theOtherSegment );
				assertEquals( theOtherSegment, theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
					System.out.println( "Other schema = " + theOtherSegment.toSchema() );
				}
			}
		}
	}

	@Test
	public void testCrossoverLoopbackPort2() throws IOException {
		final LoopbackPortHub theTransmitterHub = new LoopbackPortHub();
		final LoopbackPortHub theReceiverHub = new CrossoverLoopbackPortHub( theTransmitterHub );
		final String thePortName = "/dev/ttyLoopback" + UUID.randomUUID().toString();
		final LoopbackPort theTransmitter = theTransmitterHub.toPort( thePortName ).withOpen();
		final LoopbackPort theReceiver = theReceiverHub.toPort( thePortName ).withOpen();
		float l;
		for ( Endianess eEndianess : Endianess.values() ) {
			for ( int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++ ) {
				l = ( i == Short.MIN_VALUE ? Float.MIN_VALUE : ( i == Short.MAX_VALUE ? Float.MAX_VALUE : i * (float) Math.PI ) );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "---------- Endianess = <" + eEndianess + ">, l = <" + l + "> ----------" );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theSegment = lengthSegment( 
					floatSegment( l, eEndianess )
				);
				// @formatter:on
				theTransmitter.transmitSegment( theSegment );
				final Sequence theSequence = theSegment.toSequence();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
					System.out.println( "Transmission = " + theSegment.toString() );
				}
				// @formatter:off
				final LengthSegmentDecoratorSegment<FloatSegment> theOtherSegment = lengthSegment( 
					floatSegment( eEndianess )
				);
				// @formatter:on
				theReceiver.receiveSegment( theOtherSegment );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Other segment = " + theOtherSegment.toString() );
				}
				assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
			}
		}
	}

	@Test
	public void testEdgeCase() throws IOException {
		final LoopbackPort thePort = new LoopbackPort( "/dev/ttyLoopback" + UUID.randomUUID().toString() ).withOpen();
		final float l;
		final Endianess eEndianess = Endianess.LITTLE;
		l = -102239.99F;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "---------- Endianess = <" + eEndianess + ">, l = <" + l + "> ----------" );
		}
		// @formatter:off
		final LengthSegmentDecoratorSegment<FloatSegment> theSegment = lengthSegment( 
			floatSegment( l, eEndianess )
		);
		// @formatter:on
		thePort.transmitSegment( theSegment );
		final Sequence theSequence = theSegment.toSequence();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sequence (hex) = " + theSequence.toHexString( ", " ) );
			System.out.println( "Transmission = " + theSegment.toString() );
		}
		// @formatter:off
		final LengthSegmentDecoratorSegment<FloatSegment> theOtherSegment = lengthSegment( 
			floatSegment( eEndianess )
		);
		// @formatter:on
		thePort.receiveSegment( theOtherSegment );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Other sequence (hex) = " + theOtherSegment.toSequence().toHexString() );
			System.out.println( "Other segment = " + theOtherSegment.toString() );
		}
		assertEquals( theSegment.getDecoratee().getPayload(), theOtherSegment.getDecoratee().getPayload() );
	}
}
